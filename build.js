
import process from 'node:process';
import fs from 'node:fs/promises';
import path from 'node:path';
import child_process from 'node:child_process';
import util from 'node:util';

import {
	collectTestFiles,
	bundle,
	compileProtobufs,
	serveWebpage,
	buildTestWebpage
} from 'food-runner-build';


// because WebStorm's type defs can't seem to get this one right
/**
 * @callback ExecPromisified
 * @param {string} cmd
 * @param {Object} [options]
 * @param {string} [options.cwd]
 * @returns {Promise.<{ error: ExecException?, stdout: string, stderr: string }>}
 */
const exec = /** @type {ExecPromisified} */ util.promisify(child_process.exec);


(async () => await main())();


async function main() {

	// parse the arguments
	// ignore the first two arguments: `node rollup.js`
	let args = process.argv.slice(2);

	let target = null;
	if (args.length >= 1) {
		target = args[0];
	} else {
		throw new Error("required arguments: target");
	}

	switch (target) {
		case "protobuf": return await buildProtobuf();
		case "portal-ext": return await buildPortalExt();
		case "portal-web": return await buildPortalWeb();
		case "serve-tests": return await serveTests();
		case "serve-web": return await servePortalWeb();
		default: throw Error(`Invalid target: ${target}`);
	}
}


const suppressWarnings = [
	// ignore warnings in dependencies that we can't do anything about
	/^Circular dependency: .*node_modules\/protobufjs\//,
	/^.*node_modules\/@protobufjs\/inquire\/index\.js \(12:18\): Use of eval in/,
	/^Circular dependency: node_modules\/chai\//,
	/^Circular dependency: node_modules\/luxon\//
];


async function buildProtobuf() {
	await compileProtobufs(
		'src/protobuf',
		'smolweb.proto',
		'type.cuchazinteractive.org',
		'src/protobuf/gen.js'
	);
}


async function serveTests() {

	await buildTestWebpage(
		'build/test',
		{
			testFiles: await collectTestFiles('test'),
			suppressWarnings
		}
	);

	await serveWebpage('build/test');
}


async function buildPortalExt() {

	const outDir = 'build/portal-ext';

	// cleanup the old folder first
	try {
		await fs.rm(outDir, { recursive: true });
	} catch {}

	// compile the js
	await bundle(outDir, {
		'portal': 'src/portal/portal.js',
		'bg': 'src/extension/bg.js'
	}, {
		suppressWarnings
	});

	// copy over static files
	console.log('Copied:');
	await copyFile('static/manifest.json', outDir);
	await copyFile('static/portal.html', outDir);
	await copyFile('static/portal.css', outDir);
}


async function buildPortalWeb() {

	const outDir = 'build/portal-web';

	// cleanup the old folder first
	try {
		await fs.rm(outDir, { recursive: true });
	} catch {}

	// compile the js
	await bundle(outDir, {
		'portal': 'src/portal/portal.js'
	}, {
		suppressWarnings
	});

	// pre-compress the js file
	await gzip(path.join(outDir, 'portal.js'), outDir);

	// copy over static files
	console.log('Copied:');
	await copyFile('static/portal.html', outDir, 'index.html');
	await copyFile('static/portal.css', outDir);
}


async function servePortalWeb() {

	await buildPortalWeb();
	await serveWebpage('build/portal-web');
}


/**
 * @param {string} src
 * @param {string} outDir
 * @param {?string} [dst]
 * @returns {Promise.<void>}
 */
async function copyFile(src, outDir, dst) {
	if (dst == null) {
		dst = path.basename(src);
	}
	await fs.copyFile(src, path.join(outDir, dst));
	console.log(`\t${dst}`);
}


/**
 * @param {string} src
 * @param {string} outDir
 * @returns {Promise.<void>}
 */
async function gzip(src, outDir) {

	const dst = path.join(outDir, `${path.basename(src)}.gz`);

	const result = await exec(`gzip -c "${src}" > "${dst}"`);
	if (result.stderr.length > 0) {
		console.error('gzip stderr:', result.stderr);
	}
	if (result.error != null) {
		throw result.error;
	}

	console.log('GZipped:');
	console.log(`\t${path.relative(outDir, dst)}`);
	async function kib(p) {
		const stat = await fs.stat(p);
		return Math.ceil(stat.size/1024);
	}
	console.log('\tBefore:', await kib(src), 'KiB');
	console.log('\t After:', await kib(dst), 'KiB');
}
