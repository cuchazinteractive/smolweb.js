
import { expect } from 'chai';

import * as buffers from 'food-runner/src/buffers.js';

import * as sizes from 'smolweb/src/framed/sizes.js';
import { Framer } from 'smolweb/src/framed/framer.js';


describe("framed.sizes", function () {

	it("overhead", async function () {

		const maxPayload = buffers.zero(sizes.MAX_PAYLOAD_SIZE);

		const framer = new Framer(sizes.MAX_STREAM_SIZE);
		const startFrame = framer.frame(maxPayload);
		const startOverhead = startFrame.byteLength - sizes.MAX_PAYLOAD_SIZE;
		console.log('start frame overhead', startOverhead);

		const continueFrame = framer.frame(maxPayload);
		const continueOverhead = continueFrame.byteLength - sizes.MAX_PAYLOAD_SIZE;
		console.log('continue frame overhead', continueOverhead);

		// the start frame should hit the limits
		// start frame overhead: 16  [10, 253, 127, 8, 255, 255, 255, 255, 255, 255, 255, 255, 127, 18, 240, 127, 0, 0, 0, 0, ...]
		expect(startOverhead).eq(sizes.MAX_OVERHEAD_SIZE);
		expect(startFrame.byteLength).eq(sizes.MAX_MESSAGE_SIZE);

		// but the continue frame won't, because the index is low
		// continue frame overhead: 8  [18, 245, 127, 8, 1, 18, 240, 127, 0, 0, 0, 0, ...]
		expect(continueOverhead).lessThanOrEqual(sizes.MAX_OVERHEAD_SIZE);
		expect(continueFrame.byteLength).lessThanOrEqual(sizes.MAX_MESSAGE_SIZE);
	});
});
