
import { expect } from 'chai';

import * as buffers from 'food-runner/src/buffers.js';

import * as sizes from 'smolweb/src/framed/sizes.js';
import { Deframer, LocalDeframerError, RemoteFrameError } from 'smolweb/src/framed/deframer.js';

import { startFrame, continueFrame, errorFrame } from 'smolweb/testlib/frames.js';


describe("framed.deframer", function () {

	it("new, decode error", function () {

		const e = expect(() => Deframer.new(buffers.of())).throw()._obj;
		expect(e).instanceof(LocalDeframerError);
		expect(e.frameError).property('decode');
	});

	it("new, remote error, invalid", function () {

		const frame = errorFrame({});
		const e = expect(() => Deframer.new(frame)).throw()._obj;
		expect(e).instanceof(RemoteFrameError);
		expect(e.frameError.error).undefined;
	});

	it("new, remote error", function () {

		const frame = errorFrame({ abort: true });
		const e = expect(() => Deframer.new(frame)).throw()._obj;
		expect(e).instanceof(RemoteFrameError);
		expect(e.frameError.error).eq('abort');
	});

	it("new, unexpected kind", function () {

		const frame = continueFrame(1, buffers.of());
		const e = expect(() => Deframer.new(frame)).throw()._obj;
		expect(e).instanceof(LocalDeframerError);
		expect(e.frameError.error).eq('unexpectedKind');
	});

	it("new, sizes", function () {

		function expectInvalidStreamSize(e, size) {
			expect(e).instanceof(LocalDeframerError);
			expect(e.frameError.error).eq('invalidStreamSize');
			expect(e.frameError.invalidStreamSize.observed).eq(size);
		}

		let frame = startFrame(-1, buffers.of());
		let e = expect(() => Deframer.new(frame)).throw()._obj;
		expectInvalidStreamSize(e, -1n);

		// TODO: is this technically an error?
		frame = startFrame(0, buffers.of());
		e = expect(() => Deframer.new(frame)).throw()._obj;
		expectInvalidStreamSize(e, 0n);

		frame = startFrame(1, buffers.of(5));
		let deframer = Deframer.new(frame)[0];
		expect(deframer.bytesTotal).eq(1n);

		frame = startFrame(sizes.MAX_STREAM_SIZE, buffers.zero(sizes.MAX_PAYLOAD_SIZE));
		deframer = Deframer.new(frame)[0];
		expect(deframer.bytesTotal).eq(sizes.MAX_STREAM_SIZE);

		frame = startFrame(sizes.MAX_STREAM_SIZE + 1n, buffers.zero(sizes.MAX_PAYLOAD_SIZE));
		e = expect(() => Deframer.new(frame)).throw()._obj;
		expectInvalidStreamSize(e, -sizes.MAX_STREAM_SIZE - 1n);
		// NOTE: MAX_STREAM_SIZE + 1 overflows an i64 and wraps around to the min value
	});

	it("new, payloads", function () {

		function expectUnexpectedPayloadSize(e, exp, obs) {
			expect(e).instanceof(LocalDeframerError);
			expect(e.frameError.error).eq('unexpectedPayloadSize');
			expect(e.frameError.unexpectedPayloadSize.expected).eq(exp);
			expect(e.frameError.unexpectedPayloadSize.observed).eq(obs);
		}

		let frame = startFrame(1, buffers.of(5));
		Deframer.new(frame);

		frame = startFrame(1, buffers.of(1, 2));
		let e = expect(() => Deframer.new(frame)).throw()._obj;
		expectUnexpectedPayloadSize(e, 1, 2);

		frame = startFrame(2, buffers.of(5));
		e = expect(() => Deframer.new(frame)).throw()._obj;
		expectUnexpectedPayloadSize(e, 2, 1);

		frame = startFrame(sizes.MAX_PAYLOAD_SIZE, buffers.of(5));
		e = expect(() => Deframer.new(frame)).throw()._obj;
		expectUnexpectedPayloadSize(e, sizes.MAX_PAYLOAD_SIZE, 1);

		frame = startFrame(sizes.MAX_PAYLOAD_SIZE, buffers.zero(sizes.MAX_PAYLOAD_SIZE));
		Deframer.new(frame);

		frame = startFrame(sizes.MAX_PAYLOAD_SIZE + 1, buffers.of(5));
		e = expect(() => Deframer.new(frame)).throw()._obj;
		expectUnexpectedPayloadSize(e, sizes.MAX_PAYLOAD_SIZE, 1);

		frame = startFrame(sizes.MAX_PAYLOAD_SIZE + 1, buffers.zero(sizes.MAX_PAYLOAD_SIZE));
		Deframer.new(frame);
	});

	it("new", function () {

		let payload = buffers.of(1, 2, 3);
		let frame = startFrame(3, payload);
		let [deframer, payload2] = Deframer.new(frame);

		expect(payload2).deep.eq(payload);
		expect(deframer.isComplete).true;
		expect(deframer.isClosed).true;

		payload = buffers.zero(sizes.MAX_PAYLOAD_SIZE);
		frame = startFrame(sizes.MAX_PAYLOAD_SIZE + 3, payload);
		[deframer, payload2] = Deframer.new(frame);

		expect(payload2).deep.eq(payload);
		expect(deframer.isComplete).false;
		expect(deframer.isClosed).false;
	});

	it("deframe, complete", function () {

		const frame = startFrame(3, buffers.of(1, 2, 3));
		const [deframer, _] = Deframer.new(frame);

		const e = expect(() => deframer.deframe(buffers.of())).throw()._obj;
		expect(e).instanceof(LocalDeframerError);
		expect(e.frameError).property('closed');
	});

	it("deframe, decode error", function () {

		const frame = startFrame(sizes.MAX_PAYLOAD_SIZE + 3, buffers.zero(sizes.MAX_PAYLOAD_SIZE));
		const [deframer, _] = Deframer.new(frame);

		const e = expect(() => deframer.deframe(buffers.of(1, 2, 3))).throw()._obj;
		expect(e).instanceof(LocalDeframerError);
		expect(e.frameError).property('decode');
	});

	it("deframe, unexpected kind", function () {

		let frame = startFrame(sizes.MAX_PAYLOAD_SIZE + 3, buffers.zero(sizes.MAX_PAYLOAD_SIZE));
		const [deframer, _] = Deframer.new(frame);

		frame = startFrame(1, buffers.of());
		const e = expect(() => deframer.deframe(frame)).throw()._obj;
		expect(e).instanceof(LocalDeframerError);
		expect(e.frameError).property('unexpectedKind');
	});

	it("deframe, remote error, error", function () {

		let frame = startFrame(sizes.MAX_PAYLOAD_SIZE + 3, buffers.zero(sizes.MAX_PAYLOAD_SIZE));
		const [deframer, _] = Deframer.new(frame);

		frame = errorFrame({});
		const e = expect(() => deframer.deframe(frame)).throw()._obj;
		expect(e).instanceof(RemoteFrameError);
		expect(e.frameError.error).undefined;

		expect(deframer.isComplete).false;
		expect(deframer.isClosed).true;
	});

	it("deframe, remote error", function () {

		let frame = startFrame(sizes.MAX_PAYLOAD_SIZE + 3, buffers.zero(sizes.MAX_PAYLOAD_SIZE));
		const [deframer, _] = Deframer.new(frame);

		frame = errorFrame({ abort: true });
		const e = expect(() => deframer.deframe(frame)).throw()._obj;
		expect(e).instanceof(RemoteFrameError);
		expect(e.frameError).property('abort');

		expect(deframer.isComplete).false;
		expect(deframer.isClosed).true;
	});

	it("deframe, unexpected frame index", function () {

		function shouldUnexpectedFrameIndex(e, exp, obs) {
			expect(e).instanceof(LocalDeframerError);
			expect(e.frameError.error).eq('unexpectedFrameIndex');
			expect(e.frameError.unexpectedFrameIndex.expected).eq(BigInt(exp));
			expect(e.frameError.unexpectedFrameIndex.observed).eq(BigInt(obs));
		}

		let frame = startFrame(sizes.MAX_PAYLOAD_SIZE + 3, buffers.zero(sizes.MAX_PAYLOAD_SIZE));
		const [deframer, _] = Deframer.new(frame);

		frame = continueFrame(-5, buffers.of());
		let e = expect(() => deframer.deframe(frame)).throw()._obj;
		shouldUnexpectedFrameIndex(e, 1, -5);

		frame = continueFrame(0, buffers.of());
		e = expect(() => deframer.deframe(frame)).throw()._obj;
		shouldUnexpectedFrameIndex(e, 1, 0);
	});

	it("deframe, unexpected payload", function () {

		function shouldUnexpectedPayloadSize(e, exp, obs) {
			expect(e).instanceof(LocalDeframerError);
			expect(e.frameError.error).eq('unexpectedPayloadSize');
			expect(e.frameError.unexpectedPayloadSize.expected).eq(exp);
			expect(e.frameError.unexpectedPayloadSize.observed).eq(obs);
		}

		let frame = startFrame(sizes.MAX_PAYLOAD_SIZE + 3, buffers.zero(sizes.MAX_PAYLOAD_SIZE));
		const [deframer, _] = Deframer.new(frame);

		frame = continueFrame(1, buffers.of());
		const e = expect(() => deframer.deframe(frame)).throw()._obj;
		shouldUnexpectedPayloadSize(e, 3, 0);
	});

	it("deframe, one", function () {

		let frame = startFrame(sizes.MAX_PAYLOAD_SIZE + 3, buffers.zero(sizes.MAX_PAYLOAD_SIZE));
		const [deframer, _] = Deframer.new(frame);

		const payload = buffers.of(1, 2, 3);
		frame = continueFrame(1, payload);
		const payload2 = deframer.deframe(frame);

		expect(payload2).deep.eq(payload);
		expect(deframer.isComplete).true;
		expect(deframer.isClosed).true;
	});

	it("deframe, two", function () {

		let frame = startFrame(sizes.MAX_PAYLOAD_SIZE*2 + 3, buffers.zero(sizes.MAX_PAYLOAD_SIZE));
		const [deframer, _] = Deframer.new(frame);

		let payload = buffers.fill(sizes.MAX_PAYLOAD_SIZE, 42);
		frame = continueFrame(1, payload);
		let payload2 = deframer.deframe(frame);

		expect(payload2).deep.eq(payload);
		expect(deframer.isComplete).false;
		expect(deframer.isClosed).false;

		payload = buffers.of(1, 2, 3);
		frame = continueFrame(2, payload);
		payload2 = deframer.deframe(frame);

		expect(payload2).deep.eq(payload);
		expect(deframer.isComplete).true;
		expect(deframer.isClosed).true;
	});
});
