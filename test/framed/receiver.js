
import { expect } from 'chai';

import * as buffers from 'food-runner/src/buffers.js';

import { FrameReceiver } from 'smolweb/src/framed/receiver.js';
import { Deframer, LocalDeframerError, RemoteFrameError } from 'smolweb/src/framed/deframer.js';
import { ChannelReceiver } from 'smolweb/src/framed/reader.js';

import { RTCDataChannelMock } from 'smolweb/testlib/channel.js';


describe("framed.receiver", function () {

	it("recv, error", async function () {

		const channel = new RTCDataChannelMock();
		const receiver = FrameReceiver.fromDataChannel(channel.mock, new ChannelReceiver(channel.mock));

		// send anything that would trigger an error from the frame reader
		await channel.remoteSend(buffers.of(1, 2, 3));

		// we should get the error here
		const err = await expect(receiver.recv()).eventually.rejected;
		expect(err).instanceof(LocalDeframerError);

		// the response error should show up on the channel
		const response = channel.remoteRecv();
		expect(response).instanceof(ArrayBuffer);
		const e = expect(() => Deframer.new(response)).throw()._obj;
		expect(e).instanceof(RemoteFrameError);
		expect(e.frameError).property('decode');
	});

	it("recvAll, error", async function () {

		const channel = new RTCDataChannelMock();
		const receiver = FrameReceiver.fromDataChannel(channel.mock, new ChannelReceiver(channel.mock));

		// send anything that would trigger an error from the frame reader
		await channel.remoteSend(buffers.of(1, 2, 3));

		// we should get the error here
		const err = await expect(receiver.recvAll()).eventually.rejected;
		expect(err).instanceof(LocalDeframerError);

		// the response error should show up on the channel
		const response = channel.remoteRecv();
		expect(response).instanceof(ArrayBuffer);
		const e = expect(() => Deframer.new(response)).throw()._obj;
		expect(e).instanceof(RemoteFrameError);
		expect(e.frameError).property('decode');
	});
});
