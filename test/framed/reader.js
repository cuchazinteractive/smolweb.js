
import { expect } from 'chai';

import * as buffers from 'food-runner/src/buffers.js';
import * as promises from 'food-runner/src/promises.js';

import * as sizes from 'smolweb/src/framed/sizes.js';
import { ChannelReceiver, FrameReader, FrameReaderError, FrameReaderTimeoutError, FrameEvent } from 'smolweb/src/framed/reader.js';
import { LocalDeframerError, RemoteFrameError } from 'smolweb/src/framed/deframer.js';

import { RTCDataChannelMock } from 'smolweb/testlib/channel.js';
import { startFrame, continueFrame, errorFrame } from 'smolweb/testlib/frames.js';


describe("framed.ChannelReceiver", function () {

	it("recv, send first", async function () {

		const channel = new RTCDataChannelMock();
		const receiver = new ChannelReceiver(channel);

		const msg = buffers.of(1, 2, 3);
		await channel.remoteSend(msg);

		expect((await receiver.recv())).deep.eq(msg);
	});

	it("recv, recv first", async function () {

		const channel = new RTCDataChannelMock();
		const receiver = new ChannelReceiver(channel);

		const p = receiver.recv();

		const msg = buffers.of(1, 2, 3);
		await channel.remoteSend(msg);

		expect((await p)).deep.eq(msg);
	});

	it("recv, two, send first", async function () {

		const channel = new RTCDataChannelMock();
		const receiver = new ChannelReceiver(channel);

		const msg1 = buffers.of(1, 2, 3);
		const msg2 = buffers.of(4, 5, 6);
		await channel.remoteSend(msg1);
		await channel.remoteSend(msg2);

		expect((await receiver.recv())).deep.eq(msg1);
		expect((await receiver.recv())).deep.eq(msg2);
	});

	it("recv, two, recv first", async function () {

		const channel = new RTCDataChannelMock();
		const receiver = new ChannelReceiver(channel);

		let p = receiver.recv();

		const msg1 = buffers.of(1, 2, 3);
		await channel.remoteSend(msg1);

		expect((await p)).deep.eq(msg1);

		p = receiver.recv();

		const msg2 = buffers.of(4, 5, 6);
		await channel.remoteSend(msg2);

		expect((await p)).deep.eq(msg2);
	});

	it("recv, two, recv first, then send first", async function () {

		const channel = new RTCDataChannelMock();
		const receiver = new ChannelReceiver(channel);

		const p = receiver.recv();

		const msg1 = buffers.of(1, 2, 3);
		const msg2 = buffers.of(4, 5, 6);
		await channel.remoteSend(msg1);
		await channel.remoteSend(msg2);

		expect((await p)).deep.eq(msg1);
		expect((await receiver.recv())).deep.eq(msg2);
	});

	it("recv, two, async send", async function () {

		const channel = new RTCDataChannelMock();
		const receiver = new ChannelReceiver(channel);

		const p = receiver.recv();

		const msg1 = buffers.of(1, 2, 3);
		const msg2 = buffers.of(4, 5, 6);
		(async () => {
			await channel.remoteSend(msg1);
		})().catch(() => {});
		(async () => {
			await channel.remoteSend(msg2);
		})().catch(() => {});

		expect((await p)).deep.eq(msg1);
		expect((await receiver.recv())).deep.eq(msg2);
	});

	it("recv, timeout", async function () {

		const channel = new RTCDataChannelMock();
		const receiver = new ChannelReceiver(channel);

		const p = receiver.recv({ timeoutMs: 100 });

		expect((await p)).eq(promises.TimeoutSignal);
	});

	it("recv, timeout twice", async function () {

		const channel = new RTCDataChannelMock();
		const receiver = new ChannelReceiver(channel);

		let p = receiver.recv({ timeoutMs: 100 });

		expect((await p)).eq(promises.TimeoutSignal);

		p = receiver.recv({ timeoutMs: 100 });

		expect((await p)).eq(promises.TimeoutSignal);
	});

	it("recv, timeout, recovery", async function () {

		const channel = new RTCDataChannelMock();
		const receiver = new ChannelReceiver(channel);

		let p = receiver.recv({ timeoutMs: 100 });

		expect((await p)).eq(promises.TimeoutSignal);

		p = receiver.recv({ timeoutMs: 100 });

		// send a message
		const msg = buffers.of(1, 2, 3);
		await channel.remoteSend(msg);

		expect((await p)).deep.eq(msg);
	});
});


describe("framed.reader", function () {

	it("read, bad frame", async function () {

		const channel = new RTCDataChannelMock();
		const reader = new FrameReader(new ChannelReceiver(channel.mock));

		// send anything that would trigger an error from the frame reader
		await channel.remoteSend(buffers.of(1, 2, 3));

		// reader should drop the frame and we should get an error response
		const err = await expect(reader.read()).eventually.rejected;
		expect(err).instanceof(FrameReaderError);
		expect(err.cause).instanceof(LocalDeframerError);
		expect(err.response).instanceof(RemoteFrameError);
	});

	it("read, one", async function () {

		const channel = new RTCDataChannelMock();
		const reader = new FrameReader(new ChannelReceiver(channel.mock));

		// send a stream with one frame
		const payload = buffers.of(1, 2, 3);
		await channel.remoteSend(startFrame(3, payload));

		// reader should get it in one event
		const event = await reader.read();
		expect(event.type).eq(FrameEvent.FINISH);
		expect(event.finish.payload).deep.eq(payload);

		expect(channel.remoteIsEmpty()).true;
	});

	it("read, two", async function () {

		const channel = new RTCDataChannelMock();
		const reader = new FrameReader(new ChannelReceiver(channel.mock));

		// send a stream with two frames
		const payloads = [
			buffers.fill(sizes.MAX_PAYLOAD_SIZE, 5),
			buffers.of(1, 2, 3)
		];
		await channel.remoteSend(startFrame(sizes.MAX_PAYLOAD_SIZE + 3, payloads[0]));
		await channel.remoteSend(continueFrame(1, payloads[1]));

		// first event should be a start frame
		let event = await reader.read();
		expect(event.type).eq(FrameEvent.START);
		expect(event.start.payload).deep.eq(payloads[0]);

		// second event should be a finish framme
		event = await reader.read();
		expect(event.type).eq(FrameEvent.FINISH);
		expect(event.finish.payload).deep.eq(payloads[1]);

		expect(channel.remoteIsEmpty()).true;
	});

	it("read, three", async function () {

		const channel = new RTCDataChannelMock();
		const reader = new FrameReader(new ChannelReceiver(channel.mock));

		// send a stream with three frames
		const payloads = [
			buffers.fill(sizes.MAX_PAYLOAD_SIZE, 5),
			buffers.fill(sizes.MAX_PAYLOAD_SIZE, 42),
			buffers.of(1, 2, 3)
		];
		await channel.remoteSend(startFrame(sizes.MAX_PAYLOAD_SIZE*2 + 3, payloads[0]));
		await channel.remoteSend(continueFrame(1, payloads[1]));
		await channel.remoteSend(continueFrame(2, payloads[2]));

		// first event should be a start frame
		let event = await reader.read();
		expect(event.type).eq(FrameEvent.START);
		expect(event.start.payload).deep.eq(payloads[0]);

		// second event should be a continue framme
		event = await reader.read();
		expect(event.type).eq(FrameEvent.CONTINUE);
		expect(event.continue.bytesRead).eq(BigInt(sizes.MAX_PAYLOAD_SIZE*2));
		expect(event.continue.payload).deep.eq(payloads[1]);

		// third even should be the finish frame
		event = await reader.read();
		expect(event.type).eq(FrameEvent.FINISH);
		expect(event.finish.payload).deep.eq(payloads[2]);

		expect(channel.remoteIsEmpty()).true;
	});

	it("read, one, abort", async function () {

		const channel = new RTCDataChannelMock();
		const reader = new FrameReader(new ChannelReceiver(channel.mock));

		// send a stream with one frame, then abort
		const payload = buffers.fill(sizes.MAX_PAYLOAD_SIZE, 5);
		await channel.remoteSend(startFrame(sizes.MAX_PAYLOAD_SIZE + 3, payload));
		await channel.remoteSend(errorFrame({ abort: true }));

		// first event should be a start frame
		let event = await reader.read();
		expect(event.type).eq(FrameEvent.START);
		expect(event.start.payload).deep.eq(payload);

		// next read should throw an abort error
		const err = await expect(reader.read()).eventually.rejected;
		expect(err).instanceof(FrameReaderError);
		expect(err.cause).instanceof(RemoteFrameError);
		expect(err.cause.frameError).property('abort');
		expect(err.response).null;
	});

	it("readAll, error", async function () {

		const channel = new RTCDataChannelMock();
		const reader = new FrameReader(new ChannelReceiver(channel.mock));

		// send anything that would trigger an error from the frame reader
		await channel.remoteSend(buffers.of(1, 2, 3));

		// the reader should get an error
		const err = await expect(reader.readAll()).eventually.rejected;
		expect(err).instanceof(FrameReaderError);
		expect(err.cause).instanceof(LocalDeframerError);
		expect(err.response).instanceof(RemoteFrameError);
	});

	it("readAll, timeout", async function () {

		const channel = new RTCDataChannelMock();
		const reader = new FrameReader(new ChannelReceiver(channel.mock));

		// the reader should timeout
		const options = { timeoutNowMs: 100 };
		const err = await expect(reader.readAll(options)).eventually.rejected;
		expect(err).instanceof(FrameReaderTimeoutError);
		expect(err.type).eq(FrameReaderTimeoutError.NOW);
	});

	it("readAll, one", async function () {

		const channel = new RTCDataChannelMock();
		const reader = new FrameReader(new ChannelReceiver(channel.mock));

		// send a stream with one frame
		const payload = buffers.of(1, 2, 3);
		await channel.remoteSend(startFrame(3, payload));

		const buf = await reader.readAll();
		expect(buf).deep.eq(payload);
	});

	it("readAll, one, timeout", async function () {

		const channel = new RTCDataChannelMock();
		const reader = new FrameReader(new ChannelReceiver(channel.mock));

		// start a stream with two frames, but send only one
		const payload = buffers.fill(sizes.MAX_PAYLOAD_SIZE, 5);
		await channel.remoteSend(startFrame(sizes.MAX_PAYLOAD_SIZE + 3, payload));

		const options = { timeoutNowMs: 100 };
		const err = await expect(reader.readAll(options)).eventually.rejected;
		expect(err).instanceof(FrameReaderTimeoutError);
		expect(err.type).eq(FrameReaderTimeoutError.AFTER_START);
	});

	it("readAll, two", async function () {

		const channel = new RTCDataChannelMock();
		const reader = new FrameReader(new ChannelReceiver(channel.mock));

		// send a stream with two frames
		const payloads = [
			buffers.fill(sizes.MAX_PAYLOAD_SIZE, 5),
			buffers.of(1, 2, 3)
		];
		await channel.remoteSend(startFrame(sizes.MAX_PAYLOAD_SIZE + 3, payloads[0]));
		await channel.remoteSend(continueFrame(1, payloads[1]));

		const buf = await reader.readAll();
		expect(buf).deep.eq(buffers.concat(... payloads));
	});

	it("readAll, three", async function () {

		const channel = new RTCDataChannelMock();
		const reader = new FrameReader(new ChannelReceiver(channel.mock));

		// send a stream with three frames
		const payloads = [
			buffers.fill(sizes.MAX_PAYLOAD_SIZE, 5),
			buffers.fill(sizes.MAX_PAYLOAD_SIZE, 42),
			buffers.of(1, 2, 3)
		];
		await channel.remoteSend(startFrame(sizes.MAX_PAYLOAD_SIZE*2 + 3, payloads[0]));
		await channel.remoteSend(continueFrame(1, payloads[1]));
		await channel.remoteSend(continueFrame(2, payloads[2]));

		const buf = await reader.readAll();
		expect(buf).deep.eq(buffers.concat(... payloads));
	});

	it("readAll, three delayed", async function () {

		const channel = new RTCDataChannelMock();
		const reader = new FrameReader(new ChannelReceiver(channel.mock));

		const options = { timeoutNowMs: 200 };
		const p = reader.readAll(options);

		// send a stream with three frames, but spaced out
		const payloads = [
			buffers.fill(sizes.MAX_PAYLOAD_SIZE, 5),
			buffers.fill(sizes.MAX_PAYLOAD_SIZE, 42),
			buffers.of(1, 2, 3)
		];
		await promises.sleep(100);
		await channel.remoteSend(startFrame(sizes.MAX_PAYLOAD_SIZE*2 + 3, payloads[0]));
		await promises.sleep(100);
		await channel.remoteSend(continueFrame(1, payloads[1]));
		await promises.sleep(100);
		await channel.remoteSend(continueFrame(2, payloads[2]));

		const buf = await p;
		expect(buf).deep.eq(buffers.concat(... payloads));
	});

	it("readAll, twice", async function () {

		const channel = new RTCDataChannelMock();
		const reader = new FrameReader(new ChannelReceiver(channel.mock));

		// send a stream with two frames
		const payloads = [
			buffers.fill(sizes.MAX_PAYLOAD_SIZE, 5),
			buffers.of(1, 2, 3)
		];
		await channel.remoteSend(startFrame(sizes.MAX_PAYLOAD_SIZE + 3, payloads[0]));
		await channel.remoteSend(continueFrame(1, payloads[1]));

		let buf = await reader.readAll();
		expect(buf).deep.eq(buffers.concat(... payloads));

		// do it again
		await channel.remoteSend(startFrame(sizes.MAX_PAYLOAD_SIZE + 3, payloads[0]));
		await channel.remoteSend(continueFrame(1, payloads[1]));

		buf = await reader.readAll();
		expect(buf).deep.eq(buffers.concat(... payloads));
	});

	it("readAll, timeoutAfterStart", async function () {

		const channel = new RTCDataChannelMock();
		const reader = new FrameReader(new ChannelReceiver(channel.mock));

		const options = { timeoutAfterStartMs: 100 };
		const p = reader.readAll(options);

		// send a stream with two frames, after a delay
		const payloads = [
			buffers.fill(sizes.MAX_PAYLOAD_SIZE, 5),
			buffers.of(1, 2, 3)
		];
		await promises.sleep(200);
		await channel.remoteSend(startFrame(sizes.MAX_PAYLOAD_SIZE + 3, payloads[0]));
		await channel.remoteSend(continueFrame(1, payloads[1]));

		const buf = await p;
		expect(buf).deep.eq(buffers.concat(... payloads));
	});

	it("readAll, timeoutAfterStart, timeout", async function () {

		const channel = new RTCDataChannelMock();
		const reader = new FrameReader(new ChannelReceiver(channel.mock));

		const options = { timeoutAfterStartMs: 100 };
		const p = reader.readAll(options);

		// send a stream with two frames, after a delay
		const payloads = [
			buffers.fill(sizes.MAX_PAYLOAD_SIZE, 5),
			buffers.of(1, 2, 3)
		];
		await promises.sleep(200);
		await channel.remoteSend(startFrame(sizes.MAX_PAYLOAD_SIZE + 3, payloads[0]));
		await promises.sleep(200);
		await channel.remoteSend(continueFrame(1, payloads[1]));

		const err = await expect(p).eventually.rejected;
		expect(err).instanceof(FrameReaderTimeoutError);
		expect(err.type).eq(FrameReaderTimeoutError.AFTER_START);
	});

	it("readAll, error recovery", async function () {

		const channel = new RTCDataChannelMock();
		const reader = new FrameReader(new ChannelReceiver(channel.mock));

		// send a partial stream
		let payloads = [
			buffers.fill(sizes.MAX_PAYLOAD_SIZE, 5)
		];
		await channel.remoteSend(startFrame(sizes.MAX_PAYLOAD_SIZE + 3, payloads[0]));

		// timeout reading the stream
		const options = { timeoutAfterStartMs: 100 };
		await expect(reader.readAll(options)).eventually.rejected;

		// try to read another stream
		const p = reader.readAll(options);

		// send a stream with two frames
		payloads = [
			buffers.fill(sizes.MAX_PAYLOAD_SIZE, 5),
			buffers.of(1, 2, 3)
		];
		await channel.remoteSend(startFrame(sizes.MAX_PAYLOAD_SIZE + 3, payloads[0]));
		await channel.remoteSend(continueFrame(1, payloads[1]));

		const buf = await p;
		expect(buf).deep.eq(buffers.concat(... payloads));
	});
});
