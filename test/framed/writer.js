
import { expect } from 'chai';

import * as buffers from 'food-runner/src/buffers.js';

import * as sizes from 'smolweb/src/framed/sizes.js';
import { FrameWriter } from 'smolweb/src/framed/writer.js';
import { Deframer, RemoteFrameError } from 'smolweb/src/framed/deframer.js';

import { RTCDataChannelMock } from 'smolweb/testlib/channel.js';


describe("framed.writer", function () {

	it("write, one", function () {

		const channel = new RTCDataChannelMock();
		const writer = new FrameWriter(channel);

		const data = buffers.of(1, 2, 3);
		writer.write(data);

		// should have written one frame
		const [deframer, payload] = Deframer.new(channel.remoteRecv());
		expect(deframer.bytesTotal).eq(BigInt(3));
		expect(deframer.isComplete).true;
		expect(payload).deep.eq(data);

		expect(channel.remoteIsEmpty()).true;
	});

	it("write, two", function () {

		const channel = new RTCDataChannelMock();
		const writer = new FrameWriter(channel);

		const data = buffers.fill(sizes.MAX_PAYLOAD_SIZE + 3, 5);
		writer.write(data);

		// should have written two frames
		let [deframer, payload] = Deframer.new(channel.remoteRecv());
		expect(deframer.bytesTotal).eq(BigInt(sizes.MAX_PAYLOAD_SIZE + 3));
		expect(deframer.isComplete).false;
		expect(payload).deep.eq(buffers.fill(sizes.MAX_PAYLOAD_SIZE, 5));

		payload = deframer.deframe(channel.remoteRecv());
		expect(payload).deep.eq(buffers.fill(3, 5));
		expect(deframer.isComplete).true;

		expect(channel.remoteIsEmpty()).true;
	});

	it("writeError", function () {

		const channel = new RTCDataChannelMock();
		const writer = new FrameWriter(channel);

		writer.writeError({ abort: true });

		// should have written one error frame
		const e = expect(() => Deframer.new(channel.remoteRecv())).throw()._obj;
		expect(e).instanceof(RemoteFrameError);
		expect(e.frameError.error).eq('abort');

		expect(channel.remoteIsEmpty()).true;
	});
});


describe("framed.streamer", function () {

	it("write, one", function () {

		const channel = new RTCDataChannelMock();
		const writer = new FrameWriter(channel);

		const stream = writer.stream(3);
		const data = buffers.of(1, 2, 3);
		stream.write(data);

		// should have written one frame
		const [deframer, payload] = Deframer.new(channel.remoteRecv());
		expect(deframer.bytesTotal).eq(BigInt(3));
		expect(deframer.isComplete).true;
		expect(payload).deep.eq(data);

		expect(channel.remoteIsEmpty()).true;
	});

	it("write, two", function () {

		const channel = new RTCDataChannelMock();
		const writer = new FrameWriter(channel);

		const stream = writer.stream(sizes.MAX_PAYLOAD_SIZE + 3);
		expect(stream.isClosed).false
		stream.write(buffers.fill(sizes.MAX_PAYLOAD_SIZE, 42));
		expect(stream.isClosed).false
		stream.write(buffers.of(1, 2, 3));
		expect(stream.isClosed).true;

		// should have written two frames
		let [deframer, payload] = Deframer.new(channel.remoteRecv());
		expect(deframer.bytesTotal).eq(BigInt(sizes.MAX_PAYLOAD_SIZE + 3));
		expect(deframer.isComplete).false;
		expect(payload).deep.eq(buffers.fill(sizes.MAX_PAYLOAD_SIZE,  42));

		payload = deframer.deframe(channel.remoteRecv());
		expect(deframer.isComplete).true;
		expect(payload).deep.eq(buffers.of(1, 2, 3));

		expect(channel.remoteIsEmpty()).true;
	});

	it("write, abort", function () {

		const channel = new RTCDataChannelMock();
		const writer = new FrameWriter(channel);

		const stream = writer.stream(sizes.MAX_PAYLOAD_SIZE + 5);
		expect(stream.isClosed).false;
		stream.write(buffers.fill(sizes.MAX_PAYLOAD_SIZE, 42));
		expect(stream.isClosed).false;
		stream.abort();

		// should have written two frames
		let [deframer, payload] = Deframer.new(channel.remoteRecv());
		expect(deframer.bytesTotal).eq(BigInt(sizes.MAX_PAYLOAD_SIZE + 5));
		expect(deframer.isComplete).false;
		expect(payload).deep.eq(buffers.fill(sizes.MAX_PAYLOAD_SIZE,  42));

		const e = expect(() => deframer.deframe(channel.remoteRecv())).throw()._obj;
		expect(e).instanceof(RemoteFrameError);
		expect(e.frameError.error).eq('abort');
		expect(deframer.isComplete).false;
		expect(deframer.isClosed).true;

		expect(channel.remoteIsEmpty()).true;
	});
});
