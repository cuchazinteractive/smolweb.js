
import { expect } from 'chai';

import * as buffers from 'food-runner/src/buffers.js';

import * as sizes from 'smolweb/src/framed/sizes.js';
import { Framer } from 'smolweb/src/framed/framer.js';


describe("framed.framer", function () {

	it("max size", function () {

		expect(() => new Framer(-1)).throw();
		expect(() => new Framer(-1n)).throw();
		expect(() => new Framer(sizes.MAX_STREAM_SIZE + 1)).throw();
		expect(() => new Framer(BigInt(sizes.MAX_STREAM_SIZE + 1))).throw();

		new Framer(0);
		new Framer(0n);
		new Framer(1);
		new Framer(1n);
		new Framer(sizes.MAX_STREAM_SIZE - 1n);
		new Framer(sizes.MAX_STREAM_SIZE);
	});

	it("init", function () {

		const framer = new Framer(5);

		expect(framer.bytesTotal).eq(5n);
		expect(framer.bytesWritten).eq(0n);
		expect(framer.isClosed).false;
	});

	it("frame, single", function () {

		const framer = new Framer(3);
		const payload = buffers.of(1, 2, 3);
		const frame = framer.frame(payload);

		expect(frame.byteLength)
			.greaterThanOrEqual(payload.byteLength)
			.lessThanOrEqual(payload.byteLength + sizes.MAX_OVERHEAD_SIZE);
		expect(framer.bytesWritten).eq(3n);
		expect(framer.isClosed).true;

		expect(() => framer.frame(buffers.of())).throw();
	});

	it("frame, unexpected payload", function () {

		const framer = new Framer(42);
		const payload = buffers.of(1, 2, 3);

		expect(() => framer.frame(payload)).throw();

		// but the stream should still be open to try again
		expect(framer.bytesWritten).eq(0n);
		expect(framer.isClosed).false;
	});

	it("frame, multi", function () {

		const framer = new Framer(sizes.MAX_PAYLOAD_SIZE + 3);
		let payload = buffers.arbitrary(sizes.MAX_PAYLOAD_SIZE);
		let frame = framer.frame(payload);

		expect(frame.byteLength)
			.greaterThanOrEqual(payload.byteLength)
			.lessThanOrEqual(payload.byteLength + sizes.MAX_OVERHEAD_SIZE);
		expect(framer.bytesWritten).eq(BigInt(sizes.MAX_PAYLOAD_SIZE));
		expect(framer.isClosed).false;

		payload = buffers.of(1, 2, 3);
		frame = framer.frame(payload);

		expect(frame.byteLength)
			.greaterThanOrEqual(payload.byteLength)
			.lessThanOrEqual(payload.byteLength + sizes.MAX_OVERHEAD_SIZE);
		expect(framer.bytesWritten).eq(BigInt(sizes.MAX_PAYLOAD_SIZE + 3));
		expect(framer.isClosed).true;
	});

	it("frameError", function () {

		const framer = new Framer(3);
		const frame = framer.frameError({ closed: true });

		expect(frame.byteLength).greaterThanOrEqual(0);

	});

	it("frameError, closed", function () {

		const framer = new Framer(3);
		framer.frame(buffers.of(1, 2, 3));

		expect(() => framer.frameError({ closed: true })).throw();
	});
});
