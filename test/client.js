
import { expect } from 'chai';

import * as buffers from 'food-runner/src/buffers.js';
import * as base64 from 'food-runner/src/base64.js';
import { IdnTag } from 'burgerid/src/identity.js';

import { SmolWebClient } from 'smolweb/src/client.js';
import { HostAddress } from 'smolweb/src/addresses.js';
import { SmolWebRawAuthority } from 'smolweb/src/proto.js';


const AUTHORITY = new SmolWebRawAuthority(
    IdnTag.fromUID(base64.decode("wueDBnzIe-sWtXYXlLGRYIXbV-hiFuaTs05E9SqQfF4")),
    new HostAddress('beacon.localhost', 40443)
);


// NOTE: these tests require a beacon server to be running at the address listed above
describe.skip("client", function () {

    it("connect", async function () {
        this.timeout(5_000);

        const client = new SmolWebClient(AUTHORITY);
        await client.connect();
        expect(client.isConnected).true;
    });

    it("connected, host unknown", async function () {
        this.timeout(5_000);

        const authority = new SmolWebRawAuthority(
            IdnTag.fromUID(buffers.of(5)),
            AUTHORITY.beaconAddr
        );
        const client = new SmolWebClient(authority);
        await expect(client.connect()).eventually.rejected;
    });
});
