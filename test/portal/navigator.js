
import { expect } from 'chai';

import * as buffers from 'food-runner/src/buffers.js';
import { IdnTag } from 'burgerid/src/identity.js';

import { PortalBar } from 'smolweb/src/portal/bar.js';
import { PortalFrame } from 'smolweb/src/portal/frame.js';
import { PortalNavigator } from 'smolweb/src/portal/navigator.js';
import { SmolWebAddress, SmolWebDomainAuthority, SmolWebRawAuthority } from 'smolweb/src/proto.js';
import { DnsCache, SmolWebDnsRecord } from 'smolweb/src/dns.js';
import { makeBeaconAddr } from 'smolweb/src/addresses.js';

import { TestConnectionPool } from 'smolweb/testlib/connectionPool.js';
import { TestDnsResolver } from 'smolweb/testlib/dns.js';
import { FetchServer } from 'smolweb/testlib/fetch.js';


// noinspection JSUnusedGlobalSymbols
class WindowMock {

	addEventListener(name, listener, options) {
		console.log('add event listener: ignoring in mock', name, listener, options);
	}

	/** @type {?string} */
	hashUrl = null;

	get location() {
		return {
			hash: this.hashUrl != null ? `#${encodeURIComponent(this.hashUrl)}` : ''
		};
	}

	set location(value) {
		console.log('set location', value);
	}

	history = {

		pushState: (/*any*/ data, _unused, /*string*/ url) => {
			console.log('push state', data, url);
		}
	};

	/** @returns {Window} */
	get mock() {
		return /** @type {Window} */ this;
	}
}


class Tester {

	bar = new PortalBar();
	frame = new PortalFrame();
	dnsResolver = new TestDnsResolver();
	dnsCache = new DnsCache();
	pool = new TestConnectionPool();
	window = new WindowMock();
	navigator = new PortalNavigator(this.bar, this.frame, {
		timeoutMs: 1000,
		dnsResolver: this.dnsResolver,
		dnsCache: this.dnsCache,
		connectionPool: this.pool,
		window: this.window.mock
	});

	constructor() {

		// put the iframe in the real DOM, otherwise we won't get events
		const frame = this.frame;
		frame.style.display = 'none';
		window.document.body.appendChild(frame);
	}

	/**
	 * @param {SmolWebRawAuthority} authority
	 * @returns {FetchServer}
	 */
	makeFetchServer(authority) {
		const server = new FetchServer(authority);
		this.pool.servers.set(authority.id(), server);
		return server;
	}

	/** @returns {?Document} */
	get doc() {
		return this.navigator.frame.getDoc();
	}
}


describe("portal.Navigator", function () {

	it("init, no hash", async function () {

		const tester = new Tester();

		expect(tester.navigator.address).null;

		await tester.navigator.init();

		expect(tester.navigator.address).null;
	});

	it("init, invalid", async function () {

		const tester = new Tester();
		tester.window.hashUrl = 'beer';

		expect(tester.navigator.address).null;

		await tester.navigator.init();

		expect(tester.navigator.address).null;
	});

	it("init/load, raw authority", async function () {

		const tester = new Tester();
		const addr = new SmolWebAddress(
			new SmolWebRawAuthority(
				IdnTag.fromUID(buffers.of(1, 2, 3)),
				makeBeaconAddr({ hostname: 'beacon.tld' })
			),
			'/foo'
		);
		tester.window.hashUrl = addr.toUrl();
		const server = tester.makeFetchServer(addr.authority);

		const p = tester.navigator.init();

		// the navigator should request the page
		const request = await server.readRequest();
		expect(request.hasBody).false;
		expect(request.path).eq(addr.path);

		// send some HTML back
		server.writeHtml(`
			<html lang="en_US">
				<body>
					<div id="foo"></div>
				</body>
			</html>
		`);

		await p;

		// the page should be loaded
		expect(tester.navigator.address).methodEquals(addr);
		expect(tester.doc.querySelector('#foo')).not.null;
	});

	it("init/load, domain authority", async function () {

		const tester = new Tester();
		const authority = new SmolWebRawAuthority(
			IdnTag.fromUID(buffers.of(1, 2, 3)),
			makeBeaconAddr({ hostname: 'beacon.tld' })
		);
		tester.dnsResolver.zone['domain.tld'] = [SmolWebDnsRecord.fromRawAuthority(authority)];
		const addr = new SmolWebAddress(
			new SmolWebDomainAuthority('domain.tld'),
			'/bar'
		);
		tester.window.hashUrl = addr.toUrl();
		const server = tester.makeFetchServer(authority);

		const p = tester.navigator.init();

		// the navigator should request the page
		const request = await server.readRequest();
		expect(request.hasBody).false;
		expect(request.path).eq(addr.path);

		// send some HTML back
		server.writeHtml(`
			<html lang="en_US">
				<body>
					<div id="bar"></div>
				</body>
			</html>
		`);

		await p;

		// the page should be loaded
		expect(tester.navigator.address).methodEquals(addr);
		expect(tester.doc.querySelector('#bar')).not.null;
	});

	// TODO: test navigation?
	// TODO: test loading of sub-resources?
});
