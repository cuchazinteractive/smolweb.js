
import { expect } from 'chai';

import * as paths from 'smolweb/src/portal/paths.js';


describe("paths", function () {

	it("join", function () {

		expect(paths.join('', '')).eq('');

		expect(paths.join('/', '')).eq('/');
		expect(paths.join('/', '/')).eq('/');
		expect(paths.join('', '/')).eq('/');

		expect(paths.join('', 'beer')).eq('beer');
		expect(paths.join('beer', '/')).eq('beer');
		expect(paths.join('beer/', '/')).eq('beer');
		expect(paths.join('beer', '')).eq('beer');
		expect(paths.join('beer/', '')).eq('beer');

		expect(paths.join('', '/beer')).eq('beer');
		expect(paths.join('/', 'beer')).eq('/beer');
		expect(paths.join('/', '/beer')).eq('/beer');

		expect(paths.join('beer', 'cheese')).eq('beer/cheese');
		expect(paths.join('beer/', 'cheese')).eq('beer/cheese');
		expect(paths.join('beer', '/cheese')).eq('beer/cheese');
		expect(paths.join('beer/', '/cheese')).eq('beer/cheese');

		expect(paths.join('/beer', 'cheese')).eq('/beer/cheese');
		expect(paths.join('/beer/', 'cheese')).eq('/beer/cheese');
		expect(paths.join('/beer', '/cheese')).eq('/beer/cheese');
		expect(paths.join('/beer/', '/cheese')).eq('/beer/cheese');
	});

	it("canonicalize", function () {

		expect(paths.canonicalize('')).eq('');
		expect(paths.canonicalize('/')).eq('/');

		expect(paths.canonicalize('beer')).eq('beer');
		expect(paths.canonicalize('beer/')).eq('beer/');
		expect(paths.canonicalize('/beer')).eq('/beer');
		expect(paths.canonicalize('/beer/')).eq('/beer/');

		expect(paths.canonicalize('beer/cheese')).eq('beer/cheese');
		expect(paths.canonicalize('/beer/cheese')).eq('/beer/cheese');
		expect(paths.canonicalize('beer/cheese/')).eq('beer/cheese/');
		expect(paths.canonicalize('/beer/cheese/')).eq('/beer/cheese/');

		expect(paths.canonicalize('.')).eq('');
		expect(paths.canonicalize('./')).eq('');
		expect(paths.canonicalize('/.')).eq('/');
		expect(paths.canonicalize('/./')).eq('/');

		expect(paths.canonicalize('..')).eq('..');
		expect(paths.canonicalize('../')).eq('../');
		expect(paths.canonicalize('/..')).null;
		expect(paths.canonicalize('/../')).null;

		expect(paths.canonicalize('beer/.')).eq('beer/');
		expect(paths.canonicalize('/beer/.')).eq('/beer/');
		expect(paths.canonicalize('beer/./')).eq('beer/');
		expect(paths.canonicalize('/beer/./')).eq('/beer/');

		expect(paths.canonicalize('./beer')).eq('beer');
		expect(paths.canonicalize('/./beer')).eq('/beer');
		expect(paths.canonicalize('./beer/')).eq('beer/');
		expect(paths.canonicalize('/./beer/')).eq('/beer/');

		expect(paths.canonicalize('beer/..')).eq('');
		expect(paths.canonicalize('/beer/..')).eq('/');
		expect(paths.canonicalize('beer/../')).eq('');
		expect(paths.canonicalize('/beer/../')).eq('/');

		expect(paths.canonicalize('../beer')).eq('../beer');
		expect(paths.canonicalize('/../beer')).null;
		expect(paths.canonicalize('../beer/')).eq('../beer/');
		expect(paths.canonicalize('/../beer/')).null;

		expect(paths.canonicalize('beer/cheese/..')).eq('beer');
		expect(paths.canonicalize('/beer/cheese/..')).eq('/beer');
		expect(paths.canonicalize('beer/cheese/../')).eq('beer/');
		expect(paths.canonicalize('/beer/cheese/../')).eq('/beer/');

		expect(paths.canonicalize('../beer/cheese')).eq('../beer/cheese');
		expect(paths.canonicalize('/../beer/cheese')).null;
		expect(paths.canonicalize('../beer/cheese/')).eq('../beer/cheese/');
		expect(paths.canonicalize('/../beer/cheese/')).null;

		expect(paths.canonicalize('beer/../cheese')).eq('cheese');
		expect(paths.canonicalize('/beer/../cheese')).eq('/cheese');
		expect(paths.canonicalize('beer/../cheese/')).eq('cheese/');
		expect(paths.canonicalize('/beer/../cheese/')).eq('/cheese/');

		expect(paths.canonicalize('beer/cheese/../../wine/food/../grapes')).eq('wine/grapes');
	});

	it("basename", function () {

		expect(paths.basename('')).null;
		expect(paths.basename('/')).eq('/');

		expect(paths.basename('beer')).null;
		expect(paths.basename('/beer')).eq('/');

		expect(paths.basename('beer/')).eq('beer/');
		expect(paths.basename('beer/cheese')).eq('beer/');
		expect(paths.basename('/beer/')).eq('/beer/');
		expect(paths.basename('/beer/cheese')).eq('/beer/');
	});
});
