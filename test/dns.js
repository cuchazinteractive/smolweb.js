
import { expect } from 'chai';

import * as buffers from 'food-runner/src/buffers.js';
import { IdnTag } from 'burgerid/src/identity.js';

import { lookup, DnsCache, SmolWebDnsRecord } from 'smolweb/src/dns.js';
import { makeBeaconAddr } from 'smolweb/src/addresses.js';

import { TestDnsResolver } from 'smolweb/testlib/dns.js';


describe("DNS", function () {

	/**
	 * @param {DnsResolver} resolver
	 * @returns {Object}
	 */
	function options(resolver) {
		return {
			resolver,
			// use a new cache every test
			cache: new DnsCache()
		};
	}


	it("lookup, empty", async function () {

		const domain = 'domain.tld';
		const resolver = new TestDnsResolver();

		const authorities = await lookup(domain, options(resolver));

		expect(authorities.length).eq(0);
	});

	it("lookup, one", async function () {

		const domain = 'domain.tld';
		const r1 = new SmolWebDnsRecord(
			IdnTag.fromUID(buffers.of(1, 2, 3)),
			makeBeaconAddr({ hostname: 'beacon.foo' })
		);
		const resolver = new TestDnsResolver();
		resolver.zone[domain] = [r1];

		const records = await lookup(domain, options(resolver));

		expect(records.length).eq(1);
		expect(records[0]).methodEquals(r1);
	});

	it("lookup, two", async function () {

		const domain = 'domain.tld';
		const r1 = new SmolWebDnsRecord(
			IdnTag.fromUID(buffers.of(1, 2, 3)),
			makeBeaconAddr({ hostname: 'beacon.foo' })
		);
		const r2 = new SmolWebDnsRecord(
			IdnTag.fromUID(buffers.of(4, 5, 6)),
			makeBeaconAddr({ hostname: 'beacon.bar' })
		);
		const resolver = new TestDnsResolver();
		resolver.zone[domain] = [r1, r2];

		const records = await lookup(domain, options(resolver));

		expect(records.length).eq(2);
		expect(records[0]).methodEquals(r1);
		expect(records[1]).methodEquals(r2);
	});

	it("lookup, cache", async function () {

		const domain = 'domain.tld';
		const r1 = new SmolWebDnsRecord(
			IdnTag.fromUID(buffers.of(1, 2, 3)),
			makeBeaconAddr({ hostname: 'beacon.foo' })
		);
		const r2 = new SmolWebDnsRecord(
			IdnTag.fromUID(buffers.of(4, 5, 6)),
			makeBeaconAddr({ hostname: 'beacon.bar' })
		);
		const resolver = new TestDnsResolver();
		resolver.zone[domain] = [r1];

		const opts = options(resolver);

		// start with r2 in the cache
		opts.cache.set(domain, [r2]);

		// should get it instead of r1
		let records = await lookup(domain, opts);
		expect(records.length).eq(1);
		expect(records[0]).methodEquals(r2);

		// evict it
		opts.cache.evict(domain);

		// should get r1 next time
		records = await lookup(domain, opts);
		expect(records.length).eq(1);
		expect(records[0]).methodEquals(r1);
	});
});
