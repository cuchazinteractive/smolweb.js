
import chai from 'chai';
import chaiAsPromised from 'chai-as-promised';

import * as mochaMods from 'food-runner-build/src/mocha.js';

import 'smolweb/testlib/assertions.js';


chai.use(chaiAsPromised);


mochaMods.configure({
	chai: {
		trimErrors: true
	}
});
