
import { expect } from 'chai';

import * as buffers from 'food-runner/src/buffers.js';
import { IdnTag } from 'burgerid/src/identity.js';

import { PROTO, SmolWebAddress, SmolWebRawAuthority, SmolWebDomainAuthority } from 'smolweb/src/proto.js';
import { makeBeaconAddr } from 'smolweb/src/addresses.js';


describe("proto", function () {

	it("SmolWebAddress.toUrl", formatErrors(function () {

		expect(new SmolWebAddress(
			new SmolWebRawAuthority(
				IdnTag.fromUID(buffers.of(1, 2, 3)),
				makeBeaconAddr({ hostname: 'localhost' })
			)
		).toUrl()).eq(`${PROTO}://AQID@localhost/`);

		expect(new SmolWebAddress(
			new SmolWebRawAuthority(
				new IdnTag(
					buffers.of(1, 2, 3),
					5,
					buffers.of(4, 5, 6)
				),
				makeBeaconAddr({ hostname: 'localhost' })
			)
		).toUrl()).eq(`${PROTO}://AQID:5:BAUG@localhost/`);

		expect(new SmolWebAddress(
			new SmolWebRawAuthority(
				IdnTag.fromUID(buffers.of(1, 2, 3)),
				makeBeaconAddr({ hostname: 'beacon.tld' })
			),
		).toUrl()).eq(`${PROTO}://AQID@beacon.tld/`);

		expect(new SmolWebAddress(
			new SmolWebRawAuthority(
				IdnTag.fromUID(buffers.of(1, 2, 3)),
				makeBeaconAddr({ hostname: 'beacon.tld', port: 8080 })
			)
		).toUrl()).eq(`${PROTO}://AQID@beacon.tld:8080/`);

		expect(new SmolWebAddress(
			new SmolWebRawAuthority(
				IdnTag.fromUID(buffers.of(1, 2, 3)),
				makeBeaconAddr({ hostname: 'beacon.tld' })
			),
			'/the/resource?and+query'
		).toUrl()).eq(`${PROTO}://AQID@beacon.tld/the/resource?and+query`);

		expect(new SmolWebAddress(
			new SmolWebDomainAuthority('domain.tld')
		).toUrl()).eq(`${PROTO}://domain.tld/`);

		expect(new SmolWebAddress(
			new SmolWebDomainAuthority('domain.tld'),
			'/the/resource?and+query'
		).toUrl()).eq(`${PROTO}://domain.tld/the/resource?and+query`);

	}));

	it("SmolWebAddress.fromUrl", formatErrors(function () {

		function shouldFail(url) {
			expect(() => SmolWebAddress.fromUrl(url)).throw;
		}

		// bad protocol
		shouldFail('foo://');

		// no authority
		shouldFail(`${PROTO}://`);

		// bad authority
		shouldFail(`${PROTO}://foo@bar@cow`);

		// bad host UID
		shouldFail(`${PROTO}://not.an.idn.tag@beacon`);

		// bad port
		shouldFail(`${PROTO}://AQID@beacon.tld:port`);
		shouldFail(`${PROTO}://AQID@beacon.tld:0x123`);

		// domains shouldn't have ports
		shouldFail(`${PROTO}://domain.tld:123`);

		expect(SmolWebAddress.fromUrl(`${PROTO}://AQID@beacon.tld`)).methodEquals(new SmolWebAddress(
			new SmolWebRawAuthority(
				IdnTag.fromUID(buffers.of(1, 2, 3)),
				makeBeaconAddr({ hostname: 'beacon.tld' })
			),
			'/'
		));

		expect(SmolWebAddress.fromUrl(`${PROTO}://AQID:5:BAUG@beacon.tld`)).methodEquals(new SmolWebAddress(
			new SmolWebRawAuthority(
				new IdnTag(
					buffers.of(1, 2, 3),
					5,
					buffers.of(4, 5, 6)
				),
				makeBeaconAddr({ hostname: 'beacon.tld' })
			),
			'/'
		));

		expect(SmolWebAddress.fromUrl(`${PROTO}://AQID@beacon.tld/`)).methodEquals(new SmolWebAddress(
			new SmolWebRawAuthority(
				IdnTag.fromUID(buffers.of(1, 2, 3)),
				makeBeaconAddr({ hostname: 'beacon.tld' })
			),
			'/'
		));

		expect(SmolWebAddress.fromUrl(`${PROTO}://AQID@beacon.tld:123`)).methodEquals(new SmolWebAddress(
			new SmolWebRawAuthority(
				IdnTag.fromUID(buffers.of(1, 2, 3)),
				makeBeaconAddr({ hostname: 'beacon.tld', port: 123 })
			),
			'/'
		));

		expect(SmolWebAddress.fromUrl(`${PROTO}://AQID@beacon.tld:123/`)).methodEquals(new SmolWebAddress(
			new SmolWebRawAuthority(
				IdnTag.fromUID(buffers.of(1, 2, 3)),
				makeBeaconAddr({ hostname: 'beacon.tld', port: 123 })
			),
			'/'
		));

		expect(SmolWebAddress.fromUrl(`${PROTO}://AQID@beacon.tld/doc`)).methodEquals(new SmolWebAddress(
			new SmolWebRawAuthority(
				IdnTag.fromUID(buffers.of(1, 2, 3)),
				makeBeaconAddr({ hostname: 'beacon.tld' })
			),
			'/doc'
		));

		expect(SmolWebAddress.fromUrl(`${PROTO}://AQID@beacon.tld/path/to/doc`)).methodEquals(new SmolWebAddress(
			new SmolWebRawAuthority(
				IdnTag.fromUID(buffers.of(1, 2, 3)),
				makeBeaconAddr({ hostname: 'beacon.tld' })
			),
			'/path/to/doc'
		));

		expect(SmolWebAddress.fromUrl(`${PROTO}://AQID@beacon.tld/doc?query`)).methodEquals(new SmolWebAddress(
			new SmolWebRawAuthority(
				IdnTag.fromUID(buffers.of(1, 2, 3)),
				makeBeaconAddr({ hostname: 'beacon.tld' })
			),
			'/doc?query'
		));

		expect(SmolWebAddress.fromUrl(`${PROTO}://domain.tld`)).methodEquals(new SmolWebAddress(
			new SmolWebDomainAuthority('domain.tld'),
			'/'
		));

		expect(SmolWebAddress.fromUrl(`${PROTO}://domain.tld/`)).methodEquals(new SmolWebAddress(
			new SmolWebDomainAuthority('domain.tld'),
			'/'
		));

		expect(SmolWebAddress.fromUrl(`${PROTO}://domain.tld/doc?query`)).methodEquals(new SmolWebAddress(
			new SmolWebDomainAuthority('domain.tld'),
			'/doc?query'
		));
	}));
});
