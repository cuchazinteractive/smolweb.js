
import { expect } from 'chai';

import * as buffers from 'food-runner/src/buffers.js';
import { IdnTag } from 'burgerid/src/identity.js';

import * as fetch from 'smolweb/src/fetch.js';
import { smolweb } from 'smolweb/src/protobuf/gen.js';
import * as sizes from 'smolweb/src/framed/sizes.js';
import { DEFAULT_BEACON_PORT, HostAddress } from 'smolweb/src/addresses.js';
import { SmolWebRawAuthority } from 'smolweb/src/proto.js';

import { FetchServer } from 'smolweb/testlib/fetch.js';


describe("fetchOverSmolWeb", function () {

    const AUTHORITY = new SmolWebRawAuthority(
        IdnTag.fromUID(buffers.of(1, 2, 3)),
        new HostAddress("domain.tld", DEFAULT_BEACON_PORT)
    );

    /**
     * @param {FetchServer} server
     * @param {number} [timeoutMs]
     * @returns {FetchOverSmolWebOptions}
     */
    function options(server, timeoutMs=100) {

        // mock a connection pool to always return the same client
        const connectionPool = /** @type {ConnectionPool} */ {
            get: async () => {
                const client = server.client();
                await client.connect();
                return client.mock;
            }
        };

        return {
            timeoutMs,
            connectionPool
        };
    }


    it("request path", async function () {

        const server = new FetchServer(AUTHORITY);
        const path = '/cheese/beer';
        const p = expect(fetch.fetchOverSmolWeb(server.url(path), {}, options(server, 0)))
            .frameReader.rejectedTimeout();

        const request = await server.readRequest();
        expect(request.path).eq(path);

        await p;
    });

    it("request method", async function () {

        const server = new FetchServer(AUTHORITY);
        const init = {
            method: 'POST'
        };
        const p = expect(fetch.fetchOverSmolWeb(server.url(), init, options(server, 0)))
            .frameReader.rejectedTimeout();

        const request = await server.readRequest();
        expect(request.method).eq('POST');

        await p;
    });

    it("request headers, array", async function () {

        const server = new FetchServer(AUTHORITY);

        let headers = [];
        let p = expect(fetch.fetchOverSmolWeb(server.url(), { headers }, options(server, 0)))
            .frameReader.rejectedTimeout();

        let request = await server.readRequest();
        expect(request.headers).deep.eq([]);

        await p;

        const header1 = smolweb.fetch.Header.new({ name: 'foo', value: 'bar' });
        const header2 = smolweb.fetch.Header.new({ name: 'moo', value: 'cow' });
        headers = [
            [header1.name, header1.value],
            [header2.name, header2.value],
        ];
        p = expect(fetch.fetchOverSmolWeb(server.url(), { headers }, options(server, 0)))
            .frameReader.rejectedTimeout();

        request = await server.readRequest();
        expect(request.headers).deep.eq([header1, header2]);

        await p;
    });

    it("request headers, record", async function () {

        const server = new FetchServer(AUTHORITY);

        let headers = {};
        let p = expect(fetch.fetchOverSmolWeb(server.url(), { headers }, options(server, 0)))
            .frameReader.rejectedTimeout();

        let request = await server.readRequest();
        expect(request.headers).deep.eq([]);

        await p;

        const header1 = smolweb.fetch.Header.new({ name: 'cheese', value: 'beer' });
        const header2 = smolweb.fetch.Header.new({ name: 'foods', value: 'drinks' });
        headers = {
            [header1.name]: header1.value,
            [header2.name]: header2.value
        };
        p = expect(fetch.fetchOverSmolWeb(server.url(), { headers }, options(server, 0)))
            .frameReader.rejectedTimeout();

        request = await server.readRequest();
        expect(request.headers).deep.eq([header1, header2]);

        await p;
    });

    it("request headers, Headers", async function () {

        const server = new FetchServer(AUTHORITY);

        let headers = new Headers();
        let p = expect(fetch.fetchOverSmolWeb(server.url(), { headers }, options(server, 0)))
            .frameReader.rejectedTimeout();

        let request = await server.readRequest();
        expect(request.headers).deep.eq([]);

        await p;

        const header1 = smolweb.fetch.Header.new({ name: 'answer', value: 'fourty-two' });
        const header2 = smolweb.fetch.Header.new({ name: 'best', value: 'five' });
        headers = new Headers();
        headers.set(header1.name, header1.value);
        headers.set(header2.name, header2.value);
        p = expect(fetch.fetchOverSmolWeb(server.url(), { headers }, options(server, 0)))
            .frameReader.rejectedTimeout();

        request = await server.readRequest();
        expect(request.headers).deep.eq([header1, header2]);

        await p;
    });

    it("request body, none", async function () {

        const server = new FetchServer(AUTHORITY);

        const p = expect(fetch.fetchOverSmolWeb(server.url(), {}, options(server, 0)))
            .frameReader.rejectedTimeout();

        const request = await server.readRequest();
        expect(request.hasBody).false;

        await p;
    });

    it("request body, not allowed on get", async function () {

        const server = new FetchServer(AUTHORITY);

        const body = 'hello world!';
        await expect(fetch.fetchOverSmolWeb(server.url(), { body }, options(server, 0)))
            .frameReader.rejectedNotTimeout();
        // TODO: inspect the error??
    });

    it("request body, string", async function () {

        const server = new FetchServer(AUTHORITY);

        const body = 'hello world!';
        const init = {
            method: 'POST',
            body
        };
        const p = expect(fetch.fetchOverSmolWeb(server.url(), init, options(server, 0)))
            .frameReader.rejectedTimeout();

        const request = await server.readRequest();
        expect(request.hasBody).true;
        const bodyBuf = await server.readBody();
        expect(bodyBuf).deep.eq(buffers.fromString(body));

        await p;
    });

    it("request body, buffer", async function () {

        const server = new FetchServer(AUTHORITY);

        const body = buffers.of(1, 2, 3);
        const init = {
            method: 'POST',
            body
        };
        const p = expect(fetch.fetchOverSmolWeb(server.url(), init, options(server, 0)))
            .frameReader.rejectedTimeout();

        const request = await server.readRequest();
        expect(request.hasBody).true;
        const bodyBuf = await server.readBody();
        expect(bodyBuf).deep.eq(body);

        await p;
    });

    it("request body, Blob", async function () {

        const server = new FetchServer(AUTHORITY);

        const body = buffers.of(1, 2, 3);
        const init = {
            method: 'POST',
            body: new Blob([body])
        };
        const p = expect(fetch.fetchOverSmolWeb(server.url(), init, options(server, 0)))
            .frameReader.rejectedTimeout();

        const request = await server.readRequest();
        expect(request.hasBody).true;
        const bodyBuf = await server.readBody();
        expect(bodyBuf).deep.eq(body);

        await p;
    });

    it("request body, ReadableStream, no content-length", async function () {

        const server = new FetchServer(AUTHORITY);

        const init = {
            method: 'POST',
            body: new ReadableStream()
        };
        await expect(fetch.fetchOverSmolWeb(server.url(), init, options(server, 0)))
            .frameReader.rejectedNotTimeout();
        // TODO: inspect the error??
    });

    it("request body, ReadableStream, string", async function () {

        const server = new FetchServer(AUTHORITY);

        const body = 'hello world!';
        const init = {
            method: 'POST',
            headers: {
                'content-length': '12'
            },
            body: new ReadableStream({

                async pull(controller) {
                    controller.enqueue(body);
                    controller.close();
                }
            })
        };
        const p = expect(fetch.fetchOverSmolWeb(server.url(), init, options(server, 0)))
            .frameReader.rejectedTimeout();

        const request = await server.readRequest();
        expect(request.hasBody).true;
        const bodyBuf = await server.readBody();
        expect(bodyBuf).deep.eq(buffers.fromString(body));

        await p;
    });

    /**
     * @param {ArrayBuffer} body
     * @param {function(ReadableByteStreamController):Promise.<void>} streamer
     */
    async function fetchStreamed(body, streamer) {

        const server = new FetchServer(AUTHORITY);
        const init = {
            method: 'POST',
            headers: {
                'content-length': body.byteLength.toString()
            },
            body: new ReadableStream({
                type: 'bytes',
                start: streamer
            })
        };
        const p = expect(fetch.fetchOverSmolWeb(server.url(), init, options(server, 0)))
            .frameReader.rejectedTimeout();

        const request = await server.readRequest();
        expect(request.hasBody).true;
        const bodyBuf = await server.readBody();
        expect(bodyBuf).deep.eq(body);

        await p;
    }

    it("request body, ReadableStream, bytes", async function () {
        const body = buffers.of(1, 2, 3);
        await fetchStreamed(body, controller => {
            // NOTE: alters the buffer, so make a copy
            controller.enqueue(new Uint8Array(body.slice(0)));
            controller.close();
        });
    });

    it("request body, ReadableStream, large bytes", async function () {
        const body = buffers.arbitrary(sizes.MAX_PAYLOAD_SIZE*3 + 16);
        await fetchStreamed(body, controller => {
            // NOTE: alters the buffer, so make a copy
            controller.enqueue(new Uint8Array(body.slice(0)));
            controller.close();
        });
    });

    it("request body, ReadableStream, multi large bytes", async function () {
        const body = buffers.arbitrary(sizes.MAX_PAYLOAD_SIZE*5 + 32);
        await fetchStreamed(body, controller => {
            // send the chunks in larger-than-payload sizes, so we have leftovers every time
            let sent = 0;
            while (sent < body.byteLength) {
                const size = Math.min(sizes.MAX_PAYLOAD_SIZE + 1024, body.byteLength - sent);
                controller.enqueue(new Uint8Array(body.slice(sent, sent + size)));
                sent += size;
            }
        });
    });

    it("request body, ReadableStream, multi small bytes", async function () {
        const body = buffers.arbitrary(sizes.MAX_PAYLOAD_SIZE*3 + 8);
        await fetchStreamed(body, controller => {
            // send the chunks in smaller-than-payload sizes, so we have to fill lots of payloads
            let sent = 0;
            while (sent < body.byteLength) {
                const size = Math.min(1024, body.byteLength - sent);
                controller.enqueue(new Uint8Array(body.slice(sent, sent + size)));
                sent += size;
            }
            controller.close();
        });
    });

    it("gateway response", async function () {

        const server = new FetchServer(AUTHORITY);
        const p = fetch.fetchOverSmolWeb(server.url(), {}, options(server));

        server.writeGatewayResponse({
            serverUnreachable: true
        });

        const err = await expect(p).eventually.rejected;
        expect(err).instanceof(fetch.FetchGatewayError);
        expect(err.response.response).eq('serverUnreachable');
    });

    it("response status", async function () {

        const server = new FetchServer(AUTHORITY);
        const url = server.url();
        const p = fetch.fetchOverSmolWeb(url, {}, options(server));

        server.writeServerResponse({
            status: 200
        });

        const response = await p;
        expect(response.url).eq(url);
        expect(response.status).eq(200);
    });

    it("response headers", async function () {

        const server = new FetchServer(AUTHORITY);
        const url = server.url();
        const p = fetch.fetchOverSmolWeb(url, {}, options(server));

        server.writeServerResponse({
            status: 200,
            headers: [
                smolweb.fetch.Header.new({ name: 'foo', value: 'bar' })
            ]
        });

        const response = await p;
        expect(response.headers.get('foo')).eq('bar');
    });

    it("response, no body", async function () {

        const server = new FetchServer(AUTHORITY);
        const url = server.url();
        const p = fetch.fetchOverSmolWeb(url, {}, options(server));

        server.writeServerResponse({
            status: 200
        });

        const response = await p;
        const body = await response.arrayBuffer();
        expect(body.byteLength).eq(0);
    });

    it("response body, text", async function () {

        const server = new FetchServer(AUTHORITY);
        const url = server.url();
        const p = fetch.fetchOverSmolWeb(url, {}, options(server));

        server.writeServerResponse({
            status: 200,
            bodyType: smolweb.fetch.Response.ServerResponse.BodyType.SingleStream
        });
        const body = 'hello world!';
        server.writeBody(buffers.fromString(body));

        const response = await p;
        expect(await response.text()).deep.eq(body);
        expect(response.bodyUsed).true;
    });

    it("response body, buf", async function () {

        const server = new FetchServer(AUTHORITY);
        const url = server.url();
        const p = fetch.fetchOverSmolWeb(url, {}, options(server));

        server.writeServerResponse({
            status: 200,
            bodyType: smolweb.fetch.Response.ServerResponse.BodyType.SingleStream
        });
        const body = buffers.of(1, 2, 3);
        server.writeBody(body);

        const response = await p;
        expect(await response.arrayBuffer()).deep.eq(body);
        expect(response.bodyUsed).true;
    });

    it("response body, long", async function () {

        const server = new FetchServer(AUTHORITY);
        const url = server.url();
        const p = fetch.fetchOverSmolWeb(url, {}, options(server));

        server.writeServerResponse({
            status: 200,
            bodyType: smolweb.fetch.Response.ServerResponse.BodyType.SingleStream
        });
        const body = buffers.fill(sizes.MAX_PAYLOAD_SIZE*16, 5);
        server.writeBody(body);

        const response = await p;
        expect(await response.arrayBuffer()).deep.eq(body);
        expect(response.bodyUsed).true;
    });
});
