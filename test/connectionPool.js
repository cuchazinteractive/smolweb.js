
import { expect } from 'chai';

import * as promises from 'food-runner/src/promises.js';
import * as buffers from 'food-runner/src/buffers.js';

import { ConnectionPool } from 'smolweb/src/connectionPool.js';
import { makeBeaconAddr } from 'smolweb/src/addresses.js';
import { SmolWebRawAuthority } from 'smolweb/src/proto.js';

import { TestConnectionPool } from 'smolweb/testlib/connectionPool.js';


describe("ConnectionPool", function () {

	it("clean, empty", async function () {

		// noinspection JSUnusedAssignment (the connection pool has a rich internal life)
		let pool = new TestConnectionPool();

		// let the cleaner run for a few cycles
		await promises.sleep(500);

		// stop using the connection pool
		pool = null;

		// hopefully nothing bad happens after a few more cycles
		await promises.sleep(500);
	});

	it("get", async function () {

		let pool = new TestConnectionPool();

		expect(pool.connections).eq(0);

		// first get should create a new connection
		const authority = new SmolWebRawAuthority(
			buffers.of(1, 2, 3),
			makeBeaconAddr({ hostname: 'foo.bar' })
		);
		let client = await pool.get(authority);

		expect(client.authority).methodEquals(authority);
		expect(client.isConnected).true;
		expect(pool.connections).eq(1);
		expect(pool.numConnections).eq(1);

		// second one shouldn't
		client = await pool.get(authority);

		expect(client.authority).methodEquals(authority);
		expect(client.isConnected).true;
		expect(pool.connections).eq(1);
		expect(pool.numConnections).eq(1);
	});

	it("get, clean", async function () {

		let pool = new TestConnectionPool();

		const authority = new SmolWebRawAuthority(
			buffers.of(1, 2, 3),
			makeBeaconAddr({ hostname: 'foo.bar' })
		);
		const client = await pool.get(authority);

		expect(pool.numConnections).eq(1);
		expect(client.isConnected).true;

		// the connection should still be there after the first cleaning
		await promises.sleep(150);
		expect(pool.numConnections).eq(1);
		expect(client.isConnected).true;

		// but not the second
		await promises.sleep(150);
		expect(pool.numConnections).eq(0);
		expect(client.isConnected).false;
	});

	it("get, use, clean", async function () {

		let pool = new TestConnectionPool();

		const authority = new SmolWebRawAuthority(
			buffers.of(1, 2, 3),
			makeBeaconAddr({ hostname: 'foo.bar' })
		);
		const client = await pool.get(authority);

		expect(pool.numConnections).eq(1);
		expect(client.isConnected).true;

		// use the channel to keep the connection alive
		let channel = await client.reserveChannel();

		// the connection should still be there after a few cleanings
		await promises.sleep(600);
		expect(pool.numConnections).eq(1);
		expect(client.isConnected).true;

		// but not after returning the channel
		client.returnChannel(channel);
		channel = null;
		await promises.sleep(300);
		expect(pool.numConnections).eq(0);
		expect(client.isConnected).false;
	});
});
