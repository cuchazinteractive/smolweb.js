
import { iterSync } from 'food-runner/src/iter.js';

import { SmolWebClient } from 'smolweb/src/client.js';


export class ConnectionPool {

	/**
	 * @typedef {Object} ClientInfo
	 * @property {SmolWebClient} client
	 * @property {number} lastUsed
	 */

	/**
	 * @callback ClientConnector
	 * @param {SmolWebRawAuthority} authority
	 * @param {SmolWebClientOptions} [options]
	 * @returns {Promise.<SmolWebClient>}
	 */

	/** @type {Map.<string,ClientInfo>} */
	#clients = new Map();

	/** @type {number} */
	#keepAliveMs;

	/** @type {ClientConnector} */
	#connector;


	/**
	 * @param {Object} [options]
	 * @param {number} [options.keepAliveMs] Don't close unused connections until at least this many milliseconds of inactivity
	 * @param {number} [options.cleanIntervalMs]
	 * @param {ClientConnector} [options.clientConnector]
	 */
	constructor(options=undefined) {

		this.#keepAliveMs = options?.keepAliveMs ?? 15_000;
		this.#connector = options?.clientConnector ?? this.#defaultClientConnector;

		// set an interval to close unused connections
		const cleanIntervalMs = options?.cleanIntervalMs ?? 5_000;
		weakInterval(this.#clean, this, cleanIntervalMs);
	}


	/**
	 * @param {SmolWebRawAuthority} authority
	 * @param {?SmolWebClientOptions} [options]
	 * @returns {Promise.<SmolWebClient>}
	 */
	async #defaultClientConnector(authority, options=undefined) {
		const client = new SmolWebClient(authority, options);
		await client.connect();
		return client;
	}

	/**
	 * @param {SmolWebRawAuthority} authority
	 * @param {SmolWebClientOptions} [options]
	 * @returns {Promise.<SmolWebClient>}
	 */
	async get(authority, options=undefined) {

		// check the cache first
		const info = this.#clients.get(authority.id());
		if (info != null) {
			info.lastUsed = Date.now();
			return info.client;
		}

		// cache miss, make a new connection
		const client = await this.#connector(authority, options);
		if (!client.isConnected) {
			throw new Error("Connection pool connector returned an unconnected client");
		}

		// update the cache
		this.#clients.set(client.authority.id(), {
			client,
			lastUsed: Date.now()
		});
		return client;
	}

	#clean() {

		const now = Date.now();

		// look for still-active connections and update their timestamps
		// in case a client hasn't been get()'d recently, but is still being used
		for (const info of this.#clients.values()) {
			if (info.client.anyChannelsReserved()) {
				info.lastUsed = now;
			}
		}

		// remove the expired clients from the pool immediately
		const expired = iterSync(this.#clients)
			.filter(([_key, info]) => now - info.lastUsed > this.#keepAliveMs)
			.toArray();
		for (const [key, _info] of expired) {
			this.#clients.delete(key);
		}

		// close the expired clients, eventually
		for (const [_key, info] of expired) {
			(async () => {
				info.client.close();
			})();
		}
	}

	/** @returns {number} */
	get numConnections() {
		return this.#clients.size;
	}
}


/**
 * @param {function()} func
 * @param {any} target
 * @param {number} timeoutMs
 * @returns {function}
 */
function weakInterval(func, target, timeoutMs) {
	const weakTarget = new WeakRef(target);
	// noinspection JSUnusedAssignment (except by the garbage collector)
	target = undefined; // get rid of this function's reference to the target, so we only hold the WeakRef
	const id = window.setInterval(() => {
		const t = weakTarget.deref();
		if (t != null) {
			func.call(t);
		} else {
			window.clearInterval(id);
		}
	}, timeoutMs);
}
