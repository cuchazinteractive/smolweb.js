
import dnsPacket from 'dns-packet';
import { Buffer } from 'buffer';

import * as buffers from 'food-runner/src/buffers.js';
import { iterSync } from 'food-runner/src/iter.js';
import { IdnTag } from 'burgerid/src/identity.js';

import { DEFAULT_BEACON_PORT, HostAddress, makeBeaconAddr } from 'smolweb/src/addresses.js';
import { SmolWebDomainAuthority, SmolWebRawAuthority } from 'smolweb/src/proto.js';


export class DnsResolver {

	/** @type {string} */
	#url;


	/**
	 * @param {string} url
	 */
	constructor(url) {
		this.#url = url;
	}


	/**
	 * @param {SmolWebDomainAuthority} domain
	 * @returns {Promise.<ArrayBuffer>}
	 */
	async resolve(domain) {

		// send the DoH request, wait for a response
		const response = await fetch(this.#url, {
			method: 'POST',
			mode: 'cors',
			credentials: 'omit',
			headers: {
				'accept': 'application/dns-message',
				'user-agent': 'smolweb/v0.1',
				'content-type': 'application/dns-message'
			},
			body: dnsPacket.encode({
				type: 'query',
				id: 0,
				flags: dnsPacket.RECURSION_DESIRED,
				questions: [{
					type: 'TXT',
					name: domain.domain
				}]
			})
		});

		if (!response.ok) {
			throw new Error(`Failed to resolve DNS query: ${response.status}: ${response.statusText}`);
			// TODO: read any extra info from the response body?
		}

		return await response.arrayBuffer();
	}
}

/**
 * Resolves DNS queries using CloudFlare's Firefox/Mozilla resolver,
 * since it's the only service I could find that passes Mozilla's privacy bar and supports CORS.
 * See: https://wiki.mozilla.org/Security/DOH-resolver-policy#Conforming_Resolvers
 *
 * CloudFlare Privacy Policy: https://developers.cloudflare.com/1.1.1.1/privacy/firefox/
 */
export const DEFAULT_RESOLVER = new DnsResolver("https://mozilla.cloudflare-dns.com/dns-query");


export class DnsCache {

	/** @type {Map.<string,SmolWebDnsRecord[]>} */
	#cache = new Map();

	/**
	 * @param {string | SmolWebDomainAuthority} domain
	 * @returns {?(SmolWebDnsRecord[])}
	 */
	get(domain) {
		domain = SmolWebDomainAuthority.wrap(domain);
		return this.#cache.get(domain.domain) ?? null;
	}

	/**
	 * @param {string | SmolWebDomainAuthority} domain
	 * @param {SmolWebDnsRecord[]} records
	 */
	set(domain, records) {
		domain = SmolWebDomainAuthority.wrap(domain);
		this.#cache.set(domain.domain, records);
	}

	/**
	 * @param {string | SmolWebDomainAuthority} domain
	 */
	evict(domain) {
		domain = SmolWebDomainAuthority.wrap(domain);
		this.#cache.delete(domain.domain);
	}

	clear() {
		this.#cache.clear();
	}
}

export const DEFAULT_CACHE = new DnsCache();


/**
 * @param {string | SmolWebDomainAuthority} domain
 * @param {Object} [options]
 * @param {DnsResolver} [options.resolver]
 * @param {DnsCache} [options.cache]
 * @returns {Promise.<SmolWebDnsRecord[]>}
 */
export async function lookup(domain, options={}) {

	// init default options
	options = {
		resolver: DEFAULT_RESOLVER,
		cache: DEFAULT_CACHE,
		... options
	};

	domain = SmolWebDomainAuthority.wrap(domain);

	// cache lookup
	let records = options.cache.get(domain);
	if (records != null) {
		return records;
	}

	// cache miss, do the DNS lookup
	const packetBuf = await options.resolver.resolve(domain);

	// read the response
	const packet = dnsPacket.decode(new Buffer(packetBuf));

	// filter down to just the TXT record data
	const rawRecords = iterSync(packet.answers ?? [])
		.filter(a => a.name === domain.domain && a.type === 'TXT')
		// NOTE: decode() always returns an array of Buffers for TXT record data.
		// NOTE: dns-packet was originally written for Node.js, but polyfilled for browsers,
		//       so the types don't make much sense here in browser-land.
		//       So cast the nonexistent Buffer type to the actually-implemented-in-the-browser type: Uint8Array
		.map(a => /** @type {Uint8Array[]} */ a.data)
		.toArray();
	records = parseRecords(rawRecords);
	options.cache.set(domain, records);
	return records;
}


/**
 * @param {Uint8Array[][]} records
 * @returns {SmolWebDnsRecord[]}
 */
function parseRecords(records) {

	const prefix = new Uint8Array(buffers.fromString('smolweb '));

	/**
	 * @param {Uint8Array[]} record
	 * @returns {boolean}
	 */
	const isSmolWebRecord = record => {

		// only single-segment records supported for now
		if (record.length !== 1) {
			return false;
		}

		// match the prefix
		const segment = record[0];
		if (segment.length < prefix.length) {
			return false;
		}
		for (let i=0; i<prefix.length; i++) {
			if (prefix[i] !== segment[i]) {
				return false;
			}
		}

		// ok, I guess ...
		return true;
	};

	return records
		.filter(record => isSmolWebRecord(record))
		.map(record => SmolWebDnsRecord.decode(record))
		.filter(record => record != null);
}


export class SmolWebDnsRecord {

	/**
	 * @param {SmolWebRawAuthority} authority
	 * @returns {SmolWebDnsRecord}
	 */
	static fromRawAuthority(authority) {
		return new this(
			authority.hostIdnTag,
			authority.beaconAddr
		);
	}

	/**
	 * @param {(ArrayBuffer | Uint8Array)[]} segments
	 * @returns {SmolWebDnsRecord}
	 */
	static decode(segments) {

		// normalize the segments to Uint8Array, since we need to accept the ones created by dnsPacket anyway
		const arrays = segments
			.map(s => {
				if (s instanceof ArrayBuffer) {
					s = new Uint8Array(s);
				}
				return s;
			});

		// only one segment supported for now
		if (arrays.length !== 1) {
			return null;
		}
		const segment = arrays[0];

		// cast off the first layer of encoding to get the string data
		const decoder = new TextDecoder('utf-8');
		let /** @type {string} */ str;
		try {
			str = decoder.decode(segment);
		} catch (err) {
			console.warn("TXT record was not valid UTF-8", segment);
			return null;
		}

		// decode the string data
		const parts = str.split(' ');

		// check the prefix
		if (parts[0] !== 'smolweb') {
			return null;
		}

		// check the version
		if (parts[1] !== 'v1') {
			console.warn("TXT record had unexpected version", parts[1]);
			return null;
		}

		// decode the host identification tag
		const hostIdnTag = IdnTag.fromString(parts[2]);
		if (hostIdnTag == null) {
			console.warn("TXT record had invalid hostIdnTag", parts[2]);
			return null;
		}

		// decode the beacon address
		let /** @type {HostAddress} */ beaconAddr;
		try {
			beaconAddr = HostAddress.fromString(parts[3], DEFAULT_BEACON_PORT);
		} catch {
			console.warn("TXT record has invalid beacon address", parts[3]);
			return null;
		}

		// goddem!
		return new this(hostIdnTag, beaconAddr);
	}


	/** @type {IdnTag} */
	#hostIdnTag;

	/** @type {HostAddress} */
	#beaconAddr;


	/**
	 * @param {IdnTag} hostIdnTag
	 * @param {HostAddress} beaconAddr
	 */
	constructor(hostIdnTag, beaconAddr) {
		this.#hostIdnTag = hostIdnTag;
		this.#beaconAddr = beaconAddr;
	}


	/** @returns {IdnTag} */
	get hostIdnTag() {
		return this.#hostIdnTag;
	}

	/** @returns {HostAddress} */
	get beaconAddr() {
		return this.#beaconAddr;
	}


	/**
	 * @returns {ArrayBuffer[]}
	 */
	encode() {

		const hostIdnTagStr = this.#hostIdnTag.toString();
		const beaconAddrStr = makeBeaconAddr(this.#beaconAddr).toString(DEFAULT_BEACON_PORT);
		const encoded = `smolweb v1 ${hostIdnTagStr} ${beaconAddrStr}`;

		if (encoded.length > 255) {
			throw new Error("SmolWeb address is too long for single-record encoding");
			// TODO: implement a multi-record encoding?
			//   Experiments with NameCheap's DNS panel showed that it appeared to
			//   allow setting a TXT record longer than 255 bytes,
			//   but the record never appeared in DNS queries.
			//   Reloading the DNS panel no longer shows the TXT record at all.
			//   I can only assume this means NameCheap doesn't implement multi-segment TXT records at all,
			//   even though they're allowed by the authoritative spec: RFC 1035.
		}

		return [buffers.fromString(encoded)];
	}

	/**
	 * @returns {SmolWebRawAuthority}
	 */
	rawAuthority() {
		return new SmolWebRawAuthority(
			this.#hostIdnTag,
			this.#beaconAddr
		)
	}

	/**
	 * @param {SmolWebDnsRecord} other
	 * @returns {boolean}
	 */
	equals(other) {
		return other instanceof this.constructor
			&& this.#hostIdnTag.equals(other.#hostIdnTag)
			&& this.#beaconAddr.equals(other.#beaconAddr);
	}

	toString() {
		return `SmolWebDnsRecord[host=${this.#hostIdnTag}, beacon=${this.#beaconAddr}]`;
	}
}
