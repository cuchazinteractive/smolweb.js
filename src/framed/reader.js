
import { Promiser, orTimeout, TimeoutSignal } from 'food-runner/src/promises.js';

import { Deframer, LocalDeframerError, RemoteFrameError } from 'smolweb/src/framed/deframer.js';


/**
 * @typedef {string | ArrayBuffer} RTCMessage
 */

/**
 * @typedef {MessageEvent<string | Blob>} RTCMessageEvent
 */

/**
 * @typedef {function(RTCMessageEvent):(void | Promise.<void>)} RTCMessageListener
 */

/**
 * @typedef {Object} ChannelListener
 * @property {function(string, RTCMessageListener)} addEventListener
 * @property {function(string, RTCMessageListener)} removeEventListener
 */


/**
 * @typedef {Object} FrameEvent
 * @property {'start' | 'continue' | 'finish'} type
 * @property {FrameStartEvent} [start]
 * @property {FrameContinueEvent} [continue]
 * @property {FrameFinishEvent} [finish]
 */

export const FrameEvent = {
	START: 'start',
	CONTINUE: 'continue',
	FINISH: 'finish'
};

/**
 * @typedef {Object} FrameStartEvent
 * @property {BigInt} bytesTotal
 * @property {ArrayBuffer} payload
 */

/**
 * @typedef {Object} FrameContinueEvent
 * @property {BigInt} bytesRead
 * @property {ArrayBuffer} payload
 */

/**
 * @typedef {Object} FrameFinishEvent
 * @property {ArrayBuffer} payload
 */


export class FrameReader {

	/** @type {ChannelReceiver} */
	#receiver;

	/** @type {FrameReadStream} */
	#stream = null;


	/**
	 * @param {ChannelReceiver} receiver
	 */
	constructor(receiver) {
		this.#receiver = receiver;
	}


	/**
	 * Reads one frame.
	 * @params {Object} [options]
	 * @params {number} [options.timeoutMs] Timeout if it takes too long to receive the next frame.
	 * @returns {Promise.<FrameEvent | TimeoutSignal>}
	 */
	async read(options=undefined) {

		// set option defaults
		if (options === undefined) {
			options = {};
		}

		// wait for the next message
		const msg = await this.#receiver.recv({ timeoutMs: options.timeoutMs });
		if (msg === TimeoutSignal) {
			return TimeoutSignal;
		} else if (typeof msg == 'string') {
			throw new Error("string message was not a frame");
		}
		const frame = msg;

		// start a new stream if needed
		if (this.#stream == null) {
			const [stream, payload] = FrameReadStream.new(frame);
			if (!stream.isComplete) {
				this.#stream = stream;
			}
			return payload;
		}

		// otherwise, try to continue the existing stream
		const event = this.#stream.continue(frame);
		if (this.#stream.isComplete) {
			this.#stream = null;
		}
		return event;
	}

	/**
	 * Discards any stream that was started by previous calls to read()
	 */
	abandonStream() {
		this.#stream = null;
	}

	/**
	 * Reads all frames from a stream and reassembles the buffer.
	 * @params {Object} [options]
	 * @params {number} [options.timeoutNowMs] Timeout if it takes too long to receive the first frame
	 *                                         or if the time between any two consecutive frames is too long.
	 * @params {number} [options.timeoutAfterStartMs] Wait indefinitely for the first frame
	 *                                                and timeout if the time between any two consecutive frames is too long.
	 * @returns {Promise.<ArrayBuffer>}
	 * @throws {FrameReaderTimeoutError}
	 */
	async readAll(options=undefined) {

		// set option defaults
		if (options === undefined) {
			options = {
				timeoutNowMs: 30_000
			};
		}

		if (this.#stream != null) {
			throw new Error("already streaming");
		}

		try {

			// wait for the first event
			let event = await this.read({ timeoutMs: options.timeoutNowMs });
			if (event === TimeoutSignal) {
				throw new FrameReaderTimeoutError(FrameReaderTimeoutError.NOW, options.timeoutNowMs)
			}

			let /** @type {ArrayBuffer} */ buf;
			let bytesAppended = 0;

			/**
			 * @param {ArrayBuffer} payload
			 */
			const appendBuf = payload => {
				(new Uint8Array(buf)).set(new Uint8Array(payload), bytesAppended);
				bytesAppended += payload.byteLength;
			};

			// try to start the buffer
			switch (event.type) {

				case FrameEvent.START: {
					if (event.start.bytesTotal > BigInt(Number.MAX_SAFE_INTEGER)) {
						throw new Error(`Stream (${event.start.bytesTotal} bytes}) too large to be buffered, max of ${Number.MAX_SAFE_INTEGER}`);
					}
					buf = new ArrayBuffer(Number(event.start.bytesTotal));
					appendBuf(event.start.payload);
				} break;

				case FrameEvent.CONTINUE: throw new Error("unexpected continue event");

				// stream finished on the first frame
				case FrameEvent.FINISH: return event.finish.payload;
			}

			// keep streaming into the buffer
			while (true) {

				// wait for the next event
				const event = await this.read({ timeoutMs: options.timeoutNowMs ?? options.timeoutAfterStartMs });
				if (event === TimeoutSignal) {
					throw new FrameReaderTimeoutError(FrameReaderTimeoutError.AFTER_START, options.timeoutAfterStartMs);
				}

				switch (event.type) {

					case FrameEvent.START: throw new Error("unexpected start event");

					case FrameEvent.CONTINUE:
						appendBuf(event.continue.payload);
						break;

					case FrameEvent.FINISH:
						appendBuf(event.finish.payload);
						return buf;
				}
			}

		} finally {
			this.abandonStream();
		}
	}
}


export class FrameReaderTimeoutError extends Error {

	static NOW = 'now';
	static AFTER_START = 'afterStart';


	/** @type {string} */
	type;

	/** @type {number} */
	timeoutMs;


	/**
	 * @param {string} type
	 * @param {number} timeoutMs
	 */
	constructor(type, timeoutMs) {
		super(`Timed out ${type} after ${timeoutMs} ms`);
		this.type = type;
		this.timeoutMs = timeoutMs;
	}
}


class FrameReadStream {

	/** @type {Deframer} */
	#deframer;


	/**
	 * @param {ArrayBuffer} frame
	 * @returns {[FrameReadStream,FrameEvent]}
	 */
	static new(frame) {
		try {
			const [deframer, payload] = Deframer.new(frame);
			const stream = new this();
			stream.#deframer = deframer;

			if (deframer.isComplete) {
				// stream is complete, send a finish event
				return [stream, {
					type: FrameEvent.FINISH,
					finish: {
						payload
					}
				}];
			} else {
				// stream started, send a start event
				return [stream, {
					type: FrameEvent.START,
					start: {
						bytesTotal: deframer.bytesTotal,
						payload
					}
				}];
			}

		} catch (err) {
			if (err instanceof LocalDeframerError) {
				throw new FrameReaderError(err, new RemoteFrameError(err.frameError));
			} else if (err instanceof RemoteFrameError) {
				throw new FrameReaderError(err, null);
			}
		}
	}


	/**
	 * @param {ArrayBuffer} frame
	 * @returns {FrameEvent}
	 */
	async continue(frame) {
		try {
			const payload = this.#deframer.deframe(frame);

			if (this.#deframer.isComplete) {
				// stream is complete, send a finish frame
				return {
					type: FrameEvent.FINISH,
					finish: {
						payload
					}
				};
			} else {
				// stream still going, send a continue frame
				return {
					type: FrameEvent.CONTINUE,
					continue: {
						bytesRead: this.#deframer.bytesRead,
						payload
					}
				};
			}

		} catch (err) {
			if (err instanceof LocalDeframerError) {
				throw new FrameReaderError(err, new RemoteFrameError(err.frameError));
			} else if (err instanceof RemoteFrameError) {
				throw new FrameReaderError(err, null);
			}
		}
	}

	/** @returns {BigInt} */
	get bytesRead() {
		return this.#deframer.bytesRead;
	}

	/** @returns {boolean} */
	get isComplete() {
		return this.#deframer.isComplete;
	}
}


export class FrameReaderError extends Error {

	/** @type {LocalDeframerError | RemoteFrameError} */
	cause;

	/** @type {?RemoteFrameError} */
	response = null;


	/**
	 * @param {LocalDeframerError | RemoteFrameError} cause
	 * @param {?RemoteFrameError} response
	 */
	constructor(cause, response) {
		super(cause.message);
		this.cause = cause;
		this.response = response;
	}
}


export class ChannelReceiver {

	/** @type {ChannelListener} */
	#channel;

	#listener = weakBind(this.#onMessage, this);

	/** @type {RTCMessageEvent[]} */
	#events = [];

	/** @type {Promiser.<void>} */
	#pEvent = null;


	/**
	 * @param {ChannelListener} channel
	 */
	constructor(channel) {
		this.#channel = channel;

		// bind events
		this.#channel.addEventListener('message', this.#listener);
	}


	close() {
		this.#channel.removeEventListener('message', this.#listener);
	}

	/**
	 * @param {RTCMessageEvent} event
	 */
	#onMessage(event) {

		this.#events.push(event);

		// notify any listners of the new event
		const p = this.#pEvent;
		this.#pEvent = null;
		p?.resolve();
	}

	/**
	 * @params {Object} [options]
	 * @params {number} [options.timeoutMs] Timeout if it takes too long to receive the next frame.
	 * @returns {Promise.<RTCMessage | TimeoutSignal>}
	 */
	async recv(options=undefined) {

		// set option defaults
		if (options === undefined) {
			options = {};
		}

		while (true) {

			// try to get the next event
			const event = this.#events.shift();
			if (event != null) {
				if (typeof event.data === 'string') {
					return event.data;
				} else if (event.data instanceof Blob) {
					// NOTE: firefox seems to return Blobs
					return await event.data.arrayBuffer();
					// NOTE: chrome seems to return ArrayBuffers
				} else if (event.data instanceof ArrayBuffer) {
					return event.data;
				} else {
					throw new Error(`unrecognized RTC message data type: ${event.data.constructor?.name}`);
				}
			}

			// no events, wait for more
			if (this.#pEvent != null) {
				throw new Error("already receiving");
			}
			this.#pEvent = new Promiser();
			let p = this.#pEvent.promise();
			if (options.timeoutMs != null) {
				p = orTimeout(this.#pEvent.promise(), options.timeoutMs);
			}
			if (await p === TimeoutSignal) {
				this.#pEvent = null;
				return TimeoutSignal;
			}
		}
	}
}


/**
 * @param {function(...)} func
 * @param {any} target
 * @returns {function}
 */
function weakBind(func, target) {
	const weakTarget = new WeakRef(target);
	// noinspection JSUnusedAssignment (except by the garbage collector)
	target = undefined; // get rid of this function's reference to the target, so we only hold the WeakRef
	return (... args) => {
		const t = weakTarget.deref();
		if (t != null) {
			func.call(t, ... args)
		}
	}
}
