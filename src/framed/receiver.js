
import { TimeoutSignal } from 'food-runner/src/promises.js';

import { FrameReader, FrameReaderError } from 'smolweb/src/framed/reader.js';
import { FrameStreamer, FrameWriter } from 'smolweb/src/framed/writer.js';


/**
 * Reads frames from the underlying reader and automatically sends response errors
 * to the remote endpoint using the underlying writer.
 */
export class FrameReceiver {

	/**
	 * @param {RTCDataChannel} channel
	 * @param {ChannelReceiver} channelReceiver
	 * @returns {FrameReceiver}
	 */
	static fromDataChannel(channel, channelReceiver) {
		return new this(
			new FrameWriter(channel),
			new FrameReader(channelReceiver)
		);
	}


	/** @type {FrameWriter} */
	#writer;

	/** @type {FrameReader} */
	#reader;


	/**
	 * @param {FrameWriter} writer
	 * @param {FrameReader} reader
	 */
	constructor(writer, reader) {
		this.#writer = writer;
		this.#reader = reader;
	}


	/** @returns {FrameWriter} */
	get writer() {
		return this.#writer;
	}

	/** @returns {FrameReader} */
	get reader() {
		return this.#reader;
	}

	/**
	 * Writes the data as a stream of frames.
	 * @param {ArrayBuffer} data
	 */
	writeAll(data) {
		this.#writer.write(data);
	}

	/**
	 * @param {number | BigInt} size
	 * @returns {FrameStreamer}
	 */
	writeStream(size) {
		return this.#writer.stream(size);
	}

	/**
	 * @params {Object} [options]
	 * @params {number} [options.timeoutMs] Timeout if it takes too long to receive the next frame.
	 * @returns {Promise.<FrameEvent | TimeoutSignal>}
	 * @throws {LocalDeframerError, RemoteFrameError}
	 */
	async recv(options=undefined) {

		// set option defaults
		if (options === undefined) {
			options = {};
		}

		try {
			return await this.#reader.read({ timeoutMs: options.timeoutMs });
		} catch (err) {
			if (err instanceof FrameReaderError) {
				// consume the response and throw the inner error
				await this.#respondError(err.response);
				throw err.cause;
			} else {
				throw err;
			}
		}
	}

	/**
	 * Discards any stream that was started by previous calls to recv()
	 */
	abandonStream() {
		this.#reader.abandonStream();
	}

	/**
	 * @params {Object} [options]
	 * @params {number} [options.timeoutNowMs] Timeout if it takes too long to receive the first frame
	 *                                         or if the time between any two consecutive frames is too long.
	 * @params {number} [options.timeoutAfterStartMs] Wait indefinitely for the first frame
	 *                                                and timeout if the time between any two consecutive frames is too long.
	 * @returns {Promise.<ArrayBuffer>}
	 * @throws {LocalDeframerError, RemoteFrameError, FrameReaderTimeoutError}
	 */
	async recvAll(options=undefined) {

		// set option defaults
		if (options === undefined) {
			options = {
				timeoutNowMs: 30_000
			};
		}

		try {
			return await this.#reader.readAll({
				timeoutNowMs: options.timeoutNowMs,
				timeoutAfterStartMs: options.timeoutAfterStartMs
			});
		} catch (err) {
			if (err instanceof FrameReaderError) {
				// consume the response and throw the inner error
				await this.#respondError(err.response);
				throw err.cause;
			} else {
				throw err;
			}
		}
	}

	/**
	 * @param {RemoteFrameError} err
	 * @returns {Promise.<void>}
	 */
	async #respondError(err) {
		try {
			this.#writer.writeError(err.frameError);
		} catch (e) {
			console.warn("failed to send error reponse:", e.message);
		}
	}
}
