
import { Framer } from 'smolweb/src/framed/framer.js';
import * as sizes from 'smolweb/src/framed/sizes.js';
import { smolweb } from 'smolweb/src/protobuf/gen.js';


/**
 * @typedef {Object} ChannelSender
 * @property {function(data:ArrayBuffer)} send
 */


export class FrameWriter {

	/** @type {ChannelSender} */
	#channel;


	/**
	 * @param {ChannelSender} channel
	 */
	constructor(channel) {
		this.#channel = channel;
	}


	/**
	 * Writes all the data to the underlying channel as a stream of frames
	 * @param {ArrayBuffer} data
	 */
	write(data) {

		let bytesSent = 0;

		const stream = this.stream(data.byteLength);
		while (!stream.isClosed) {

			// make the next payload out of the data
			// TODO: get rid of this buffer copy?
			const payload = data.slice(bytesSent, Math.min(bytesSent + sizes.MAX_PAYLOAD_SIZE, data.byteLength));
			stream.write(payload);
			bytesSent += payload.byteLength;

			// TODO: write() isn't async, do we need rate limiting?
		}
	}

	/**
	 * Writes the error as an error frame
	 * @param {smolweb_framed_TFrameError | smolweb.framed.FrameError} err
	 */
	writeError(err) {
		let /** @type {smolweb.framed.FrameError} */ frameError;
		if (err instanceof smolweb.framed.FrameError) {
			frameError = err;
		} else {
			frameError = smolweb.framed.FrameError.new(err);
		}
		const frame = smolweb.framed.Frame.new({
			error: frameError
		}).toBuffer();
		this.#channel.send(frame);
	}

	/**
	 * @param {number | BigInt} size
	 * @returns {FrameStreamer}
	 */
	stream(size) {
		const framer = new Framer(size);
		return new FrameStreamer(framer, this.#channel);
	}
}


export class FrameStreamer {

	/** @type {Framer} */
	#framer;

	/** @type {ChannelSender} */
	#channel;


	/**
	 * @param {Framer} framer
	 * @param {ChannelSender} channel
	 */
	constructor(framer, channel) {
		this.#framer = framer;
		this.#channel = channel;
	}


	/**
	 * Writes the payload as one frame.
	 * @param {ArrayBuffer} payload
	 */
	write(payload) {
		const frame = this.#framer.frame(payload);
		this.#channel.send(frame);
	}

	/**
	 * Writes an error frame containing the abort error
	 */
	abort() {
		const frame = this.#framer.frameError({ abort: true });
		this.#channel.send(frame);
	}

	/** @returns {boolean} */
	get isClosed() {
		return this.#framer.isClosed;
	}
}
