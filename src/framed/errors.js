
import { smolweb } from 'smolweb/src/protobuf/gen.js';


smolweb.framed.FrameError.prototype.errMsg = function () {
	switch (this.error) {
		case 'decode':
			return `Error decoding frame: ${this.decode.message}`;
		case 'unexpectedKind':
			return `Unexpected frame kind ${this.unexpectedKind.observed}, expected one of ${this.unexpectedKind.expected}`;
		case 'unexpectedFrameIndex':
			return `Unexpected frame index ${this.unexpectedFrameIndex.observed}, expected ${this.unexpectedFrameIndex.expected}`;
		case 'invalidStreamSize':
			return `Invalid stream size: ${this.invalidStreamSize.observed}`;
		case 'unexpectedPayloadSize':
			return `Unexpected payload size ${this.unexpectedPayloadSize.observed}, expected ${this.unexpectedPayloadSize.expected}`;
		case 'closed':
			return `No more frames were expected`;
		case 'abort':
			return `The remote side has aborted the stream`;
		default:
			return `(unrecognized error)`;
	}
};
