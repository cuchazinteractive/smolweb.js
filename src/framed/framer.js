
import * as sizes from 'smolweb/src/framed/sizes.js';
import { smolweb } from 'smolweb/src/protobuf/gen.js';


export class Framer {

	/** @type {bigint} */
	#bytesTotal;

	/** @type {bigint} */
	#bytesWritten = 0n;

	/** @type {?bigint} */
	#nextFrame;


	/**
	 * @param {number | bigint} size
	 */
	constructor(size) {

		// validate the size
		if (size < 0) {
			throw new Error(`invalid stream size: ${size}`);
		} else if (size > sizes.MAX_STREAM_SIZE) {
			throw new Error(`invalid stream size: ${size}, max allowed: ${sizes.MAX_STREAM_SIZE}`);
		}
		this.#bytesTotal = BigInt(size);

		// determine the frame sequence
		const framesTotal = sizes.divCeil(size, sizes.MAX_PAYLOAD_SIZE);
		if (framesTotal >= 1) {
			this.#nextFrame = 0n;
		}
	}

	/**
	 * @param {ArrayBuffer} payload
	 * @return {ArrayBuffer} the frame
	 */
	frame(payload) {

		// check the payload size
		const bytesRemaining = this.#bytesTotal - this.#bytesWritten;
		if (bytesRemaining < 0) {
			throw new Error("Too many bytes written");
		}
		let bytesExpected;
		if (bytesRemaining < BigInt(sizes.MAX_PAYLOAD_SIZE)) {
			bytesExpected = Number(bytesRemaining);
		} else {
			bytesExpected = sizes.MAX_PAYLOAD_SIZE;
		}
		if (payload.byteLength !== bytesExpected) {
			throw new Error(`Unexpected payload size: got ${payload.byteLength} but expected ${bytesExpected}`);
		}

		// write the frame
		let /** @type {ArrayBuffer} */ frame;
		if (this.#nextFrame === null) {
			throw new Error("stream closed");
		} else if (this.#nextFrame === 0n) {
			// write a start frame
			frame = smolweb.framed.Frame.new({
				start: smolweb.framed.FrameStart.new({
					streamSize: this.#bytesTotal,
					payload
				})
			}).toBuffer();
		} else {
			// write a continue frame
			frame = smolweb.framed.Frame.new({
				continue: smolweb.framed.FrameContinue.new({
					frameIndex: this.#nextFrame,
					payload
				})
			}).toBuffer();
		}

		// update state
		this.#bytesWritten += BigInt(payload.byteLength);
		if (this.#bytesWritten < this.#bytesTotal) {
			this.#nextFrame += 1n;
		} else {
			this.#nextFrame = null;
		}

		return frame;
	}

	/**
	 * @param {smolweb_framed_TFrameError} err
	 * @returns {ArrayBuffer} the frame
	 */
	frameError(err) {

		// if the stream is already closed, no need to send the error
		// the remote side isn't listening anyway
		if (this.#nextFrame == null) {
			throw new Error("Framer is closed");
		}

		return smolweb.framed.Frame.new({
			error: smolweb.framed.FrameError.new(err)
		}).toBuffer();
	}

	/** @returns {bigint} */
	get bytesTotal() {
		return this.#bytesTotal;
	}

	/** @returns {bigint} */
	get bytesWritten() {
		return this.#bytesWritten;
	}

	/** @returns {boolean} */
	get isClosed() {
		return this.#nextFrame == null;
	}
}
