
import * as sizes from 'smolweb/src/framed/sizes.js';
import { smolweb } from 'smolweb/src/protobuf/gen.js';
import 'smolweb/src/framed/errors.js';


export class Deframer {

	/** @type {?bigint} */
	#nextFrame;

	/** @type {bigint} */
	#bytesTotal;

	/** @type {bigint} */
	#bytesRead;


	/**
	 * @param {ArrayBuffer} frame
	 * @returns {[Deframer,ArrayBuffer]} the deframer and the first payload
	 * @throws {LocalDeframerError, RemoteFrameError}
	 */
	static new(frame) {

		// because javascript is a completely insane language ...
		if (frame == null) {
			throw new LocalDeframerError({
				decode: smolweb.framed.FrameError.Decode.new({
					message: 'no frame'
				})
			});
		}

		// deserialize the message, we should get a frame
		let /** @type {smolweb.framed.Frame} */ frameMsg;
		try {
			frameMsg = smolweb.framed.Frame.fromBuffer(frame);
		} catch (err) {
			throw new LocalDeframerError({
				decode: smolweb.framed.FrameError.Decode.new({
					message: err.toString()
				})
			});
		}
		if (frameMsg.kind == null) {
			throw new LocalDeframerError({
				decode: smolweb.framed.FrameError.Decode.new({
					message: "missing field: kind"
				})
			});
		}

		// expect a start frame, or an error
		switch (frameMsg.kind) {

			case 'start':
				break; // all is well

			case 'error':
				throw new RemoteFrameError(frameMsg.error);

			default:
				throw new LocalDeframerError({
					unexpectedKind: smolweb.framed.FrameError.UnexpectedKind.new({
						expected: ['start', 'error'],
						observed: frameMsg.kind
					})
				});
		}
		const startFrame = frameMsg.start;

		// validate
		if (startFrame.streamSize <= 0) {
			throw new LocalDeframerError({
				invalidStreamSize: smolweb.framed.FrameError.InvalidStreamSize.new({
					observed: startFrame.streamSize
				})
			});
		}

		let /** @type {number} */ nextPayloadSize;
		if (startFrame.streamSize < BigInt(sizes.MAX_PAYLOAD_SIZE)) {
			nextPayloadSize = Number(startFrame.streamSize);
		} else {
			nextPayloadSize = sizes.MAX_PAYLOAD_SIZE;
		}
		if (startFrame.payload.byteLength !== nextPayloadSize) {
			throw new LocalDeframerError({
				unexpectedPayloadSize: smolweb.framed.FrameError.UnexpectedPayloadSize.new({
					expected: nextPayloadSize,
					observed: startFrame.payload.byteLength
				})
			});
		}

		// count how many frames there should be
		const numFrames = sizes.divCeil(startFrame.streamSize, BigInt(sizes.MAX_PAYLOAD_SIZE));

		const deframer = new this();
		deframer.#nextFrame = null;
		deframer.#bytesTotal = startFrame.streamSize;
		deframer.#bytesRead = BigInt(startFrame.payload.byteLength);

		if (numFrames > 1n) {
			deframer.#nextFrame = 1n;
		}

		return [deframer, startFrame.payload];
	}


	/** @type {bigint} */
	get bytesTotal() {
		return this.#bytesTotal;
	}

	/** @type {bigint} */
	get bytesRead() {
		return this.#bytesRead;
	}

	/** @type {boolean} */
	get isClosed() {
		return this.#nextFrame === null;
	}

	/** @type {boolean} */
	get isComplete() {
		return this.#bytesRead >= this.#bytesTotal;
	}

	/**
	 * @param {ArrayBuffer} frame
	 * @returns {ArrayBuffer} the payload
	 */
	deframe(frame) {

		// are we even expecting another frame?
		if (this.#nextFrame === null) {
			throw new LocalDeframerError({
				closed: true
			});
		}

		// yup, what should the size of the next frame be?
		const bytesRemaining = this.#bytesTotal - this.#bytesRead;
		if (bytesRemaining < 0) {
			throw new Error("read too many bytes");
		}
		let /** @type {number} */ nextPayloadSize;
		if (bytesRemaining < BigInt(sizes.MAX_PAYLOAD_SIZE)) {
			nextPayloadSize = Number(bytesRemaining);
		} else {
			nextPayloadSize = sizes.MAX_PAYLOAD_SIZE;
		}

		// deserialize the message, we should get a frame
		let /** @type {smolweb.framed.Frame} */ frameMsg;
		try {
			frameMsg = smolweb.framed.Frame.fromBuffer(frame);
		} catch (err) {
			throw new LocalDeframerError({
				decode: smolweb.framed.FrameError.Decode.new({
					message: err.toString()
				})
			});
		}

		// expect a start frame, or an error
		switch (frameMsg.kind) {

			case 'continue':
				break; // all is well

			case 'error':
				// close the stream
				this.#nextFrame = null;
				throw new RemoteFrameError(frameMsg.error);

			default:
				throw new LocalDeframerError({
					unexpectedKind: smolweb.framed.FrameError.UnexpectedKind.new({
						expected: ['continue', 'error'],
						observed: frameMsg.kind
					})
				});
		}
		const continueFrame = frameMsg.continue;

		// validate
		if (continueFrame.frameIndex !== this.#nextFrame) {
			throw new LocalDeframerError({
				unexpectedFrameIndex: smolweb.framed.FrameError.UnexpectedFrameIndex.new({
					expected: this.#nextFrame,
					observed: continueFrame.frameIndex
				})
			});
		}
		if (continueFrame.payload.byteLength !== nextPayloadSize) {
			throw new LocalDeframerError({
				unexpectedPayloadSize: smolweb.framed.FrameError.UnexpectedPayloadSize.new({
					expected: nextPayloadSize,
					observed: continueFrame.payload.byteLength
				})
			});
		}

		// consume the payload
		this.#bytesRead += BigInt(continueFrame.payload.byteLength);

		// should we expect a next frame?
		if (this.#bytesRead < this.#bytesTotal) {
			this.#nextFrame += 1n;
		} else {
			this.#nextFrame = null;
		}

		return continueFrame.payload;
	}
}


export class LocalDeframerError extends Error {

	/** @type {smolweb.framed.FrameError} */
	frameError;

	/**
	 * @param {smolweb_framed_TFrameError} err
	 */
	constructor(err) {
		const frameError = smolweb.framed.FrameError.new(err);
		super(frameError.errMsg());
		this.frameError = frameError;
	}
}


export class RemoteFrameError extends Error {

	/** @type {smolweb.framed.FrameError} */
	frameError;

	/**
	 * @param {smolweb.framed.FrameError} err
	 */
	constructor(err) {
		super(err.errMsg());
		this.frameError = err;
	}
}
