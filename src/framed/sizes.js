
/**
 * MDN says we should stay under 16 KiB messages:
 * https://developer.mozilla.org/en-US/docs/Web/API/WebRTC_API/Using_data_channels#understanding_message_size_limits
 *
 * @type {number}
 */
export const MAX_MESSAGE_SIZE = 16*1024;


/**
 * The maximum size of the portion of a frame not used for the payload.
 * Since protobufs use variable encodings, that complicates size analysis a bit.
 * This number shows the maximum size that could be taken up by the non-payload portion of the frame.
 * It's up to 0.1% overhead for a full frame. Not bad.
 *
 * @type {number}
 */
export const MAX_OVERHEAD_SIZE = 16;
// submessage(len<=16k)=1+2 + record(int64)=1+9 + record(bytes, len <=16k)=1+2
// assuming message field numbers are < 16
// See Protobuf3 encoding rules: https://protobuf.dev/programming-guides/encoding/


/**
 * The max payload size in a frame, 16368 bytes
 *
 * @type {number}
 */
export const MAX_PAYLOAD_SIZE = MAX_MESSAGE_SIZE - MAX_OVERHEAD_SIZE;


/**
 * The maximum size allowed by a signed 64-bit integer.
 *
 * @type {bigint}
 */
export const MAX_STREAM_SIZE = 9223372036854775807n;


/**
 * Integer division, but rounded towards positive infinity
 * @param {bigint | number} n the numerator
 * @param {bigint | number} d the denominator
 * @returns {bigint}
 */
export function divCeil(n, d) {
	n = BigInt(n);
	d = BigInt(d);
	const div = n/d;
	const rem = n%d;
	if (rem > 0n && d > 0n) {
		return div + 1n;
	} else {
		return div;
	}
}
