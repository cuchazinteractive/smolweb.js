

export const DEFAULT_STUN_PORT = 3478;
export const DEFAULT_BEACON_PORT = 443;


/**
 * @typedef {Object} HostAddressInit
 * @property {string} hostname
 * @property {number} [port]
 */


export class HostAddress {

	/**
	 * @param {HostAddressInit} init
	 * @param {number} defaultPort
	 * @returns {HostAddress}
	 */
	static from(init, defaultPort) {
		return new this(
			init.hostname,
			init.port ?? defaultPort
		);
	}


	/**
	 * @param {string} str
	 * @param {number} defaultPort
	 * @returns {HostAddressInit}
	 */
	static initFromString(str, defaultPort) {

		const parts = str.split(':');

		const init = {
			hostname: parts[0]
		};

		switch (parts.length) {

			// hostname only, already done
			case 1: break;

			// hostname and port, read the port
			case 2: {
				const port = parsePort(parts[1]);
				if (port === undefined) {
					throw new Error(`invalid port: ${parts[1]}`);
				}
				if (port !== defaultPort) {
					init.port = port;
				}
			} break;

			default: throw new Error(`failed to parse address: ${str}`);
		}

		return init;
	}


	/**
	 * @param {string} str
	 * @param {number} defaultPort
	 * @returns {HostAddress}
	 */
	static fromString(str, defaultPort) {
		return this.from(this.initFromString(str, defaultPort), defaultPort);
	}


	/** @type {string} */
	hostname;

	/** @type {number} */
	port;


	/**
	 * @param {string} hostname
	 * @param {number} port
	 */
	constructor(hostname, port) {
		this.hostname = hostname;
		this.port = port;
	}


	/**
	 * @param {HostAddress} other
	 * @returns {boolean}
	 */
	equals(other) {
		return this.hostname === other.hostname
			&& this.port === other.port;
	}

	/**
	 * @returns {HostAddress}
	 */
	copy() {
		return new this.constructor(
			this.hostname,
			this.port
		);
	}

	/**
	 * @param {?number} [defaultPort]
	 * @returns {string}
	 */
	toString(defaultPort=undefined) {
		if (this.port !== defaultPort) {
			return `${this.hostname}:${this.port}`;
		} else {
			return this.hostname;
		}
	}
}


/**
 * @param {HostAddressInit} init
 */
export function makeBeaconAddr(init) {
	return HostAddress.from(init, DEFAULT_BEACON_PORT);
}


/**
 * @param {HostAddress} beaconAddr
 * @param {?HostAddressInit | undefined} init
 * @returns {HostAddress}
 */
export function makeStunAddr(beaconAddr, init) {
	if (init != null) {
		return HostAddress.from(init, DEFAULT_STUN_PORT);
	} else {
		return new HostAddress(beaconAddr.hostname, DEFAULT_STUN_PORT);
	}
}


/**
 * @param {string} str
 * @returns {number | undefined}
 */
export function parsePort(str) {

	// each character must be a decimal digit
	for (let i=0; i<str.length; i++) {
		switch (str.charAt(i)) {

			case '0':
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
				break; // ok

			default:
				return undefined;
		}
	}

	// now that we know there's no weirdness going on, call the usual parseInt()
	const result = parseInt(str);
	if (Number.isNaN(result)) {
		return undefined;
	}

	return result;
}
