
import { Promiser } from 'food-runner/src/promises.js';
import * as errors from 'food-runner/src/errors.js';

import * as buffers from 'food-runner/src/buffers.js';
import 'burgerid';
import { Identity } from 'burgerid/src/identity.js';

import { HostAddress, makeStunAddr } from 'smolweb/src/addresses.js';
import * as comms from 'smolweb/src/comms.js';
import { smolweb } from 'smolweb/src/protobuf/gen.js';
import { SmolWebRawAuthority } from 'smolweb/src/proto.js';
import { GuestConnectionRace } from 'smolweb/src/webrtc/connectionRace.js';


/**
 * @typedef {Object} SmolWebClientOptions
 * @property {HostAddressInit} [stunAddr]
 * @property {number} [timeoutIceGatheringMs]
 * @property {number} [timeoutChannelOpenMs]
 * @property {SmolWebClientEvents} [events]
 */

/**
 * @typedef {Object} SmolWebClientEvents
 * @property {function(HostAddress)} [onStunAddr]
 * @property {function()} [onConnecting]
 * @property {function(string)} [onOffer1]
 * @property {function(BeaconResponseInfo)} [onBeaconResponse]
 * @property {function(string)} [onAnswer1]
 * @property {function(string)} [onOffer2]
 * @property {function(string)} [onAnswer2]
 * @property {function(BeaconResponseInfo)} [onBeaconConfirm]
 * @property {function(ClientConnectedInfo)} [onConnected]
 */

/**
 * @typedef {Object} BeaconResponseInfo
 * @property {number} status
 * @property {?string} contentType
 */

/**
 * @typedef {Object} ClientConnectedInfo
 * @property {string} winningName
 */


export class SmolWebClient {

    /** @type {SmolWebRawAuthority} */
    #authority;

    /** @type {SmolWebClientOptions} */
    #options;

    /** @type {HostAddress} */
    #stunAddr;

    /** @type {GuestConnectionRace} */
    #connectionRace;

    /** @type {?OpenDataChannel} */
    #dataChannel = null;

    /** @type {OpenDataChannel[]} */
    #availableChannels = [];

    /** @type {Promiser[]} */
    #channelWaiters = [];


    /**
     * @param {SmolWebRawAuthority} authority
     * @param {SmolWebClientOptions} [options]
     */
    constructor(authority, options={}) {

        // just in case ...
        if (!(authority instanceof SmolWebRawAuthority)) {
            throw new Error(`wrong SmolWeb authority type: ${authority?.constructor?.name}`);
        }

        this.#authority = authority;
        this.#stunAddr = makeStunAddr(authority.beaconAddr, options.stunAddr);
        this.#options = options;

        // send events
        this.#options.events?.onStunAddr?.(this.#stunAddr);

        // create the WebRTC connection race
        const /** @type {SmolwebConnectionOptions} */ config = {
            stunAddr: this.#stunAddr
        };
        this.#connectionRace = new GuestConnectionRace(config, config);
    }


    /** @returns {SmolWebRawAuthority} */
    get authority() {
        return this.#authority;
    }

    /** @returns {HostAddress} */
    get stunAddr() {
        return this.#stunAddr;
    }

    /**
     * @param {string} path
     * @returns {string}
     */
    #guestsUrl(path) {
        return `https://${this.#authority.beaconAddr}/guests/${path}`
    }

    /**
     * @returns {Promise.<Identity>}
     */
    async #getHostId() {
        
        // build the request
        const request = smolweb.guests.HostIdentityRequest.new({
            hostUid: this.#authority.hostIdnTag.uid
        });
        
        const response = await comms.post(this.#guestsUrl('host_id'), request);
        
        // parse the response
        const responseMsg = await response.proto(smolweb.guests.HostIdentityResponse.opener());
        switch (responseMsg.response) {
            case "hostUnknown": throw new Error("host unknown");
            case "identity": {

                const hostId = await Identity.open(responseMsg.identity);

                // check this is the correct identity
                if (!hostId.matchesIdnTag(this.#authority.hostIdnTag)) {
                    throw new Error("invalid host id returned from beacon server");
                }

                return hostId;
            }
        }
    }

    /**
     * @param {Identity} guestId
     * @param {Identity} hostId
     * @param {smolweb.secrets.ConnectionRequestSecret} requestSecret
     * @returns {Promise.<smolweb.secrets.ConnectionAcceptSecret>}
     */
    async #connect(guestId, hostId, requestSecret) {

        // encrypt the request secret
        const requestPlaintext = requestSecret.toBuffer();
        const requestContext = buffers.fromString("smolweb/guests/connection/request");
        const requestSecretMsg = await guestId.encrypt(requestPlaintext, [hostId], requestContext);

        // build the request
        const request = smolweb.guests.ConnectionRequest.new({
            hostUid: this.#authority.hostIdnTag.uid,
            identity: guestId.save().toBuffer(),
            secret: requestSecretMsg
        });

        const response = await comms.post(this.#guestsUrl('connect'), request);

        this.#options.events?.onBeaconResponse?.({
            status: response.status(),
            contentType: response.contentType()
        });

        // parse the response
        const responseMsg = await response.proto(smolweb.guests.ConnectionResponse.opener());
        switch (responseMsg.response) {
            case "hostUnknown": throw new Error("host unknown");
            case "hostUnreachable": throw new Error("host unreachable");
            case "accept": {

                // decrypt the secret
                const responseContext = buffers.fromString("smolweb/guests/connection/response");
                const responseSecretMsg = await guestId.decrypt(responseMsg.accept.secret, hostId, responseContext);
                return smolweb.secrets.ConnectionAcceptSecret.fromBuffer(responseSecretMsg);
            }
        }
    }

    /**
     * @param {Identity} guestId
     * @param {Identity} hostId
     * @param {smolweb.secrets.ConnectionConfirmSecret} confirmSecret
     * @returns {Promise.<void>}
     */
    async #confirm(guestId, hostId, confirmSecret) {

        // encrypt the confirm secret
        const plaintext = confirmSecret.toBuffer();
        const context = buffers.fromString("smolweb/guests/connection/confirm");
        const secretMsg = await guestId.encrypt(plaintext, [hostId], context);

        // build the request
        const request = smolweb.guests.ConnectionConfirm.new({
            hostUid: this.#authority.hostIdnTag.uid,
            guestUid: guestId.uid(),
            secret: secretMsg
        });

        const response = await comms.post(this.#guestsUrl('confirm'), request);

        this.#options.events?.onBeaconConfirm?.({
            status: response.status(),
            contentType: response.contentType()
        });
    }

    /**
     * Returns if the connection was successful.
     * Otherwise, closes then connection and then throws an error.
     * @returns {Promise.<void>}
     */
    async connect() {

        if (this.#connectionRace === null) {
            throw new Error("Connection is closed, create a new one rather than trying to re-open a closed one");
        }
        if (this.#dataChannel != null) {
            throw new Error("Connection already open");
        }

        try {

            this.#options.events?.onConnecting?.();

            // start making the WebRTC offer
            const pOffer1 = this.#connectionRace.offer();

            // get the host identity from the beacon server
            const hostId = await this.#getHostId();

            // wait for the offer to finish
            // WARNING: the session description contains cryptographic secrets for transit encryption, treat it with care!
            const offer1 = await pOffer1;
            this.#options.events?.onOffer1?.(offer1);

            // generate an ephemeral identity to use for the connection
            const [guestId, guestKey] = await Identity.newEphemeral();
            await guestId.unlock(guestKey);

            // send the connect message, wait for the response
            const connectSecret = smolweb.secrets.ConnectionRequestSecret.new({
                offer1
            });
            const acceptSecret = await this.#connect(guestId, hostId, connectSecret);
            this.#options.events?.onAnswer1?.(acceptSecret.answer1);
            this.#options.events?.onOffer2?.(acceptSecret.offer2);

            const answer2 = await this.#connectionRace.connect(acceptSecret.answer1, acceptSecret.offer2);
            this.#options.events?.onAnswer2?.(answer2);

            // send the confirm message
            const confirmSecret = smolweb.secrets.ConnectionConfirmSecret.new({
                connectionId: acceptSecret.connectionId,
                answer2
            });
            await this.#confirm(guestId, hostId, confirmSecret);

            // wait for the connection to open
            this.#dataChannel = await this.#connectionRace.open();
            if (this.#dataChannel == null) {
                // noinspection ExceptionCaughtLocallyJS (this is a stupid warning)
                throw new Error("Timed out trying to connect");
            }

            this.#options.events?.onConnected?.({
                winningName: this.#connectionRace.winningName
            });

            // connected! the data channel is now available
            this.#availableChannels.push(this.#dataChannel);

        } catch (err) {
            this.close();
            throw errors.wrap(new Error("Failed to connect"), err);
        }
    }

    /** @returns {boolean} */
    get isConnected() {
        return this.#dataChannel != null;
    }

    /**
     * @returns {Promise.<OpenDataChannel>}
     */
    async reserveChannel() {

        while (true) {

            // try to check out a channel
            const dataChannel = this.#availableChannels.shift();
            if (dataChannel != null) {
                return dataChannel;
            }

            // otherwise, wait for one to be available
            const p = new Promiser();
            this.#channelWaiters.push(p);
            await p.promise();
        }
    }

    /**
     * @param {OpenDataChannel} dataChannel
     */
    returnChannel(dataChannel) {

        if (dataChannel !== this.#dataChannel) {
            throw new Error("invalid data channel");
        }

        // return it to the pool
        this.#availableChannels.push(dataChannel);

        // notify the next listener, if any, of the newly-available channel
        this.#channelWaiters.shift()?.resolve();
    }

    /**
     * @returns {boolean}
     **/
    anyChannelsReserved() {
        return this.#availableChannels.length < 1;
    }

    close() {

        // close the data channel and the connection
        this.#dataChannel = null;
        this.#connectionRace.close();
        this.#connectionRace = null;

        // and kick out anyone waiting for a data channel
        const waiters = this.#channelWaiters.splice(0, this.#channelWaiters.length);
        for (const waiter of waiters) {
            waiter.reject(new Error("connection closed"));
        }
    }
}
