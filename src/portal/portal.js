
/*!
 * SmolWeb JS Portal
 * by Jeff Martin
 * License: AGPL-3
 */


import { mount } from 'redom';

import { PortalNavigator } from 'smolweb/src/portal/navigator.js';
import { PortalFrame } from 'smolweb/src/portal/frame.js';
import { PortalBar } from 'smolweb/src/portal/bar.js';


async function main() {

	// make the portal bar
	const bar = new PortalBar();
	mount(document.body, bar);

	// create the iframe to hold the sub-document
	const frame = new PortalFrame();
	mount(document.body, frame);

	// load the current url, if any
	const portalNavigator = new PortalNavigator(bar, frame);
	await portalNavigator.init();
}

(async () => {
	await main();
})();
