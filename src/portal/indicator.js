
import { text, html, setChildren, mount, unmount } from 'redom';

import * as arrays from 'food-runner/src/arrays.js';

import { parseSessionDescription } from 'smolweb/src/sdp.js';
import { Popover } from 'smolweb/src/portal/popover.js';


const DISCLOSURE_CLOSED = "➙";
const DISCLOSURE_OPEN = "➘";
const MAX_NUM_REQUESTS = 50;


export class PortalIndicator extends HTMLElement {

	/** @type {HTMLElement} */
	#content = html('div', {
		class: 'content',
		title: "Status and Logs"
	});

	/** @type {PortalRequest[]} */
	#requests = [];

	/** @type {?PortalLog} */
	#log = null;


	constructor() {
		super();

		this.setAttribute("id", "indicator");

		// wire up events
		this.#content.addEventListener("click", this.#toggleLog.bind(this));

		setChildren(this, [
			this.#content
		]);
		this.update();
	}


	update() {

		this.#content.classList.remove('error', 'loading', 'ok');

		// check for errors
		if (arrays.any(this.#requests, r => r.error != null)) {
			this.#content.classList.add('error');
			setChildren(this.#content, [
				text("Error!")
			]);
			return;
		}

		// check for loading requests
		if (arrays.any(this.#requests, r => r.httpStatus == null)) {
			this.#content.classList.add('loading');
			setChildren(this.#content, [
				text("...")
			]);
			return;
		}

		// check if everything's ok
		if (arrays.all(this.#requests, r => r.httpStatus === 200)) {
			this.#content.classList.add('ok');
			setChildren(this.#content, [
				text("Ok")
			]);
			return;
		}

		// nothing to show
		setChildren(this.#content, []);
	}

	/**
	 * @param {string} url
	 * @returns {PortalRequest}
	 */
	request(url) {
		const request = new PortalRequest(this, url);
		this.#requests.push(request);
		this.#log?.add(request);
		this.#cleanOldRequests();
		this.update();
		return request;
	}

	#cleanOldRequests() {
		const numToRemove = this.#requests.length - MAX_NUM_REQUESTS;
		if (numToRemove <= 0) {
			return;
		}
		for (const request of this.#requests.splice(0, numToRemove)) {
			this.#log?.remove(request);
		}
	}

	#showLog() {
		this.#log = /** @type {PortalLog} */ html(
			'smolweb-portal-log',
			{
				'popover-target': this.id,
				'popover-location': 'lower-left'
			}
		);
		mount(this, this.#log);
		this.#log.init(this.#requests);
	}

	#hideLog() {
		unmount(this, this.#log);
		this.#log = null;
	}

	#toggleLog() {
		if (this.#log == null) {
			this.#showLog();
		} else {
			this.#hideLog();
		}
	}
}

customElements.define('smolweb-portal-indicator', PortalIndicator);


export class PortalRequest {

	/** @type {PortalIndicator} */
	#indicator;

	/** @type {string} */
	#url;

	/**
	 * @type {HTMLElement}
	 * @private
	 */
	_elem = html('div', {
		class: 'request'
	});

	/** @type {HTMLElement} */
	#iconElem = html('span', DISCLOSURE_CLOSED, {
		class: 'icon'
	});

	#httpStatusElem = html('span', "...", {
		class: 'http-status chip'
	});

	/** @type {HTMLElement} */
	#detailElem = html('div', {
		class: 'details',
		style: {
			display: 'none'
		}
	});

	/** @type {?(SmolWebDnsRecord[])} */
	#dnsRecords = null;

	/** @type {?HostAddress} */
	#stunAddr = null;

	/** @type {boolean} */
	#connecting = false;

	/** @type {?(ParsedSessionDescription | string)} */
	#offer1 = null;

	/** @type {?BeaconResponseInfo} */
	#connectResponse = null;

	/** @type {?(ParsedSessionDescription | string)} */
	#answer1 = null;

	/** @type {?(ParsedSessionDescription | string)} */
	#offer2 = null;

	/** @type {?(ParsedSessionDescription | string)} */
	#answer2 = null;

	/** @type {?BeaconResponseInfo} */
	#connectConfirm = null;

	/** @type {?ClientConnectedInfo} */
	#connected = null;

	/** @type {?Error} */
	#error = null;

	/** @type {?number} */
	#httpStatus = null;


	/**
	 * @param {PortalIndicator} indicator
	 * @param {string} url
	 */
	constructor(indicator, url) {
		this.#indicator = indicator;
		this.#url = url;

		const summaryElem = html('div', {
			class: 'summary'
		}, [
			this.#iconElem,
			this.#httpStatusElem,
			html('span', { class: 'url' }, this.#url)
		]);

		setChildren(this._elem, [
			summaryElem,
			this.#detailElem
		]);

		this.#updateDetails();

		// wire up events
		summaryElem.addEventListener('click', () => {
			if (this.#detailElem.style.display !== 'none') {
				this.#iconElem.textContent = DISCLOSURE_CLOSED;
				this.#detailElem.style.display = 'none';
			} else {
				this.#iconElem.textContent = DISCLOSURE_OPEN;
				this.#detailElem.style.display = null;
			}
		});
	}


	/** @returns {string} */
	get url() {
		return this.#url;
	}

	/**
	 * @param {SmolWebDnsRecord[]} records
	 */
	setDnsRecords(records) {
		this.#dnsRecords = records;
		this.#updateDetails();
	}

	/**
	 * @returns {SmolWebClientEvents}
	 */
	get events() {

		// NOTE: don't throw exceptions in event handlers, or we'll break the connection for dumb reasons
		const update = () => {
			try {
				this.#updateDetails();
			} catch (err) {
				console.warn("Failed to update request log", err);
			}
		}

		/**
		 * @param {string} sd
		 * @returns {ParsedSessionDescription | string}
		 */
		const tryParseSd = (sd) => {
			try {
				return parseSessionDescription(sd);
			} catch (err) {
				console.warn("Failed to parse session description", err);
				return sd;
			}
		};

		return {

			onStunAddr: (/*HostAddress*/ addr) => {
				this.#stunAddr = addr;
				update();
			},

			onConnecting: () => {
				this.#connecting = true;
				update();
			},

			onOffer1: (/*string*/ sd) => {
				this.#offer1 = tryParseSd(sd);
				update();
			},

			onBeaconResponse: (/*BeaconResponseInfo*/ response) => {
				this.#connectResponse = response;
				update();
			},

			onAnswer1: (/*string*/ sd) => {
				this.#answer1 = tryParseSd(sd);
				update();
			},

			onOffer2: (/*string*/ sd) => {
				this.#offer2 = tryParseSd(sd);
				update();
			},

			onAnswer2: (/*string*/ sd) => {
				this.#answer2 = tryParseSd(sd);
				update();
			},

			onBeaconConfirm: (/*BeaconResponseInfo*/ confirm) => {
				this.#connectConfirm = confirm;
				update();
			},

			onConnected: (/*ClientConnectedInfo*/ info) => {
				this.#connected = info;
				update();
			}
		};
	}

	/**
	 * @param {number} status
	 */
	setHttpStatus(status) {

		this.#httpStatus = status;

		setChildren(this.#httpStatusElem, [
			text(`${status}`)
		]);

		if (status >= 200 && status < 300) {
			this.#httpStatusElem.classList.add("ok");
		} else if (status >= 400 && status < 600) {
			this.#httpStatusElem.classList.add("error");
		} else {
			this.#httpStatusElem.classList.add("other");
		}

		this.#indicator.update();
	}

	/** @returns {?number} */
	get httpStatus() {
		return this.#httpStatus;
	}

	/**
	 * @param {Error} err
	 */
	setError(err) {

		this.#error = err;
		this.#updateDetails();

		setChildren(this.#httpStatusElem, [
			text(`Error`)
		]);
		this.#httpStatusElem.classList.add("error");

		this.#indicator.update();
	}

	/** @return {?Error} */
	get error() {
		return this.#error;
	}

	#updateDetails() {
		setChildren(this.#detailElem, [

			// show the records from DNS lookup, if possible
			html('div', [
				text("DNS Records: "),
				(() => {
					if (this.#dnsRecords == null) {
						return text("(no DNS lookup performed)");
					} else if (this.#dnsRecords.length <= 0) {
						return text("(DNS lookup returned no results)");
					} else {
						return html('ol',
							this.#dnsRecords
								.map(r => html('li', r.toString()))
						);
					}
				})()
			]),

			// show client connection, if any
			(/** @returns {HTMLElement[]} */ () => {

				const hasAnyEvent =
					this.#stunAddr != null
					|| this.#connecting
					|| this.#offer1 != null
					|| this.#connectResponse != null
					|| this.#answer1 != null
					|| this.#offer2 != null
					|| this.#answer2 != null
					|| this.#connectConfirm != null
					|| this.#connected != null;
				if (!hasAnyEvent) {
					return [];
				}

				/**
				 * @param {ParsedICECandidate} c
				 * @returns {HTMLElement[]}
				 */
				const renderC = (c) => {

					/**
					 * @param {string} label
					 * @param {string | number | HTMLElement[]} content
					 * @returns {HTMLElement}
					 */
					const field = (label, content) => {

						if (!(content instanceof Array)) {
							content = [
								html('span', `${content ?? ""}`)
							];
						}

						return html('div', {
							class: 'ice-candidate-field'
						}, [
							html('span', `${label}:`, {
								class: 'label'
							}),
							content
						])
					};

					const iconElem = html('span', DISCLOSURE_CLOSED, {
						class: 'ice-candidate-icon'
					});

					const summaryElem = html('span', c.raw, {
						class: 'ice-candidate-summary'
					});

					const detailElem = html('div', {
						class: 'ice-candidate-detail',
						style: {
							display: 'none'
						}
					}, [
						field("Foundation", c.foundation),
						field("Component ID", c.componentId),
						field("Transport", c.transport),
						field("Priority", c.priority),
						field("Connection Address", c.connectionAddress),
						field("Port", c.port),
						field("Candidate Type", c.candidateType)
					]);

					// wire up events
					summaryElem.addEventListener('click', () => {
						if (detailElem.style.display === 'none') {
							iconElem.textContent = DISCLOSURE_OPEN;
							detailElem.style.display = null;
						} else {
							iconElem.textContent = DISCLOSURE_CLOSED;
							detailElem.style.display = 'none';
						}
					});

					return [
						html('div', [
							iconElem,
							summaryElem
						]),
						detailElem
					];
				};

				/**
				 * @param {ParsedSessionDescription | string} sd
				 * @returns {HTMLElement}
				 */
				const renderSd = (sd) => {
					if (typeof sd === 'string') {
						return html('div', [
							html('div', "Failed to parse session description"),
							html('pre', sd)
						]);
					} else {
						return html('ul', [
							html('li', "Ice Candidates:",
								sd.iceCandidates
									// sort by priority, ascending (ie, highest priority (lowest number) first)
									.toSorted((a, b) => a.priority - b.priority)
									.map(c => renderC(c))
							),
							html('li', `End of candidates? ${sd.endOfCandidates}`)
						]);
					}
				};

				/**
				 * @param {BeaconResponseInfo} response
				 * @returns {HTMLElement}
				 */
				const renderBeaconResponse = (response) => {
					return html('span', `${response.status} ${response.contentType ?? ""}`);
				}

				return [
					html('h2', "SmolWeb Client Connection Events:"),
					html('ul', [

						(() => {
							if (this.#stunAddr != null) {
								return html('li', `STUN address: ${this.#stunAddr}`);
							}
						})(),

						(() => {
							if (this.#connecting) {
								return html('li', "Connecting");
							}
						})(),

						(() => {
							if (this.#offer1 != null) {
								return html('li', "Offer 1:", renderSd(this.#offer1));
							}
						})(),

						(() => {
							if (this.#connectResponse != null) {
								return html('li', "Beacon Connect Response:", renderBeaconResponse(this.#connectResponse));
							}
						})(),

						(() => {
							if (this.#answer1 != null) {
								return html('li', "Answer 1:", renderSd(this.#answer1));
							}
						})(),

						(() => {
							if (this.#offer2 != null) {
								return html('li', "Offer 2:", renderSd(this.#offer2));
							}
						})(),

						(() => {
							if (this.#answer2 != null) {
								return html('li', "Answer 2:", renderSd(this.#answer2));
							}
						})(),

						(() => {
							if (this.#connectConfirm != null) {
								return html('li', "Beacon Connect Confirm:", renderBeaconResponse(this.#connectConfirm));
							}
						})(),

						(() => {
							if (this.#connected != null) {
								return html('li', `Connected: ${this.#connected.winningName} Connection`);
							}
						})()
					])
				];
			})(),

			// show any errors
			(/** @returns {HTMLElement[]} */ () => {

				const out = [];

				/**
				 * @param {Error} err
				 * @param {string} msg
				 */
				const render = (err, msg) => {

					let typeinfo = "";
					if (err.name !== 'Error') {
						typeinfo = `${err.name}: `;
					}

					out.push(html('div', {
						class: 'errors'
					}, [
						html('span', msg, { class: 'chip' }),
						html('span', `${typeinfo}${err.message}`)
					]));
				};

				// render the error, if any
				let err = this.#error;
				if (err != null) {
					render(err, "Error");
				}

				// render causes, if any
				err = err?.cause;
				while (err != null) {
					render(err, "Cause");
					err = err.cause;
				}

				return out;
			})()

		]);
	}
}


class PortalLog  extends Popover {

	/** @type {HTMLElement} */
	#requestsElem = html('div', {
		class: 'requests'
	});


	constructor() {
		super();
	}

	connectedCallback() {

		setChildren(this, [
			html('h1', "SmolWeb Requests"),
			this.#requestsElem
		]);

		super.connectedCallback();
	}


	/**
	 * @param {PortalRequest[]} requests
	 */
	init(requests) {
		setChildren(this.#requestsElem, requests.map(r => r._elem));
	}

	/**
	 * @param {PortalRequest} request
	 */
	add(request) {
		mount(this.#requestsElem, request._elem);
	}

	/**
	 * @param {PortalRequest} request
	 */
	remove(request) {
		unmount(this.#requestsElem, request._elem);
	}
}

customElements.define('smolweb-portal-log', PortalLog);
