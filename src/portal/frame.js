
import { html, mount } from 'redom';

import { Promiser } from 'food-runner/src/promises.js';


export class PortalFrame extends HTMLElement {

	/** @type {HTMLIFrameElement} */
	#iframe;


	constructor() {
		super();

		this.#iframe = html('iframe', {
			sandbox: (() => {
				const permissions = [
					'allow-same-origin',
					'allow-downloads',
					'allow-modals',
					'allow-forms',
					'allow-popups',
					'allow-popups-to-escape-sandbox',
					'allow-top-navigation-by-user-activation',
					'allow-top-navigation-to-custom-protocols'
				];
				return permissions.join(' ');
			})()
		});
	}


	connectedCallback() {
		mount(this, this.#iframe);
	}

	/**
	 * @returns {?Document}
	 */
	getDoc() {
		return this.#iframe.contentDocument;
	}

	/**
	 * @param {Document} value
	 * @param {?function(Document)} [hydrator]
	 * @returns {Promise.<Document>}
	 **/
	setDoc(value, hydrator=undefined) {

		const p = new Promiser();

		// once the iframe document is loaded, call the hydrator, if needed
		if (hydrator != null) {
			this.#iframe.addEventListener(
				'load',
				() => {
					const doc = this.#iframe.contentDocument;
					hydrator(doc);
					p.resolve(doc);
				},
				{
					once: true
				}
			);
		}

		// finally, show the doc in the iframe
		// NOTE: this definitely causes an HTML serialization/parsing roundtrip =(
		// TODO: any way to set the doc without the roundtrip?
		//   then we wouldn't need the hydrator either
		this.#iframe.srcdoc = value.documentElement.outerHTML;

		return p.promise();
	}
}

customElements.define('smolweb-portal-frame', PortalFrame);
