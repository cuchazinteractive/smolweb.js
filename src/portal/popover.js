
import { html, mount, setChildren } from 'redom';


const CARET_BORDER_SIZE = 10;

const TEMPLATE = /** @type {HTMLTemplateElement} */ html('template');
setChildren(TEMPLATE.content, [
	html('style', `
			
		:host {
			--popover-color: #ffffff;
			--popover-max-width: 100%;
			--popover-max-height: 100%;
			--popover-padding: 16px;
		}
	
		dialog {
			position: absolute;
			margin: 0px;
			background-color: var(--popover-color);
			border: none;
			box-shadow: rgba(50, 50, 93, 0.25) 0px 13px 27px -5px, rgba(0, 0, 0, 0.3) 0px 8px 16px -8px;
			overflow: auto;
			padding: var(--popover-padding);
		}
		
		.caret {
			position: absolute;
			width: 0;
			height: 0;
			border-width: ${CARET_BORDER_SIZE}px;
			border-style: solid;
			border-color: transparent;
		}
	`),
	html('div', {
		class: 'caret'
	}),
	html(
		'dialog',
		{
			open: true
		},
		html('slot')
	)
]);


export class Popover extends HTMLElement {

	constructor() {
		super();

		// build the shadow DOM from the template
		this.attachShadow({ mode: "open" });
		mount(this.shadowRoot, TEMPLATE.content.cloneNode(true));
	}


	connectedCallback() {

		const caret = this.shadowRoot.querySelector('.caret');
		const dialog = this.shadowRoot.querySelector('dialog');

		// position the dialog at the target
		const target = this.target;
		if (target != null) {
			const rect = boundingRect(target);
			switch (this.location) {

				case 'lower-left': {
					dialog.style.left = `${rect.left}px`;
					dialog.style.top = `${rect.bottom + CARET_BORDER_SIZE}px`;
					dialog.style.maxWidth = `min(calc(100% - ${rect.left}px - 2*var(--popover-padding)), var(--popover-max-width))`;
					dialog.style.maxHeight = `min(calc(100% - ${rect.bottom + CARET_BORDER_SIZE}px - 2*var(--popover-padding)), var(--popover-max-height))`;
					caret.style.left = `${rect.left}px`;
					caret.style.top = `${rect.bottom - CARET_BORDER_SIZE + 1}px`;
					caret.style.borderBottomColor = 'var(--popover-color)';
				} break;
			}
		}
	}

	/**
	 * @returns {?Element}
	 */
	get target() {
		const id = this.getAttribute('popover-target');
		if (id == null) {
			return null;
		}
		return window.document.querySelector(`#${id}`)
	}

	/**
	 * @returns {?string}
	 */
	get location() {
		return this.getAttribute('popover-location');
	}
}

customElements.define('smolweb-portal-popover', Popover);


/**
 * Get the bounding rect of the target relative to the body's client rect
 * @param {Element} target
 * @return {DOMRect}
 */
function boundingRect(target) {

	const targetRect = target.getBoundingClientRect();

	// get the body rect, but offset the borders and padding
	// so we get the same rect that position: absolute uses
	const bodyRect = window.document.body.getBoundingClientRect();
	const bodyStyles = window.getComputedStyle(window.document.body);
	const px = (str) => parseInt(str); // computed styles are always "XXpx", so just parse it like an int
	const borderLeft = px(bodyStyles.borderLeftWidth);
	const borderTop = px(bodyStyles.borderTopWidth);
	const borderRight = px(bodyStyles.borderRightWidth);
	const borderBottom = px(bodyStyles.borderBottomWidth);
	const paddingLeft = px(bodyStyles.paddingLeft);
	const paddingTop = px(bodyStyles.paddingTop);
	const paddingRight = px(bodyStyles.paddingRight);
	const paddingBottom = px(bodyStyles.paddingBottom);
	bodyRect.x += borderLeft + paddingLeft;
	bodyRect.y += borderTop + paddingTop;
	bodyRect.width -= borderLeft - borderRight - paddingLeft - paddingRight;
	bodyRect.height -= borderTop - borderBottom - paddingTop - paddingBottom;

	return new DOMRect(
		targetRect.x - bodyRect.x,
		targetRect.y - bodyRect.y,
		targetRect.width,
		targetRect.height
	);
}
