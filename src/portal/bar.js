
import { html, setChildren, mount, unmount } from 'redom';

import 'smolweb/src/portal/popover.js';
import { PortalIndicator } from 'smolweb/src/portal/indicator.js';


export class PortalBar extends HTMLElement {

	/** @type {HTMLButtonElement} */
	#refreshButton;

	/** @type {PortalIndicator} */
	#indicator

	/** @type {HTMLInputElement} */
	#addressBar

	/** @type {function()} */
	onRefresh = null;

	/** @type {function(string)} */
	onAddress = null;

	/** @type {?Popover} */
	#addressPopover = null;


	constructor() {
		super();

		this.#refreshButton = html('button', "⟳", {
			id: 'refresh',
			title: "Refresh",
			onclick: () => this.onRefresh?.()
		});
		this.#indicator = new PortalIndicator();
		this.#addressBar = html('input', {
			id: 'address',
			onchange: () => this.onAddress?.(this.#addressBar.value),
			onfocus: () => this.showAddressError(null),
			onkeydown: () => this.showAddressError(null)
		});

		setChildren(this, [
			this.#refreshButton,
			this.#indicator,
			this.#addressBar
		]);
	}

	/** @returns {string} */
	get address() {
		return this.#addressBar.value;
	}

	/** @param {string} value */
	set address(value) {
		this.#addressBar.value = value;
	}

	/** @returns {PortalIndicator} */
	get indicator() {
		return this.#indicator;
	}

	/**
	 * @param {?string} msg
	 */
	showAddressError(msg) {

		// cleanup any old error
		if (this.#addressPopover != null) {
			unmount(this, this.#addressPopover);
			this.#addressPopover = null;
		}

		if (msg == null) {
			return;
		}

		// show a new error
		this.#addressPopover = /** @type {Popover} */ html(
			'smolweb-portal-popover',
			{
				class: 'error',
				'popover-target': this.#addressBar.id,
				'popover-location': 'lower-left'
			},
			html('div', msg)
		);
		mount(this, this.#addressPopover);
	}
}

customElements.define('smolweb-portal-bar', PortalBar);
