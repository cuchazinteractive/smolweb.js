
/**
 * @param {string} parent
 * @param {string} child
 * @returns {string}
 */
export function join(parent, child) {

	if (parent.length > 1 && parent.endsWith('/')) {
		parent = parent.substring(0, parent.length - 1);
	}

	if (child.length > 1 && child.startsWith('/')) {
		child = child.substring(1);
	}

	if (parent === '') {
		if (child === '') {
			return '';
		} else if (child === '/') {
			return '/';
		} else {
			return child;
		}
	} else if (parent === '/') {
		if (child === '') {
			return '/';
		} else if (child === '/') {
			return '/';
		} else {
			return '/' + child;
		}
	} else {
		if (child === '') {
			return parent;
		} else if (child === '/') {
			return parent;
		} else {
			return `${parent}/${child}`;
		}
	}
}


/**
 * @param {string} path
 * @returns {?string}
 */
export function canonicalize(path) {

	// easy cases
	if (path === '') {
		return '';
	} else if (path === '/') {
		return '/';
	}

	const isAbsolute = path.startsWith('/');
	if (isAbsolute) {
		path = path.substring(1);
	}

	let endSlash = path.endsWith('/');
	if (endSlash) {
		path = path.substring(0, path.length - 1);
	}

	// break the path into components
	const parts = path.split('/');

	// collapse . and .. entries, where possible, using a stack
	const stack = [];
	for (let i=0; i<parts.length; i++) {
		const part = parts[i];
		if (part === '.') {
			// don't add . components
			if (i === parts.length - 1) {
				endSlash = true;
			}
		} else if (part === '..') {
			if (stack.length > 0) {
				stack.pop();
			} else if (isAbsolute) {
				return null;
			} else {
				stack.push(part);
			}
		} else {
			stack.push(part);
		}
	}

	// rebuild the path from the stack and maybe add flanking slashes
	path = stack.join('/');
	if (isAbsolute) {
		path = '/' + path;
	}
	if (endSlash && path !== '' && path !== '/') {
		path += '/';
	}
	return path;
}


/**
 * @param {string} path
 * @returns {?string}
 */
export function basename(path) {

	// already a folder? just use that
	if (path.endsWith('/')) {
		return path;
	}

	// otherwise, strip off the file name
	const pos = path.lastIndexOf('/');
	if (pos >= 0) {
		return path.substring(0, pos + 1);
	}

	// path has no folder component
	return null;
}
