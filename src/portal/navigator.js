
import { mount, html } from 'redom';

import { DEFAULT_CONNECTION_POOL, fetchOverSmolWeb } from 'smolweb/src/fetch.js';
import { PortalFrame } from 'smolweb/src/portal/frame.js';
import { PROTO, SmolWebAddress } from 'smolweb/src/proto.js';
import * as dns from 'smolweb/src/dns.js';
import * as paths from 'smolweb/src/portal/paths.js';


export class PortalNavigator {

	/** @type {Window} */
	#window;

	/** @type {PortalBar} */
	#bar;

	/** @type {PortalFrame} */
	#frame;

	/** @type {DnsResolver} */
	#dnsResolver;

	/** @type {DnsCache} */
	#dnsCache;

	/** @type {ConnectionPool} */
	#connectionPool;

	/** @type {?SmolWebAddress} */
	#addr = null;

	/** @type {?string} */
	#baseUrl = null;

	/** @type {?number} */
	#timeoutMs = null;


	/**
	 * @param {PortalBar} bar
	 * @param {PortalFrame} frame
	 * @param {Object} [options]
	 * @param {number} [options.timeoutMs]
	 * @param {DnsResolver} [options.dnsResolver]
	 * @param {DnsCache} [options.dnsCache]
	 * @param {ConnectionPool} [options.connectionPool]
	 * @param {Window} [options.window]
	 */
	constructor(bar, frame, options=undefined) {

		this.#window = options?.window ?? window;
		this.#bar = bar;
		this.#frame = frame;
		this.#dnsResolver = options?.dnsResolver ?? dns.DEFAULT_RESOLVER;
		this.#dnsCache = options?.dnsCache ?? dns.DEFAULT_CACHE;
		this.#connectionPool = options?.connectionPool ?? DEFAULT_CONNECTION_POOL;
		this.#timeoutMs = options?.timeoutMs ?? 15_000;

		// wire up bar events
		this.#bar.onRefresh = async () => {
			await this.#loadPage(this.#addr);
		};
		this.#bar.onAddress = this.#onAddress.bind(this);

		// capture eg back,forward button events to restore the page
		this.#window.addEventListener('popstate', async _event => {
			await this.#setAddrFromBrowserAndLoad();
		});
	}


	/** @returns {?SmolWebAddress} */
	get address() {
		// do a deep copy so the address isn't mutable by callers
		return this.#addr?.copy() ?? null;
	}

	/** @returns {PortalFrame} */
	get frame() {
		return this.#frame;
	}

	/**
	 * @returns {Promise.<void>}
	 */
	async init() {
		await this.#setAddrFromBrowserAndLoad();
	}

	/**
	 * @returns {Promise.<void>}
	 */
	async #setAddrFromBrowserAndLoad() {

		if (this.#window.location.hash === '') {
			this.#bar.showAddressError("No URL to show. Try entering a SmolWeb URL into the address bar above.");
			return;
		}

		const url = decodeURIComponent(this.#window.location.hash.substring(1));
		try {
			this.#addr = SmolWebAddress.fromUrl(url);
		} catch (err) {
			console.warn('error parsing SmolWeb url', err);
			this.#addr = null;
		}

		if (this.#addr != null) {
			this.#bar.address = this.#addr.toUrl();
			await this.#loadPage(this.#addr);
		}
	}

	/**
	 * @param {string} input
	 * @returns {?SmolWebAddress}
	 */
	buildAddress(input) {

		// is the input already a full smolweb address?
		let /** @type {?SmolWebAddress} */ addr = null;
		try {
			addr = SmolWebAddress.fromUrl(input);
		} catch {
			// nope
		}
		if (addr != null) {
			// yeah, just use that
			return addr;
		}

		// no current location? can't build anything
		if (this.#addr == null) {
			return null;
		}

		// absolute path
		if (input.startsWith('/')) {
			return new SmolWebAddress(this.#addr.authority, input);
		}

		// relative path, join to the base
		let base = this.#baseUrl;
		if (base == null) {
			// no current base
			console.warn(`can't build relative address from ${input}, no base`);
			return null;
		}
		const path = paths.join(base, input);
		const canonicalizedPath = paths.canonicalize(path);
		if (canonicalizedPath == null) {
			console.warn(`failed to canonicalize path: ${path}`);
			return null;
		}
		return new SmolWebAddress(this.#addr.authority, canonicalizedPath);
	}

	/**
	 * @param {string} url
	 * @returns {Promise.<void>}
	 */
	async #onAddress(url) {

		if (url === '') {
			return;
		}

		let /** @type {SmolWebAddress} */ addr;
		try {
			addr = SmolWebAddress.fromUrl(url);
		} catch (err) {
			this.#bar.showAddressError(err.message);
			return;
		}

		// if it's the same address, no need to change anything
		if (addr.equals(this.#addr)) {
			return;
		}

		await this.navigateTo(addr);
	}

	/**
	 * @param {?SmolWebAddress} addr
	 * @returns {Promise.<void>}
	 */
	async navigateTo(addr) {

		if (addr == null) {
			return;
		}

		// set the current location to here
		this.#addr = addr;

		// wipe the base URL until we get a new page
		this.#baseUrl = null;

		// update the address bar
		const url = this.#addr.toUrl();
		this.#bar.address = url;

		// update the real browser address bar
		this.#window.history.pushState(undefined, undefined, `#${url}`);

		await this.#loadPage(addr);
	}

	/**
	 * @param {SmolWebAddress} addr
	 * @returns {Promise.<?Document>}
	 */
	async #loadPage(addr) {

		if (addr == null) {
			throw new Error("addr is required");
		}

		// set the base URL based on the address path
		// for a file path, use the containing folder
		const base = paths.basename(addr.path);
		if (base != null) {
			this.#baseUrl = base;
		} else {
			console.warn(`page address path ${addr.path} has no folder component, shouldn't all address paths be absolute?`);
		}

		// fetch the page
		const response = await this.#fetch(addr);
		if (response.status !== 200) {
			console.warn('page fetch failed', response);
			return null;
		}
		const contentType = response.headers.get('content-type');
		switch (contentType) {
			case 'text/plain': return await this.#loadPageText(await response.text());
			case 'text/html': return await this.#loadPageHtml(await response.text());
			// TODO: other content types?
			default: return await this.#loadPageMessage(`unrecognized content type: ${contentType}`);
		}
	}

	/**
	 * @param {string} message
	 * @returns {Promise.<Document>}
	 */
	async #loadPageMessage(message) {

		// make an HTML page for the message
		let doc = new Document();
		mount(doc, html('html', [
			html('body', message)
		]));

		await this.#frame.setDoc(doc);
	}

	/**
	 * @param {string} text
	 * @returns {Promise.<Document>}
	 */
	async #loadPageText(text) {

		// make an HTML page for the message
		let doc = new Document();
		mount(doc, html('html', [
			html('body', [
				html('pre', text)
			])
		]));

		await this.#frame.setDoc(doc);
	}

	/**
	 * @param {string} htmlText
	 * @returns {Promise.<Document>}
	 */
	async #loadPageHtml(htmlText) {

		// parse it into a DOM and apply transformations
		let doc = new DOMParser().parseFromString(htmlText, 'text/html');
		doc = this.#transformDoc(doc);

		// look for a base tag, if any
		const base = /** @type {?HTMLBaseElement} */ doc.querySelector('html > head > base');
		if (base != null) {
			const baseHref = base.getAttribute('href');
			if (baseHref.startsWith('/')) {
				// absolute path, replace the current base URL
				this.#baseUrl = baseHref;
			} else {
				// relateive path, join to the current base URL
				this.#baseUrl = paths.join(this.#baseUrl, baseHref);
			}
		}

		// load the doc into the frame and hydrate it afterwards
		await this.#frame.setDoc(doc, this.#hydrateDoc.bind(this));
	}

	/**
	 * @param {SmolWebAddress} addr
	 * @returns {Promise.<Response>}
	 */
	async #fetch(addr) {

		if (addr == null) {
			throw new Error("no address to fetch");
		}

		const url = addr.toUrl();
		const request = this.#bar.indicator.request(url);

		return await fetchOverSmolWeb(url, {}, {
			timeoutMs: this.#timeoutMs,
			dnsResolver: this.#dnsResolver,
			dnsCache: this.#dnsCache,
			connectionPool: this.#connectionPool,
			log: request
		});
	}

	/**
	 * @param {Document} doc
	 * @returns {Document}
	 */
	#transformDoc(doc) {
		this.#transformDocImages(doc);
		this.#transformDocStyles(doc);
		this.#transformDocScripts(doc);
		return doc;
	}

	/**
	 * @param {Document} doc
	 * @returns {Document}
	 */
	#transformDocImages(doc) {
		for (const img of doc.querySelectorAll('img')) {

			const src = img.getAttribute('src');
			if (src == null) {
				continue;
			}

			// only intercept internal urls
			if (isExternalUrl(src)) {
				continue;
			}

			// move the src to another attribute so the browser doesn't try to load the image normally
			// we'll load the images ourselves at hydration
			img.setAttribute('smolweb-src', src);
			img.removeAttribute('src');
		}

		return doc;
	}

	/**
	 * @param {Document} doc
	 * @returns {Document}
	 */
	#transformDocStyles(doc) {

		for (const link of doc.querySelectorAll('link[rel=stylesheet]')) {

			const href = link.getAttribute('href');
			if (href == null) {
				continue;
			}

			// only intercept internal urls
			if (isExternalUrl(href)) {
				continue;
			}

			// move the src to another attribute so the browser doesn't try to load the image normally
			// we'll load the images ourselves at hydration
			link.setAttribute('smolweb-href', href);
			link.removeAttribute('href');
		}

		return doc;
	}

	/**
	 * @param {Document} doc
	 * @returns {Document}
	 */
	#transformDocScripts(doc) {

		for (const script of doc.querySelectorAll('script')) {

			// disable all scripts
			script.innerText = "// scripts are disabled in the SmolWeb portal since browsers don't give extensions the tools to execute scripts safely.";

			// move the src to another attribute so the browser doesn't try to load the script normally
			// we'll load the images ourselves at hydration
			const src = script.getAttribute('src');
			if (src != null) {
				script.setAttribute('disabled-src', src);
				script.removeAttribute('src');
			}
		}

		return doc;
	}

	/**
	 * @param {Document} doc
	 */
	#hydrateDoc(doc) {

		// hydrate styles (and related resources) first, to help fight flash-of-unstyled-content
		// TODO: fonts?
		this.#hydrateDocStyles(doc);

		// then hydrate other resources
		this.#hydrateDocLinks(doc);
		this.#hydrateDocImages(doc);

		// NOTE: scripts are not supported, so don't hydrate them
	}

	/**
	 * @param {Document} doc
	 */
	#hydrateDocLinks(doc) {

		for (const a of doc.querySelectorAll('a')) {

			// get the raw href from the anchor tag
			// NOTE: don't just call .href here, since the browser modifies the URLs
			const href = a.getAttribute('href');

			// always apply the link to the top-level navigator, even external links
			a.target = "_top";

			// only intercept internal links
			if (isExternalUrl(href)) {
				continue;
			}

			// rewrite the link to use the portal's navigator instead of the browser's navigator
			a.href = paths.join(window.location.pathname, `#${this.buildAddress(href).toUrl()}`);

			/* NOTE: Don't try to override the link with javascript!
			         It works in Firefox, but it doesn't work in Safari
			         due to differences in the iframe sandbox implementation.
			a.addEventListener('click', event => {
				(async () => {
					await this.navigateTo(this.buildAddress(href));
				})();
				event.preventDefault();
			});
			*/
		}
	}

	/**
	 * @param {Document} doc
	 */
	#hydrateDocImages(doc) {

		const /** @type {Map.<string,HTMLImageElement[]>} */ images = new Map();

		for (const img of doc.querySelectorAll('img')) {

			// get the image url, if any
			const src = img.getAttribute('smolweb-src');
			if (src == null || src === '') {
				continue;
			}

			// collect the image elements by src
			let imgs = images.get(src);
			if (imgs == null) {
				imgs = [];
				images.set(src, imgs);
			}
			imgs.push(img);
		}

		for (const [src, imgs] of images) {

			// load the image, asynchronously
			(async () => {

				// build the smolweb address, if possible
				const addr = this.buildAddress(src);
				if (addr == null) {
					return;
				}

				// load the image
				const response = await this.#fetch(addr);
				if (response.status !== 200) {
					console.warn('image fetch failed', response);
					return;
				}
				const blob = await response.blob();
				// noinspection JSCheckFunctionSignatures (WebStorm is picking up the node.js API here instead of the browser one)
				const objectUrl = URL.createObjectURL(blob)

				// show the image in the DOM
				for (const img of imgs) {
					img.src = objectUrl;
				}
			})();
		}
	}

	/**
	 * @param {Document} doc
	 */
	#hydrateDocStyles(doc) {

		for (const link of doc.querySelectorAll('link[rel=stylesheet]')) {

			// get the image url, if any
			const href = link.getAttribute('smolweb-href');
			if (href == null || href === '') {
				continue;
			}

			// load the style, asynchronously
			(async () => {

				// build the smolweb address, if possible
				const addr = this.buildAddress(href);
				if (addr == null) {
					return;
				}

				// load the style
				const response = await this.#fetch(addr);
				if (response.status !== 200) {
					console.warn('style fetch failed', response);
					return;
				}
				const css = await response.text();

				// replace the link with the style
				const style = html('style', css);
				mount(link.parentElement, style, link, true);
			})();
		}
	}
}


/**
 * @param {string} url
 * @returns {boolean}
 */
function isExternalUrl(url) {
	// check if the url starts with `<scheme>:/`
	// and the scheme isn't smolweb
	const scheme = url.split('/')[0];
	if (!scheme.endsWith(':')) {
		// no scheme, must be internal
		return false;
	}
	return scheme !== `${PROTO}:`;
}
