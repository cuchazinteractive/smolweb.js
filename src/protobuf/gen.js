/*eslint-disable block-scoped-var, id-length, no-control-regex, no-magic-numbers, no-prototype-builtins, no-redeclare, no-shadow, no-var, sort-vars*/

// noinspection ES6UnusedImports, NpmUsedModulesInstalled, JSFileReferences (used by gen'd code, peer dependency)

// noinspection ES6UnusedImports (used by gen'd code)
import { SaveModes, $protobuf } from 'food-runner-protobuf-runtime';


// Common aliases
const $Reader = $protobuf.Reader, $Writer = $protobuf.Writer, $util = $protobuf.util;

// Exported root namespace
const $root = $protobuf.roots["default"] || ($protobuf.roots["default"] = {});

export const smolweb = $root.smolweb = (() => {

	/**
	 * Namespace smolweb.
	 *
	 * @namespace
	 */
	const smolweb = {};

	smolweb.guests = (function() {

		/**
		 * Namespace guests.
		 * @memberof smolweb
		 * @namespace
		 */
		const guests = {};

		guests.About = (function() {

			/**
			 * Properties of an About.
			 * @memberof smolweb.guests
			 * @typedef {Object} smolweb_guests_TAbout
			 * @property {string|null} [version] About version
			 */

			/**
			 * Constructs a new About.
			 * @memberof smolweb.guests
			 * @classdesc Represents an About.
			 * @implements ProtobufSave
			 * @constructor
			 * @param {smolweb.guests.About=} [properties] Properties to set
			 */
			function About(properties) {
				if (properties)
					for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
						if (properties[keys[i]] != null)
							this[keys[i]] = properties[keys[i]];
			}

			/**
			 * About version.
			 * @member {string} version
			 * @memberof smolweb.guests.About
			 * @instance
			 */
			About.prototype.version = "";

			/**
			 * Creates a new About instance using the specified properties.
			 * @function create
			 * @memberof smolweb.guests.About
			 * @static
			 * @param {smolweb.guests.About=} [properties] Properties to set
			 * @returns {smolweb.guests.About} About instance
			 */
			About.create = function create(properties) {
				return new About(properties);
			};

			/**
			 * Encodes the specified About message. Does not implicitly {@link smolweb.guests.About.verify|verify} messages.
			 * @function encode
			 * @memberof smolweb.guests.About
			 * @static
			 * @param {smolweb.guests.About} message About message or plain object to encode
			 * @param {ArrayBufferWriter} [writer] Writer to encode to
			 * @returns {ArrayBufferWriter} Writer
			 */
			About.encode = function encode(message, writer) {
				if (!writer)
					writer = $Writer.create();
				if (message.version != null && Object.hasOwnProperty.call(message, "version"))
					writer.uint32(/* id 1, wireType 2 =*/10).string(message.version);
				return writer;
			};

			/**
			 * Decodes an About message from the specified reader or buffer.
			 * @function decode
			 * @memberof smolweb.guests.About
			 * @static
			 * @param {ArrayBufferReader|ArrayBuffer} reader Reader or buffer to decode from
			 * @param {number} [length] Message length if known beforehand
			 * @returns {smolweb.guests.About} About
			 * @throws {Error} If the payload is not a reader or valid buffer
			 * @throws {$protobuf.util.ProtocolError} If required fields are missing
			 */
			About.decode = function decode(reader, length) {
				if (!(reader instanceof $Reader))
					reader = $Reader.create(reader);
				let end = length === undefined ? reader.len : reader.pos + length, message = new $root.smolweb.guests.About();
				while (reader.pos < end) {
					let tag = reader.uint32();
					switch (tag >>> 3) {
					case 1: {
							message.version = reader.string();
							break;
						}
					default:
						reader.skipType(tag & 7);
						break;
					}
				}
				return message;
			};

			/**
			 * Verifies an About message.
			 * @function verify
			 * @memberof smolweb.guests.About
			 * @static
			 * @param {Object.<string,*>} message Plain object to verify
			 * @returns {string|null} `null` if valid, otherwise the reason why it is not
			 */
			About.verify = function verify(message) {
				if (typeof message !== "object" || message === null)
					return "object expected";
				if (message.version != null && message.hasOwnProperty("version"))
					if (!$util.isString(message.version))
						return "version: string expected";
				return null;
			};

			/**
			 * Creates an About message from a plain object. Also converts values to their respective internal types.
			 * @function fromObject
			 * @memberof smolweb.guests.About
			 * @static
			 * @param {Object.<string,*>} object Plain object
			 * @returns {smolweb.guests.About} About
			 */
			About.fromObject = function fromObject(object) {
				if (object instanceof $root.smolweb.guests.About)
					return object;
				let message = new $root.smolweb.guests.About();
				if (object.version != null)
					message.version = String(object.version);
				return message;
			};

			/**
			 * Creates a plain object from an About message. Also converts values to other types if specified.
			 * @function toObject
			 * @memberof smolweb.guests.About
			 * @static
			 * @param {smolweb.guests.About} message About
			 * @param {$protobuf.IConversionOptions} [options] Conversion options
			 * @returns {Object.<string,*>} Plain object
			 */
			About.toObject = function toObject(message, options) {
				if (!options)
					options = {};
				let object = {};
				if (options.defaults)
					object.version = "";
				if (message.version != null && message.hasOwnProperty("version"))
					object.version = message.version;
				return object;
			};

			/**
			 * Converts this About to JSON.
			 * @function toJSON
			 * @memberof smolweb.guests.About
			 * @instance
			 * @returns {Object.<string,*>} JSON object
			 */
			About.prototype.toJSON = function toJSON() {
				return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
			};

			/**
			 * Gets the default type url for About
			 * @function getTypeUrl
			 * @memberof smolweb.guests.About
			 * @static
			 * @param {string} [typeUrlPrefix] your custom typeUrlPrefix(default "type.cuchazinteractive.org")
			 * @returns {string} The default type url
			 */
			About.getTypeUrl = function getTypeUrl(typeUrlPrefix) {
				if (typeUrlPrefix === undefined) {
					typeUrlPrefix = "type.cuchazinteractive.org";
				}
				return typeUrlPrefix + "/smolweb.guests.About";
			};

			
			/**
			 * @returns {?string}
			 */		
			About.prototype.verify = function () {
				const err = About.verify(this);
				if (err != null) {
					throw new Error("Verification failed for About: " + err);
				}
			};
			
			/**
			 * @returns {ArrayBuffer}
			 */
			About.prototype.toBuffer = function () {
				return About.encode(this).finish();
			};
			
			/**
			 * @param {string | number} [spacer]
			 * @returns {string}
			 */
			About.prototype.toString = function (spacer) {
				return JSON.stringify(this.toJSON(), null, spacer);
			};
			
			/**
			 * @param {SaveMode} mode
			 * @returns {ArrayBuffer | string | Object}
			 */
			About.prototype.to = function (mode) {
				switch (mode) {
					case SaveModes.BUF: return this.toBuffer();
					case SaveModes.STR: return this.toString();
					case SaveModes.JSON: return this.toJSON();
					default: throw new Error("unrecognized save mode: " + mode);
				}
			};
			
			/**
			 * @param {smolweb_guests_TAbout} obj
			 * @returns {smolweb.guests.About}
			 */
			About.new = (obj) => {
				const msg = About.create(obj);
				msg.verify();
				return msg;
			};
			
			/**
			 * @param {ArrayBuffer} buf
			 * @returns {smolweb.guests.About}
			 */
			About.fromBuffer = (buf) => {
				const msg = About.decode(buf);
				msg.verify();
				return msg;
			};
			
			/**
			 * @param {string} str
			 * @returns {smolweb.guests.About}
			 */
			About.fromString = (str) => {
				const msg = About.fromJSON(JSON.parse(str));
				msg.verify();
				return msg;
			};
			
			/**
			 * @param {Object} json
			 * @returns {smolweb.guests.About}
			 */
			About.fromJSON = (obj) => {
				const msg = About.fromObject(obj);
				msg.verify();
				return msg;
			};
			
			/**
			 * @param {smolweb.guests.About | Object | ArrayBuffer | string} saved
			 * @returns {smolweb.guests.About}
			 */
			About.from = (saved) => {
				if (saved instanceof About) {
					return saved;
				} else if (saved instanceof ArrayBuffer) {
					return About.fromBuffer(saved);
				} else if (typeof saved === 'string') {
					return About.fromString(saved);
				} else if (typeof saved === 'object') {
					return About.fromJSON(saved);
				} else {
					throw new Error("Unrecognized save format: " + saved?.constructor?.name);
				}
			};
			
			/**
			 * @returns {smolweb.guests.About}
			 */
			About.prototype.copy = function () {
				// TODO: find more efficient way to do this?
				//   we only need to make defensive copies to prevent mutation
				//   but there's no need for a full buffer encoding/decoding roundtrip
				return this.constructor.fromBuffer(this.toBuffer());
			};
			
			/**
			 * WebStorm needs a little help understanding that this class implements ProtobufOpen
			 * @returns {ProtobufOpen.<smolweb.guests.About>}
			 */
			About.opener = function () {
				return this;
			};
			
			return About;
			
		})();

		guests.HostIdentityRequest = (function() {

			/**
			 * Properties of a HostIdentityRequest.
			 * @memberof smolweb.guests
			 * @typedef {Object} smolweb_guests_THostIdentityRequest
			 * @property {ArrayBuffer|null} [hostUid] HostIdentityRequest hostUid
			 */

			/**
			 * Constructs a new HostIdentityRequest.
			 * @memberof smolweb.guests
			 * @classdesc Represents a HostIdentityRequest.
			 * @implements ProtobufSave
			 * @constructor
			 * @param {smolweb.guests.HostIdentityRequest=} [properties] Properties to set
			 */
			function HostIdentityRequest(properties) {
				if (properties)
					for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
						if (properties[keys[i]] != null)
							this[keys[i]] = properties[keys[i]];
			}

			/**
			 * HostIdentityRequest hostUid.
			 * @member {ArrayBuffer} hostUid
			 * @memberof smolweb.guests.HostIdentityRequest
			 * @instance
			 */
			HostIdentityRequest.prototype.hostUid = $util.newBuffer([]);

			/**
			 * Creates a new HostIdentityRequest instance using the specified properties.
			 * @function create
			 * @memberof smolweb.guests.HostIdentityRequest
			 * @static
			 * @param {smolweb.guests.HostIdentityRequest=} [properties] Properties to set
			 * @returns {smolweb.guests.HostIdentityRequest} HostIdentityRequest instance
			 */
			HostIdentityRequest.create = function create(properties) {
				return new HostIdentityRequest(properties);
			};

			/**
			 * Encodes the specified HostIdentityRequest message. Does not implicitly {@link smolweb.guests.HostIdentityRequest.verify|verify} messages.
			 * @function encode
			 * @memberof smolweb.guests.HostIdentityRequest
			 * @static
			 * @param {smolweb.guests.HostIdentityRequest} message HostIdentityRequest message or plain object to encode
			 * @param {ArrayBufferWriter} [writer] Writer to encode to
			 * @returns {ArrayBufferWriter} Writer
			 */
			HostIdentityRequest.encode = function encode(message, writer) {
				if (!writer)
					writer = $Writer.create();
				if (message.hostUid != null && Object.hasOwnProperty.call(message, "hostUid"))
					writer.uint32(/* id 1, wireType 2 =*/10).bytes(message.hostUid);
				return writer;
			};

			/**
			 * Decodes a HostIdentityRequest message from the specified reader or buffer.
			 * @function decode
			 * @memberof smolweb.guests.HostIdentityRequest
			 * @static
			 * @param {ArrayBufferReader|ArrayBuffer} reader Reader or buffer to decode from
			 * @param {number} [length] Message length if known beforehand
			 * @returns {smolweb.guests.HostIdentityRequest} HostIdentityRequest
			 * @throws {Error} If the payload is not a reader or valid buffer
			 * @throws {$protobuf.util.ProtocolError} If required fields are missing
			 */
			HostIdentityRequest.decode = function decode(reader, length) {
				if (!(reader instanceof $Reader))
					reader = $Reader.create(reader);
				let end = length === undefined ? reader.len : reader.pos + length, message = new $root.smolweb.guests.HostIdentityRequest();
				while (reader.pos < end) {
					let tag = reader.uint32();
					switch (tag >>> 3) {
					case 1: {
							message.hostUid = reader.bytes();
							break;
						}
					default:
						reader.skipType(tag & 7);
						break;
					}
				}
				return message;
			};

			/**
			 * Verifies a HostIdentityRequest message.
			 * @function verify
			 * @memberof smolweb.guests.HostIdentityRequest
			 * @static
			 * @param {Object.<string,*>} message Plain object to verify
			 * @returns {string|null} `null` if valid, otherwise the reason why it is not
			 */
			HostIdentityRequest.verify = function verify(message) {
				if (typeof message !== "object" || message === null)
					return "object expected";
				if (message.hostUid != null && message.hasOwnProperty("hostUid"))
					if (!(message.hostUid && typeof message.hostUid.byteLength === 'number' || $util.isString(message.hostUid)))
						return "hostUid: buffer expected";
				return null;
			};

			/**
			 * Creates a HostIdentityRequest message from a plain object. Also converts values to their respective internal types.
			 * @function fromObject
			 * @memberof smolweb.guests.HostIdentityRequest
			 * @static
			 * @param {Object.<string,*>} object Plain object
			 * @returns {smolweb.guests.HostIdentityRequest} HostIdentityRequest
			 */
			HostIdentityRequest.fromObject = function fromObject(object) {
				if (object instanceof $root.smolweb.guests.HostIdentityRequest)
					return object;
				let message = new $root.smolweb.guests.HostIdentityRequest();
				if (object.hostUid != null)
					if (typeof object.hostUid === "string")
						$util.base64.decode(object.hostUid, message.hostUid = $util.newBuffer($util.base64.length(object.hostUid)), 0);
					else if (object.hostUid.byteLength >= 0)
						message.hostUid = object.hostUid;
				return message;
			};

			/**
			 * Creates a plain object from a HostIdentityRequest message. Also converts values to other types if specified.
			 * @function toObject
			 * @memberof smolweb.guests.HostIdentityRequest
			 * @static
			 * @param {smolweb.guests.HostIdentityRequest} message HostIdentityRequest
			 * @param {$protobuf.IConversionOptions} [options] Conversion options
			 * @returns {Object.<string,*>} Plain object
			 */
			HostIdentityRequest.toObject = function toObject(message, options) {
				if (!options)
					options = {};
				let object = {};
				if (options.defaults)
					if (options.bytes === String)
						object.hostUid = "";
					else {
						object.hostUid = [];
						if (options.bytes !== Array)
							object.hostUid = $util.newBuffer(object.hostUid);
					}
				if (message.hostUid != null && message.hasOwnProperty("hostUid"))
					object.hostUid = options.bytes === String ? $util.base64.encode(message.hostUid, 0, message.hostUid.byteLength) : options.bytes === Array ? Array.prototype.slice.call(message.hostUid) : message.hostUid;
				return object;
			};

			/**
			 * Converts this HostIdentityRequest to JSON.
			 * @function toJSON
			 * @memberof smolweb.guests.HostIdentityRequest
			 * @instance
			 * @returns {Object.<string,*>} JSON object
			 */
			HostIdentityRequest.prototype.toJSON = function toJSON() {
				return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
			};

			/**
			 * Gets the default type url for HostIdentityRequest
			 * @function getTypeUrl
			 * @memberof smolweb.guests.HostIdentityRequest
			 * @static
			 * @param {string} [typeUrlPrefix] your custom typeUrlPrefix(default "type.cuchazinteractive.org")
			 * @returns {string} The default type url
			 */
			HostIdentityRequest.getTypeUrl = function getTypeUrl(typeUrlPrefix) {
				if (typeUrlPrefix === undefined) {
					typeUrlPrefix = "type.cuchazinteractive.org";
				}
				return typeUrlPrefix + "/smolweb.guests.HostIdentityRequest";
			};

			
			/**
			 * @returns {?string}
			 */		
			HostIdentityRequest.prototype.verify = function () {
				const err = HostIdentityRequest.verify(this);
				if (err != null) {
					throw new Error("Verification failed for HostIdentityRequest: " + err);
				}
			};
			
			/**
			 * @returns {ArrayBuffer}
			 */
			HostIdentityRequest.prototype.toBuffer = function () {
				return HostIdentityRequest.encode(this).finish();
			};
			
			/**
			 * @param {string | number} [spacer]
			 * @returns {string}
			 */
			HostIdentityRequest.prototype.toString = function (spacer) {
				return JSON.stringify(this.toJSON(), null, spacer);
			};
			
			/**
			 * @param {SaveMode} mode
			 * @returns {ArrayBuffer | string | Object}
			 */
			HostIdentityRequest.prototype.to = function (mode) {
				switch (mode) {
					case SaveModes.BUF: return this.toBuffer();
					case SaveModes.STR: return this.toString();
					case SaveModes.JSON: return this.toJSON();
					default: throw new Error("unrecognized save mode: " + mode);
				}
			};
			
			/**
			 * @param {smolweb_guests_THostIdentityRequest} obj
			 * @returns {smolweb.guests.HostIdentityRequest}
			 */
			HostIdentityRequest.new = (obj) => {
				const msg = HostIdentityRequest.create(obj);
				msg.verify();
				return msg;
			};
			
			/**
			 * @param {ArrayBuffer} buf
			 * @returns {smolweb.guests.HostIdentityRequest}
			 */
			HostIdentityRequest.fromBuffer = (buf) => {
				const msg = HostIdentityRequest.decode(buf);
				msg.verify();
				return msg;
			};
			
			/**
			 * @param {string} str
			 * @returns {smolweb.guests.HostIdentityRequest}
			 */
			HostIdentityRequest.fromString = (str) => {
				const msg = HostIdentityRequest.fromJSON(JSON.parse(str));
				msg.verify();
				return msg;
			};
			
			/**
			 * @param {Object} json
			 * @returns {smolweb.guests.HostIdentityRequest}
			 */
			HostIdentityRequest.fromJSON = (obj) => {
				const msg = HostIdentityRequest.fromObject(obj);
				msg.verify();
				return msg;
			};
			
			/**
			 * @param {smolweb.guests.HostIdentityRequest | Object | ArrayBuffer | string} saved
			 * @returns {smolweb.guests.HostIdentityRequest}
			 */
			HostIdentityRequest.from = (saved) => {
				if (saved instanceof HostIdentityRequest) {
					return saved;
				} else if (saved instanceof ArrayBuffer) {
					return HostIdentityRequest.fromBuffer(saved);
				} else if (typeof saved === 'string') {
					return HostIdentityRequest.fromString(saved);
				} else if (typeof saved === 'object') {
					return HostIdentityRequest.fromJSON(saved);
				} else {
					throw new Error("Unrecognized save format: " + saved?.constructor?.name);
				}
			};
			
			/**
			 * @returns {smolweb.guests.HostIdentityRequest}
			 */
			HostIdentityRequest.prototype.copy = function () {
				// TODO: find more efficient way to do this?
				//   we only need to make defensive copies to prevent mutation
				//   but there's no need for a full buffer encoding/decoding roundtrip
				return this.constructor.fromBuffer(this.toBuffer());
			};
			
			/**
			 * WebStorm needs a little help understanding that this class implements ProtobufOpen
			 * @returns {ProtobufOpen.<smolweb.guests.HostIdentityRequest>}
			 */
			HostIdentityRequest.opener = function () {
				return this;
			};
			
			return HostIdentityRequest;
			
		})();

		guests.HostIdentityResponse = (function() {

			/**
			 * Properties of a HostIdentityResponse.
			 * @memberof smolweb.guests
			 * @typedef {Object} smolweb_guests_THostIdentityResponse
			 * @property {boolean|null} [hostUnknown] HostIdentityResponse hostUnknown
			 * @property {ArrayBuffer|null} [identity] HostIdentityResponse identity
			 */

			/**
			 * Constructs a new HostIdentityResponse.
			 * @memberof smolweb.guests
			 * @classdesc Represents a HostIdentityResponse.
			 * @implements ProtobufSave
			 * @constructor
			 * @param {smolweb.guests.HostIdentityResponse=} [properties] Properties to set
			 */
			function HostIdentityResponse(properties) {
				if (properties)
					for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
						if (properties[keys[i]] != null)
							this[keys[i]] = properties[keys[i]];
			}

			/**
			 * HostIdentityResponse hostUnknown.
			 * @member {boolean|null|undefined} hostUnknown
			 * @memberof smolweb.guests.HostIdentityResponse
			 * @instance
			 */
			HostIdentityResponse.prototype.hostUnknown = null;

			/**
			 * HostIdentityResponse identity.
			 * @member {ArrayBuffer|null|undefined} identity
			 * @memberof smolweb.guests.HostIdentityResponse
			 * @instance
			 */
			HostIdentityResponse.prototype.identity = null;

			// OneOf field names bound to virtual getters and setters
			let $oneOfFields;

			/**
			 * HostIdentityResponse response.
			 * @member {"hostUnknown"|"identity"|undefined} response
			 * @memberof smolweb.guests.HostIdentityResponse
			 * @instance
			 */
			Object.defineProperty(HostIdentityResponse.prototype, "response", {
				get: $util.oneOfGetter($oneOfFields = ["hostUnknown", "identity"]),
				set: $util.oneOfSetter($oneOfFields)
			});

			/**
			 * Creates a new HostIdentityResponse instance using the specified properties.
			 * @function create
			 * @memberof smolweb.guests.HostIdentityResponse
			 * @static
			 * @param {smolweb.guests.HostIdentityResponse=} [properties] Properties to set
			 * @returns {smolweb.guests.HostIdentityResponse} HostIdentityResponse instance
			 */
			HostIdentityResponse.create = function create(properties) {
				return new HostIdentityResponse(properties);
			};

			/**
			 * Encodes the specified HostIdentityResponse message. Does not implicitly {@link smolweb.guests.HostIdentityResponse.verify|verify} messages.
			 * @function encode
			 * @memberof smolweb.guests.HostIdentityResponse
			 * @static
			 * @param {smolweb.guests.HostIdentityResponse} message HostIdentityResponse message or plain object to encode
			 * @param {ArrayBufferWriter} [writer] Writer to encode to
			 * @returns {ArrayBufferWriter} Writer
			 */
			HostIdentityResponse.encode = function encode(message, writer) {
				if (!writer)
					writer = $Writer.create();
				if (message.hostUnknown != null && Object.hasOwnProperty.call(message, "hostUnknown"))
					writer.uint32(/* id 1, wireType 0 =*/8).bool(message.hostUnknown);
				if (message.identity != null && Object.hasOwnProperty.call(message, "identity"))
					writer.uint32(/* id 2, wireType 2 =*/18).bytes(message.identity);
				return writer;
			};

			/**
			 * Decodes a HostIdentityResponse message from the specified reader or buffer.
			 * @function decode
			 * @memberof smolweb.guests.HostIdentityResponse
			 * @static
			 * @param {ArrayBufferReader|ArrayBuffer} reader Reader or buffer to decode from
			 * @param {number} [length] Message length if known beforehand
			 * @returns {smolweb.guests.HostIdentityResponse} HostIdentityResponse
			 * @throws {Error} If the payload is not a reader or valid buffer
			 * @throws {$protobuf.util.ProtocolError} If required fields are missing
			 */
			HostIdentityResponse.decode = function decode(reader, length) {
				if (!(reader instanceof $Reader))
					reader = $Reader.create(reader);
				let end = length === undefined ? reader.len : reader.pos + length, message = new $root.smolweb.guests.HostIdentityResponse();
				while (reader.pos < end) {
					let tag = reader.uint32();
					switch (tag >>> 3) {
					case 1: {
							message.hostUnknown = reader.bool();
							break;
						}
					case 2: {
							message.identity = reader.bytes();
							break;
						}
					default:
						reader.skipType(tag & 7);
						break;
					}
				}
				return message;
			};

			/**
			 * Verifies a HostIdentityResponse message.
			 * @function verify
			 * @memberof smolweb.guests.HostIdentityResponse
			 * @static
			 * @param {Object.<string,*>} message Plain object to verify
			 * @returns {string|null} `null` if valid, otherwise the reason why it is not
			 */
			HostIdentityResponse.verify = function verify(message) {
				if (typeof message !== "object" || message === null)
					return "object expected";
				let properties = {};
				if (message.hostUnknown != null && message.hasOwnProperty("hostUnknown")) {
					properties.response = 1;
					if (typeof message.hostUnknown !== "boolean")
						return "hostUnknown: boolean expected";
				}
				if (message.identity != null && message.hasOwnProperty("identity")) {
					if (properties.response === 1)
						return "response: multiple values";
					properties.response = 1;
					if (!(message.identity && typeof message.identity.byteLength === 'number' || $util.isString(message.identity)))
						return "identity: buffer expected";
				}
				return null;
			};

			/**
			 * Creates a HostIdentityResponse message from a plain object. Also converts values to their respective internal types.
			 * @function fromObject
			 * @memberof smolweb.guests.HostIdentityResponse
			 * @static
			 * @param {Object.<string,*>} object Plain object
			 * @returns {smolweb.guests.HostIdentityResponse} HostIdentityResponse
			 */
			HostIdentityResponse.fromObject = function fromObject(object) {
				if (object instanceof $root.smolweb.guests.HostIdentityResponse)
					return object;
				let message = new $root.smolweb.guests.HostIdentityResponse();
				if (object.hostUnknown != null)
					message.hostUnknown = Boolean(object.hostUnknown);
				if (object.identity != null)
					if (typeof object.identity === "string")
						$util.base64.decode(object.identity, message.identity = $util.newBuffer($util.base64.length(object.identity)), 0);
					else if (object.identity.byteLength >= 0)
						message.identity = object.identity;
				return message;
			};

			/**
			 * Creates a plain object from a HostIdentityResponse message. Also converts values to other types if specified.
			 * @function toObject
			 * @memberof smolweb.guests.HostIdentityResponse
			 * @static
			 * @param {smolweb.guests.HostIdentityResponse} message HostIdentityResponse
			 * @param {$protobuf.IConversionOptions} [options] Conversion options
			 * @returns {Object.<string,*>} Plain object
			 */
			HostIdentityResponse.toObject = function toObject(message, options) {
				if (!options)
					options = {};
				let object = {};
				if (message.hostUnknown != null && message.hasOwnProperty("hostUnknown")) {
					object.hostUnknown = message.hostUnknown;
					if (options.oneofs)
						object.response = "hostUnknown";
				}
				if (message.identity != null && message.hasOwnProperty("identity")) {
					object.identity = options.bytes === String ? $util.base64.encode(message.identity, 0, message.identity.byteLength) : options.bytes === Array ? Array.prototype.slice.call(message.identity) : message.identity;
					if (options.oneofs)
						object.response = "identity";
				}
				return object;
			};

			/**
			 * Converts this HostIdentityResponse to JSON.
			 * @function toJSON
			 * @memberof smolweb.guests.HostIdentityResponse
			 * @instance
			 * @returns {Object.<string,*>} JSON object
			 */
			HostIdentityResponse.prototype.toJSON = function toJSON() {
				return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
			};

			/**
			 * Gets the default type url for HostIdentityResponse
			 * @function getTypeUrl
			 * @memberof smolweb.guests.HostIdentityResponse
			 * @static
			 * @param {string} [typeUrlPrefix] your custom typeUrlPrefix(default "type.cuchazinteractive.org")
			 * @returns {string} The default type url
			 */
			HostIdentityResponse.getTypeUrl = function getTypeUrl(typeUrlPrefix) {
				if (typeUrlPrefix === undefined) {
					typeUrlPrefix = "type.cuchazinteractive.org";
				}
				return typeUrlPrefix + "/smolweb.guests.HostIdentityResponse";
			};

			
			/**
			 * @returns {?string}
			 */		
			HostIdentityResponse.prototype.verify = function () {
				const err = HostIdentityResponse.verify(this);
				if (err != null) {
					throw new Error("Verification failed for HostIdentityResponse: " + err);
				}
			};
			
			/**
			 * @returns {ArrayBuffer}
			 */
			HostIdentityResponse.prototype.toBuffer = function () {
				return HostIdentityResponse.encode(this).finish();
			};
			
			/**
			 * @param {string | number} [spacer]
			 * @returns {string}
			 */
			HostIdentityResponse.prototype.toString = function (spacer) {
				return JSON.stringify(this.toJSON(), null, spacer);
			};
			
			/**
			 * @param {SaveMode} mode
			 * @returns {ArrayBuffer | string | Object}
			 */
			HostIdentityResponse.prototype.to = function (mode) {
				switch (mode) {
					case SaveModes.BUF: return this.toBuffer();
					case SaveModes.STR: return this.toString();
					case SaveModes.JSON: return this.toJSON();
					default: throw new Error("unrecognized save mode: " + mode);
				}
			};
			
			/**
			 * @param {smolweb_guests_THostIdentityResponse} obj
			 * @returns {smolweb.guests.HostIdentityResponse}
			 */
			HostIdentityResponse.new = (obj) => {
				const msg = HostIdentityResponse.create(obj);
				msg.verify();
				return msg;
			};
			
			/**
			 * @param {ArrayBuffer} buf
			 * @returns {smolweb.guests.HostIdentityResponse}
			 */
			HostIdentityResponse.fromBuffer = (buf) => {
				const msg = HostIdentityResponse.decode(buf);
				msg.verify();
				return msg;
			};
			
			/**
			 * @param {string} str
			 * @returns {smolweb.guests.HostIdentityResponse}
			 */
			HostIdentityResponse.fromString = (str) => {
				const msg = HostIdentityResponse.fromJSON(JSON.parse(str));
				msg.verify();
				return msg;
			};
			
			/**
			 * @param {Object} json
			 * @returns {smolweb.guests.HostIdentityResponse}
			 */
			HostIdentityResponse.fromJSON = (obj) => {
				const msg = HostIdentityResponse.fromObject(obj);
				msg.verify();
				return msg;
			};
			
			/**
			 * @param {smolweb.guests.HostIdentityResponse | Object | ArrayBuffer | string} saved
			 * @returns {smolweb.guests.HostIdentityResponse}
			 */
			HostIdentityResponse.from = (saved) => {
				if (saved instanceof HostIdentityResponse) {
					return saved;
				} else if (saved instanceof ArrayBuffer) {
					return HostIdentityResponse.fromBuffer(saved);
				} else if (typeof saved === 'string') {
					return HostIdentityResponse.fromString(saved);
				} else if (typeof saved === 'object') {
					return HostIdentityResponse.fromJSON(saved);
				} else {
					throw new Error("Unrecognized save format: " + saved?.constructor?.name);
				}
			};
			
			/**
			 * @returns {smolweb.guests.HostIdentityResponse}
			 */
			HostIdentityResponse.prototype.copy = function () {
				// TODO: find more efficient way to do this?
				//   we only need to make defensive copies to prevent mutation
				//   but there's no need for a full buffer encoding/decoding roundtrip
				return this.constructor.fromBuffer(this.toBuffer());
			};
			
			/**
			 * WebStorm needs a little help understanding that this class implements ProtobufOpen
			 * @returns {ProtobufOpen.<smolweb.guests.HostIdentityResponse>}
			 */
			HostIdentityResponse.opener = function () {
				return this;
			};
			
			return HostIdentityResponse;
			
		})();

		guests.ConnectionRequest = (function() {

			/**
			 * Properties of a ConnectionRequest.
			 * @memberof smolweb.guests
			 * @typedef {Object} smolweb_guests_TConnectionRequest
			 * @property {ArrayBuffer|null} [hostUid] ConnectionRequest hostUid
			 * @property {ArrayBuffer|null} [identity] ConnectionRequest identity
			 * @property {ArrayBuffer|null} [secret] ConnectionRequest secret
			 */

			/**
			 * Constructs a new ConnectionRequest.
			 * @memberof smolweb.guests
			 * @classdesc Represents a ConnectionRequest.
			 * @implements ProtobufSave
			 * @constructor
			 * @param {smolweb.guests.ConnectionRequest=} [properties] Properties to set
			 */
			function ConnectionRequest(properties) {
				if (properties)
					for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
						if (properties[keys[i]] != null)
							this[keys[i]] = properties[keys[i]];
			}

			/**
			 * ConnectionRequest hostUid.
			 * @member {ArrayBuffer} hostUid
			 * @memberof smolweb.guests.ConnectionRequest
			 * @instance
			 */
			ConnectionRequest.prototype.hostUid = $util.newBuffer([]);

			/**
			 * ConnectionRequest identity.
			 * @member {ArrayBuffer} identity
			 * @memberof smolweb.guests.ConnectionRequest
			 * @instance
			 */
			ConnectionRequest.prototype.identity = $util.newBuffer([]);

			/**
			 * ConnectionRequest secret.
			 * @member {ArrayBuffer} secret
			 * @memberof smolweb.guests.ConnectionRequest
			 * @instance
			 */
			ConnectionRequest.prototype.secret = $util.newBuffer([]);

			/**
			 * Creates a new ConnectionRequest instance using the specified properties.
			 * @function create
			 * @memberof smolweb.guests.ConnectionRequest
			 * @static
			 * @param {smolweb.guests.ConnectionRequest=} [properties] Properties to set
			 * @returns {smolweb.guests.ConnectionRequest} ConnectionRequest instance
			 */
			ConnectionRequest.create = function create(properties) {
				return new ConnectionRequest(properties);
			};

			/**
			 * Encodes the specified ConnectionRequest message. Does not implicitly {@link smolweb.guests.ConnectionRequest.verify|verify} messages.
			 * @function encode
			 * @memberof smolweb.guests.ConnectionRequest
			 * @static
			 * @param {smolweb.guests.ConnectionRequest} message ConnectionRequest message or plain object to encode
			 * @param {ArrayBufferWriter} [writer] Writer to encode to
			 * @returns {ArrayBufferWriter} Writer
			 */
			ConnectionRequest.encode = function encode(message, writer) {
				if (!writer)
					writer = $Writer.create();
				if (message.hostUid != null && Object.hasOwnProperty.call(message, "hostUid"))
					writer.uint32(/* id 1, wireType 2 =*/10).bytes(message.hostUid);
				if (message.identity != null && Object.hasOwnProperty.call(message, "identity"))
					writer.uint32(/* id 2, wireType 2 =*/18).bytes(message.identity);
				if (message.secret != null && Object.hasOwnProperty.call(message, "secret"))
					writer.uint32(/* id 3, wireType 2 =*/26).bytes(message.secret);
				return writer;
			};

			/**
			 * Decodes a ConnectionRequest message from the specified reader or buffer.
			 * @function decode
			 * @memberof smolweb.guests.ConnectionRequest
			 * @static
			 * @param {ArrayBufferReader|ArrayBuffer} reader Reader or buffer to decode from
			 * @param {number} [length] Message length if known beforehand
			 * @returns {smolweb.guests.ConnectionRequest} ConnectionRequest
			 * @throws {Error} If the payload is not a reader or valid buffer
			 * @throws {$protobuf.util.ProtocolError} If required fields are missing
			 */
			ConnectionRequest.decode = function decode(reader, length) {
				if (!(reader instanceof $Reader))
					reader = $Reader.create(reader);
				let end = length === undefined ? reader.len : reader.pos + length, message = new $root.smolweb.guests.ConnectionRequest();
				while (reader.pos < end) {
					let tag = reader.uint32();
					switch (tag >>> 3) {
					case 1: {
							message.hostUid = reader.bytes();
							break;
						}
					case 2: {
							message.identity = reader.bytes();
							break;
						}
					case 3: {
							message.secret = reader.bytes();
							break;
						}
					default:
						reader.skipType(tag & 7);
						break;
					}
				}
				return message;
			};

			/**
			 * Verifies a ConnectionRequest message.
			 * @function verify
			 * @memberof smolweb.guests.ConnectionRequest
			 * @static
			 * @param {Object.<string,*>} message Plain object to verify
			 * @returns {string|null} `null` if valid, otherwise the reason why it is not
			 */
			ConnectionRequest.verify = function verify(message) {
				if (typeof message !== "object" || message === null)
					return "object expected";
				if (message.hostUid != null && message.hasOwnProperty("hostUid"))
					if (!(message.hostUid && typeof message.hostUid.byteLength === 'number' || $util.isString(message.hostUid)))
						return "hostUid: buffer expected";
				if (message.identity != null && message.hasOwnProperty("identity"))
					if (!(message.identity && typeof message.identity.byteLength === 'number' || $util.isString(message.identity)))
						return "identity: buffer expected";
				if (message.secret != null && message.hasOwnProperty("secret"))
					if (!(message.secret && typeof message.secret.byteLength === 'number' || $util.isString(message.secret)))
						return "secret: buffer expected";
				return null;
			};

			/**
			 * Creates a ConnectionRequest message from a plain object. Also converts values to their respective internal types.
			 * @function fromObject
			 * @memberof smolweb.guests.ConnectionRequest
			 * @static
			 * @param {Object.<string,*>} object Plain object
			 * @returns {smolweb.guests.ConnectionRequest} ConnectionRequest
			 */
			ConnectionRequest.fromObject = function fromObject(object) {
				if (object instanceof $root.smolweb.guests.ConnectionRequest)
					return object;
				let message = new $root.smolweb.guests.ConnectionRequest();
				if (object.hostUid != null)
					if (typeof object.hostUid === "string")
						$util.base64.decode(object.hostUid, message.hostUid = $util.newBuffer($util.base64.length(object.hostUid)), 0);
					else if (object.hostUid.byteLength >= 0)
						message.hostUid = object.hostUid;
				if (object.identity != null)
					if (typeof object.identity === "string")
						$util.base64.decode(object.identity, message.identity = $util.newBuffer($util.base64.length(object.identity)), 0);
					else if (object.identity.byteLength >= 0)
						message.identity = object.identity;
				if (object.secret != null)
					if (typeof object.secret === "string")
						$util.base64.decode(object.secret, message.secret = $util.newBuffer($util.base64.length(object.secret)), 0);
					else if (object.secret.byteLength >= 0)
						message.secret = object.secret;
				return message;
			};

			/**
			 * Creates a plain object from a ConnectionRequest message. Also converts values to other types if specified.
			 * @function toObject
			 * @memberof smolweb.guests.ConnectionRequest
			 * @static
			 * @param {smolweb.guests.ConnectionRequest} message ConnectionRequest
			 * @param {$protobuf.IConversionOptions} [options] Conversion options
			 * @returns {Object.<string,*>} Plain object
			 */
			ConnectionRequest.toObject = function toObject(message, options) {
				if (!options)
					options = {};
				let object = {};
				if (options.defaults) {
					if (options.bytes === String)
						object.hostUid = "";
					else {
						object.hostUid = [];
						if (options.bytes !== Array)
							object.hostUid = $util.newBuffer(object.hostUid);
					}
					if (options.bytes === String)
						object.identity = "";
					else {
						object.identity = [];
						if (options.bytes !== Array)
							object.identity = $util.newBuffer(object.identity);
					}
					if (options.bytes === String)
						object.secret = "";
					else {
						object.secret = [];
						if (options.bytes !== Array)
							object.secret = $util.newBuffer(object.secret);
					}
				}
				if (message.hostUid != null && message.hasOwnProperty("hostUid"))
					object.hostUid = options.bytes === String ? $util.base64.encode(message.hostUid, 0, message.hostUid.byteLength) : options.bytes === Array ? Array.prototype.slice.call(message.hostUid) : message.hostUid;
				if (message.identity != null && message.hasOwnProperty("identity"))
					object.identity = options.bytes === String ? $util.base64.encode(message.identity, 0, message.identity.byteLength) : options.bytes === Array ? Array.prototype.slice.call(message.identity) : message.identity;
				if (message.secret != null && message.hasOwnProperty("secret"))
					object.secret = options.bytes === String ? $util.base64.encode(message.secret, 0, message.secret.byteLength) : options.bytes === Array ? Array.prototype.slice.call(message.secret) : message.secret;
				return object;
			};

			/**
			 * Converts this ConnectionRequest to JSON.
			 * @function toJSON
			 * @memberof smolweb.guests.ConnectionRequest
			 * @instance
			 * @returns {Object.<string,*>} JSON object
			 */
			ConnectionRequest.prototype.toJSON = function toJSON() {
				return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
			};

			/**
			 * Gets the default type url for ConnectionRequest
			 * @function getTypeUrl
			 * @memberof smolweb.guests.ConnectionRequest
			 * @static
			 * @param {string} [typeUrlPrefix] your custom typeUrlPrefix(default "type.cuchazinteractive.org")
			 * @returns {string} The default type url
			 */
			ConnectionRequest.getTypeUrl = function getTypeUrl(typeUrlPrefix) {
				if (typeUrlPrefix === undefined) {
					typeUrlPrefix = "type.cuchazinteractive.org";
				}
				return typeUrlPrefix + "/smolweb.guests.ConnectionRequest";
			};

			
			/**
			 * @returns {?string}
			 */		
			ConnectionRequest.prototype.verify = function () {
				const err = ConnectionRequest.verify(this);
				if (err != null) {
					throw new Error("Verification failed for ConnectionRequest: " + err);
				}
			};
			
			/**
			 * @returns {ArrayBuffer}
			 */
			ConnectionRequest.prototype.toBuffer = function () {
				return ConnectionRequest.encode(this).finish();
			};
			
			/**
			 * @param {string | number} [spacer]
			 * @returns {string}
			 */
			ConnectionRequest.prototype.toString = function (spacer) {
				return JSON.stringify(this.toJSON(), null, spacer);
			};
			
			/**
			 * @param {SaveMode} mode
			 * @returns {ArrayBuffer | string | Object}
			 */
			ConnectionRequest.prototype.to = function (mode) {
				switch (mode) {
					case SaveModes.BUF: return this.toBuffer();
					case SaveModes.STR: return this.toString();
					case SaveModes.JSON: return this.toJSON();
					default: throw new Error("unrecognized save mode: " + mode);
				}
			};
			
			/**
			 * @param {smolweb_guests_TConnectionRequest} obj
			 * @returns {smolweb.guests.ConnectionRequest}
			 */
			ConnectionRequest.new = (obj) => {
				const msg = ConnectionRequest.create(obj);
				msg.verify();
				return msg;
			};
			
			/**
			 * @param {ArrayBuffer} buf
			 * @returns {smolweb.guests.ConnectionRequest}
			 */
			ConnectionRequest.fromBuffer = (buf) => {
				const msg = ConnectionRequest.decode(buf);
				msg.verify();
				return msg;
			};
			
			/**
			 * @param {string} str
			 * @returns {smolweb.guests.ConnectionRequest}
			 */
			ConnectionRequest.fromString = (str) => {
				const msg = ConnectionRequest.fromJSON(JSON.parse(str));
				msg.verify();
				return msg;
			};
			
			/**
			 * @param {Object} json
			 * @returns {smolweb.guests.ConnectionRequest}
			 */
			ConnectionRequest.fromJSON = (obj) => {
				const msg = ConnectionRequest.fromObject(obj);
				msg.verify();
				return msg;
			};
			
			/**
			 * @param {smolweb.guests.ConnectionRequest | Object | ArrayBuffer | string} saved
			 * @returns {smolweb.guests.ConnectionRequest}
			 */
			ConnectionRequest.from = (saved) => {
				if (saved instanceof ConnectionRequest) {
					return saved;
				} else if (saved instanceof ArrayBuffer) {
					return ConnectionRequest.fromBuffer(saved);
				} else if (typeof saved === 'string') {
					return ConnectionRequest.fromString(saved);
				} else if (typeof saved === 'object') {
					return ConnectionRequest.fromJSON(saved);
				} else {
					throw new Error("Unrecognized save format: " + saved?.constructor?.name);
				}
			};
			
			/**
			 * @returns {smolweb.guests.ConnectionRequest}
			 */
			ConnectionRequest.prototype.copy = function () {
				// TODO: find more efficient way to do this?
				//   we only need to make defensive copies to prevent mutation
				//   but there's no need for a full buffer encoding/decoding roundtrip
				return this.constructor.fromBuffer(this.toBuffer());
			};
			
			/**
			 * WebStorm needs a little help understanding that this class implements ProtobufOpen
			 * @returns {ProtobufOpen.<smolweb.guests.ConnectionRequest>}
			 */
			ConnectionRequest.opener = function () {
				return this;
			};
			
			return ConnectionRequest;
			
		})();

		guests.ConnectionResponse = (function() {

			/**
			 * Properties of a ConnectionResponse.
			 * @memberof smolweb.guests
			 * @typedef {Object} smolweb_guests_TConnectionResponse
			 * @property {boolean|null} [hostUnknown] ConnectionResponse hostUnknown
			 * @property {boolean|null} [hostUnreachable] ConnectionResponse hostUnreachable
			 * @property {smolweb.guests.ConnectionResponse.Accept|null} [accept] ConnectionResponse accept
			 */

			/**
			 * Constructs a new ConnectionResponse.
			 * @memberof smolweb.guests
			 * @classdesc Represents a ConnectionResponse.
			 * @implements ProtobufSave
			 * @constructor
			 * @param {smolweb.guests.ConnectionResponse=} [properties] Properties to set
			 */
			function ConnectionResponse(properties) {
				if (properties)
					for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
						if (properties[keys[i]] != null)
							this[keys[i]] = properties[keys[i]];
			}

			/**
			 * ConnectionResponse hostUnknown.
			 * @member {boolean|null|undefined} hostUnknown
			 * @memberof smolweb.guests.ConnectionResponse
			 * @instance
			 */
			ConnectionResponse.prototype.hostUnknown = null;

			/**
			 * ConnectionResponse hostUnreachable.
			 * @member {boolean|null|undefined} hostUnreachable
			 * @memberof smolweb.guests.ConnectionResponse
			 * @instance
			 */
			ConnectionResponse.prototype.hostUnreachable = null;

			/**
			 * ConnectionResponse accept.
			 * @member {smolweb.guests.ConnectionResponse.Accept|null|undefined} accept
			 * @memberof smolweb.guests.ConnectionResponse
			 * @instance
			 */
			ConnectionResponse.prototype.accept = null;

			// OneOf field names bound to virtual getters and setters
			let $oneOfFields;

			/**
			 * ConnectionResponse response.
			 * @member {"hostUnknown"|"hostUnreachable"|"accept"|undefined} response
			 * @memberof smolweb.guests.ConnectionResponse
			 * @instance
			 */
			Object.defineProperty(ConnectionResponse.prototype, "response", {
				get: $util.oneOfGetter($oneOfFields = ["hostUnknown", "hostUnreachable", "accept"]),
				set: $util.oneOfSetter($oneOfFields)
			});

			/**
			 * Creates a new ConnectionResponse instance using the specified properties.
			 * @function create
			 * @memberof smolweb.guests.ConnectionResponse
			 * @static
			 * @param {smolweb.guests.ConnectionResponse=} [properties] Properties to set
			 * @returns {smolweb.guests.ConnectionResponse} ConnectionResponse instance
			 */
			ConnectionResponse.create = function create(properties) {
				return new ConnectionResponse(properties);
			};

			/**
			 * Encodes the specified ConnectionResponse message. Does not implicitly {@link smolweb.guests.ConnectionResponse.verify|verify} messages.
			 * @function encode
			 * @memberof smolweb.guests.ConnectionResponse
			 * @static
			 * @param {smolweb.guests.ConnectionResponse} message ConnectionResponse message or plain object to encode
			 * @param {ArrayBufferWriter} [writer] Writer to encode to
			 * @returns {ArrayBufferWriter} Writer
			 */
			ConnectionResponse.encode = function encode(message, writer) {
				if (!writer)
					writer = $Writer.create();
				if (message.hostUnknown != null && Object.hasOwnProperty.call(message, "hostUnknown"))
					writer.uint32(/* id 1, wireType 0 =*/8).bool(message.hostUnknown);
				if (message.hostUnreachable != null && Object.hasOwnProperty.call(message, "hostUnreachable"))
					writer.uint32(/* id 2, wireType 0 =*/16).bool(message.hostUnreachable);
				if (message.accept != null && Object.hasOwnProperty.call(message, "accept"))
					$root.smolweb.guests.ConnectionResponse.Accept.encode(message.accept, writer.uint32(/* id 3, wireType 2 =*/26).fork()).ldelim();
				return writer;
			};

			/**
			 * Decodes a ConnectionResponse message from the specified reader or buffer.
			 * @function decode
			 * @memberof smolweb.guests.ConnectionResponse
			 * @static
			 * @param {ArrayBufferReader|ArrayBuffer} reader Reader or buffer to decode from
			 * @param {number} [length] Message length if known beforehand
			 * @returns {smolweb.guests.ConnectionResponse} ConnectionResponse
			 * @throws {Error} If the payload is not a reader or valid buffer
			 * @throws {$protobuf.util.ProtocolError} If required fields are missing
			 */
			ConnectionResponse.decode = function decode(reader, length) {
				if (!(reader instanceof $Reader))
					reader = $Reader.create(reader);
				let end = length === undefined ? reader.len : reader.pos + length, message = new $root.smolweb.guests.ConnectionResponse();
				while (reader.pos < end) {
					let tag = reader.uint32();
					switch (tag >>> 3) {
					case 1: {
							message.hostUnknown = reader.bool();
							break;
						}
					case 2: {
							message.hostUnreachable = reader.bool();
							break;
						}
					case 3: {
							message.accept = $root.smolweb.guests.ConnectionResponse.Accept.decode(reader, reader.uint32());
							break;
						}
					default:
						reader.skipType(tag & 7);
						break;
					}
				}
				return message;
			};

			/**
			 * Verifies a ConnectionResponse message.
			 * @function verify
			 * @memberof smolweb.guests.ConnectionResponse
			 * @static
			 * @param {Object.<string,*>} message Plain object to verify
			 * @returns {string|null} `null` if valid, otherwise the reason why it is not
			 */
			ConnectionResponse.verify = function verify(message) {
				if (typeof message !== "object" || message === null)
					return "object expected";
				let properties = {};
				if (message.hostUnknown != null && message.hasOwnProperty("hostUnknown")) {
					properties.response = 1;
					if (typeof message.hostUnknown !== "boolean")
						return "hostUnknown: boolean expected";
				}
				if (message.hostUnreachable != null && message.hasOwnProperty("hostUnreachable")) {
					if (properties.response === 1)
						return "response: multiple values";
					properties.response = 1;
					if (typeof message.hostUnreachable !== "boolean")
						return "hostUnreachable: boolean expected";
				}
				if (message.accept != null && message.hasOwnProperty("accept")) {
					if (properties.response === 1)
						return "response: multiple values";
					properties.response = 1;
					{
						let error = $root.smolweb.guests.ConnectionResponse.Accept.verify(message.accept);
						if (error)
							return "accept." + error;
					}
				}
				return null;
			};

			/**
			 * Creates a ConnectionResponse message from a plain object. Also converts values to their respective internal types.
			 * @function fromObject
			 * @memberof smolweb.guests.ConnectionResponse
			 * @static
			 * @param {Object.<string,*>} object Plain object
			 * @returns {smolweb.guests.ConnectionResponse} ConnectionResponse
			 */
			ConnectionResponse.fromObject = function fromObject(object) {
				if (object instanceof $root.smolweb.guests.ConnectionResponse)
					return object;
				let message = new $root.smolweb.guests.ConnectionResponse();
				if (object.hostUnknown != null)
					message.hostUnknown = Boolean(object.hostUnknown);
				if (object.hostUnreachable != null)
					message.hostUnreachable = Boolean(object.hostUnreachable);
				if (object.accept != null) {
					if (typeof object.accept !== "object")
						throw TypeError(".smolweb.guests.ConnectionResponse.accept: object expected");
					message.accept = $root.smolweb.guests.ConnectionResponse.Accept.fromObject(object.accept);
				}
				return message;
			};

			/**
			 * Creates a plain object from a ConnectionResponse message. Also converts values to other types if specified.
			 * @function toObject
			 * @memberof smolweb.guests.ConnectionResponse
			 * @static
			 * @param {smolweb.guests.ConnectionResponse} message ConnectionResponse
			 * @param {$protobuf.IConversionOptions} [options] Conversion options
			 * @returns {Object.<string,*>} Plain object
			 */
			ConnectionResponse.toObject = function toObject(message, options) {
				if (!options)
					options = {};
				let object = {};
				if (message.hostUnknown != null && message.hasOwnProperty("hostUnknown")) {
					object.hostUnknown = message.hostUnknown;
					if (options.oneofs)
						object.response = "hostUnknown";
				}
				if (message.hostUnreachable != null && message.hasOwnProperty("hostUnreachable")) {
					object.hostUnreachable = message.hostUnreachable;
					if (options.oneofs)
						object.response = "hostUnreachable";
				}
				if (message.accept != null && message.hasOwnProperty("accept")) {
					object.accept = $root.smolweb.guests.ConnectionResponse.Accept.toObject(message.accept, options);
					if (options.oneofs)
						object.response = "accept";
				}
				return object;
			};

			/**
			 * Converts this ConnectionResponse to JSON.
			 * @function toJSON
			 * @memberof smolweb.guests.ConnectionResponse
			 * @instance
			 * @returns {Object.<string,*>} JSON object
			 */
			ConnectionResponse.prototype.toJSON = function toJSON() {
				return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
			};

			/**
			 * Gets the default type url for ConnectionResponse
			 * @function getTypeUrl
			 * @memberof smolweb.guests.ConnectionResponse
			 * @static
			 * @param {string} [typeUrlPrefix] your custom typeUrlPrefix(default "type.cuchazinteractive.org")
			 * @returns {string} The default type url
			 */
			ConnectionResponse.getTypeUrl = function getTypeUrl(typeUrlPrefix) {
				if (typeUrlPrefix === undefined) {
					typeUrlPrefix = "type.cuchazinteractive.org";
				}
				return typeUrlPrefix + "/smolweb.guests.ConnectionResponse";
			};

			ConnectionResponse.Accept = (function() {

				/**
				 * Properties of an Accept.
				 * @memberof smolweb.guests.ConnectionResponse
				 * @typedef {Object} smolweb_guests_ConnectionResponse_TAccept
				 * @property {ArrayBuffer|null} [secret] Accept secret
				 */

				/**
				 * Constructs a new Accept.
				 * @memberof smolweb.guests.ConnectionResponse
				 * @classdesc Represents an Accept.
				 * @implements ProtobufSave
				 * @constructor
				 * @param {smolweb.guests.ConnectionResponse.Accept=} [properties] Properties to set
				 */
				function Accept(properties) {
					if (properties)
						for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
							if (properties[keys[i]] != null)
								this[keys[i]] = properties[keys[i]];
				}

				/**
				 * Accept secret.
				 * @member {ArrayBuffer} secret
				 * @memberof smolweb.guests.ConnectionResponse.Accept
				 * @instance
				 */
				Accept.prototype.secret = $util.newBuffer([]);

				/**
				 * Creates a new Accept instance using the specified properties.
				 * @function create
				 * @memberof smolweb.guests.ConnectionResponse.Accept
				 * @static
				 * @param {smolweb.guests.ConnectionResponse.Accept=} [properties] Properties to set
				 * @returns {smolweb.guests.ConnectionResponse.Accept} Accept instance
				 */
				Accept.create = function create(properties) {
					return new Accept(properties);
				};

				/**
				 * Encodes the specified Accept message. Does not implicitly {@link smolweb.guests.ConnectionResponse.Accept.verify|verify} messages.
				 * @function encode
				 * @memberof smolweb.guests.ConnectionResponse.Accept
				 * @static
				 * @param {smolweb.guests.ConnectionResponse.Accept} message Accept message or plain object to encode
				 * @param {ArrayBufferWriter} [writer] Writer to encode to
				 * @returns {ArrayBufferWriter} Writer
				 */
				Accept.encode = function encode(message, writer) {
					if (!writer)
						writer = $Writer.create();
					if (message.secret != null && Object.hasOwnProperty.call(message, "secret"))
						writer.uint32(/* id 1, wireType 2 =*/10).bytes(message.secret);
					return writer;
				};

				/**
				 * Decodes an Accept message from the specified reader or buffer.
				 * @function decode
				 * @memberof smolweb.guests.ConnectionResponse.Accept
				 * @static
				 * @param {ArrayBufferReader|ArrayBuffer} reader Reader or buffer to decode from
				 * @param {number} [length] Message length if known beforehand
				 * @returns {smolweb.guests.ConnectionResponse.Accept} Accept
				 * @throws {Error} If the payload is not a reader or valid buffer
				 * @throws {$protobuf.util.ProtocolError} If required fields are missing
				 */
				Accept.decode = function decode(reader, length) {
					if (!(reader instanceof $Reader))
						reader = $Reader.create(reader);
					let end = length === undefined ? reader.len : reader.pos + length, message = new $root.smolweb.guests.ConnectionResponse.Accept();
					while (reader.pos < end) {
						let tag = reader.uint32();
						switch (tag >>> 3) {
						case 1: {
								message.secret = reader.bytes();
								break;
							}
						default:
							reader.skipType(tag & 7);
							break;
						}
					}
					return message;
				};

				/**
				 * Verifies an Accept message.
				 * @function verify
				 * @memberof smolweb.guests.ConnectionResponse.Accept
				 * @static
				 * @param {Object.<string,*>} message Plain object to verify
				 * @returns {string|null} `null` if valid, otherwise the reason why it is not
				 */
				Accept.verify = function verify(message) {
					if (typeof message !== "object" || message === null)
						return "object expected";
					if (message.secret != null && message.hasOwnProperty("secret"))
						if (!(message.secret && typeof message.secret.byteLength === 'number' || $util.isString(message.secret)))
							return "secret: buffer expected";
					return null;
				};

				/**
				 * Creates an Accept message from a plain object. Also converts values to their respective internal types.
				 * @function fromObject
				 * @memberof smolweb.guests.ConnectionResponse.Accept
				 * @static
				 * @param {Object.<string,*>} object Plain object
				 * @returns {smolweb.guests.ConnectionResponse.Accept} Accept
				 */
				Accept.fromObject = function fromObject(object) {
					if (object instanceof $root.smolweb.guests.ConnectionResponse.Accept)
						return object;
					let message = new $root.smolweb.guests.ConnectionResponse.Accept();
					if (object.secret != null)
						if (typeof object.secret === "string")
							$util.base64.decode(object.secret, message.secret = $util.newBuffer($util.base64.length(object.secret)), 0);
						else if (object.secret.byteLength >= 0)
							message.secret = object.secret;
					return message;
				};

				/**
				 * Creates a plain object from an Accept message. Also converts values to other types if specified.
				 * @function toObject
				 * @memberof smolweb.guests.ConnectionResponse.Accept
				 * @static
				 * @param {smolweb.guests.ConnectionResponse.Accept} message Accept
				 * @param {$protobuf.IConversionOptions} [options] Conversion options
				 * @returns {Object.<string,*>} Plain object
				 */
				Accept.toObject = function toObject(message, options) {
					if (!options)
						options = {};
					let object = {};
					if (options.defaults)
						if (options.bytes === String)
							object.secret = "";
						else {
							object.secret = [];
							if (options.bytes !== Array)
								object.secret = $util.newBuffer(object.secret);
						}
					if (message.secret != null && message.hasOwnProperty("secret"))
						object.secret = options.bytes === String ? $util.base64.encode(message.secret, 0, message.secret.byteLength) : options.bytes === Array ? Array.prototype.slice.call(message.secret) : message.secret;
					return object;
				};

				/**
				 * Converts this Accept to JSON.
				 * @function toJSON
				 * @memberof smolweb.guests.ConnectionResponse.Accept
				 * @instance
				 * @returns {Object.<string,*>} JSON object
				 */
				Accept.prototype.toJSON = function toJSON() {
					return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
				};

				/**
				 * Gets the default type url for Accept
				 * @function getTypeUrl
				 * @memberof smolweb.guests.ConnectionResponse.Accept
				 * @static
				 * @param {string} [typeUrlPrefix] your custom typeUrlPrefix(default "type.cuchazinteractive.org")
				 * @returns {string} The default type url
				 */
				Accept.getTypeUrl = function getTypeUrl(typeUrlPrefix) {
					if (typeUrlPrefix === undefined) {
						typeUrlPrefix = "type.cuchazinteractive.org";
					}
					return typeUrlPrefix + "/smolweb.guests.ConnectionResponse.Accept";
				};

				
				/**
				 * @returns {?string}
				 */		
				Accept.prototype.verify = function () {
					const err = Accept.verify(this);
					if (err != null) {
						throw new Error("Verification failed for Accept: " + err);
					}
				};
				
				/**
				 * @returns {ArrayBuffer}
				 */
				Accept.prototype.toBuffer = function () {
					return Accept.encode(this).finish();
				};
				
				/**
				 * @param {string | number} [spacer]
				 * @returns {string}
				 */
				Accept.prototype.toString = function (spacer) {
					return JSON.stringify(this.toJSON(), null, spacer);
				};
				
				/**
				 * @param {SaveMode} mode
				 * @returns {ArrayBuffer | string | Object}
				 */
				Accept.prototype.to = function (mode) {
					switch (mode) {
						case SaveModes.BUF: return this.toBuffer();
						case SaveModes.STR: return this.toString();
						case SaveModes.JSON: return this.toJSON();
						default: throw new Error("unrecognized save mode: " + mode);
					}
				};
				
				/**
				 * @param {smolweb_guests_ConnectionResponse_TAccept} obj
				 * @returns {smolweb.guests.ConnectionResponse.Accept}
				 */
				Accept.new = (obj) => {
					const msg = Accept.create(obj);
					msg.verify();
					return msg;
				};
				
				/**
				 * @param {ArrayBuffer} buf
				 * @returns {smolweb.guests.ConnectionResponse.Accept}
				 */
				Accept.fromBuffer = (buf) => {
					const msg = Accept.decode(buf);
					msg.verify();
					return msg;
				};
				
				/**
				 * @param {string} str
				 * @returns {smolweb.guests.ConnectionResponse.Accept}
				 */
				Accept.fromString = (str) => {
					const msg = Accept.fromJSON(JSON.parse(str));
					msg.verify();
					return msg;
				};
				
				/**
				 * @param {Object} json
				 * @returns {smolweb.guests.ConnectionResponse.Accept}
				 */
				Accept.fromJSON = (obj) => {
					const msg = Accept.fromObject(obj);
					msg.verify();
					return msg;
				};
				
				/**
				 * @param {smolweb.guests.ConnectionResponse.Accept | Object | ArrayBuffer | string} saved
				 * @returns {smolweb.guests.ConnectionResponse.Accept}
				 */
				Accept.from = (saved) => {
					if (saved instanceof Accept) {
						return saved;
					} else if (saved instanceof ArrayBuffer) {
						return Accept.fromBuffer(saved);
					} else if (typeof saved === 'string') {
						return Accept.fromString(saved);
					} else if (typeof saved === 'object') {
						return Accept.fromJSON(saved);
					} else {
						throw new Error("Unrecognized save format: " + saved?.constructor?.name);
					}
				};
				
				/**
				 * @returns {smolweb.guests.ConnectionResponse.Accept}
				 */
				Accept.prototype.copy = function () {
					// TODO: find more efficient way to do this?
					//   we only need to make defensive copies to prevent mutation
					//   but there's no need for a full buffer encoding/decoding roundtrip
					return this.constructor.fromBuffer(this.toBuffer());
				};
				
				/**
				 * WebStorm needs a little help understanding that this class implements ProtobufOpen
				 * @returns {ProtobufOpen.<smolweb.guests.ConnectionResponse.Accept>}
				 */
				Accept.opener = function () {
					return this;
				};
				
				return Accept;
				
			})();

			
			/**
			 * @returns {?string}
			 */		
			ConnectionResponse.prototype.verify = function () {
				const err = ConnectionResponse.verify(this);
				if (err != null) {
					throw new Error("Verification failed for ConnectionResponse: " + err);
				}
			};
			
			/**
			 * @returns {ArrayBuffer}
			 */
			ConnectionResponse.prototype.toBuffer = function () {
				return ConnectionResponse.encode(this).finish();
			};
			
			/**
			 * @param {string | number} [spacer]
			 * @returns {string}
			 */
			ConnectionResponse.prototype.toString = function (spacer) {
				return JSON.stringify(this.toJSON(), null, spacer);
			};
			
			/**
			 * @param {SaveMode} mode
			 * @returns {ArrayBuffer | string | Object}
			 */
			ConnectionResponse.prototype.to = function (mode) {
				switch (mode) {
					case SaveModes.BUF: return this.toBuffer();
					case SaveModes.STR: return this.toString();
					case SaveModes.JSON: return this.toJSON();
					default: throw new Error("unrecognized save mode: " + mode);
				}
			};
			
			/**
			 * @param {smolweb_guests_TConnectionResponse} obj
			 * @returns {smolweb.guests.ConnectionResponse}
			 */
			ConnectionResponse.new = (obj) => {
				const msg = ConnectionResponse.create(obj);
				msg.verify();
				return msg;
			};
			
			/**
			 * @param {ArrayBuffer} buf
			 * @returns {smolweb.guests.ConnectionResponse}
			 */
			ConnectionResponse.fromBuffer = (buf) => {
				const msg = ConnectionResponse.decode(buf);
				msg.verify();
				return msg;
			};
			
			/**
			 * @param {string} str
			 * @returns {smolweb.guests.ConnectionResponse}
			 */
			ConnectionResponse.fromString = (str) => {
				const msg = ConnectionResponse.fromJSON(JSON.parse(str));
				msg.verify();
				return msg;
			};
			
			/**
			 * @param {Object} json
			 * @returns {smolweb.guests.ConnectionResponse}
			 */
			ConnectionResponse.fromJSON = (obj) => {
				const msg = ConnectionResponse.fromObject(obj);
				msg.verify();
				return msg;
			};
			
			/**
			 * @param {smolweb.guests.ConnectionResponse | Object | ArrayBuffer | string} saved
			 * @returns {smolweb.guests.ConnectionResponse}
			 */
			ConnectionResponse.from = (saved) => {
				if (saved instanceof ConnectionResponse) {
					return saved;
				} else if (saved instanceof ArrayBuffer) {
					return ConnectionResponse.fromBuffer(saved);
				} else if (typeof saved === 'string') {
					return ConnectionResponse.fromString(saved);
				} else if (typeof saved === 'object') {
					return ConnectionResponse.fromJSON(saved);
				} else {
					throw new Error("Unrecognized save format: " + saved?.constructor?.name);
				}
			};
			
			/**
			 * @returns {smolweb.guests.ConnectionResponse}
			 */
			ConnectionResponse.prototype.copy = function () {
				// TODO: find more efficient way to do this?
				//   we only need to make defensive copies to prevent mutation
				//   but there's no need for a full buffer encoding/decoding roundtrip
				return this.constructor.fromBuffer(this.toBuffer());
			};
			
			/**
			 * WebStorm needs a little help understanding that this class implements ProtobufOpen
			 * @returns {ProtobufOpen.<smolweb.guests.ConnectionResponse>}
			 */
			ConnectionResponse.opener = function () {
				return this;
			};
			
			return ConnectionResponse;
			
		})();

		guests.ConnectionConfirm = (function() {

			/**
			 * Properties of a ConnectionConfirm.
			 * @memberof smolweb.guests
			 * @typedef {Object} smolweb_guests_TConnectionConfirm
			 * @property {ArrayBuffer|null} [hostUid] ConnectionConfirm hostUid
			 * @property {ArrayBuffer|null} [guestUid] ConnectionConfirm guestUid
			 * @property {ArrayBuffer|null} [secret] ConnectionConfirm secret
			 */

			/**
			 * Constructs a new ConnectionConfirm.
			 * @memberof smolweb.guests
			 * @classdesc Represents a ConnectionConfirm.
			 * @implements ProtobufSave
			 * @constructor
			 * @param {smolweb.guests.ConnectionConfirm=} [properties] Properties to set
			 */
			function ConnectionConfirm(properties) {
				if (properties)
					for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
						if (properties[keys[i]] != null)
							this[keys[i]] = properties[keys[i]];
			}

			/**
			 * ConnectionConfirm hostUid.
			 * @member {ArrayBuffer} hostUid
			 * @memberof smolweb.guests.ConnectionConfirm
			 * @instance
			 */
			ConnectionConfirm.prototype.hostUid = $util.newBuffer([]);

			/**
			 * ConnectionConfirm guestUid.
			 * @member {ArrayBuffer} guestUid
			 * @memberof smolweb.guests.ConnectionConfirm
			 * @instance
			 */
			ConnectionConfirm.prototype.guestUid = $util.newBuffer([]);

			/**
			 * ConnectionConfirm secret.
			 * @member {ArrayBuffer} secret
			 * @memberof smolweb.guests.ConnectionConfirm
			 * @instance
			 */
			ConnectionConfirm.prototype.secret = $util.newBuffer([]);

			/**
			 * Creates a new ConnectionConfirm instance using the specified properties.
			 * @function create
			 * @memberof smolweb.guests.ConnectionConfirm
			 * @static
			 * @param {smolweb.guests.ConnectionConfirm=} [properties] Properties to set
			 * @returns {smolweb.guests.ConnectionConfirm} ConnectionConfirm instance
			 */
			ConnectionConfirm.create = function create(properties) {
				return new ConnectionConfirm(properties);
			};

			/**
			 * Encodes the specified ConnectionConfirm message. Does not implicitly {@link smolweb.guests.ConnectionConfirm.verify|verify} messages.
			 * @function encode
			 * @memberof smolweb.guests.ConnectionConfirm
			 * @static
			 * @param {smolweb.guests.ConnectionConfirm} message ConnectionConfirm message or plain object to encode
			 * @param {ArrayBufferWriter} [writer] Writer to encode to
			 * @returns {ArrayBufferWriter} Writer
			 */
			ConnectionConfirm.encode = function encode(message, writer) {
				if (!writer)
					writer = $Writer.create();
				if (message.hostUid != null && Object.hasOwnProperty.call(message, "hostUid"))
					writer.uint32(/* id 1, wireType 2 =*/10).bytes(message.hostUid);
				if (message.guestUid != null && Object.hasOwnProperty.call(message, "guestUid"))
					writer.uint32(/* id 2, wireType 2 =*/18).bytes(message.guestUid);
				if (message.secret != null && Object.hasOwnProperty.call(message, "secret"))
					writer.uint32(/* id 3, wireType 2 =*/26).bytes(message.secret);
				return writer;
			};

			/**
			 * Decodes a ConnectionConfirm message from the specified reader or buffer.
			 * @function decode
			 * @memberof smolweb.guests.ConnectionConfirm
			 * @static
			 * @param {ArrayBufferReader|ArrayBuffer} reader Reader or buffer to decode from
			 * @param {number} [length] Message length if known beforehand
			 * @returns {smolweb.guests.ConnectionConfirm} ConnectionConfirm
			 * @throws {Error} If the payload is not a reader or valid buffer
			 * @throws {$protobuf.util.ProtocolError} If required fields are missing
			 */
			ConnectionConfirm.decode = function decode(reader, length) {
				if (!(reader instanceof $Reader))
					reader = $Reader.create(reader);
				let end = length === undefined ? reader.len : reader.pos + length, message = new $root.smolweb.guests.ConnectionConfirm();
				while (reader.pos < end) {
					let tag = reader.uint32();
					switch (tag >>> 3) {
					case 1: {
							message.hostUid = reader.bytes();
							break;
						}
					case 2: {
							message.guestUid = reader.bytes();
							break;
						}
					case 3: {
							message.secret = reader.bytes();
							break;
						}
					default:
						reader.skipType(tag & 7);
						break;
					}
				}
				return message;
			};

			/**
			 * Verifies a ConnectionConfirm message.
			 * @function verify
			 * @memberof smolweb.guests.ConnectionConfirm
			 * @static
			 * @param {Object.<string,*>} message Plain object to verify
			 * @returns {string|null} `null` if valid, otherwise the reason why it is not
			 */
			ConnectionConfirm.verify = function verify(message) {
				if (typeof message !== "object" || message === null)
					return "object expected";
				if (message.hostUid != null && message.hasOwnProperty("hostUid"))
					if (!(message.hostUid && typeof message.hostUid.byteLength === 'number' || $util.isString(message.hostUid)))
						return "hostUid: buffer expected";
				if (message.guestUid != null && message.hasOwnProperty("guestUid"))
					if (!(message.guestUid && typeof message.guestUid.byteLength === 'number' || $util.isString(message.guestUid)))
						return "guestUid: buffer expected";
				if (message.secret != null && message.hasOwnProperty("secret"))
					if (!(message.secret && typeof message.secret.byteLength === 'number' || $util.isString(message.secret)))
						return "secret: buffer expected";
				return null;
			};

			/**
			 * Creates a ConnectionConfirm message from a plain object. Also converts values to their respective internal types.
			 * @function fromObject
			 * @memberof smolweb.guests.ConnectionConfirm
			 * @static
			 * @param {Object.<string,*>} object Plain object
			 * @returns {smolweb.guests.ConnectionConfirm} ConnectionConfirm
			 */
			ConnectionConfirm.fromObject = function fromObject(object) {
				if (object instanceof $root.smolweb.guests.ConnectionConfirm)
					return object;
				let message = new $root.smolweb.guests.ConnectionConfirm();
				if (object.hostUid != null)
					if (typeof object.hostUid === "string")
						$util.base64.decode(object.hostUid, message.hostUid = $util.newBuffer($util.base64.length(object.hostUid)), 0);
					else if (object.hostUid.byteLength >= 0)
						message.hostUid = object.hostUid;
				if (object.guestUid != null)
					if (typeof object.guestUid === "string")
						$util.base64.decode(object.guestUid, message.guestUid = $util.newBuffer($util.base64.length(object.guestUid)), 0);
					else if (object.guestUid.byteLength >= 0)
						message.guestUid = object.guestUid;
				if (object.secret != null)
					if (typeof object.secret === "string")
						$util.base64.decode(object.secret, message.secret = $util.newBuffer($util.base64.length(object.secret)), 0);
					else if (object.secret.byteLength >= 0)
						message.secret = object.secret;
				return message;
			};

			/**
			 * Creates a plain object from a ConnectionConfirm message. Also converts values to other types if specified.
			 * @function toObject
			 * @memberof smolweb.guests.ConnectionConfirm
			 * @static
			 * @param {smolweb.guests.ConnectionConfirm} message ConnectionConfirm
			 * @param {$protobuf.IConversionOptions} [options] Conversion options
			 * @returns {Object.<string,*>} Plain object
			 */
			ConnectionConfirm.toObject = function toObject(message, options) {
				if (!options)
					options = {};
				let object = {};
				if (options.defaults) {
					if (options.bytes === String)
						object.hostUid = "";
					else {
						object.hostUid = [];
						if (options.bytes !== Array)
							object.hostUid = $util.newBuffer(object.hostUid);
					}
					if (options.bytes === String)
						object.guestUid = "";
					else {
						object.guestUid = [];
						if (options.bytes !== Array)
							object.guestUid = $util.newBuffer(object.guestUid);
					}
					if (options.bytes === String)
						object.secret = "";
					else {
						object.secret = [];
						if (options.bytes !== Array)
							object.secret = $util.newBuffer(object.secret);
					}
				}
				if (message.hostUid != null && message.hasOwnProperty("hostUid"))
					object.hostUid = options.bytes === String ? $util.base64.encode(message.hostUid, 0, message.hostUid.byteLength) : options.bytes === Array ? Array.prototype.slice.call(message.hostUid) : message.hostUid;
				if (message.guestUid != null && message.hasOwnProperty("guestUid"))
					object.guestUid = options.bytes === String ? $util.base64.encode(message.guestUid, 0, message.guestUid.byteLength) : options.bytes === Array ? Array.prototype.slice.call(message.guestUid) : message.guestUid;
				if (message.secret != null && message.hasOwnProperty("secret"))
					object.secret = options.bytes === String ? $util.base64.encode(message.secret, 0, message.secret.byteLength) : options.bytes === Array ? Array.prototype.slice.call(message.secret) : message.secret;
				return object;
			};

			/**
			 * Converts this ConnectionConfirm to JSON.
			 * @function toJSON
			 * @memberof smolweb.guests.ConnectionConfirm
			 * @instance
			 * @returns {Object.<string,*>} JSON object
			 */
			ConnectionConfirm.prototype.toJSON = function toJSON() {
				return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
			};

			/**
			 * Gets the default type url for ConnectionConfirm
			 * @function getTypeUrl
			 * @memberof smolweb.guests.ConnectionConfirm
			 * @static
			 * @param {string} [typeUrlPrefix] your custom typeUrlPrefix(default "type.cuchazinteractive.org")
			 * @returns {string} The default type url
			 */
			ConnectionConfirm.getTypeUrl = function getTypeUrl(typeUrlPrefix) {
				if (typeUrlPrefix === undefined) {
					typeUrlPrefix = "type.cuchazinteractive.org";
				}
				return typeUrlPrefix + "/smolweb.guests.ConnectionConfirm";
			};

			
			/**
			 * @returns {?string}
			 */		
			ConnectionConfirm.prototype.verify = function () {
				const err = ConnectionConfirm.verify(this);
				if (err != null) {
					throw new Error("Verification failed for ConnectionConfirm: " + err);
				}
			};
			
			/**
			 * @returns {ArrayBuffer}
			 */
			ConnectionConfirm.prototype.toBuffer = function () {
				return ConnectionConfirm.encode(this).finish();
			};
			
			/**
			 * @param {string | number} [spacer]
			 * @returns {string}
			 */
			ConnectionConfirm.prototype.toString = function (spacer) {
				return JSON.stringify(this.toJSON(), null, spacer);
			};
			
			/**
			 * @param {SaveMode} mode
			 * @returns {ArrayBuffer | string | Object}
			 */
			ConnectionConfirm.prototype.to = function (mode) {
				switch (mode) {
					case SaveModes.BUF: return this.toBuffer();
					case SaveModes.STR: return this.toString();
					case SaveModes.JSON: return this.toJSON();
					default: throw new Error("unrecognized save mode: " + mode);
				}
			};
			
			/**
			 * @param {smolweb_guests_TConnectionConfirm} obj
			 * @returns {smolweb.guests.ConnectionConfirm}
			 */
			ConnectionConfirm.new = (obj) => {
				const msg = ConnectionConfirm.create(obj);
				msg.verify();
				return msg;
			};
			
			/**
			 * @param {ArrayBuffer} buf
			 * @returns {smolweb.guests.ConnectionConfirm}
			 */
			ConnectionConfirm.fromBuffer = (buf) => {
				const msg = ConnectionConfirm.decode(buf);
				msg.verify();
				return msg;
			};
			
			/**
			 * @param {string} str
			 * @returns {smolweb.guests.ConnectionConfirm}
			 */
			ConnectionConfirm.fromString = (str) => {
				const msg = ConnectionConfirm.fromJSON(JSON.parse(str));
				msg.verify();
				return msg;
			};
			
			/**
			 * @param {Object} json
			 * @returns {smolweb.guests.ConnectionConfirm}
			 */
			ConnectionConfirm.fromJSON = (obj) => {
				const msg = ConnectionConfirm.fromObject(obj);
				msg.verify();
				return msg;
			};
			
			/**
			 * @param {smolweb.guests.ConnectionConfirm | Object | ArrayBuffer | string} saved
			 * @returns {smolweb.guests.ConnectionConfirm}
			 */
			ConnectionConfirm.from = (saved) => {
				if (saved instanceof ConnectionConfirm) {
					return saved;
				} else if (saved instanceof ArrayBuffer) {
					return ConnectionConfirm.fromBuffer(saved);
				} else if (typeof saved === 'string') {
					return ConnectionConfirm.fromString(saved);
				} else if (typeof saved === 'object') {
					return ConnectionConfirm.fromJSON(saved);
				} else {
					throw new Error("Unrecognized save format: " + saved?.constructor?.name);
				}
			};
			
			/**
			 * @returns {smolweb.guests.ConnectionConfirm}
			 */
			ConnectionConfirm.prototype.copy = function () {
				// TODO: find more efficient way to do this?
				//   we only need to make defensive copies to prevent mutation
				//   but there's no need for a full buffer encoding/decoding roundtrip
				return this.constructor.fromBuffer(this.toBuffer());
			};
			
			/**
			 * WebStorm needs a little help understanding that this class implements ProtobufOpen
			 * @returns {ProtobufOpen.<smolweb.guests.ConnectionConfirm>}
			 */
			ConnectionConfirm.opener = function () {
				return this;
			};
			
			return ConnectionConfirm;
			
		})();

		return guests;
	})();

	smolweb.secrets = (function() {

		/**
		 * Namespace secrets.
		 * @memberof smolweb
		 * @namespace
		 */
		const secrets = {};

		secrets.ConnectionRequestSecret = (function() {

			/**
			 * Properties of a ConnectionRequestSecret.
			 * @memberof smolweb.secrets
			 * @typedef {Object} smolweb_secrets_TConnectionRequestSecret
			 * @property {string|null} [offer1] ConnectionRequestSecret offer1
			 */

			/**
			 * Constructs a new ConnectionRequestSecret.
			 * @memberof smolweb.secrets
			 * @classdesc Represents a ConnectionRequestSecret.
			 * @implements ProtobufSave
			 * @constructor
			 * @param {smolweb.secrets.ConnectionRequestSecret=} [properties] Properties to set
			 */
			function ConnectionRequestSecret(properties) {
				if (properties)
					for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
						if (properties[keys[i]] != null)
							this[keys[i]] = properties[keys[i]];
			}

			/**
			 * ConnectionRequestSecret offer1.
			 * @member {string} offer1
			 * @memberof smolweb.secrets.ConnectionRequestSecret
			 * @instance
			 */
			ConnectionRequestSecret.prototype.offer1 = "";

			/**
			 * Creates a new ConnectionRequestSecret instance using the specified properties.
			 * @function create
			 * @memberof smolweb.secrets.ConnectionRequestSecret
			 * @static
			 * @param {smolweb.secrets.ConnectionRequestSecret=} [properties] Properties to set
			 * @returns {smolweb.secrets.ConnectionRequestSecret} ConnectionRequestSecret instance
			 */
			ConnectionRequestSecret.create = function create(properties) {
				return new ConnectionRequestSecret(properties);
			};

			/**
			 * Encodes the specified ConnectionRequestSecret message. Does not implicitly {@link smolweb.secrets.ConnectionRequestSecret.verify|verify} messages.
			 * @function encode
			 * @memberof smolweb.secrets.ConnectionRequestSecret
			 * @static
			 * @param {smolweb.secrets.ConnectionRequestSecret} message ConnectionRequestSecret message or plain object to encode
			 * @param {ArrayBufferWriter} [writer] Writer to encode to
			 * @returns {ArrayBufferWriter} Writer
			 */
			ConnectionRequestSecret.encode = function encode(message, writer) {
				if (!writer)
					writer = $Writer.create();
				if (message.offer1 != null && Object.hasOwnProperty.call(message, "offer1"))
					writer.uint32(/* id 1, wireType 2 =*/10).string(message.offer1);
				return writer;
			};

			/**
			 * Decodes a ConnectionRequestSecret message from the specified reader or buffer.
			 * @function decode
			 * @memberof smolweb.secrets.ConnectionRequestSecret
			 * @static
			 * @param {ArrayBufferReader|ArrayBuffer} reader Reader or buffer to decode from
			 * @param {number} [length] Message length if known beforehand
			 * @returns {smolweb.secrets.ConnectionRequestSecret} ConnectionRequestSecret
			 * @throws {Error} If the payload is not a reader or valid buffer
			 * @throws {$protobuf.util.ProtocolError} If required fields are missing
			 */
			ConnectionRequestSecret.decode = function decode(reader, length) {
				if (!(reader instanceof $Reader))
					reader = $Reader.create(reader);
				let end = length === undefined ? reader.len : reader.pos + length, message = new $root.smolweb.secrets.ConnectionRequestSecret();
				while (reader.pos < end) {
					let tag = reader.uint32();
					switch (tag >>> 3) {
					case 1: {
							message.offer1 = reader.string();
							break;
						}
					default:
						reader.skipType(tag & 7);
						break;
					}
				}
				return message;
			};

			/**
			 * Verifies a ConnectionRequestSecret message.
			 * @function verify
			 * @memberof smolweb.secrets.ConnectionRequestSecret
			 * @static
			 * @param {Object.<string,*>} message Plain object to verify
			 * @returns {string|null} `null` if valid, otherwise the reason why it is not
			 */
			ConnectionRequestSecret.verify = function verify(message) {
				if (typeof message !== "object" || message === null)
					return "object expected";
				if (message.offer1 != null && message.hasOwnProperty("offer1"))
					if (!$util.isString(message.offer1))
						return "offer1: string expected";
				return null;
			};

			/**
			 * Creates a ConnectionRequestSecret message from a plain object. Also converts values to their respective internal types.
			 * @function fromObject
			 * @memberof smolweb.secrets.ConnectionRequestSecret
			 * @static
			 * @param {Object.<string,*>} object Plain object
			 * @returns {smolweb.secrets.ConnectionRequestSecret} ConnectionRequestSecret
			 */
			ConnectionRequestSecret.fromObject = function fromObject(object) {
				if (object instanceof $root.smolweb.secrets.ConnectionRequestSecret)
					return object;
				let message = new $root.smolweb.secrets.ConnectionRequestSecret();
				if (object.offer1 != null)
					message.offer1 = String(object.offer1);
				return message;
			};

			/**
			 * Creates a plain object from a ConnectionRequestSecret message. Also converts values to other types if specified.
			 * @function toObject
			 * @memberof smolweb.secrets.ConnectionRequestSecret
			 * @static
			 * @param {smolweb.secrets.ConnectionRequestSecret} message ConnectionRequestSecret
			 * @param {$protobuf.IConversionOptions} [options] Conversion options
			 * @returns {Object.<string,*>} Plain object
			 */
			ConnectionRequestSecret.toObject = function toObject(message, options) {
				if (!options)
					options = {};
				let object = {};
				if (options.defaults)
					object.offer1 = "";
				if (message.offer1 != null && message.hasOwnProperty("offer1"))
					object.offer1 = message.offer1;
				return object;
			};

			/**
			 * Converts this ConnectionRequestSecret to JSON.
			 * @function toJSON
			 * @memberof smolweb.secrets.ConnectionRequestSecret
			 * @instance
			 * @returns {Object.<string,*>} JSON object
			 */
			ConnectionRequestSecret.prototype.toJSON = function toJSON() {
				return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
			};

			/**
			 * Gets the default type url for ConnectionRequestSecret
			 * @function getTypeUrl
			 * @memberof smolweb.secrets.ConnectionRequestSecret
			 * @static
			 * @param {string} [typeUrlPrefix] your custom typeUrlPrefix(default "type.cuchazinteractive.org")
			 * @returns {string} The default type url
			 */
			ConnectionRequestSecret.getTypeUrl = function getTypeUrl(typeUrlPrefix) {
				if (typeUrlPrefix === undefined) {
					typeUrlPrefix = "type.cuchazinteractive.org";
				}
				return typeUrlPrefix + "/smolweb.secrets.ConnectionRequestSecret";
			};

			
			/**
			 * @returns {?string}
			 */		
			ConnectionRequestSecret.prototype.verify = function () {
				const err = ConnectionRequestSecret.verify(this);
				if (err != null) {
					throw new Error("Verification failed for ConnectionRequestSecret: " + err);
				}
			};
			
			/**
			 * @returns {ArrayBuffer}
			 */
			ConnectionRequestSecret.prototype.toBuffer = function () {
				return ConnectionRequestSecret.encode(this).finish();
			};
			
			/**
			 * @param {string | number} [spacer]
			 * @returns {string}
			 */
			ConnectionRequestSecret.prototype.toString = function (spacer) {
				return JSON.stringify(this.toJSON(), null, spacer);
			};
			
			/**
			 * @param {SaveMode} mode
			 * @returns {ArrayBuffer | string | Object}
			 */
			ConnectionRequestSecret.prototype.to = function (mode) {
				switch (mode) {
					case SaveModes.BUF: return this.toBuffer();
					case SaveModes.STR: return this.toString();
					case SaveModes.JSON: return this.toJSON();
					default: throw new Error("unrecognized save mode: " + mode);
				}
			};
			
			/**
			 * @param {smolweb_secrets_TConnectionRequestSecret} obj
			 * @returns {smolweb.secrets.ConnectionRequestSecret}
			 */
			ConnectionRequestSecret.new = (obj) => {
				const msg = ConnectionRequestSecret.create(obj);
				msg.verify();
				return msg;
			};
			
			/**
			 * @param {ArrayBuffer} buf
			 * @returns {smolweb.secrets.ConnectionRequestSecret}
			 */
			ConnectionRequestSecret.fromBuffer = (buf) => {
				const msg = ConnectionRequestSecret.decode(buf);
				msg.verify();
				return msg;
			};
			
			/**
			 * @param {string} str
			 * @returns {smolweb.secrets.ConnectionRequestSecret}
			 */
			ConnectionRequestSecret.fromString = (str) => {
				const msg = ConnectionRequestSecret.fromJSON(JSON.parse(str));
				msg.verify();
				return msg;
			};
			
			/**
			 * @param {Object} json
			 * @returns {smolweb.secrets.ConnectionRequestSecret}
			 */
			ConnectionRequestSecret.fromJSON = (obj) => {
				const msg = ConnectionRequestSecret.fromObject(obj);
				msg.verify();
				return msg;
			};
			
			/**
			 * @param {smolweb.secrets.ConnectionRequestSecret | Object | ArrayBuffer | string} saved
			 * @returns {smolweb.secrets.ConnectionRequestSecret}
			 */
			ConnectionRequestSecret.from = (saved) => {
				if (saved instanceof ConnectionRequestSecret) {
					return saved;
				} else if (saved instanceof ArrayBuffer) {
					return ConnectionRequestSecret.fromBuffer(saved);
				} else if (typeof saved === 'string') {
					return ConnectionRequestSecret.fromString(saved);
				} else if (typeof saved === 'object') {
					return ConnectionRequestSecret.fromJSON(saved);
				} else {
					throw new Error("Unrecognized save format: " + saved?.constructor?.name);
				}
			};
			
			/**
			 * @returns {smolweb.secrets.ConnectionRequestSecret}
			 */
			ConnectionRequestSecret.prototype.copy = function () {
				// TODO: find more efficient way to do this?
				//   we only need to make defensive copies to prevent mutation
				//   but there's no need for a full buffer encoding/decoding roundtrip
				return this.constructor.fromBuffer(this.toBuffer());
			};
			
			/**
			 * WebStorm needs a little help understanding that this class implements ProtobufOpen
			 * @returns {ProtobufOpen.<smolweb.secrets.ConnectionRequestSecret>}
			 */
			ConnectionRequestSecret.opener = function () {
				return this;
			};
			
			return ConnectionRequestSecret;
			
		})();

		secrets.ConnectionAcceptSecret = (function() {

			/**
			 * Properties of a ConnectionAcceptSecret.
			 * @memberof smolweb.secrets
			 * @typedef {Object} smolweb_secrets_TConnectionAcceptSecret
			 * @property {number|bigint|null} [connectionId] ConnectionAcceptSecret connectionId
			 * @property {string|null} [answer1] ConnectionAcceptSecret answer1
			 * @property {string|null} [offer2] ConnectionAcceptSecret offer2
			 */

			/**
			 * Constructs a new ConnectionAcceptSecret.
			 * @memberof smolweb.secrets
			 * @classdesc Represents a ConnectionAcceptSecret.
			 * @implements ProtobufSave
			 * @constructor
			 * @param {smolweb.secrets.ConnectionAcceptSecret=} [properties] Properties to set
			 */
			function ConnectionAcceptSecret(properties) {
				if (properties)
					for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
						if (properties[keys[i]] != null)
							this[keys[i]] = properties[keys[i]];
			}

			/**
			 * ConnectionAcceptSecret connectionId.
			 * @member {number|bigint} connectionId
			 * @memberof smolweb.secrets.ConnectionAcceptSecret
			 * @instance
			 */
			ConnectionAcceptSecret.prototype.connectionId = $util.Long ? $util.Long.fromBits(0,0,true) : 0;

			/**
			 * ConnectionAcceptSecret answer1.
			 * @member {string} answer1
			 * @memberof smolweb.secrets.ConnectionAcceptSecret
			 * @instance
			 */
			ConnectionAcceptSecret.prototype.answer1 = "";

			/**
			 * ConnectionAcceptSecret offer2.
			 * @member {string} offer2
			 * @memberof smolweb.secrets.ConnectionAcceptSecret
			 * @instance
			 */
			ConnectionAcceptSecret.prototype.offer2 = "";

			/**
			 * Creates a new ConnectionAcceptSecret instance using the specified properties.
			 * @function create
			 * @memberof smolweb.secrets.ConnectionAcceptSecret
			 * @static
			 * @param {smolweb.secrets.ConnectionAcceptSecret=} [properties] Properties to set
			 * @returns {smolweb.secrets.ConnectionAcceptSecret} ConnectionAcceptSecret instance
			 */
			ConnectionAcceptSecret.create = function create(properties) {
				return new ConnectionAcceptSecret(properties);
			};

			/**
			 * Encodes the specified ConnectionAcceptSecret message. Does not implicitly {@link smolweb.secrets.ConnectionAcceptSecret.verify|verify} messages.
			 * @function encode
			 * @memberof smolweb.secrets.ConnectionAcceptSecret
			 * @static
			 * @param {smolweb.secrets.ConnectionAcceptSecret} message ConnectionAcceptSecret message or plain object to encode
			 * @param {ArrayBufferWriter} [writer] Writer to encode to
			 * @returns {ArrayBufferWriter} Writer
			 */
			ConnectionAcceptSecret.encode = function encode(message, writer) {
				if (!writer)
					writer = $Writer.create();
				if (message.connectionId != null && Object.hasOwnProperty.call(message, "connectionId"))
					writer.uint32(/* id 1, wireType 0 =*/8).uint64(message.connectionId);
				if (message.answer1 != null && Object.hasOwnProperty.call(message, "answer1"))
					writer.uint32(/* id 2, wireType 2 =*/18).string(message.answer1);
				if (message.offer2 != null && Object.hasOwnProperty.call(message, "offer2"))
					writer.uint32(/* id 3, wireType 2 =*/26).string(message.offer2);
				return writer;
			};

			/**
			 * Decodes a ConnectionAcceptSecret message from the specified reader or buffer.
			 * @function decode
			 * @memberof smolweb.secrets.ConnectionAcceptSecret
			 * @static
			 * @param {ArrayBufferReader|ArrayBuffer} reader Reader or buffer to decode from
			 * @param {number} [length] Message length if known beforehand
			 * @returns {smolweb.secrets.ConnectionAcceptSecret} ConnectionAcceptSecret
			 * @throws {Error} If the payload is not a reader or valid buffer
			 * @throws {$protobuf.util.ProtocolError} If required fields are missing
			 */
			ConnectionAcceptSecret.decode = function decode(reader, length) {
				if (!(reader instanceof $Reader))
					reader = $Reader.create(reader);
				let end = length === undefined ? reader.len : reader.pos + length, message = new $root.smolweb.secrets.ConnectionAcceptSecret();
				while (reader.pos < end) {
					let tag = reader.uint32();
					switch (tag >>> 3) {
					case 1: {
							message.connectionId = reader.uint64();
							break;
						}
					case 2: {
							message.answer1 = reader.string();
							break;
						}
					case 3: {
							message.offer2 = reader.string();
							break;
						}
					default:
						reader.skipType(tag & 7);
						break;
					}
				}
				return message;
			};

			/**
			 * Verifies a ConnectionAcceptSecret message.
			 * @function verify
			 * @memberof smolweb.secrets.ConnectionAcceptSecret
			 * @static
			 * @param {Object.<string,*>} message Plain object to verify
			 * @returns {string|null} `null` if valid, otherwise the reason why it is not
			 */
			ConnectionAcceptSecret.verify = function verify(message) {
				if (typeof message !== "object" || message === null)
					return "object expected";
				if (message.connectionId != null && message.hasOwnProperty("connectionId"))
					if (!$util.isInteger(message.connectionId) && !(typeof message.connectionId === 'bigint'))
						return "connectionId: integer|bigint expected";
				if (message.answer1 != null && message.hasOwnProperty("answer1"))
					if (!$util.isString(message.answer1))
						return "answer1: string expected";
				if (message.offer2 != null && message.hasOwnProperty("offer2"))
					if (!$util.isString(message.offer2))
						return "offer2: string expected";
				return null;
			};

			/**
			 * Creates a ConnectionAcceptSecret message from a plain object. Also converts values to their respective internal types.
			 * @function fromObject
			 * @memberof smolweb.secrets.ConnectionAcceptSecret
			 * @static
			 * @param {Object.<string,*>} object Plain object
			 * @returns {smolweb.secrets.ConnectionAcceptSecret} ConnectionAcceptSecret
			 */
			ConnectionAcceptSecret.fromObject = function fromObject(object) {
				if (object instanceof $root.smolweb.secrets.ConnectionAcceptSecret)
					return object;
				let message = new $root.smolweb.secrets.ConnectionAcceptSecret();
				if (object.connectionId != null)
					if ($util.Long)
						message.connectionId = BigInt(object.connectionId);
					else if (typeof object.connectionId === "string")
						message.connectionId = parseInt(object.connectionId, 10);
					else if (typeof object.connectionId === "number")
						message.connectionId = object.connectionId;
					else if (typeof object.connectionId === "object")
						message.connectionId = new $util.LongBits(object.connectionId.low >>> 0, object.connectionId.high >>> 0).toNumber(true);
				if (object.answer1 != null)
					message.answer1 = String(object.answer1);
				if (object.offer2 != null)
					message.offer2 = String(object.offer2);
				return message;
			};

			/**
			 * Creates a plain object from a ConnectionAcceptSecret message. Also converts values to other types if specified.
			 * @function toObject
			 * @memberof smolweb.secrets.ConnectionAcceptSecret
			 * @static
			 * @param {smolweb.secrets.ConnectionAcceptSecret} message ConnectionAcceptSecret
			 * @param {$protobuf.IConversionOptions} [options] Conversion options
			 * @returns {Object.<string,*>} Plain object
			 */
			ConnectionAcceptSecret.toObject = function toObject(message, options) {
				if (!options)
					options = {};
				let object = {};
				if (options.defaults) {
					if ($util.Long) {
						let long = $util.BigInt.fromBits(0, 0, true);
						object.connectionId = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
					} else
						object.connectionId = options.longs === String ? "0" : 0;
					object.answer1 = "";
					object.offer2 = "";
				}
				if (message.connectionId != null && message.hasOwnProperty("connectionId"))
					if (typeof message.connectionId === "number")
						object.connectionId = options.longs === String ? String(message.connectionId) : message.connectionId;
					else
						object.connectionId = options.longs === String ? message.connectionId.toString() : options.longs === Number ? new $util.LongBits(message.connectionId.low >>> 0, message.connectionId.high >>> 0).toNumber(true) : message.connectionId;
				if (message.answer1 != null && message.hasOwnProperty("answer1"))
					object.answer1 = message.answer1;
				if (message.offer2 != null && message.hasOwnProperty("offer2"))
					object.offer2 = message.offer2;
				return object;
			};

			/**
			 * Converts this ConnectionAcceptSecret to JSON.
			 * @function toJSON
			 * @memberof smolweb.secrets.ConnectionAcceptSecret
			 * @instance
			 * @returns {Object.<string,*>} JSON object
			 */
			ConnectionAcceptSecret.prototype.toJSON = function toJSON() {
				return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
			};

			/**
			 * Gets the default type url for ConnectionAcceptSecret
			 * @function getTypeUrl
			 * @memberof smolweb.secrets.ConnectionAcceptSecret
			 * @static
			 * @param {string} [typeUrlPrefix] your custom typeUrlPrefix(default "type.cuchazinteractive.org")
			 * @returns {string} The default type url
			 */
			ConnectionAcceptSecret.getTypeUrl = function getTypeUrl(typeUrlPrefix) {
				if (typeUrlPrefix === undefined) {
					typeUrlPrefix = "type.cuchazinteractive.org";
				}
				return typeUrlPrefix + "/smolweb.secrets.ConnectionAcceptSecret";
			};

			
			/**
			 * @returns {?string}
			 */		
			ConnectionAcceptSecret.prototype.verify = function () {
				const err = ConnectionAcceptSecret.verify(this);
				if (err != null) {
					throw new Error("Verification failed for ConnectionAcceptSecret: " + err);
				}
			};
			
			/**
			 * @returns {ArrayBuffer}
			 */
			ConnectionAcceptSecret.prototype.toBuffer = function () {
				return ConnectionAcceptSecret.encode(this).finish();
			};
			
			/**
			 * @param {string | number} [spacer]
			 * @returns {string}
			 */
			ConnectionAcceptSecret.prototype.toString = function (spacer) {
				return JSON.stringify(this.toJSON(), null, spacer);
			};
			
			/**
			 * @param {SaveMode} mode
			 * @returns {ArrayBuffer | string | Object}
			 */
			ConnectionAcceptSecret.prototype.to = function (mode) {
				switch (mode) {
					case SaveModes.BUF: return this.toBuffer();
					case SaveModes.STR: return this.toString();
					case SaveModes.JSON: return this.toJSON();
					default: throw new Error("unrecognized save mode: " + mode);
				}
			};
			
			/**
			 * @param {smolweb_secrets_TConnectionAcceptSecret} obj
			 * @returns {smolweb.secrets.ConnectionAcceptSecret}
			 */
			ConnectionAcceptSecret.new = (obj) => {
				const msg = ConnectionAcceptSecret.create(obj);
				msg.verify();
				return msg;
			};
			
			/**
			 * @param {ArrayBuffer} buf
			 * @returns {smolweb.secrets.ConnectionAcceptSecret}
			 */
			ConnectionAcceptSecret.fromBuffer = (buf) => {
				const msg = ConnectionAcceptSecret.decode(buf);
				msg.verify();
				return msg;
			};
			
			/**
			 * @param {string} str
			 * @returns {smolweb.secrets.ConnectionAcceptSecret}
			 */
			ConnectionAcceptSecret.fromString = (str) => {
				const msg = ConnectionAcceptSecret.fromJSON(JSON.parse(str));
				msg.verify();
				return msg;
			};
			
			/**
			 * @param {Object} json
			 * @returns {smolweb.secrets.ConnectionAcceptSecret}
			 */
			ConnectionAcceptSecret.fromJSON = (obj) => {
				const msg = ConnectionAcceptSecret.fromObject(obj);
				msg.verify();
				return msg;
			};
			
			/**
			 * @param {smolweb.secrets.ConnectionAcceptSecret | Object | ArrayBuffer | string} saved
			 * @returns {smolweb.secrets.ConnectionAcceptSecret}
			 */
			ConnectionAcceptSecret.from = (saved) => {
				if (saved instanceof ConnectionAcceptSecret) {
					return saved;
				} else if (saved instanceof ArrayBuffer) {
					return ConnectionAcceptSecret.fromBuffer(saved);
				} else if (typeof saved === 'string') {
					return ConnectionAcceptSecret.fromString(saved);
				} else if (typeof saved === 'object') {
					return ConnectionAcceptSecret.fromJSON(saved);
				} else {
					throw new Error("Unrecognized save format: " + saved?.constructor?.name);
				}
			};
			
			/**
			 * @returns {smolweb.secrets.ConnectionAcceptSecret}
			 */
			ConnectionAcceptSecret.prototype.copy = function () {
				// TODO: find more efficient way to do this?
				//   we only need to make defensive copies to prevent mutation
				//   but there's no need for a full buffer encoding/decoding roundtrip
				return this.constructor.fromBuffer(this.toBuffer());
			};
			
			/**
			 * WebStorm needs a little help understanding that this class implements ProtobufOpen
			 * @returns {ProtobufOpen.<smolweb.secrets.ConnectionAcceptSecret>}
			 */
			ConnectionAcceptSecret.opener = function () {
				return this;
			};
			
			return ConnectionAcceptSecret;
			
		})();

		secrets.ConnectionConfirmSecret = (function() {

			/**
			 * Properties of a ConnectionConfirmSecret.
			 * @memberof smolweb.secrets
			 * @typedef {Object} smolweb_secrets_TConnectionConfirmSecret
			 * @property {number|bigint|null} [connectionId] ConnectionConfirmSecret connectionId
			 * @property {string|null} [answer2] ConnectionConfirmSecret answer2
			 */

			/**
			 * Constructs a new ConnectionConfirmSecret.
			 * @memberof smolweb.secrets
			 * @classdesc Represents a ConnectionConfirmSecret.
			 * @implements ProtobufSave
			 * @constructor
			 * @param {smolweb.secrets.ConnectionConfirmSecret=} [properties] Properties to set
			 */
			function ConnectionConfirmSecret(properties) {
				if (properties)
					for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
						if (properties[keys[i]] != null)
							this[keys[i]] = properties[keys[i]];
			}

			/**
			 * ConnectionConfirmSecret connectionId.
			 * @member {number|bigint} connectionId
			 * @memberof smolweb.secrets.ConnectionConfirmSecret
			 * @instance
			 */
			ConnectionConfirmSecret.prototype.connectionId = $util.Long ? $util.Long.fromBits(0,0,true) : 0;

			/**
			 * ConnectionConfirmSecret answer2.
			 * @member {string} answer2
			 * @memberof smolweb.secrets.ConnectionConfirmSecret
			 * @instance
			 */
			ConnectionConfirmSecret.prototype.answer2 = "";

			/**
			 * Creates a new ConnectionConfirmSecret instance using the specified properties.
			 * @function create
			 * @memberof smolweb.secrets.ConnectionConfirmSecret
			 * @static
			 * @param {smolweb.secrets.ConnectionConfirmSecret=} [properties] Properties to set
			 * @returns {smolweb.secrets.ConnectionConfirmSecret} ConnectionConfirmSecret instance
			 */
			ConnectionConfirmSecret.create = function create(properties) {
				return new ConnectionConfirmSecret(properties);
			};

			/**
			 * Encodes the specified ConnectionConfirmSecret message. Does not implicitly {@link smolweb.secrets.ConnectionConfirmSecret.verify|verify} messages.
			 * @function encode
			 * @memberof smolweb.secrets.ConnectionConfirmSecret
			 * @static
			 * @param {smolweb.secrets.ConnectionConfirmSecret} message ConnectionConfirmSecret message or plain object to encode
			 * @param {ArrayBufferWriter} [writer] Writer to encode to
			 * @returns {ArrayBufferWriter} Writer
			 */
			ConnectionConfirmSecret.encode = function encode(message, writer) {
				if (!writer)
					writer = $Writer.create();
				if (message.connectionId != null && Object.hasOwnProperty.call(message, "connectionId"))
					writer.uint32(/* id 1, wireType 0 =*/8).uint64(message.connectionId);
				if (message.answer2 != null && Object.hasOwnProperty.call(message, "answer2"))
					writer.uint32(/* id 2, wireType 2 =*/18).string(message.answer2);
				return writer;
			};

			/**
			 * Decodes a ConnectionConfirmSecret message from the specified reader or buffer.
			 * @function decode
			 * @memberof smolweb.secrets.ConnectionConfirmSecret
			 * @static
			 * @param {ArrayBufferReader|ArrayBuffer} reader Reader or buffer to decode from
			 * @param {number} [length] Message length if known beforehand
			 * @returns {smolweb.secrets.ConnectionConfirmSecret} ConnectionConfirmSecret
			 * @throws {Error} If the payload is not a reader or valid buffer
			 * @throws {$protobuf.util.ProtocolError} If required fields are missing
			 */
			ConnectionConfirmSecret.decode = function decode(reader, length) {
				if (!(reader instanceof $Reader))
					reader = $Reader.create(reader);
				let end = length === undefined ? reader.len : reader.pos + length, message = new $root.smolweb.secrets.ConnectionConfirmSecret();
				while (reader.pos < end) {
					let tag = reader.uint32();
					switch (tag >>> 3) {
					case 1: {
							message.connectionId = reader.uint64();
							break;
						}
					case 2: {
							message.answer2 = reader.string();
							break;
						}
					default:
						reader.skipType(tag & 7);
						break;
					}
				}
				return message;
			};

			/**
			 * Verifies a ConnectionConfirmSecret message.
			 * @function verify
			 * @memberof smolweb.secrets.ConnectionConfirmSecret
			 * @static
			 * @param {Object.<string,*>} message Plain object to verify
			 * @returns {string|null} `null` if valid, otherwise the reason why it is not
			 */
			ConnectionConfirmSecret.verify = function verify(message) {
				if (typeof message !== "object" || message === null)
					return "object expected";
				if (message.connectionId != null && message.hasOwnProperty("connectionId"))
					if (!$util.isInteger(message.connectionId) && !(typeof message.connectionId === 'bigint'))
						return "connectionId: integer|bigint expected";
				if (message.answer2 != null && message.hasOwnProperty("answer2"))
					if (!$util.isString(message.answer2))
						return "answer2: string expected";
				return null;
			};

			/**
			 * Creates a ConnectionConfirmSecret message from a plain object. Also converts values to their respective internal types.
			 * @function fromObject
			 * @memberof smolweb.secrets.ConnectionConfirmSecret
			 * @static
			 * @param {Object.<string,*>} object Plain object
			 * @returns {smolweb.secrets.ConnectionConfirmSecret} ConnectionConfirmSecret
			 */
			ConnectionConfirmSecret.fromObject = function fromObject(object) {
				if (object instanceof $root.smolweb.secrets.ConnectionConfirmSecret)
					return object;
				let message = new $root.smolweb.secrets.ConnectionConfirmSecret();
				if (object.connectionId != null)
					if ($util.Long)
						message.connectionId = BigInt(object.connectionId);
					else if (typeof object.connectionId === "string")
						message.connectionId = parseInt(object.connectionId, 10);
					else if (typeof object.connectionId === "number")
						message.connectionId = object.connectionId;
					else if (typeof object.connectionId === "object")
						message.connectionId = new $util.LongBits(object.connectionId.low >>> 0, object.connectionId.high >>> 0).toNumber(true);
				if (object.answer2 != null)
					message.answer2 = String(object.answer2);
				return message;
			};

			/**
			 * Creates a plain object from a ConnectionConfirmSecret message. Also converts values to other types if specified.
			 * @function toObject
			 * @memberof smolweb.secrets.ConnectionConfirmSecret
			 * @static
			 * @param {smolweb.secrets.ConnectionConfirmSecret} message ConnectionConfirmSecret
			 * @param {$protobuf.IConversionOptions} [options] Conversion options
			 * @returns {Object.<string,*>} Plain object
			 */
			ConnectionConfirmSecret.toObject = function toObject(message, options) {
				if (!options)
					options = {};
				let object = {};
				if (options.defaults) {
					if ($util.Long) {
						let long = $util.BigInt.fromBits(0, 0, true);
						object.connectionId = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
					} else
						object.connectionId = options.longs === String ? "0" : 0;
					object.answer2 = "";
				}
				if (message.connectionId != null && message.hasOwnProperty("connectionId"))
					if (typeof message.connectionId === "number")
						object.connectionId = options.longs === String ? String(message.connectionId) : message.connectionId;
					else
						object.connectionId = options.longs === String ? message.connectionId.toString() : options.longs === Number ? new $util.LongBits(message.connectionId.low >>> 0, message.connectionId.high >>> 0).toNumber(true) : message.connectionId;
				if (message.answer2 != null && message.hasOwnProperty("answer2"))
					object.answer2 = message.answer2;
				return object;
			};

			/**
			 * Converts this ConnectionConfirmSecret to JSON.
			 * @function toJSON
			 * @memberof smolweb.secrets.ConnectionConfirmSecret
			 * @instance
			 * @returns {Object.<string,*>} JSON object
			 */
			ConnectionConfirmSecret.prototype.toJSON = function toJSON() {
				return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
			};

			/**
			 * Gets the default type url for ConnectionConfirmSecret
			 * @function getTypeUrl
			 * @memberof smolweb.secrets.ConnectionConfirmSecret
			 * @static
			 * @param {string} [typeUrlPrefix] your custom typeUrlPrefix(default "type.cuchazinteractive.org")
			 * @returns {string} The default type url
			 */
			ConnectionConfirmSecret.getTypeUrl = function getTypeUrl(typeUrlPrefix) {
				if (typeUrlPrefix === undefined) {
					typeUrlPrefix = "type.cuchazinteractive.org";
				}
				return typeUrlPrefix + "/smolweb.secrets.ConnectionConfirmSecret";
			};

			
			/**
			 * @returns {?string}
			 */		
			ConnectionConfirmSecret.prototype.verify = function () {
				const err = ConnectionConfirmSecret.verify(this);
				if (err != null) {
					throw new Error("Verification failed for ConnectionConfirmSecret: " + err);
				}
			};
			
			/**
			 * @returns {ArrayBuffer}
			 */
			ConnectionConfirmSecret.prototype.toBuffer = function () {
				return ConnectionConfirmSecret.encode(this).finish();
			};
			
			/**
			 * @param {string | number} [spacer]
			 * @returns {string}
			 */
			ConnectionConfirmSecret.prototype.toString = function (spacer) {
				return JSON.stringify(this.toJSON(), null, spacer);
			};
			
			/**
			 * @param {SaveMode} mode
			 * @returns {ArrayBuffer | string | Object}
			 */
			ConnectionConfirmSecret.prototype.to = function (mode) {
				switch (mode) {
					case SaveModes.BUF: return this.toBuffer();
					case SaveModes.STR: return this.toString();
					case SaveModes.JSON: return this.toJSON();
					default: throw new Error("unrecognized save mode: " + mode);
				}
			};
			
			/**
			 * @param {smolweb_secrets_TConnectionConfirmSecret} obj
			 * @returns {smolweb.secrets.ConnectionConfirmSecret}
			 */
			ConnectionConfirmSecret.new = (obj) => {
				const msg = ConnectionConfirmSecret.create(obj);
				msg.verify();
				return msg;
			};
			
			/**
			 * @param {ArrayBuffer} buf
			 * @returns {smolweb.secrets.ConnectionConfirmSecret}
			 */
			ConnectionConfirmSecret.fromBuffer = (buf) => {
				const msg = ConnectionConfirmSecret.decode(buf);
				msg.verify();
				return msg;
			};
			
			/**
			 * @param {string} str
			 * @returns {smolweb.secrets.ConnectionConfirmSecret}
			 */
			ConnectionConfirmSecret.fromString = (str) => {
				const msg = ConnectionConfirmSecret.fromJSON(JSON.parse(str));
				msg.verify();
				return msg;
			};
			
			/**
			 * @param {Object} json
			 * @returns {smolweb.secrets.ConnectionConfirmSecret}
			 */
			ConnectionConfirmSecret.fromJSON = (obj) => {
				const msg = ConnectionConfirmSecret.fromObject(obj);
				msg.verify();
				return msg;
			};
			
			/**
			 * @param {smolweb.secrets.ConnectionConfirmSecret | Object | ArrayBuffer | string} saved
			 * @returns {smolweb.secrets.ConnectionConfirmSecret}
			 */
			ConnectionConfirmSecret.from = (saved) => {
				if (saved instanceof ConnectionConfirmSecret) {
					return saved;
				} else if (saved instanceof ArrayBuffer) {
					return ConnectionConfirmSecret.fromBuffer(saved);
				} else if (typeof saved === 'string') {
					return ConnectionConfirmSecret.fromString(saved);
				} else if (typeof saved === 'object') {
					return ConnectionConfirmSecret.fromJSON(saved);
				} else {
					throw new Error("Unrecognized save format: " + saved?.constructor?.name);
				}
			};
			
			/**
			 * @returns {smolweb.secrets.ConnectionConfirmSecret}
			 */
			ConnectionConfirmSecret.prototype.copy = function () {
				// TODO: find more efficient way to do this?
				//   we only need to make defensive copies to prevent mutation
				//   but there's no need for a full buffer encoding/decoding roundtrip
				return this.constructor.fromBuffer(this.toBuffer());
			};
			
			/**
			 * WebStorm needs a little help understanding that this class implements ProtobufOpen
			 * @returns {ProtobufOpen.<smolweb.secrets.ConnectionConfirmSecret>}
			 */
			ConnectionConfirmSecret.opener = function () {
				return this;
			};
			
			return ConnectionConfirmSecret;
			
		})();

		return secrets;
	})();

	smolweb.framed = (function() {

		/**
		 * Namespace framed.
		 * @memberof smolweb
		 * @namespace
		 */
		const framed = {};

		framed.Frame = (function() {

			/**
			 * Properties of a Frame.
			 * @memberof smolweb.framed
			 * @typedef {Object} smolweb_framed_TFrame
			 * @property {smolweb.framed.FrameStart|null} [start] Frame start
			 * @property {smolweb.framed.FrameContinue|null} [continue] Frame continue
			 * @property {smolweb.framed.FrameError|null} [error] Frame error
			 */

			/**
			 * Constructs a new Frame.
			 * @memberof smolweb.framed
			 * @classdesc Represents a Frame.
			 * @implements ProtobufSave
			 * @constructor
			 * @param {smolweb.framed.Frame=} [properties] Properties to set
			 */
			function Frame(properties) {
				if (properties)
					for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
						if (properties[keys[i]] != null)
							this[keys[i]] = properties[keys[i]];
			}

			/**
			 * Frame start.
			 * @member {smolweb.framed.FrameStart|null|undefined} start
			 * @memberof smolweb.framed.Frame
			 * @instance
			 */
			Frame.prototype.start = null;

			/**
			 * Frame continue.
			 * @member {smolweb.framed.FrameContinue|null|undefined} continue
			 * @memberof smolweb.framed.Frame
			 * @instance
			 */
			Frame.prototype["continue"] = null;

			/**
			 * Frame error.
			 * @member {smolweb.framed.FrameError|null|undefined} error
			 * @memberof smolweb.framed.Frame
			 * @instance
			 */
			Frame.prototype.error = null;

			// OneOf field names bound to virtual getters and setters
			let $oneOfFields;

			/**
			 * Frame kind.
			 * @member {"start"|"continue"|"error"|undefined} kind
			 * @memberof smolweb.framed.Frame
			 * @instance
			 */
			Object.defineProperty(Frame.prototype, "kind", {
				get: $util.oneOfGetter($oneOfFields = ["start", "continue", "error"]),
				set: $util.oneOfSetter($oneOfFields)
			});

			/**
			 * Creates a new Frame instance using the specified properties.
			 * @function create
			 * @memberof smolweb.framed.Frame
			 * @static
			 * @param {smolweb.framed.Frame=} [properties] Properties to set
			 * @returns {smolweb.framed.Frame} Frame instance
			 */
			Frame.create = function create(properties) {
				return new Frame(properties);
			};

			/**
			 * Encodes the specified Frame message. Does not implicitly {@link smolweb.framed.Frame.verify|verify} messages.
			 * @function encode
			 * @memberof smolweb.framed.Frame
			 * @static
			 * @param {smolweb.framed.Frame} message Frame message or plain object to encode
			 * @param {ArrayBufferWriter} [writer] Writer to encode to
			 * @returns {ArrayBufferWriter} Writer
			 */
			Frame.encode = function encode(message, writer) {
				if (!writer)
					writer = $Writer.create();
				if (message.start != null && Object.hasOwnProperty.call(message, "start"))
					$root.smolweb.framed.FrameStart.encode(message.start, writer.uint32(/* id 1, wireType 2 =*/10).fork()).ldelim();
				if (message["continue"] != null && Object.hasOwnProperty.call(message, "continue"))
					$root.smolweb.framed.FrameContinue.encode(message["continue"], writer.uint32(/* id 2, wireType 2 =*/18).fork()).ldelim();
				if (message.error != null && Object.hasOwnProperty.call(message, "error"))
					$root.smolweb.framed.FrameError.encode(message.error, writer.uint32(/* id 3, wireType 2 =*/26).fork()).ldelim();
				return writer;
			};

			/**
			 * Decodes a Frame message from the specified reader or buffer.
			 * @function decode
			 * @memberof smolweb.framed.Frame
			 * @static
			 * @param {ArrayBufferReader|ArrayBuffer} reader Reader or buffer to decode from
			 * @param {number} [length] Message length if known beforehand
			 * @returns {smolweb.framed.Frame} Frame
			 * @throws {Error} If the payload is not a reader or valid buffer
			 * @throws {$protobuf.util.ProtocolError} If required fields are missing
			 */
			Frame.decode = function decode(reader, length) {
				if (!(reader instanceof $Reader))
					reader = $Reader.create(reader);
				let end = length === undefined ? reader.len : reader.pos + length, message = new $root.smolweb.framed.Frame();
				while (reader.pos < end) {
					let tag = reader.uint32();
					switch (tag >>> 3) {
					case 1: {
							message.start = $root.smolweb.framed.FrameStart.decode(reader, reader.uint32());
							break;
						}
					case 2: {
							message["continue"] = $root.smolweb.framed.FrameContinue.decode(reader, reader.uint32());
							break;
						}
					case 3: {
							message.error = $root.smolweb.framed.FrameError.decode(reader, reader.uint32());
							break;
						}
					default:
						reader.skipType(tag & 7);
						break;
					}
				}
				return message;
			};

			/**
			 * Verifies a Frame message.
			 * @function verify
			 * @memberof smolweb.framed.Frame
			 * @static
			 * @param {Object.<string,*>} message Plain object to verify
			 * @returns {string|null} `null` if valid, otherwise the reason why it is not
			 */
			Frame.verify = function verify(message) {
				if (typeof message !== "object" || message === null)
					return "object expected";
				let properties = {};
				if (message.start != null && message.hasOwnProperty("start")) {
					properties.kind = 1;
					{
						let error = $root.smolweb.framed.FrameStart.verify(message.start);
						if (error)
							return "start." + error;
					}
				}
				if (message["continue"] != null && message.hasOwnProperty("continue")) {
					if (properties.kind === 1)
						return "kind: multiple values";
					properties.kind = 1;
					{
						let error = $root.smolweb.framed.FrameContinue.verify(message["continue"]);
						if (error)
							return "continue." + error;
					}
				}
				if (message.error != null && message.hasOwnProperty("error")) {
					if (properties.kind === 1)
						return "kind: multiple values";
					properties.kind = 1;
					{
						let error = $root.smolweb.framed.FrameError.verify(message.error);
						if (error)
							return "error." + error;
					}
				}
				return null;
			};

			/**
			 * Creates a Frame message from a plain object. Also converts values to their respective internal types.
			 * @function fromObject
			 * @memberof smolweb.framed.Frame
			 * @static
			 * @param {Object.<string,*>} object Plain object
			 * @returns {smolweb.framed.Frame} Frame
			 */
			Frame.fromObject = function fromObject(object) {
				if (object instanceof $root.smolweb.framed.Frame)
					return object;
				let message = new $root.smolweb.framed.Frame();
				if (object.start != null) {
					if (typeof object.start !== "object")
						throw TypeError(".smolweb.framed.Frame.start: object expected");
					message.start = $root.smolweb.framed.FrameStart.fromObject(object.start);
				}
				if (object["continue"] != null) {
					if (typeof object["continue"] !== "object")
						throw TypeError(".smolweb.framed.Frame.continue: object expected");
					message["continue"] = $root.smolweb.framed.FrameContinue.fromObject(object["continue"]);
				}
				if (object.error != null) {
					if (typeof object.error !== "object")
						throw TypeError(".smolweb.framed.Frame.error: object expected");
					message.error = $root.smolweb.framed.FrameError.fromObject(object.error);
				}
				return message;
			};

			/**
			 * Creates a plain object from a Frame message. Also converts values to other types if specified.
			 * @function toObject
			 * @memberof smolweb.framed.Frame
			 * @static
			 * @param {smolweb.framed.Frame} message Frame
			 * @param {$protobuf.IConversionOptions} [options] Conversion options
			 * @returns {Object.<string,*>} Plain object
			 */
			Frame.toObject = function toObject(message, options) {
				if (!options)
					options = {};
				let object = {};
				if (message.start != null && message.hasOwnProperty("start")) {
					object.start = $root.smolweb.framed.FrameStart.toObject(message.start, options);
					if (options.oneofs)
						object.kind = "start";
				}
				if (message["continue"] != null && message.hasOwnProperty("continue")) {
					object["continue"] = $root.smolweb.framed.FrameContinue.toObject(message["continue"], options);
					if (options.oneofs)
						object.kind = "continue";
				}
				if (message.error != null && message.hasOwnProperty("error")) {
					object.error = $root.smolweb.framed.FrameError.toObject(message.error, options);
					if (options.oneofs)
						object.kind = "error";
				}
				return object;
			};

			/**
			 * Converts this Frame to JSON.
			 * @function toJSON
			 * @memberof smolweb.framed.Frame
			 * @instance
			 * @returns {Object.<string,*>} JSON object
			 */
			Frame.prototype.toJSON = function toJSON() {
				return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
			};

			/**
			 * Gets the default type url for Frame
			 * @function getTypeUrl
			 * @memberof smolweb.framed.Frame
			 * @static
			 * @param {string} [typeUrlPrefix] your custom typeUrlPrefix(default "type.cuchazinteractive.org")
			 * @returns {string} The default type url
			 */
			Frame.getTypeUrl = function getTypeUrl(typeUrlPrefix) {
				if (typeUrlPrefix === undefined) {
					typeUrlPrefix = "type.cuchazinteractive.org";
				}
				return typeUrlPrefix + "/smolweb.framed.Frame";
			};

			
			/**
			 * @returns {?string}
			 */		
			Frame.prototype.verify = function () {
				const err = Frame.verify(this);
				if (err != null) {
					throw new Error("Verification failed for Frame: " + err);
				}
			};
			
			/**
			 * @returns {ArrayBuffer}
			 */
			Frame.prototype.toBuffer = function () {
				return Frame.encode(this).finish();
			};
			
			/**
			 * @param {string | number} [spacer]
			 * @returns {string}
			 */
			Frame.prototype.toString = function (spacer) {
				return JSON.stringify(this.toJSON(), null, spacer);
			};
			
			/**
			 * @param {SaveMode} mode
			 * @returns {ArrayBuffer | string | Object}
			 */
			Frame.prototype.to = function (mode) {
				switch (mode) {
					case SaveModes.BUF: return this.toBuffer();
					case SaveModes.STR: return this.toString();
					case SaveModes.JSON: return this.toJSON();
					default: throw new Error("unrecognized save mode: " + mode);
				}
			};
			
			/**
			 * @param {smolweb_framed_TFrame} obj
			 * @returns {smolweb.framed.Frame}
			 */
			Frame.new = (obj) => {
				const msg = Frame.create(obj);
				msg.verify();
				return msg;
			};
			
			/**
			 * @param {ArrayBuffer} buf
			 * @returns {smolweb.framed.Frame}
			 */
			Frame.fromBuffer = (buf) => {
				const msg = Frame.decode(buf);
				msg.verify();
				return msg;
			};
			
			/**
			 * @param {string} str
			 * @returns {smolweb.framed.Frame}
			 */
			Frame.fromString = (str) => {
				const msg = Frame.fromJSON(JSON.parse(str));
				msg.verify();
				return msg;
			};
			
			/**
			 * @param {Object} json
			 * @returns {smolweb.framed.Frame}
			 */
			Frame.fromJSON = (obj) => {
				const msg = Frame.fromObject(obj);
				msg.verify();
				return msg;
			};
			
			/**
			 * @param {smolweb.framed.Frame | Object | ArrayBuffer | string} saved
			 * @returns {smolweb.framed.Frame}
			 */
			Frame.from = (saved) => {
				if (saved instanceof Frame) {
					return saved;
				} else if (saved instanceof ArrayBuffer) {
					return Frame.fromBuffer(saved);
				} else if (typeof saved === 'string') {
					return Frame.fromString(saved);
				} else if (typeof saved === 'object') {
					return Frame.fromJSON(saved);
				} else {
					throw new Error("Unrecognized save format: " + saved?.constructor?.name);
				}
			};
			
			/**
			 * @returns {smolweb.framed.Frame}
			 */
			Frame.prototype.copy = function () {
				// TODO: find more efficient way to do this?
				//   we only need to make defensive copies to prevent mutation
				//   but there's no need for a full buffer encoding/decoding roundtrip
				return this.constructor.fromBuffer(this.toBuffer());
			};
			
			/**
			 * WebStorm needs a little help understanding that this class implements ProtobufOpen
			 * @returns {ProtobufOpen.<smolweb.framed.Frame>}
			 */
			Frame.opener = function () {
				return this;
			};
			
			return Frame;
			
		})();

		framed.FrameStart = (function() {

			/**
			 * Properties of a FrameStart.
			 * @memberof smolweb.framed
			 * @typedef {Object} smolweb_framed_TFrameStart
			 * @property {number|bigint|null} [streamSize] FrameStart streamSize
			 * @property {ArrayBuffer|null} [payload] FrameStart payload
			 */

			/**
			 * Constructs a new FrameStart.
			 * @memberof smolweb.framed
			 * @classdesc Represents a FrameStart.
			 * @implements ProtobufSave
			 * @constructor
			 * @param {smolweb.framed.FrameStart=} [properties] Properties to set
			 */
			function FrameStart(properties) {
				if (properties)
					for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
						if (properties[keys[i]] != null)
							this[keys[i]] = properties[keys[i]];
			}

			/**
			 * FrameStart streamSize.
			 * @member {number|bigint} streamSize
			 * @memberof smolweb.framed.FrameStart
			 * @instance
			 */
			FrameStart.prototype.streamSize = $util.Long ? $util.Long.fromBits(0,0,false) : 0;

			/**
			 * FrameStart payload.
			 * @member {ArrayBuffer} payload
			 * @memberof smolweb.framed.FrameStart
			 * @instance
			 */
			FrameStart.prototype.payload = $util.newBuffer([]);

			/**
			 * Creates a new FrameStart instance using the specified properties.
			 * @function create
			 * @memberof smolweb.framed.FrameStart
			 * @static
			 * @param {smolweb.framed.FrameStart=} [properties] Properties to set
			 * @returns {smolweb.framed.FrameStart} FrameStart instance
			 */
			FrameStart.create = function create(properties) {
				return new FrameStart(properties);
			};

			/**
			 * Encodes the specified FrameStart message. Does not implicitly {@link smolweb.framed.FrameStart.verify|verify} messages.
			 * @function encode
			 * @memberof smolweb.framed.FrameStart
			 * @static
			 * @param {smolweb.framed.FrameStart} message FrameStart message or plain object to encode
			 * @param {ArrayBufferWriter} [writer] Writer to encode to
			 * @returns {ArrayBufferWriter} Writer
			 */
			FrameStart.encode = function encode(message, writer) {
				if (!writer)
					writer = $Writer.create();
				if (message.streamSize != null && Object.hasOwnProperty.call(message, "streamSize"))
					writer.uint32(/* id 1, wireType 0 =*/8).int64(message.streamSize);
				if (message.payload != null && Object.hasOwnProperty.call(message, "payload"))
					writer.uint32(/* id 2, wireType 2 =*/18).bytes(message.payload);
				return writer;
			};

			/**
			 * Decodes a FrameStart message from the specified reader or buffer.
			 * @function decode
			 * @memberof smolweb.framed.FrameStart
			 * @static
			 * @param {ArrayBufferReader|ArrayBuffer} reader Reader or buffer to decode from
			 * @param {number} [length] Message length if known beforehand
			 * @returns {smolweb.framed.FrameStart} FrameStart
			 * @throws {Error} If the payload is not a reader or valid buffer
			 * @throws {$protobuf.util.ProtocolError} If required fields are missing
			 */
			FrameStart.decode = function decode(reader, length) {
				if (!(reader instanceof $Reader))
					reader = $Reader.create(reader);
				let end = length === undefined ? reader.len : reader.pos + length, message = new $root.smolweb.framed.FrameStart();
				while (reader.pos < end) {
					let tag = reader.uint32();
					switch (tag >>> 3) {
					case 1: {
							message.streamSize = reader.int64();
							break;
						}
					case 2: {
							message.payload = reader.bytes();
							break;
						}
					default:
						reader.skipType(tag & 7);
						break;
					}
				}
				return message;
			};

			/**
			 * Verifies a FrameStart message.
			 * @function verify
			 * @memberof smolweb.framed.FrameStart
			 * @static
			 * @param {Object.<string,*>} message Plain object to verify
			 * @returns {string|null} `null` if valid, otherwise the reason why it is not
			 */
			FrameStart.verify = function verify(message) {
				if (typeof message !== "object" || message === null)
					return "object expected";
				if (message.streamSize != null && message.hasOwnProperty("streamSize"))
					if (!$util.isInteger(message.streamSize) && !(typeof message.streamSize === 'bigint'))
						return "streamSize: integer|bigint expected";
				if (message.payload != null && message.hasOwnProperty("payload"))
					if (!(message.payload && typeof message.payload.byteLength === 'number' || $util.isString(message.payload)))
						return "payload: buffer expected";
				return null;
			};

			/**
			 * Creates a FrameStart message from a plain object. Also converts values to their respective internal types.
			 * @function fromObject
			 * @memberof smolweb.framed.FrameStart
			 * @static
			 * @param {Object.<string,*>} object Plain object
			 * @returns {smolweb.framed.FrameStart} FrameStart
			 */
			FrameStart.fromObject = function fromObject(object) {
				if (object instanceof $root.smolweb.framed.FrameStart)
					return object;
				let message = new $root.smolweb.framed.FrameStart();
				if (object.streamSize != null)
					if ($util.Long)
						message.streamSize = BigInt(object.streamSize);
					else if (typeof object.streamSize === "string")
						message.streamSize = parseInt(object.streamSize, 10);
					else if (typeof object.streamSize === "number")
						message.streamSize = object.streamSize;
					else if (typeof object.streamSize === "object")
						message.streamSize = new $util.LongBits(object.streamSize.low >>> 0, object.streamSize.high >>> 0).toNumber();
				if (object.payload != null)
					if (typeof object.payload === "string")
						$util.base64.decode(object.payload, message.payload = $util.newBuffer($util.base64.length(object.payload)), 0);
					else if (object.payload.byteLength >= 0)
						message.payload = object.payload;
				return message;
			};

			/**
			 * Creates a plain object from a FrameStart message. Also converts values to other types if specified.
			 * @function toObject
			 * @memberof smolweb.framed.FrameStart
			 * @static
			 * @param {smolweb.framed.FrameStart} message FrameStart
			 * @param {$protobuf.IConversionOptions} [options] Conversion options
			 * @returns {Object.<string,*>} Plain object
			 */
			FrameStart.toObject = function toObject(message, options) {
				if (!options)
					options = {};
				let object = {};
				if (options.defaults) {
					if ($util.Long) {
						let long = $util.BigInt.fromBits(0, 0, false);
						object.streamSize = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
					} else
						object.streamSize = options.longs === String ? "0" : 0;
					if (options.bytes === String)
						object.payload = "";
					else {
						object.payload = [];
						if (options.bytes !== Array)
							object.payload = $util.newBuffer(object.payload);
					}
				}
				if (message.streamSize != null && message.hasOwnProperty("streamSize"))
					if (typeof message.streamSize === "number")
						object.streamSize = options.longs === String ? String(message.streamSize) : message.streamSize;
					else
						object.streamSize = options.longs === String ? message.streamSize.toString() : options.longs === Number ? new $util.LongBits(message.streamSize.low >>> 0, message.streamSize.high >>> 0).toNumber() : message.streamSize;
				if (message.payload != null && message.hasOwnProperty("payload"))
					object.payload = options.bytes === String ? $util.base64.encode(message.payload, 0, message.payload.byteLength) : options.bytes === Array ? Array.prototype.slice.call(message.payload) : message.payload;
				return object;
			};

			/**
			 * Converts this FrameStart to JSON.
			 * @function toJSON
			 * @memberof smolweb.framed.FrameStart
			 * @instance
			 * @returns {Object.<string,*>} JSON object
			 */
			FrameStart.prototype.toJSON = function toJSON() {
				return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
			};

			/**
			 * Gets the default type url for FrameStart
			 * @function getTypeUrl
			 * @memberof smolweb.framed.FrameStart
			 * @static
			 * @param {string} [typeUrlPrefix] your custom typeUrlPrefix(default "type.cuchazinteractive.org")
			 * @returns {string} The default type url
			 */
			FrameStart.getTypeUrl = function getTypeUrl(typeUrlPrefix) {
				if (typeUrlPrefix === undefined) {
					typeUrlPrefix = "type.cuchazinteractive.org";
				}
				return typeUrlPrefix + "/smolweb.framed.FrameStart";
			};

			
			/**
			 * @returns {?string}
			 */		
			FrameStart.prototype.verify = function () {
				const err = FrameStart.verify(this);
				if (err != null) {
					throw new Error("Verification failed for FrameStart: " + err);
				}
			};
			
			/**
			 * @returns {ArrayBuffer}
			 */
			FrameStart.prototype.toBuffer = function () {
				return FrameStart.encode(this).finish();
			};
			
			/**
			 * @param {string | number} [spacer]
			 * @returns {string}
			 */
			FrameStart.prototype.toString = function (spacer) {
				return JSON.stringify(this.toJSON(), null, spacer);
			};
			
			/**
			 * @param {SaveMode} mode
			 * @returns {ArrayBuffer | string | Object}
			 */
			FrameStart.prototype.to = function (mode) {
				switch (mode) {
					case SaveModes.BUF: return this.toBuffer();
					case SaveModes.STR: return this.toString();
					case SaveModes.JSON: return this.toJSON();
					default: throw new Error("unrecognized save mode: " + mode);
				}
			};
			
			/**
			 * @param {smolweb_framed_TFrameStart} obj
			 * @returns {smolweb.framed.FrameStart}
			 */
			FrameStart.new = (obj) => {
				const msg = FrameStart.create(obj);
				msg.verify();
				return msg;
			};
			
			/**
			 * @param {ArrayBuffer} buf
			 * @returns {smolweb.framed.FrameStart}
			 */
			FrameStart.fromBuffer = (buf) => {
				const msg = FrameStart.decode(buf);
				msg.verify();
				return msg;
			};
			
			/**
			 * @param {string} str
			 * @returns {smolweb.framed.FrameStart}
			 */
			FrameStart.fromString = (str) => {
				const msg = FrameStart.fromJSON(JSON.parse(str));
				msg.verify();
				return msg;
			};
			
			/**
			 * @param {Object} json
			 * @returns {smolweb.framed.FrameStart}
			 */
			FrameStart.fromJSON = (obj) => {
				const msg = FrameStart.fromObject(obj);
				msg.verify();
				return msg;
			};
			
			/**
			 * @param {smolweb.framed.FrameStart | Object | ArrayBuffer | string} saved
			 * @returns {smolweb.framed.FrameStart}
			 */
			FrameStart.from = (saved) => {
				if (saved instanceof FrameStart) {
					return saved;
				} else if (saved instanceof ArrayBuffer) {
					return FrameStart.fromBuffer(saved);
				} else if (typeof saved === 'string') {
					return FrameStart.fromString(saved);
				} else if (typeof saved === 'object') {
					return FrameStart.fromJSON(saved);
				} else {
					throw new Error("Unrecognized save format: " + saved?.constructor?.name);
				}
			};
			
			/**
			 * @returns {smolweb.framed.FrameStart}
			 */
			FrameStart.prototype.copy = function () {
				// TODO: find more efficient way to do this?
				//   we only need to make defensive copies to prevent mutation
				//   but there's no need for a full buffer encoding/decoding roundtrip
				return this.constructor.fromBuffer(this.toBuffer());
			};
			
			/**
			 * WebStorm needs a little help understanding that this class implements ProtobufOpen
			 * @returns {ProtobufOpen.<smolweb.framed.FrameStart>}
			 */
			FrameStart.opener = function () {
				return this;
			};
			
			return FrameStart;
			
		})();

		framed.FrameContinue = (function() {

			/**
			 * Properties of a FrameContinue.
			 * @memberof smolweb.framed
			 * @typedef {Object} smolweb_framed_TFrameContinue
			 * @property {number|bigint|null} [frameIndex] FrameContinue frameIndex
			 * @property {ArrayBuffer|null} [payload] FrameContinue payload
			 */

			/**
			 * Constructs a new FrameContinue.
			 * @memberof smolweb.framed
			 * @classdesc Represents a FrameContinue.
			 * @implements ProtobufSave
			 * @constructor
			 * @param {smolweb.framed.FrameContinue=} [properties] Properties to set
			 */
			function FrameContinue(properties) {
				if (properties)
					for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
						if (properties[keys[i]] != null)
							this[keys[i]] = properties[keys[i]];
			}

			/**
			 * FrameContinue frameIndex.
			 * @member {number|bigint} frameIndex
			 * @memberof smolweb.framed.FrameContinue
			 * @instance
			 */
			FrameContinue.prototype.frameIndex = $util.Long ? $util.Long.fromBits(0,0,false) : 0;

			/**
			 * FrameContinue payload.
			 * @member {ArrayBuffer} payload
			 * @memberof smolweb.framed.FrameContinue
			 * @instance
			 */
			FrameContinue.prototype.payload = $util.newBuffer([]);

			/**
			 * Creates a new FrameContinue instance using the specified properties.
			 * @function create
			 * @memberof smolweb.framed.FrameContinue
			 * @static
			 * @param {smolweb.framed.FrameContinue=} [properties] Properties to set
			 * @returns {smolweb.framed.FrameContinue} FrameContinue instance
			 */
			FrameContinue.create = function create(properties) {
				return new FrameContinue(properties);
			};

			/**
			 * Encodes the specified FrameContinue message. Does not implicitly {@link smolweb.framed.FrameContinue.verify|verify} messages.
			 * @function encode
			 * @memberof smolweb.framed.FrameContinue
			 * @static
			 * @param {smolweb.framed.FrameContinue} message FrameContinue message or plain object to encode
			 * @param {ArrayBufferWriter} [writer] Writer to encode to
			 * @returns {ArrayBufferWriter} Writer
			 */
			FrameContinue.encode = function encode(message, writer) {
				if (!writer)
					writer = $Writer.create();
				if (message.frameIndex != null && Object.hasOwnProperty.call(message, "frameIndex"))
					writer.uint32(/* id 1, wireType 0 =*/8).int64(message.frameIndex);
				if (message.payload != null && Object.hasOwnProperty.call(message, "payload"))
					writer.uint32(/* id 2, wireType 2 =*/18).bytes(message.payload);
				return writer;
			};

			/**
			 * Decodes a FrameContinue message from the specified reader or buffer.
			 * @function decode
			 * @memberof smolweb.framed.FrameContinue
			 * @static
			 * @param {ArrayBufferReader|ArrayBuffer} reader Reader or buffer to decode from
			 * @param {number} [length] Message length if known beforehand
			 * @returns {smolweb.framed.FrameContinue} FrameContinue
			 * @throws {Error} If the payload is not a reader or valid buffer
			 * @throws {$protobuf.util.ProtocolError} If required fields are missing
			 */
			FrameContinue.decode = function decode(reader, length) {
				if (!(reader instanceof $Reader))
					reader = $Reader.create(reader);
				let end = length === undefined ? reader.len : reader.pos + length, message = new $root.smolweb.framed.FrameContinue();
				while (reader.pos < end) {
					let tag = reader.uint32();
					switch (tag >>> 3) {
					case 1: {
							message.frameIndex = reader.int64();
							break;
						}
					case 2: {
							message.payload = reader.bytes();
							break;
						}
					default:
						reader.skipType(tag & 7);
						break;
					}
				}
				return message;
			};

			/**
			 * Verifies a FrameContinue message.
			 * @function verify
			 * @memberof smolweb.framed.FrameContinue
			 * @static
			 * @param {Object.<string,*>} message Plain object to verify
			 * @returns {string|null} `null` if valid, otherwise the reason why it is not
			 */
			FrameContinue.verify = function verify(message) {
				if (typeof message !== "object" || message === null)
					return "object expected";
				if (message.frameIndex != null && message.hasOwnProperty("frameIndex"))
					if (!$util.isInteger(message.frameIndex) && !(typeof message.frameIndex === 'bigint'))
						return "frameIndex: integer|bigint expected";
				if (message.payload != null && message.hasOwnProperty("payload"))
					if (!(message.payload && typeof message.payload.byteLength === 'number' || $util.isString(message.payload)))
						return "payload: buffer expected";
				return null;
			};

			/**
			 * Creates a FrameContinue message from a plain object. Also converts values to their respective internal types.
			 * @function fromObject
			 * @memberof smolweb.framed.FrameContinue
			 * @static
			 * @param {Object.<string,*>} object Plain object
			 * @returns {smolweb.framed.FrameContinue} FrameContinue
			 */
			FrameContinue.fromObject = function fromObject(object) {
				if (object instanceof $root.smolweb.framed.FrameContinue)
					return object;
				let message = new $root.smolweb.framed.FrameContinue();
				if (object.frameIndex != null)
					if ($util.Long)
						message.frameIndex = BigInt(object.frameIndex);
					else if (typeof object.frameIndex === "string")
						message.frameIndex = parseInt(object.frameIndex, 10);
					else if (typeof object.frameIndex === "number")
						message.frameIndex = object.frameIndex;
					else if (typeof object.frameIndex === "object")
						message.frameIndex = new $util.LongBits(object.frameIndex.low >>> 0, object.frameIndex.high >>> 0).toNumber();
				if (object.payload != null)
					if (typeof object.payload === "string")
						$util.base64.decode(object.payload, message.payload = $util.newBuffer($util.base64.length(object.payload)), 0);
					else if (object.payload.byteLength >= 0)
						message.payload = object.payload;
				return message;
			};

			/**
			 * Creates a plain object from a FrameContinue message. Also converts values to other types if specified.
			 * @function toObject
			 * @memberof smolweb.framed.FrameContinue
			 * @static
			 * @param {smolweb.framed.FrameContinue} message FrameContinue
			 * @param {$protobuf.IConversionOptions} [options] Conversion options
			 * @returns {Object.<string,*>} Plain object
			 */
			FrameContinue.toObject = function toObject(message, options) {
				if (!options)
					options = {};
				let object = {};
				if (options.defaults) {
					if ($util.Long) {
						let long = $util.BigInt.fromBits(0, 0, false);
						object.frameIndex = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
					} else
						object.frameIndex = options.longs === String ? "0" : 0;
					if (options.bytes === String)
						object.payload = "";
					else {
						object.payload = [];
						if (options.bytes !== Array)
							object.payload = $util.newBuffer(object.payload);
					}
				}
				if (message.frameIndex != null && message.hasOwnProperty("frameIndex"))
					if (typeof message.frameIndex === "number")
						object.frameIndex = options.longs === String ? String(message.frameIndex) : message.frameIndex;
					else
						object.frameIndex = options.longs === String ? message.frameIndex.toString() : options.longs === Number ? new $util.LongBits(message.frameIndex.low >>> 0, message.frameIndex.high >>> 0).toNumber() : message.frameIndex;
				if (message.payload != null && message.hasOwnProperty("payload"))
					object.payload = options.bytes === String ? $util.base64.encode(message.payload, 0, message.payload.byteLength) : options.bytes === Array ? Array.prototype.slice.call(message.payload) : message.payload;
				return object;
			};

			/**
			 * Converts this FrameContinue to JSON.
			 * @function toJSON
			 * @memberof smolweb.framed.FrameContinue
			 * @instance
			 * @returns {Object.<string,*>} JSON object
			 */
			FrameContinue.prototype.toJSON = function toJSON() {
				return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
			};

			/**
			 * Gets the default type url for FrameContinue
			 * @function getTypeUrl
			 * @memberof smolweb.framed.FrameContinue
			 * @static
			 * @param {string} [typeUrlPrefix] your custom typeUrlPrefix(default "type.cuchazinteractive.org")
			 * @returns {string} The default type url
			 */
			FrameContinue.getTypeUrl = function getTypeUrl(typeUrlPrefix) {
				if (typeUrlPrefix === undefined) {
					typeUrlPrefix = "type.cuchazinteractive.org";
				}
				return typeUrlPrefix + "/smolweb.framed.FrameContinue";
			};

			
			/**
			 * @returns {?string}
			 */		
			FrameContinue.prototype.verify = function () {
				const err = FrameContinue.verify(this);
				if (err != null) {
					throw new Error("Verification failed for FrameContinue: " + err);
				}
			};
			
			/**
			 * @returns {ArrayBuffer}
			 */
			FrameContinue.prototype.toBuffer = function () {
				return FrameContinue.encode(this).finish();
			};
			
			/**
			 * @param {string | number} [spacer]
			 * @returns {string}
			 */
			FrameContinue.prototype.toString = function (spacer) {
				return JSON.stringify(this.toJSON(), null, spacer);
			};
			
			/**
			 * @param {SaveMode} mode
			 * @returns {ArrayBuffer | string | Object}
			 */
			FrameContinue.prototype.to = function (mode) {
				switch (mode) {
					case SaveModes.BUF: return this.toBuffer();
					case SaveModes.STR: return this.toString();
					case SaveModes.JSON: return this.toJSON();
					default: throw new Error("unrecognized save mode: " + mode);
				}
			};
			
			/**
			 * @param {smolweb_framed_TFrameContinue} obj
			 * @returns {smolweb.framed.FrameContinue}
			 */
			FrameContinue.new = (obj) => {
				const msg = FrameContinue.create(obj);
				msg.verify();
				return msg;
			};
			
			/**
			 * @param {ArrayBuffer} buf
			 * @returns {smolweb.framed.FrameContinue}
			 */
			FrameContinue.fromBuffer = (buf) => {
				const msg = FrameContinue.decode(buf);
				msg.verify();
				return msg;
			};
			
			/**
			 * @param {string} str
			 * @returns {smolweb.framed.FrameContinue}
			 */
			FrameContinue.fromString = (str) => {
				const msg = FrameContinue.fromJSON(JSON.parse(str));
				msg.verify();
				return msg;
			};
			
			/**
			 * @param {Object} json
			 * @returns {smolweb.framed.FrameContinue}
			 */
			FrameContinue.fromJSON = (obj) => {
				const msg = FrameContinue.fromObject(obj);
				msg.verify();
				return msg;
			};
			
			/**
			 * @param {smolweb.framed.FrameContinue | Object | ArrayBuffer | string} saved
			 * @returns {smolweb.framed.FrameContinue}
			 */
			FrameContinue.from = (saved) => {
				if (saved instanceof FrameContinue) {
					return saved;
				} else if (saved instanceof ArrayBuffer) {
					return FrameContinue.fromBuffer(saved);
				} else if (typeof saved === 'string') {
					return FrameContinue.fromString(saved);
				} else if (typeof saved === 'object') {
					return FrameContinue.fromJSON(saved);
				} else {
					throw new Error("Unrecognized save format: " + saved?.constructor?.name);
				}
			};
			
			/**
			 * @returns {smolweb.framed.FrameContinue}
			 */
			FrameContinue.prototype.copy = function () {
				// TODO: find more efficient way to do this?
				//   we only need to make defensive copies to prevent mutation
				//   but there's no need for a full buffer encoding/decoding roundtrip
				return this.constructor.fromBuffer(this.toBuffer());
			};
			
			/**
			 * WebStorm needs a little help understanding that this class implements ProtobufOpen
			 * @returns {ProtobufOpen.<smolweb.framed.FrameContinue>}
			 */
			FrameContinue.opener = function () {
				return this;
			};
			
			return FrameContinue;
			
		})();

		framed.FrameError = (function() {

			/**
			 * Properties of a FrameError.
			 * @memberof smolweb.framed
			 * @typedef {Object} smolweb_framed_TFrameError
			 * @property {smolweb.framed.FrameError.Decode|null} [decode] FrameError decode
			 * @property {smolweb.framed.FrameError.UnexpectedKind|null} [unexpectedKind] FrameError unexpectedKind
			 * @property {smolweb.framed.FrameError.InvalidStreamSize|null} [invalidStreamSize] FrameError invalidStreamSize
			 * @property {smolweb.framed.FrameError.UnexpectedFrameIndex|null} [unexpectedFrameIndex] FrameError unexpectedFrameIndex
			 * @property {smolweb.framed.FrameError.UnexpectedPayloadSize|null} [unexpectedPayloadSize] FrameError unexpectedPayloadSize
			 * @property {boolean|null} [closed] FrameError closed
			 * @property {boolean|null} [abort] FrameError abort
			 */

			/**
			 * Constructs a new FrameError.
			 * @memberof smolweb.framed
			 * @classdesc Represents a FrameError.
			 * @implements ProtobufSave
			 * @constructor
			 * @param {smolweb.framed.FrameError=} [properties] Properties to set
			 */
			function FrameError(properties) {
				if (properties)
					for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
						if (properties[keys[i]] != null)
							this[keys[i]] = properties[keys[i]];
			}

			/**
			 * FrameError decode.
			 * @member {smolweb.framed.FrameError.Decode|null|undefined} decode
			 * @memberof smolweb.framed.FrameError
			 * @instance
			 */
			FrameError.prototype.decode = null;

			/**
			 * FrameError unexpectedKind.
			 * @member {smolweb.framed.FrameError.UnexpectedKind|null|undefined} unexpectedKind
			 * @memberof smolweb.framed.FrameError
			 * @instance
			 */
			FrameError.prototype.unexpectedKind = null;

			/**
			 * FrameError invalidStreamSize.
			 * @member {smolweb.framed.FrameError.InvalidStreamSize|null|undefined} invalidStreamSize
			 * @memberof smolweb.framed.FrameError
			 * @instance
			 */
			FrameError.prototype.invalidStreamSize = null;

			/**
			 * FrameError unexpectedFrameIndex.
			 * @member {smolweb.framed.FrameError.UnexpectedFrameIndex|null|undefined} unexpectedFrameIndex
			 * @memberof smolweb.framed.FrameError
			 * @instance
			 */
			FrameError.prototype.unexpectedFrameIndex = null;

			/**
			 * FrameError unexpectedPayloadSize.
			 * @member {smolweb.framed.FrameError.UnexpectedPayloadSize|null|undefined} unexpectedPayloadSize
			 * @memberof smolweb.framed.FrameError
			 * @instance
			 */
			FrameError.prototype.unexpectedPayloadSize = null;

			/**
			 * FrameError closed.
			 * @member {boolean|null|undefined} closed
			 * @memberof smolweb.framed.FrameError
			 * @instance
			 */
			FrameError.prototype.closed = null;

			/**
			 * FrameError abort.
			 * @member {boolean|null|undefined} abort
			 * @memberof smolweb.framed.FrameError
			 * @instance
			 */
			FrameError.prototype.abort = null;

			// OneOf field names bound to virtual getters and setters
			let $oneOfFields;

			/**
			 * FrameError error.
			 * @member {"decode"|"unexpectedKind"|"invalidStreamSize"|"unexpectedFrameIndex"|"unexpectedPayloadSize"|"closed"|"abort"|undefined} error
			 * @memberof smolweb.framed.FrameError
			 * @instance
			 */
			Object.defineProperty(FrameError.prototype, "error", {
				get: $util.oneOfGetter($oneOfFields = ["decode", "unexpectedKind", "invalidStreamSize", "unexpectedFrameIndex", "unexpectedPayloadSize", "closed", "abort"]),
				set: $util.oneOfSetter($oneOfFields)
			});

			/**
			 * Creates a new FrameError instance using the specified properties.
			 * @function create
			 * @memberof smolweb.framed.FrameError
			 * @static
			 * @param {smolweb.framed.FrameError=} [properties] Properties to set
			 * @returns {smolweb.framed.FrameError} FrameError instance
			 */
			FrameError.create = function create(properties) {
				return new FrameError(properties);
			};

			/**
			 * Encodes the specified FrameError message. Does not implicitly {@link smolweb.framed.FrameError.verify|verify} messages.
			 * @function encode
			 * @memberof smolweb.framed.FrameError
			 * @static
			 * @param {smolweb.framed.FrameError} message FrameError message or plain object to encode
			 * @param {ArrayBufferWriter} [writer] Writer to encode to
			 * @returns {ArrayBufferWriter} Writer
			 */
			FrameError.encode = function encode(message, writer) {
				if (!writer)
					writer = $Writer.create();
				if (message.decode != null && Object.hasOwnProperty.call(message, "decode"))
					$root.smolweb.framed.FrameError.Decode.encode(message.decode, writer.uint32(/* id 1, wireType 2 =*/10).fork()).ldelim();
				if (message.unexpectedKind != null && Object.hasOwnProperty.call(message, "unexpectedKind"))
					$root.smolweb.framed.FrameError.UnexpectedKind.encode(message.unexpectedKind, writer.uint32(/* id 2, wireType 2 =*/18).fork()).ldelim();
				if (message.invalidStreamSize != null && Object.hasOwnProperty.call(message, "invalidStreamSize"))
					$root.smolweb.framed.FrameError.InvalidStreamSize.encode(message.invalidStreamSize, writer.uint32(/* id 3, wireType 2 =*/26).fork()).ldelim();
				if (message.unexpectedFrameIndex != null && Object.hasOwnProperty.call(message, "unexpectedFrameIndex"))
					$root.smolweb.framed.FrameError.UnexpectedFrameIndex.encode(message.unexpectedFrameIndex, writer.uint32(/* id 4, wireType 2 =*/34).fork()).ldelim();
				if (message.unexpectedPayloadSize != null && Object.hasOwnProperty.call(message, "unexpectedPayloadSize"))
					$root.smolweb.framed.FrameError.UnexpectedPayloadSize.encode(message.unexpectedPayloadSize, writer.uint32(/* id 5, wireType 2 =*/42).fork()).ldelim();
				if (message.closed != null && Object.hasOwnProperty.call(message, "closed"))
					writer.uint32(/* id 6, wireType 0 =*/48).bool(message.closed);
				if (message.abort != null && Object.hasOwnProperty.call(message, "abort"))
					writer.uint32(/* id 7, wireType 0 =*/56).bool(message.abort);
				return writer;
			};

			/**
			 * Decodes a FrameError message from the specified reader or buffer.
			 * @function decode
			 * @memberof smolweb.framed.FrameError
			 * @static
			 * @param {ArrayBufferReader|ArrayBuffer} reader Reader or buffer to decode from
			 * @param {number} [length] Message length if known beforehand
			 * @returns {smolweb.framed.FrameError} FrameError
			 * @throws {Error} If the payload is not a reader or valid buffer
			 * @throws {$protobuf.util.ProtocolError} If required fields are missing
			 */
			FrameError.decode = function decode(reader, length) {
				if (!(reader instanceof $Reader))
					reader = $Reader.create(reader);
				let end = length === undefined ? reader.len : reader.pos + length, message = new $root.smolweb.framed.FrameError();
				while (reader.pos < end) {
					let tag = reader.uint32();
					switch (tag >>> 3) {
					case 1: {
							message.decode = $root.smolweb.framed.FrameError.Decode.decode(reader, reader.uint32());
							break;
						}
					case 2: {
							message.unexpectedKind = $root.smolweb.framed.FrameError.UnexpectedKind.decode(reader, reader.uint32());
							break;
						}
					case 3: {
							message.invalidStreamSize = $root.smolweb.framed.FrameError.InvalidStreamSize.decode(reader, reader.uint32());
							break;
						}
					case 4: {
							message.unexpectedFrameIndex = $root.smolweb.framed.FrameError.UnexpectedFrameIndex.decode(reader, reader.uint32());
							break;
						}
					case 5: {
							message.unexpectedPayloadSize = $root.smolweb.framed.FrameError.UnexpectedPayloadSize.decode(reader, reader.uint32());
							break;
						}
					case 6: {
							message.closed = reader.bool();
							break;
						}
					case 7: {
							message.abort = reader.bool();
							break;
						}
					default:
						reader.skipType(tag & 7);
						break;
					}
				}
				return message;
			};

			/**
			 * Verifies a FrameError message.
			 * @function verify
			 * @memberof smolweb.framed.FrameError
			 * @static
			 * @param {Object.<string,*>} message Plain object to verify
			 * @returns {string|null} `null` if valid, otherwise the reason why it is not
			 */
			FrameError.verify = function verify(message) {
				if (typeof message !== "object" || message === null)
					return "object expected";
				let properties = {};
				if (message.decode != null && message.hasOwnProperty("decode")) {
					properties.error = 1;
					{
						let error = $root.smolweb.framed.FrameError.Decode.verify(message.decode);
						if (error)
							return "decode." + error;
					}
				}
				if (message.unexpectedKind != null && message.hasOwnProperty("unexpectedKind")) {
					if (properties.error === 1)
						return "error: multiple values";
					properties.error = 1;
					{
						let error = $root.smolweb.framed.FrameError.UnexpectedKind.verify(message.unexpectedKind);
						if (error)
							return "unexpectedKind." + error;
					}
				}
				if (message.invalidStreamSize != null && message.hasOwnProperty("invalidStreamSize")) {
					if (properties.error === 1)
						return "error: multiple values";
					properties.error = 1;
					{
						let error = $root.smolweb.framed.FrameError.InvalidStreamSize.verify(message.invalidStreamSize);
						if (error)
							return "invalidStreamSize." + error;
					}
				}
				if (message.unexpectedFrameIndex != null && message.hasOwnProperty("unexpectedFrameIndex")) {
					if (properties.error === 1)
						return "error: multiple values";
					properties.error = 1;
					{
						let error = $root.smolweb.framed.FrameError.UnexpectedFrameIndex.verify(message.unexpectedFrameIndex);
						if (error)
							return "unexpectedFrameIndex." + error;
					}
				}
				if (message.unexpectedPayloadSize != null && message.hasOwnProperty("unexpectedPayloadSize")) {
					if (properties.error === 1)
						return "error: multiple values";
					properties.error = 1;
					{
						let error = $root.smolweb.framed.FrameError.UnexpectedPayloadSize.verify(message.unexpectedPayloadSize);
						if (error)
							return "unexpectedPayloadSize." + error;
					}
				}
				if (message.closed != null && message.hasOwnProperty("closed")) {
					if (properties.error === 1)
						return "error: multiple values";
					properties.error = 1;
					if (typeof message.closed !== "boolean")
						return "closed: boolean expected";
				}
				if (message.abort != null && message.hasOwnProperty("abort")) {
					if (properties.error === 1)
						return "error: multiple values";
					properties.error = 1;
					if (typeof message.abort !== "boolean")
						return "abort: boolean expected";
				}
				return null;
			};

			/**
			 * Creates a FrameError message from a plain object. Also converts values to their respective internal types.
			 * @function fromObject
			 * @memberof smolweb.framed.FrameError
			 * @static
			 * @param {Object.<string,*>} object Plain object
			 * @returns {smolweb.framed.FrameError} FrameError
			 */
			FrameError.fromObject = function fromObject(object) {
				if (object instanceof $root.smolweb.framed.FrameError)
					return object;
				let message = new $root.smolweb.framed.FrameError();
				if (object.decode != null) {
					if (typeof object.decode !== "object")
						throw TypeError(".smolweb.framed.FrameError.decode: object expected");
					message.decode = $root.smolweb.framed.FrameError.Decode.fromObject(object.decode);
				}
				if (object.unexpectedKind != null) {
					if (typeof object.unexpectedKind !== "object")
						throw TypeError(".smolweb.framed.FrameError.unexpectedKind: object expected");
					message.unexpectedKind = $root.smolweb.framed.FrameError.UnexpectedKind.fromObject(object.unexpectedKind);
				}
				if (object.invalidStreamSize != null) {
					if (typeof object.invalidStreamSize !== "object")
						throw TypeError(".smolweb.framed.FrameError.invalidStreamSize: object expected");
					message.invalidStreamSize = $root.smolweb.framed.FrameError.InvalidStreamSize.fromObject(object.invalidStreamSize);
				}
				if (object.unexpectedFrameIndex != null) {
					if (typeof object.unexpectedFrameIndex !== "object")
						throw TypeError(".smolweb.framed.FrameError.unexpectedFrameIndex: object expected");
					message.unexpectedFrameIndex = $root.smolweb.framed.FrameError.UnexpectedFrameIndex.fromObject(object.unexpectedFrameIndex);
				}
				if (object.unexpectedPayloadSize != null) {
					if (typeof object.unexpectedPayloadSize !== "object")
						throw TypeError(".smolweb.framed.FrameError.unexpectedPayloadSize: object expected");
					message.unexpectedPayloadSize = $root.smolweb.framed.FrameError.UnexpectedPayloadSize.fromObject(object.unexpectedPayloadSize);
				}
				if (object.closed != null)
					message.closed = Boolean(object.closed);
				if (object.abort != null)
					message.abort = Boolean(object.abort);
				return message;
			};

			/**
			 * Creates a plain object from a FrameError message. Also converts values to other types if specified.
			 * @function toObject
			 * @memberof smolweb.framed.FrameError
			 * @static
			 * @param {smolweb.framed.FrameError} message FrameError
			 * @param {$protobuf.IConversionOptions} [options] Conversion options
			 * @returns {Object.<string,*>} Plain object
			 */
			FrameError.toObject = function toObject(message, options) {
				if (!options)
					options = {};
				let object = {};
				if (message.decode != null && message.hasOwnProperty("decode")) {
					object.decode = $root.smolweb.framed.FrameError.Decode.toObject(message.decode, options);
					if (options.oneofs)
						object.error = "decode";
				}
				if (message.unexpectedKind != null && message.hasOwnProperty("unexpectedKind")) {
					object.unexpectedKind = $root.smolweb.framed.FrameError.UnexpectedKind.toObject(message.unexpectedKind, options);
					if (options.oneofs)
						object.error = "unexpectedKind";
				}
				if (message.invalidStreamSize != null && message.hasOwnProperty("invalidStreamSize")) {
					object.invalidStreamSize = $root.smolweb.framed.FrameError.InvalidStreamSize.toObject(message.invalidStreamSize, options);
					if (options.oneofs)
						object.error = "invalidStreamSize";
				}
				if (message.unexpectedFrameIndex != null && message.hasOwnProperty("unexpectedFrameIndex")) {
					object.unexpectedFrameIndex = $root.smolweb.framed.FrameError.UnexpectedFrameIndex.toObject(message.unexpectedFrameIndex, options);
					if (options.oneofs)
						object.error = "unexpectedFrameIndex";
				}
				if (message.unexpectedPayloadSize != null && message.hasOwnProperty("unexpectedPayloadSize")) {
					object.unexpectedPayloadSize = $root.smolweb.framed.FrameError.UnexpectedPayloadSize.toObject(message.unexpectedPayloadSize, options);
					if (options.oneofs)
						object.error = "unexpectedPayloadSize";
				}
				if (message.closed != null && message.hasOwnProperty("closed")) {
					object.closed = message.closed;
					if (options.oneofs)
						object.error = "closed";
				}
				if (message.abort != null && message.hasOwnProperty("abort")) {
					object.abort = message.abort;
					if (options.oneofs)
						object.error = "abort";
				}
				return object;
			};

			/**
			 * Converts this FrameError to JSON.
			 * @function toJSON
			 * @memberof smolweb.framed.FrameError
			 * @instance
			 * @returns {Object.<string,*>} JSON object
			 */
			FrameError.prototype.toJSON = function toJSON() {
				return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
			};

			/**
			 * Gets the default type url for FrameError
			 * @function getTypeUrl
			 * @memberof smolweb.framed.FrameError
			 * @static
			 * @param {string} [typeUrlPrefix] your custom typeUrlPrefix(default "type.cuchazinteractive.org")
			 * @returns {string} The default type url
			 */
			FrameError.getTypeUrl = function getTypeUrl(typeUrlPrefix) {
				if (typeUrlPrefix === undefined) {
					typeUrlPrefix = "type.cuchazinteractive.org";
				}
				return typeUrlPrefix + "/smolweb.framed.FrameError";
			};

			FrameError.Decode = (function() {

				/**
				 * Properties of a Decode.
				 * @memberof smolweb.framed.FrameError
				 * @typedef {Object} smolweb_framed_FrameError_TDecode
				 * @property {string|null} [message] Decode message
				 */

				/**
				 * Constructs a new Decode.
				 * @memberof smolweb.framed.FrameError
				 * @classdesc Represents a Decode.
				 * @implements ProtobufSave
				 * @constructor
				 * @param {smolweb.framed.FrameError.Decode=} [properties] Properties to set
				 */
				function Decode(properties) {
					if (properties)
						for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
							if (properties[keys[i]] != null)
								this[keys[i]] = properties[keys[i]];
				}

				/**
				 * Decode message.
				 * @member {string} message
				 * @memberof smolweb.framed.FrameError.Decode
				 * @instance
				 */
				Decode.prototype.message = "";

				/**
				 * Creates a new Decode instance using the specified properties.
				 * @function create
				 * @memberof smolweb.framed.FrameError.Decode
				 * @static
				 * @param {smolweb.framed.FrameError.Decode=} [properties] Properties to set
				 * @returns {smolweb.framed.FrameError.Decode} Decode instance
				 */
				Decode.create = function create(properties) {
					return new Decode(properties);
				};

				/**
				 * Encodes the specified Decode message. Does not implicitly {@link smolweb.framed.FrameError.Decode.verify|verify} messages.
				 * @function encode
				 * @memberof smolweb.framed.FrameError.Decode
				 * @static
				 * @param {smolweb.framed.FrameError.Decode} message Decode message or plain object to encode
				 * @param {ArrayBufferWriter} [writer] Writer to encode to
				 * @returns {ArrayBufferWriter} Writer
				 */
				Decode.encode = function encode(message, writer) {
					if (!writer)
						writer = $Writer.create();
					if (message.message != null && Object.hasOwnProperty.call(message, "message"))
						writer.uint32(/* id 1, wireType 2 =*/10).string(message.message);
					return writer;
				};

				/**
				 * Decodes a Decode message from the specified reader or buffer.
				 * @function decode
				 * @memberof smolweb.framed.FrameError.Decode
				 * @static
				 * @param {ArrayBufferReader|ArrayBuffer} reader Reader or buffer to decode from
				 * @param {number} [length] Message length if known beforehand
				 * @returns {smolweb.framed.FrameError.Decode} Decode
				 * @throws {Error} If the payload is not a reader or valid buffer
				 * @throws {$protobuf.util.ProtocolError} If required fields are missing
				 */
				Decode.decode = function decode(reader, length) {
					if (!(reader instanceof $Reader))
						reader = $Reader.create(reader);
					let end = length === undefined ? reader.len : reader.pos + length, message = new $root.smolweb.framed.FrameError.Decode();
					while (reader.pos < end) {
						let tag = reader.uint32();
						switch (tag >>> 3) {
						case 1: {
								message.message = reader.string();
								break;
							}
						default:
							reader.skipType(tag & 7);
							break;
						}
					}
					return message;
				};

				/**
				 * Verifies a Decode message.
				 * @function verify
				 * @memberof smolweb.framed.FrameError.Decode
				 * @static
				 * @param {Object.<string,*>} message Plain object to verify
				 * @returns {string|null} `null` if valid, otherwise the reason why it is not
				 */
				Decode.verify = function verify(message) {
					if (typeof message !== "object" || message === null)
						return "object expected";
					if (message.message != null && message.hasOwnProperty("message"))
						if (!$util.isString(message.message))
							return "message: string expected";
					return null;
				};

				/**
				 * Creates a Decode message from a plain object. Also converts values to their respective internal types.
				 * @function fromObject
				 * @memberof smolweb.framed.FrameError.Decode
				 * @static
				 * @param {Object.<string,*>} object Plain object
				 * @returns {smolweb.framed.FrameError.Decode} Decode
				 */
				Decode.fromObject = function fromObject(object) {
					if (object instanceof $root.smolweb.framed.FrameError.Decode)
						return object;
					let message = new $root.smolweb.framed.FrameError.Decode();
					if (object.message != null)
						message.message = String(object.message);
					return message;
				};

				/**
				 * Creates a plain object from a Decode message. Also converts values to other types if specified.
				 * @function toObject
				 * @memberof smolweb.framed.FrameError.Decode
				 * @static
				 * @param {smolweb.framed.FrameError.Decode} message Decode
				 * @param {$protobuf.IConversionOptions} [options] Conversion options
				 * @returns {Object.<string,*>} Plain object
				 */
				Decode.toObject = function toObject(message, options) {
					if (!options)
						options = {};
					let object = {};
					if (options.defaults)
						object.message = "";
					if (message.message != null && message.hasOwnProperty("message"))
						object.message = message.message;
					return object;
				};

				/**
				 * Converts this Decode to JSON.
				 * @function toJSON
				 * @memberof smolweb.framed.FrameError.Decode
				 * @instance
				 * @returns {Object.<string,*>} JSON object
				 */
				Decode.prototype.toJSON = function toJSON() {
					return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
				};

				/**
				 * Gets the default type url for Decode
				 * @function getTypeUrl
				 * @memberof smolweb.framed.FrameError.Decode
				 * @static
				 * @param {string} [typeUrlPrefix] your custom typeUrlPrefix(default "type.cuchazinteractive.org")
				 * @returns {string} The default type url
				 */
				Decode.getTypeUrl = function getTypeUrl(typeUrlPrefix) {
					if (typeUrlPrefix === undefined) {
						typeUrlPrefix = "type.cuchazinteractive.org";
					}
					return typeUrlPrefix + "/smolweb.framed.FrameError.Decode";
				};

				
				/**
				 * @returns {?string}
				 */		
				Decode.prototype.verify = function () {
					const err = Decode.verify(this);
					if (err != null) {
						throw new Error("Verification failed for Decode: " + err);
					}
				};
				
				/**
				 * @returns {ArrayBuffer}
				 */
				Decode.prototype.toBuffer = function () {
					return Decode.encode(this).finish();
				};
				
				/**
				 * @param {string | number} [spacer]
				 * @returns {string}
				 */
				Decode.prototype.toString = function (spacer) {
					return JSON.stringify(this.toJSON(), null, spacer);
				};
				
				/**
				 * @param {SaveMode} mode
				 * @returns {ArrayBuffer | string | Object}
				 */
				Decode.prototype.to = function (mode) {
					switch (mode) {
						case SaveModes.BUF: return this.toBuffer();
						case SaveModes.STR: return this.toString();
						case SaveModes.JSON: return this.toJSON();
						default: throw new Error("unrecognized save mode: " + mode);
					}
				};
				
				/**
				 * @param {smolweb_framed_FrameError_TDecode} obj
				 * @returns {smolweb.framed.FrameError.Decode}
				 */
				Decode.new = (obj) => {
					const msg = Decode.create(obj);
					msg.verify();
					return msg;
				};
				
				/**
				 * @param {ArrayBuffer} buf
				 * @returns {smolweb.framed.FrameError.Decode}
				 */
				Decode.fromBuffer = (buf) => {
					const msg = Decode.decode(buf);
					msg.verify();
					return msg;
				};
				
				/**
				 * @param {string} str
				 * @returns {smolweb.framed.FrameError.Decode}
				 */
				Decode.fromString = (str) => {
					const msg = Decode.fromJSON(JSON.parse(str));
					msg.verify();
					return msg;
				};
				
				/**
				 * @param {Object} json
				 * @returns {smolweb.framed.FrameError.Decode}
				 */
				Decode.fromJSON = (obj) => {
					const msg = Decode.fromObject(obj);
					msg.verify();
					return msg;
				};
				
				/**
				 * @param {smolweb.framed.FrameError.Decode | Object | ArrayBuffer | string} saved
				 * @returns {smolweb.framed.FrameError.Decode}
				 */
				Decode.from = (saved) => {
					if (saved instanceof Decode) {
						return saved;
					} else if (saved instanceof ArrayBuffer) {
						return Decode.fromBuffer(saved);
					} else if (typeof saved === 'string') {
						return Decode.fromString(saved);
					} else if (typeof saved === 'object') {
						return Decode.fromJSON(saved);
					} else {
						throw new Error("Unrecognized save format: " + saved?.constructor?.name);
					}
				};
				
				/**
				 * @returns {smolweb.framed.FrameError.Decode}
				 */
				Decode.prototype.copy = function () {
					// TODO: find more efficient way to do this?
					//   we only need to make defensive copies to prevent mutation
					//   but there's no need for a full buffer encoding/decoding roundtrip
					return this.constructor.fromBuffer(this.toBuffer());
				};
				
				/**
				 * WebStorm needs a little help understanding that this class implements ProtobufOpen
				 * @returns {ProtobufOpen.<smolweb.framed.FrameError.Decode>}
				 */
				Decode.opener = function () {
					return this;
				};
				
				return Decode;
				
			})();

			FrameError.UnexpectedKind = (function() {

				/**
				 * Properties of an UnexpectedKind.
				 * @memberof smolweb.framed.FrameError
				 * @typedef {Object} smolweb_framed_FrameError_TUnexpectedKind
				 * @property {Array.<string>|null} [expected] UnexpectedKind expected
				 * @property {string|null} [observed] UnexpectedKind observed
				 */

				/**
				 * Constructs a new UnexpectedKind.
				 * @memberof smolweb.framed.FrameError
				 * @classdesc Represents an UnexpectedKind.
				 * @implements ProtobufSave
				 * @constructor
				 * @param {smolweb.framed.FrameError.UnexpectedKind=} [properties] Properties to set
				 */
				function UnexpectedKind(properties) {
					this.expected = [];
					if (properties)
						for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
							if (properties[keys[i]] != null)
								this[keys[i]] = properties[keys[i]];
				}

				/**
				 * UnexpectedKind expected.
				 * @member {Array.<string>} expected
				 * @memberof smolweb.framed.FrameError.UnexpectedKind
				 * @instance
				 */
				UnexpectedKind.prototype.expected = $util.emptyArray;

				/**
				 * UnexpectedKind observed.
				 * @member {string} observed
				 * @memberof smolweb.framed.FrameError.UnexpectedKind
				 * @instance
				 */
				UnexpectedKind.prototype.observed = "";

				/**
				 * Creates a new UnexpectedKind instance using the specified properties.
				 * @function create
				 * @memberof smolweb.framed.FrameError.UnexpectedKind
				 * @static
				 * @param {smolweb.framed.FrameError.UnexpectedKind=} [properties] Properties to set
				 * @returns {smolweb.framed.FrameError.UnexpectedKind} UnexpectedKind instance
				 */
				UnexpectedKind.create = function create(properties) {
					return new UnexpectedKind(properties);
				};

				/**
				 * Encodes the specified UnexpectedKind message. Does not implicitly {@link smolweb.framed.FrameError.UnexpectedKind.verify|verify} messages.
				 * @function encode
				 * @memberof smolweb.framed.FrameError.UnexpectedKind
				 * @static
				 * @param {smolweb.framed.FrameError.UnexpectedKind} message UnexpectedKind message or plain object to encode
				 * @param {ArrayBufferWriter} [writer] Writer to encode to
				 * @returns {ArrayBufferWriter} Writer
				 */
				UnexpectedKind.encode = function encode(message, writer) {
					if (!writer)
						writer = $Writer.create();
					if (message.expected != null && message.expected.length)
						for (let i = 0; i < message.expected.length; ++i)
							writer.uint32(/* id 1, wireType 2 =*/10).string(message.expected[i]);
					if (message.observed != null && Object.hasOwnProperty.call(message, "observed"))
						writer.uint32(/* id 2, wireType 2 =*/18).string(message.observed);
					return writer;
				};

				/**
				 * Decodes an UnexpectedKind message from the specified reader or buffer.
				 * @function decode
				 * @memberof smolweb.framed.FrameError.UnexpectedKind
				 * @static
				 * @param {ArrayBufferReader|ArrayBuffer} reader Reader or buffer to decode from
				 * @param {number} [length] Message length if known beforehand
				 * @returns {smolweb.framed.FrameError.UnexpectedKind} UnexpectedKind
				 * @throws {Error} If the payload is not a reader or valid buffer
				 * @throws {$protobuf.util.ProtocolError} If required fields are missing
				 */
				UnexpectedKind.decode = function decode(reader, length) {
					if (!(reader instanceof $Reader))
						reader = $Reader.create(reader);
					let end = length === undefined ? reader.len : reader.pos + length, message = new $root.smolweb.framed.FrameError.UnexpectedKind();
					while (reader.pos < end) {
						let tag = reader.uint32();
						switch (tag >>> 3) {
						case 1: {
								if (!(message.expected && message.expected.length))
									message.expected = [];
								message.expected.push(reader.string());
								break;
							}
						case 2: {
								message.observed = reader.string();
								break;
							}
						default:
							reader.skipType(tag & 7);
							break;
						}
					}
					return message;
				};

				/**
				 * Verifies an UnexpectedKind message.
				 * @function verify
				 * @memberof smolweb.framed.FrameError.UnexpectedKind
				 * @static
				 * @param {Object.<string,*>} message Plain object to verify
				 * @returns {string|null} `null` if valid, otherwise the reason why it is not
				 */
				UnexpectedKind.verify = function verify(message) {
					if (typeof message !== "object" || message === null)
						return "object expected";
					if (message.expected != null && message.hasOwnProperty("expected")) {
						if (!Array.isArray(message.expected))
							return "expected: array expected";
						for (let i = 0; i < message.expected.length; ++i)
							if (!$util.isString(message.expected[i]))
								return "expected: string[] expected";
					}
					if (message.observed != null && message.hasOwnProperty("observed"))
						if (!$util.isString(message.observed))
							return "observed: string expected";
					return null;
				};

				/**
				 * Creates an UnexpectedKind message from a plain object. Also converts values to their respective internal types.
				 * @function fromObject
				 * @memberof smolweb.framed.FrameError.UnexpectedKind
				 * @static
				 * @param {Object.<string,*>} object Plain object
				 * @returns {smolweb.framed.FrameError.UnexpectedKind} UnexpectedKind
				 */
				UnexpectedKind.fromObject = function fromObject(object) {
					if (object instanceof $root.smolweb.framed.FrameError.UnexpectedKind)
						return object;
					let message = new $root.smolweb.framed.FrameError.UnexpectedKind();
					if (object.expected) {
						if (!Array.isArray(object.expected))
							throw TypeError(".smolweb.framed.FrameError.UnexpectedKind.expected: array expected");
						message.expected = [];
						for (let i = 0; i < object.expected.length; ++i)
							message.expected[i] = String(object.expected[i]);
					}
					if (object.observed != null)
						message.observed = String(object.observed);
					return message;
				};

				/**
				 * Creates a plain object from an UnexpectedKind message. Also converts values to other types if specified.
				 * @function toObject
				 * @memberof smolweb.framed.FrameError.UnexpectedKind
				 * @static
				 * @param {smolweb.framed.FrameError.UnexpectedKind} message UnexpectedKind
				 * @param {$protobuf.IConversionOptions} [options] Conversion options
				 * @returns {Object.<string,*>} Plain object
				 */
				UnexpectedKind.toObject = function toObject(message, options) {
					if (!options)
						options = {};
					let object = {};
					if (options.arrays || options.defaults)
						object.expected = [];
					if (options.defaults)
						object.observed = "";
					if (message.expected && message.expected.length) {
						object.expected = [];
						for (let j = 0; j < message.expected.length; ++j)
							object.expected[j] = message.expected[j];
					}
					if (message.observed != null && message.hasOwnProperty("observed"))
						object.observed = message.observed;
					return object;
				};

				/**
				 * Converts this UnexpectedKind to JSON.
				 * @function toJSON
				 * @memberof smolweb.framed.FrameError.UnexpectedKind
				 * @instance
				 * @returns {Object.<string,*>} JSON object
				 */
				UnexpectedKind.prototype.toJSON = function toJSON() {
					return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
				};

				/**
				 * Gets the default type url for UnexpectedKind
				 * @function getTypeUrl
				 * @memberof smolweb.framed.FrameError.UnexpectedKind
				 * @static
				 * @param {string} [typeUrlPrefix] your custom typeUrlPrefix(default "type.cuchazinteractive.org")
				 * @returns {string} The default type url
				 */
				UnexpectedKind.getTypeUrl = function getTypeUrl(typeUrlPrefix) {
					if (typeUrlPrefix === undefined) {
						typeUrlPrefix = "type.cuchazinteractive.org";
					}
					return typeUrlPrefix + "/smolweb.framed.FrameError.UnexpectedKind";
				};

				
				/**
				 * @returns {?string}
				 */		
				UnexpectedKind.prototype.verify = function () {
					const err = UnexpectedKind.verify(this);
					if (err != null) {
						throw new Error("Verification failed for UnexpectedKind: " + err);
					}
				};
				
				/**
				 * @returns {ArrayBuffer}
				 */
				UnexpectedKind.prototype.toBuffer = function () {
					return UnexpectedKind.encode(this).finish();
				};
				
				/**
				 * @param {string | number} [spacer]
				 * @returns {string}
				 */
				UnexpectedKind.prototype.toString = function (spacer) {
					return JSON.stringify(this.toJSON(), null, spacer);
				};
				
				/**
				 * @param {SaveMode} mode
				 * @returns {ArrayBuffer | string | Object}
				 */
				UnexpectedKind.prototype.to = function (mode) {
					switch (mode) {
						case SaveModes.BUF: return this.toBuffer();
						case SaveModes.STR: return this.toString();
						case SaveModes.JSON: return this.toJSON();
						default: throw new Error("unrecognized save mode: " + mode);
					}
				};
				
				/**
				 * @param {smolweb_framed_FrameError_TUnexpectedKind} obj
				 * @returns {smolweb.framed.FrameError.UnexpectedKind}
				 */
				UnexpectedKind.new = (obj) => {
					const msg = UnexpectedKind.create(obj);
					msg.verify();
					return msg;
				};
				
				/**
				 * @param {ArrayBuffer} buf
				 * @returns {smolweb.framed.FrameError.UnexpectedKind}
				 */
				UnexpectedKind.fromBuffer = (buf) => {
					const msg = UnexpectedKind.decode(buf);
					msg.verify();
					return msg;
				};
				
				/**
				 * @param {string} str
				 * @returns {smolweb.framed.FrameError.UnexpectedKind}
				 */
				UnexpectedKind.fromString = (str) => {
					const msg = UnexpectedKind.fromJSON(JSON.parse(str));
					msg.verify();
					return msg;
				};
				
				/**
				 * @param {Object} json
				 * @returns {smolweb.framed.FrameError.UnexpectedKind}
				 */
				UnexpectedKind.fromJSON = (obj) => {
					const msg = UnexpectedKind.fromObject(obj);
					msg.verify();
					return msg;
				};
				
				/**
				 * @param {smolweb.framed.FrameError.UnexpectedKind | Object | ArrayBuffer | string} saved
				 * @returns {smolweb.framed.FrameError.UnexpectedKind}
				 */
				UnexpectedKind.from = (saved) => {
					if (saved instanceof UnexpectedKind) {
						return saved;
					} else if (saved instanceof ArrayBuffer) {
						return UnexpectedKind.fromBuffer(saved);
					} else if (typeof saved === 'string') {
						return UnexpectedKind.fromString(saved);
					} else if (typeof saved === 'object') {
						return UnexpectedKind.fromJSON(saved);
					} else {
						throw new Error("Unrecognized save format: " + saved?.constructor?.name);
					}
				};
				
				/**
				 * @returns {smolweb.framed.FrameError.UnexpectedKind}
				 */
				UnexpectedKind.prototype.copy = function () {
					// TODO: find more efficient way to do this?
					//   we only need to make defensive copies to prevent mutation
					//   but there's no need for a full buffer encoding/decoding roundtrip
					return this.constructor.fromBuffer(this.toBuffer());
				};
				
				/**
				 * WebStorm needs a little help understanding that this class implements ProtobufOpen
				 * @returns {ProtobufOpen.<smolweb.framed.FrameError.UnexpectedKind>}
				 */
				UnexpectedKind.opener = function () {
					return this;
				};
				
				return UnexpectedKind;
				
			})();

			FrameError.InvalidStreamSize = (function() {

				/**
				 * Properties of an InvalidStreamSize.
				 * @memberof smolweb.framed.FrameError
				 * @typedef {Object} smolweb_framed_FrameError_TInvalidStreamSize
				 * @property {number|bigint|null} [observed] InvalidStreamSize observed
				 */

				/**
				 * Constructs a new InvalidStreamSize.
				 * @memberof smolweb.framed.FrameError
				 * @classdesc Represents an InvalidStreamSize.
				 * @implements ProtobufSave
				 * @constructor
				 * @param {smolweb.framed.FrameError.InvalidStreamSize=} [properties] Properties to set
				 */
				function InvalidStreamSize(properties) {
					if (properties)
						for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
							if (properties[keys[i]] != null)
								this[keys[i]] = properties[keys[i]];
				}

				/**
				 * InvalidStreamSize observed.
				 * @member {number|bigint} observed
				 * @memberof smolweb.framed.FrameError.InvalidStreamSize
				 * @instance
				 */
				InvalidStreamSize.prototype.observed = $util.Long ? $util.Long.fromBits(0,0,false) : 0;

				/**
				 * Creates a new InvalidStreamSize instance using the specified properties.
				 * @function create
				 * @memberof smolweb.framed.FrameError.InvalidStreamSize
				 * @static
				 * @param {smolweb.framed.FrameError.InvalidStreamSize=} [properties] Properties to set
				 * @returns {smolweb.framed.FrameError.InvalidStreamSize} InvalidStreamSize instance
				 */
				InvalidStreamSize.create = function create(properties) {
					return new InvalidStreamSize(properties);
				};

				/**
				 * Encodes the specified InvalidStreamSize message. Does not implicitly {@link smolweb.framed.FrameError.InvalidStreamSize.verify|verify} messages.
				 * @function encode
				 * @memberof smolweb.framed.FrameError.InvalidStreamSize
				 * @static
				 * @param {smolweb.framed.FrameError.InvalidStreamSize} message InvalidStreamSize message or plain object to encode
				 * @param {ArrayBufferWriter} [writer] Writer to encode to
				 * @returns {ArrayBufferWriter} Writer
				 */
				InvalidStreamSize.encode = function encode(message, writer) {
					if (!writer)
						writer = $Writer.create();
					if (message.observed != null && Object.hasOwnProperty.call(message, "observed"))
						writer.uint32(/* id 1, wireType 0 =*/8).int64(message.observed);
					return writer;
				};

				/**
				 * Decodes an InvalidStreamSize message from the specified reader or buffer.
				 * @function decode
				 * @memberof smolweb.framed.FrameError.InvalidStreamSize
				 * @static
				 * @param {ArrayBufferReader|ArrayBuffer} reader Reader or buffer to decode from
				 * @param {number} [length] Message length if known beforehand
				 * @returns {smolweb.framed.FrameError.InvalidStreamSize} InvalidStreamSize
				 * @throws {Error} If the payload is not a reader or valid buffer
				 * @throws {$protobuf.util.ProtocolError} If required fields are missing
				 */
				InvalidStreamSize.decode = function decode(reader, length) {
					if (!(reader instanceof $Reader))
						reader = $Reader.create(reader);
					let end = length === undefined ? reader.len : reader.pos + length, message = new $root.smolweb.framed.FrameError.InvalidStreamSize();
					while (reader.pos < end) {
						let tag = reader.uint32();
						switch (tag >>> 3) {
						case 1: {
								message.observed = reader.int64();
								break;
							}
						default:
							reader.skipType(tag & 7);
							break;
						}
					}
					return message;
				};

				/**
				 * Verifies an InvalidStreamSize message.
				 * @function verify
				 * @memberof smolweb.framed.FrameError.InvalidStreamSize
				 * @static
				 * @param {Object.<string,*>} message Plain object to verify
				 * @returns {string|null} `null` if valid, otherwise the reason why it is not
				 */
				InvalidStreamSize.verify = function verify(message) {
					if (typeof message !== "object" || message === null)
						return "object expected";
					if (message.observed != null && message.hasOwnProperty("observed"))
						if (!$util.isInteger(message.observed) && !(typeof message.observed === 'bigint'))
							return "observed: integer|bigint expected";
					return null;
				};

				/**
				 * Creates an InvalidStreamSize message from a plain object. Also converts values to their respective internal types.
				 * @function fromObject
				 * @memberof smolweb.framed.FrameError.InvalidStreamSize
				 * @static
				 * @param {Object.<string,*>} object Plain object
				 * @returns {smolweb.framed.FrameError.InvalidStreamSize} InvalidStreamSize
				 */
				InvalidStreamSize.fromObject = function fromObject(object) {
					if (object instanceof $root.smolweb.framed.FrameError.InvalidStreamSize)
						return object;
					let message = new $root.smolweb.framed.FrameError.InvalidStreamSize();
					if (object.observed != null)
						if ($util.Long)
							message.observed = BigInt(object.observed);
						else if (typeof object.observed === "string")
							message.observed = parseInt(object.observed, 10);
						else if (typeof object.observed === "number")
							message.observed = object.observed;
						else if (typeof object.observed === "object")
							message.observed = new $util.LongBits(object.observed.low >>> 0, object.observed.high >>> 0).toNumber();
					return message;
				};

				/**
				 * Creates a plain object from an InvalidStreamSize message. Also converts values to other types if specified.
				 * @function toObject
				 * @memberof smolweb.framed.FrameError.InvalidStreamSize
				 * @static
				 * @param {smolweb.framed.FrameError.InvalidStreamSize} message InvalidStreamSize
				 * @param {$protobuf.IConversionOptions} [options] Conversion options
				 * @returns {Object.<string,*>} Plain object
				 */
				InvalidStreamSize.toObject = function toObject(message, options) {
					if (!options)
						options = {};
					let object = {};
					if (options.defaults)
						if ($util.Long) {
							let long = $util.BigInt.fromBits(0, 0, false);
							object.observed = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
						} else
							object.observed = options.longs === String ? "0" : 0;
					if (message.observed != null && message.hasOwnProperty("observed"))
						if (typeof message.observed === "number")
							object.observed = options.longs === String ? String(message.observed) : message.observed;
						else
							object.observed = options.longs === String ? message.observed.toString() : options.longs === Number ? new $util.LongBits(message.observed.low >>> 0, message.observed.high >>> 0).toNumber() : message.observed;
					return object;
				};

				/**
				 * Converts this InvalidStreamSize to JSON.
				 * @function toJSON
				 * @memberof smolweb.framed.FrameError.InvalidStreamSize
				 * @instance
				 * @returns {Object.<string,*>} JSON object
				 */
				InvalidStreamSize.prototype.toJSON = function toJSON() {
					return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
				};

				/**
				 * Gets the default type url for InvalidStreamSize
				 * @function getTypeUrl
				 * @memberof smolweb.framed.FrameError.InvalidStreamSize
				 * @static
				 * @param {string} [typeUrlPrefix] your custom typeUrlPrefix(default "type.cuchazinteractive.org")
				 * @returns {string} The default type url
				 */
				InvalidStreamSize.getTypeUrl = function getTypeUrl(typeUrlPrefix) {
					if (typeUrlPrefix === undefined) {
						typeUrlPrefix = "type.cuchazinteractive.org";
					}
					return typeUrlPrefix + "/smolweb.framed.FrameError.InvalidStreamSize";
				};

				
				/**
				 * @returns {?string}
				 */		
				InvalidStreamSize.prototype.verify = function () {
					const err = InvalidStreamSize.verify(this);
					if (err != null) {
						throw new Error("Verification failed for InvalidStreamSize: " + err);
					}
				};
				
				/**
				 * @returns {ArrayBuffer}
				 */
				InvalidStreamSize.prototype.toBuffer = function () {
					return InvalidStreamSize.encode(this).finish();
				};
				
				/**
				 * @param {string | number} [spacer]
				 * @returns {string}
				 */
				InvalidStreamSize.prototype.toString = function (spacer) {
					return JSON.stringify(this.toJSON(), null, spacer);
				};
				
				/**
				 * @param {SaveMode} mode
				 * @returns {ArrayBuffer | string | Object}
				 */
				InvalidStreamSize.prototype.to = function (mode) {
					switch (mode) {
						case SaveModes.BUF: return this.toBuffer();
						case SaveModes.STR: return this.toString();
						case SaveModes.JSON: return this.toJSON();
						default: throw new Error("unrecognized save mode: " + mode);
					}
				};
				
				/**
				 * @param {smolweb_framed_FrameError_TInvalidStreamSize} obj
				 * @returns {smolweb.framed.FrameError.InvalidStreamSize}
				 */
				InvalidStreamSize.new = (obj) => {
					const msg = InvalidStreamSize.create(obj);
					msg.verify();
					return msg;
				};
				
				/**
				 * @param {ArrayBuffer} buf
				 * @returns {smolweb.framed.FrameError.InvalidStreamSize}
				 */
				InvalidStreamSize.fromBuffer = (buf) => {
					const msg = InvalidStreamSize.decode(buf);
					msg.verify();
					return msg;
				};
				
				/**
				 * @param {string} str
				 * @returns {smolweb.framed.FrameError.InvalidStreamSize}
				 */
				InvalidStreamSize.fromString = (str) => {
					const msg = InvalidStreamSize.fromJSON(JSON.parse(str));
					msg.verify();
					return msg;
				};
				
				/**
				 * @param {Object} json
				 * @returns {smolweb.framed.FrameError.InvalidStreamSize}
				 */
				InvalidStreamSize.fromJSON = (obj) => {
					const msg = InvalidStreamSize.fromObject(obj);
					msg.verify();
					return msg;
				};
				
				/**
				 * @param {smolweb.framed.FrameError.InvalidStreamSize | Object | ArrayBuffer | string} saved
				 * @returns {smolweb.framed.FrameError.InvalidStreamSize}
				 */
				InvalidStreamSize.from = (saved) => {
					if (saved instanceof InvalidStreamSize) {
						return saved;
					} else if (saved instanceof ArrayBuffer) {
						return InvalidStreamSize.fromBuffer(saved);
					} else if (typeof saved === 'string') {
						return InvalidStreamSize.fromString(saved);
					} else if (typeof saved === 'object') {
						return InvalidStreamSize.fromJSON(saved);
					} else {
						throw new Error("Unrecognized save format: " + saved?.constructor?.name);
					}
				};
				
				/**
				 * @returns {smolweb.framed.FrameError.InvalidStreamSize}
				 */
				InvalidStreamSize.prototype.copy = function () {
					// TODO: find more efficient way to do this?
					//   we only need to make defensive copies to prevent mutation
					//   but there's no need for a full buffer encoding/decoding roundtrip
					return this.constructor.fromBuffer(this.toBuffer());
				};
				
				/**
				 * WebStorm needs a little help understanding that this class implements ProtobufOpen
				 * @returns {ProtobufOpen.<smolweb.framed.FrameError.InvalidStreamSize>}
				 */
				InvalidStreamSize.opener = function () {
					return this;
				};
				
				return InvalidStreamSize;
				
			})();

			FrameError.UnexpectedFrameIndex = (function() {

				/**
				 * Properties of an UnexpectedFrameIndex.
				 * @memberof smolweb.framed.FrameError
				 * @typedef {Object} smolweb_framed_FrameError_TUnexpectedFrameIndex
				 * @property {number|bigint|null} [expected] UnexpectedFrameIndex expected
				 * @property {number|bigint|null} [observed] UnexpectedFrameIndex observed
				 */

				/**
				 * Constructs a new UnexpectedFrameIndex.
				 * @memberof smolweb.framed.FrameError
				 * @classdesc Represents an UnexpectedFrameIndex.
				 * @implements ProtobufSave
				 * @constructor
				 * @param {smolweb.framed.FrameError.UnexpectedFrameIndex=} [properties] Properties to set
				 */
				function UnexpectedFrameIndex(properties) {
					if (properties)
						for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
							if (properties[keys[i]] != null)
								this[keys[i]] = properties[keys[i]];
				}

				/**
				 * UnexpectedFrameIndex expected.
				 * @member {number|bigint} expected
				 * @memberof smolweb.framed.FrameError.UnexpectedFrameIndex
				 * @instance
				 */
				UnexpectedFrameIndex.prototype.expected = $util.Long ? $util.Long.fromBits(0,0,false) : 0;

				/**
				 * UnexpectedFrameIndex observed.
				 * @member {number|bigint} observed
				 * @memberof smolweb.framed.FrameError.UnexpectedFrameIndex
				 * @instance
				 */
				UnexpectedFrameIndex.prototype.observed = $util.Long ? $util.Long.fromBits(0,0,false) : 0;

				/**
				 * Creates a new UnexpectedFrameIndex instance using the specified properties.
				 * @function create
				 * @memberof smolweb.framed.FrameError.UnexpectedFrameIndex
				 * @static
				 * @param {smolweb.framed.FrameError.UnexpectedFrameIndex=} [properties] Properties to set
				 * @returns {smolweb.framed.FrameError.UnexpectedFrameIndex} UnexpectedFrameIndex instance
				 */
				UnexpectedFrameIndex.create = function create(properties) {
					return new UnexpectedFrameIndex(properties);
				};

				/**
				 * Encodes the specified UnexpectedFrameIndex message. Does not implicitly {@link smolweb.framed.FrameError.UnexpectedFrameIndex.verify|verify} messages.
				 * @function encode
				 * @memberof smolweb.framed.FrameError.UnexpectedFrameIndex
				 * @static
				 * @param {smolweb.framed.FrameError.UnexpectedFrameIndex} message UnexpectedFrameIndex message or plain object to encode
				 * @param {ArrayBufferWriter} [writer] Writer to encode to
				 * @returns {ArrayBufferWriter} Writer
				 */
				UnexpectedFrameIndex.encode = function encode(message, writer) {
					if (!writer)
						writer = $Writer.create();
					if (message.expected != null && Object.hasOwnProperty.call(message, "expected"))
						writer.uint32(/* id 1, wireType 0 =*/8).int64(message.expected);
					if (message.observed != null && Object.hasOwnProperty.call(message, "observed"))
						writer.uint32(/* id 2, wireType 0 =*/16).int64(message.observed);
					return writer;
				};

				/**
				 * Decodes an UnexpectedFrameIndex message from the specified reader or buffer.
				 * @function decode
				 * @memberof smolweb.framed.FrameError.UnexpectedFrameIndex
				 * @static
				 * @param {ArrayBufferReader|ArrayBuffer} reader Reader or buffer to decode from
				 * @param {number} [length] Message length if known beforehand
				 * @returns {smolweb.framed.FrameError.UnexpectedFrameIndex} UnexpectedFrameIndex
				 * @throws {Error} If the payload is not a reader or valid buffer
				 * @throws {$protobuf.util.ProtocolError} If required fields are missing
				 */
				UnexpectedFrameIndex.decode = function decode(reader, length) {
					if (!(reader instanceof $Reader))
						reader = $Reader.create(reader);
					let end = length === undefined ? reader.len : reader.pos + length, message = new $root.smolweb.framed.FrameError.UnexpectedFrameIndex();
					while (reader.pos < end) {
						let tag = reader.uint32();
						switch (tag >>> 3) {
						case 1: {
								message.expected = reader.int64();
								break;
							}
						case 2: {
								message.observed = reader.int64();
								break;
							}
						default:
							reader.skipType(tag & 7);
							break;
						}
					}
					return message;
				};

				/**
				 * Verifies an UnexpectedFrameIndex message.
				 * @function verify
				 * @memberof smolweb.framed.FrameError.UnexpectedFrameIndex
				 * @static
				 * @param {Object.<string,*>} message Plain object to verify
				 * @returns {string|null} `null` if valid, otherwise the reason why it is not
				 */
				UnexpectedFrameIndex.verify = function verify(message) {
					if (typeof message !== "object" || message === null)
						return "object expected";
					if (message.expected != null && message.hasOwnProperty("expected"))
						if (!$util.isInteger(message.expected) && !(typeof message.expected === 'bigint'))
							return "expected: integer|bigint expected";
					if (message.observed != null && message.hasOwnProperty("observed"))
						if (!$util.isInteger(message.observed) && !(typeof message.observed === 'bigint'))
							return "observed: integer|bigint expected";
					return null;
				};

				/**
				 * Creates an UnexpectedFrameIndex message from a plain object. Also converts values to their respective internal types.
				 * @function fromObject
				 * @memberof smolweb.framed.FrameError.UnexpectedFrameIndex
				 * @static
				 * @param {Object.<string,*>} object Plain object
				 * @returns {smolweb.framed.FrameError.UnexpectedFrameIndex} UnexpectedFrameIndex
				 */
				UnexpectedFrameIndex.fromObject = function fromObject(object) {
					if (object instanceof $root.smolweb.framed.FrameError.UnexpectedFrameIndex)
						return object;
					let message = new $root.smolweb.framed.FrameError.UnexpectedFrameIndex();
					if (object.expected != null)
						if ($util.Long)
							message.expected = BigInt(object.expected);
						else if (typeof object.expected === "string")
							message.expected = parseInt(object.expected, 10);
						else if (typeof object.expected === "number")
							message.expected = object.expected;
						else if (typeof object.expected === "object")
							message.expected = new $util.LongBits(object.expected.low >>> 0, object.expected.high >>> 0).toNumber();
					if (object.observed != null)
						if ($util.Long)
							message.observed = BigInt(object.observed);
						else if (typeof object.observed === "string")
							message.observed = parseInt(object.observed, 10);
						else if (typeof object.observed === "number")
							message.observed = object.observed;
						else if (typeof object.observed === "object")
							message.observed = new $util.LongBits(object.observed.low >>> 0, object.observed.high >>> 0).toNumber();
					return message;
				};

				/**
				 * Creates a plain object from an UnexpectedFrameIndex message. Also converts values to other types if specified.
				 * @function toObject
				 * @memberof smolweb.framed.FrameError.UnexpectedFrameIndex
				 * @static
				 * @param {smolweb.framed.FrameError.UnexpectedFrameIndex} message UnexpectedFrameIndex
				 * @param {$protobuf.IConversionOptions} [options] Conversion options
				 * @returns {Object.<string,*>} Plain object
				 */
				UnexpectedFrameIndex.toObject = function toObject(message, options) {
					if (!options)
						options = {};
					let object = {};
					if (options.defaults) {
						if ($util.Long) {
							let long = $util.BigInt.fromBits(0, 0, false);
							object.expected = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
						} else
							object.expected = options.longs === String ? "0" : 0;
						if ($util.Long) {
							let long = $util.BigInt.fromBits(0, 0, false);
							object.observed = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
						} else
							object.observed = options.longs === String ? "0" : 0;
					}
					if (message.expected != null && message.hasOwnProperty("expected"))
						if (typeof message.expected === "number")
							object.expected = options.longs === String ? String(message.expected) : message.expected;
						else
							object.expected = options.longs === String ? message.expected.toString() : options.longs === Number ? new $util.LongBits(message.expected.low >>> 0, message.expected.high >>> 0).toNumber() : message.expected;
					if (message.observed != null && message.hasOwnProperty("observed"))
						if (typeof message.observed === "number")
							object.observed = options.longs === String ? String(message.observed) : message.observed;
						else
							object.observed = options.longs === String ? message.observed.toString() : options.longs === Number ? new $util.LongBits(message.observed.low >>> 0, message.observed.high >>> 0).toNumber() : message.observed;
					return object;
				};

				/**
				 * Converts this UnexpectedFrameIndex to JSON.
				 * @function toJSON
				 * @memberof smolweb.framed.FrameError.UnexpectedFrameIndex
				 * @instance
				 * @returns {Object.<string,*>} JSON object
				 */
				UnexpectedFrameIndex.prototype.toJSON = function toJSON() {
					return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
				};

				/**
				 * Gets the default type url for UnexpectedFrameIndex
				 * @function getTypeUrl
				 * @memberof smolweb.framed.FrameError.UnexpectedFrameIndex
				 * @static
				 * @param {string} [typeUrlPrefix] your custom typeUrlPrefix(default "type.cuchazinteractive.org")
				 * @returns {string} The default type url
				 */
				UnexpectedFrameIndex.getTypeUrl = function getTypeUrl(typeUrlPrefix) {
					if (typeUrlPrefix === undefined) {
						typeUrlPrefix = "type.cuchazinteractive.org";
					}
					return typeUrlPrefix + "/smolweb.framed.FrameError.UnexpectedFrameIndex";
				};

				
				/**
				 * @returns {?string}
				 */		
				UnexpectedFrameIndex.prototype.verify = function () {
					const err = UnexpectedFrameIndex.verify(this);
					if (err != null) {
						throw new Error("Verification failed for UnexpectedFrameIndex: " + err);
					}
				};
				
				/**
				 * @returns {ArrayBuffer}
				 */
				UnexpectedFrameIndex.prototype.toBuffer = function () {
					return UnexpectedFrameIndex.encode(this).finish();
				};
				
				/**
				 * @param {string | number} [spacer]
				 * @returns {string}
				 */
				UnexpectedFrameIndex.prototype.toString = function (spacer) {
					return JSON.stringify(this.toJSON(), null, spacer);
				};
				
				/**
				 * @param {SaveMode} mode
				 * @returns {ArrayBuffer | string | Object}
				 */
				UnexpectedFrameIndex.prototype.to = function (mode) {
					switch (mode) {
						case SaveModes.BUF: return this.toBuffer();
						case SaveModes.STR: return this.toString();
						case SaveModes.JSON: return this.toJSON();
						default: throw new Error("unrecognized save mode: " + mode);
					}
				};
				
				/**
				 * @param {smolweb_framed_FrameError_TUnexpectedFrameIndex} obj
				 * @returns {smolweb.framed.FrameError.UnexpectedFrameIndex}
				 */
				UnexpectedFrameIndex.new = (obj) => {
					const msg = UnexpectedFrameIndex.create(obj);
					msg.verify();
					return msg;
				};
				
				/**
				 * @param {ArrayBuffer} buf
				 * @returns {smolweb.framed.FrameError.UnexpectedFrameIndex}
				 */
				UnexpectedFrameIndex.fromBuffer = (buf) => {
					const msg = UnexpectedFrameIndex.decode(buf);
					msg.verify();
					return msg;
				};
				
				/**
				 * @param {string} str
				 * @returns {smolweb.framed.FrameError.UnexpectedFrameIndex}
				 */
				UnexpectedFrameIndex.fromString = (str) => {
					const msg = UnexpectedFrameIndex.fromJSON(JSON.parse(str));
					msg.verify();
					return msg;
				};
				
				/**
				 * @param {Object} json
				 * @returns {smolweb.framed.FrameError.UnexpectedFrameIndex}
				 */
				UnexpectedFrameIndex.fromJSON = (obj) => {
					const msg = UnexpectedFrameIndex.fromObject(obj);
					msg.verify();
					return msg;
				};
				
				/**
				 * @param {smolweb.framed.FrameError.UnexpectedFrameIndex | Object | ArrayBuffer | string} saved
				 * @returns {smolweb.framed.FrameError.UnexpectedFrameIndex}
				 */
				UnexpectedFrameIndex.from = (saved) => {
					if (saved instanceof UnexpectedFrameIndex) {
						return saved;
					} else if (saved instanceof ArrayBuffer) {
						return UnexpectedFrameIndex.fromBuffer(saved);
					} else if (typeof saved === 'string') {
						return UnexpectedFrameIndex.fromString(saved);
					} else if (typeof saved === 'object') {
						return UnexpectedFrameIndex.fromJSON(saved);
					} else {
						throw new Error("Unrecognized save format: " + saved?.constructor?.name);
					}
				};
				
				/**
				 * @returns {smolweb.framed.FrameError.UnexpectedFrameIndex}
				 */
				UnexpectedFrameIndex.prototype.copy = function () {
					// TODO: find more efficient way to do this?
					//   we only need to make defensive copies to prevent mutation
					//   but there's no need for a full buffer encoding/decoding roundtrip
					return this.constructor.fromBuffer(this.toBuffer());
				};
				
				/**
				 * WebStorm needs a little help understanding that this class implements ProtobufOpen
				 * @returns {ProtobufOpen.<smolweb.framed.FrameError.UnexpectedFrameIndex>}
				 */
				UnexpectedFrameIndex.opener = function () {
					return this;
				};
				
				return UnexpectedFrameIndex;
				
			})();

			FrameError.UnexpectedPayloadSize = (function() {

				/**
				 * Properties of an UnexpectedPayloadSize.
				 * @memberof smolweb.framed.FrameError
				 * @typedef {Object} smolweb_framed_FrameError_TUnexpectedPayloadSize
				 * @property {number|bigint|null} [expected] UnexpectedPayloadSize expected
				 * @property {number|bigint|null} [observed] UnexpectedPayloadSize observed
				 */

				/**
				 * Constructs a new UnexpectedPayloadSize.
				 * @memberof smolweb.framed.FrameError
				 * @classdesc Represents an UnexpectedPayloadSize.
				 * @implements ProtobufSave
				 * @constructor
				 * @param {smolweb.framed.FrameError.UnexpectedPayloadSize=} [properties] Properties to set
				 */
				function UnexpectedPayloadSize(properties) {
					if (properties)
						for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
							if (properties[keys[i]] != null)
								this[keys[i]] = properties[keys[i]];
				}

				/**
				 * UnexpectedPayloadSize expected.
				 * @member {number|bigint} expected
				 * @memberof smolweb.framed.FrameError.UnexpectedPayloadSize
				 * @instance
				 */
				UnexpectedPayloadSize.prototype.expected = $util.Long ? $util.Long.fromBits(0,0,true) : 0;

				/**
				 * UnexpectedPayloadSize observed.
				 * @member {number|bigint} observed
				 * @memberof smolweb.framed.FrameError.UnexpectedPayloadSize
				 * @instance
				 */
				UnexpectedPayloadSize.prototype.observed = $util.Long ? $util.Long.fromBits(0,0,true) : 0;

				/**
				 * Creates a new UnexpectedPayloadSize instance using the specified properties.
				 * @function create
				 * @memberof smolweb.framed.FrameError.UnexpectedPayloadSize
				 * @static
				 * @param {smolweb.framed.FrameError.UnexpectedPayloadSize=} [properties] Properties to set
				 * @returns {smolweb.framed.FrameError.UnexpectedPayloadSize} UnexpectedPayloadSize instance
				 */
				UnexpectedPayloadSize.create = function create(properties) {
					return new UnexpectedPayloadSize(properties);
				};

				/**
				 * Encodes the specified UnexpectedPayloadSize message. Does not implicitly {@link smolweb.framed.FrameError.UnexpectedPayloadSize.verify|verify} messages.
				 * @function encode
				 * @memberof smolweb.framed.FrameError.UnexpectedPayloadSize
				 * @static
				 * @param {smolweb.framed.FrameError.UnexpectedPayloadSize} message UnexpectedPayloadSize message or plain object to encode
				 * @param {ArrayBufferWriter} [writer] Writer to encode to
				 * @returns {ArrayBufferWriter} Writer
				 */
				UnexpectedPayloadSize.encode = function encode(message, writer) {
					if (!writer)
						writer = $Writer.create();
					if (message.expected != null && Object.hasOwnProperty.call(message, "expected"))
						writer.uint32(/* id 1, wireType 0 =*/8).uint64(message.expected);
					if (message.observed != null && Object.hasOwnProperty.call(message, "observed"))
						writer.uint32(/* id 2, wireType 0 =*/16).uint64(message.observed);
					return writer;
				};

				/**
				 * Decodes an UnexpectedPayloadSize message from the specified reader or buffer.
				 * @function decode
				 * @memberof smolweb.framed.FrameError.UnexpectedPayloadSize
				 * @static
				 * @param {ArrayBufferReader|ArrayBuffer} reader Reader or buffer to decode from
				 * @param {number} [length] Message length if known beforehand
				 * @returns {smolweb.framed.FrameError.UnexpectedPayloadSize} UnexpectedPayloadSize
				 * @throws {Error} If the payload is not a reader or valid buffer
				 * @throws {$protobuf.util.ProtocolError} If required fields are missing
				 */
				UnexpectedPayloadSize.decode = function decode(reader, length) {
					if (!(reader instanceof $Reader))
						reader = $Reader.create(reader);
					let end = length === undefined ? reader.len : reader.pos + length, message = new $root.smolweb.framed.FrameError.UnexpectedPayloadSize();
					while (reader.pos < end) {
						let tag = reader.uint32();
						switch (tag >>> 3) {
						case 1: {
								message.expected = reader.uint64();
								break;
							}
						case 2: {
								message.observed = reader.uint64();
								break;
							}
						default:
							reader.skipType(tag & 7);
							break;
						}
					}
					return message;
				};

				/**
				 * Verifies an UnexpectedPayloadSize message.
				 * @function verify
				 * @memberof smolweb.framed.FrameError.UnexpectedPayloadSize
				 * @static
				 * @param {Object.<string,*>} message Plain object to verify
				 * @returns {string|null} `null` if valid, otherwise the reason why it is not
				 */
				UnexpectedPayloadSize.verify = function verify(message) {
					if (typeof message !== "object" || message === null)
						return "object expected";
					if (message.expected != null && message.hasOwnProperty("expected"))
						if (!$util.isInteger(message.expected) && !(typeof message.expected === 'bigint'))
							return "expected: integer|bigint expected";
					if (message.observed != null && message.hasOwnProperty("observed"))
						if (!$util.isInteger(message.observed) && !(typeof message.observed === 'bigint'))
							return "observed: integer|bigint expected";
					return null;
				};

				/**
				 * Creates an UnexpectedPayloadSize message from a plain object. Also converts values to their respective internal types.
				 * @function fromObject
				 * @memberof smolweb.framed.FrameError.UnexpectedPayloadSize
				 * @static
				 * @param {Object.<string,*>} object Plain object
				 * @returns {smolweb.framed.FrameError.UnexpectedPayloadSize} UnexpectedPayloadSize
				 */
				UnexpectedPayloadSize.fromObject = function fromObject(object) {
					if (object instanceof $root.smolweb.framed.FrameError.UnexpectedPayloadSize)
						return object;
					let message = new $root.smolweb.framed.FrameError.UnexpectedPayloadSize();
					if (object.expected != null)
						if ($util.Long)
							message.expected = BigInt(object.expected);
						else if (typeof object.expected === "string")
							message.expected = parseInt(object.expected, 10);
						else if (typeof object.expected === "number")
							message.expected = object.expected;
						else if (typeof object.expected === "object")
							message.expected = new $util.LongBits(object.expected.low >>> 0, object.expected.high >>> 0).toNumber(true);
					if (object.observed != null)
						if ($util.Long)
							message.observed = BigInt(object.observed);
						else if (typeof object.observed === "string")
							message.observed = parseInt(object.observed, 10);
						else if (typeof object.observed === "number")
							message.observed = object.observed;
						else if (typeof object.observed === "object")
							message.observed = new $util.LongBits(object.observed.low >>> 0, object.observed.high >>> 0).toNumber(true);
					return message;
				};

				/**
				 * Creates a plain object from an UnexpectedPayloadSize message. Also converts values to other types if specified.
				 * @function toObject
				 * @memberof smolweb.framed.FrameError.UnexpectedPayloadSize
				 * @static
				 * @param {smolweb.framed.FrameError.UnexpectedPayloadSize} message UnexpectedPayloadSize
				 * @param {$protobuf.IConversionOptions} [options] Conversion options
				 * @returns {Object.<string,*>} Plain object
				 */
				UnexpectedPayloadSize.toObject = function toObject(message, options) {
					if (!options)
						options = {};
					let object = {};
					if (options.defaults) {
						if ($util.Long) {
							let long = $util.BigInt.fromBits(0, 0, true);
							object.expected = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
						} else
							object.expected = options.longs === String ? "0" : 0;
						if ($util.Long) {
							let long = $util.BigInt.fromBits(0, 0, true);
							object.observed = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
						} else
							object.observed = options.longs === String ? "0" : 0;
					}
					if (message.expected != null && message.hasOwnProperty("expected"))
						if (typeof message.expected === "number")
							object.expected = options.longs === String ? String(message.expected) : message.expected;
						else
							object.expected = options.longs === String ? message.expected.toString() : options.longs === Number ? new $util.LongBits(message.expected.low >>> 0, message.expected.high >>> 0).toNumber(true) : message.expected;
					if (message.observed != null && message.hasOwnProperty("observed"))
						if (typeof message.observed === "number")
							object.observed = options.longs === String ? String(message.observed) : message.observed;
						else
							object.observed = options.longs === String ? message.observed.toString() : options.longs === Number ? new $util.LongBits(message.observed.low >>> 0, message.observed.high >>> 0).toNumber(true) : message.observed;
					return object;
				};

				/**
				 * Converts this UnexpectedPayloadSize to JSON.
				 * @function toJSON
				 * @memberof smolweb.framed.FrameError.UnexpectedPayloadSize
				 * @instance
				 * @returns {Object.<string,*>} JSON object
				 */
				UnexpectedPayloadSize.prototype.toJSON = function toJSON() {
					return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
				};

				/**
				 * Gets the default type url for UnexpectedPayloadSize
				 * @function getTypeUrl
				 * @memberof smolweb.framed.FrameError.UnexpectedPayloadSize
				 * @static
				 * @param {string} [typeUrlPrefix] your custom typeUrlPrefix(default "type.cuchazinteractive.org")
				 * @returns {string} The default type url
				 */
				UnexpectedPayloadSize.getTypeUrl = function getTypeUrl(typeUrlPrefix) {
					if (typeUrlPrefix === undefined) {
						typeUrlPrefix = "type.cuchazinteractive.org";
					}
					return typeUrlPrefix + "/smolweb.framed.FrameError.UnexpectedPayloadSize";
				};

				
				/**
				 * @returns {?string}
				 */		
				UnexpectedPayloadSize.prototype.verify = function () {
					const err = UnexpectedPayloadSize.verify(this);
					if (err != null) {
						throw new Error("Verification failed for UnexpectedPayloadSize: " + err);
					}
				};
				
				/**
				 * @returns {ArrayBuffer}
				 */
				UnexpectedPayloadSize.prototype.toBuffer = function () {
					return UnexpectedPayloadSize.encode(this).finish();
				};
				
				/**
				 * @param {string | number} [spacer]
				 * @returns {string}
				 */
				UnexpectedPayloadSize.prototype.toString = function (spacer) {
					return JSON.stringify(this.toJSON(), null, spacer);
				};
				
				/**
				 * @param {SaveMode} mode
				 * @returns {ArrayBuffer | string | Object}
				 */
				UnexpectedPayloadSize.prototype.to = function (mode) {
					switch (mode) {
						case SaveModes.BUF: return this.toBuffer();
						case SaveModes.STR: return this.toString();
						case SaveModes.JSON: return this.toJSON();
						default: throw new Error("unrecognized save mode: " + mode);
					}
				};
				
				/**
				 * @param {smolweb_framed_FrameError_TUnexpectedPayloadSize} obj
				 * @returns {smolweb.framed.FrameError.UnexpectedPayloadSize}
				 */
				UnexpectedPayloadSize.new = (obj) => {
					const msg = UnexpectedPayloadSize.create(obj);
					msg.verify();
					return msg;
				};
				
				/**
				 * @param {ArrayBuffer} buf
				 * @returns {smolweb.framed.FrameError.UnexpectedPayloadSize}
				 */
				UnexpectedPayloadSize.fromBuffer = (buf) => {
					const msg = UnexpectedPayloadSize.decode(buf);
					msg.verify();
					return msg;
				};
				
				/**
				 * @param {string} str
				 * @returns {smolweb.framed.FrameError.UnexpectedPayloadSize}
				 */
				UnexpectedPayloadSize.fromString = (str) => {
					const msg = UnexpectedPayloadSize.fromJSON(JSON.parse(str));
					msg.verify();
					return msg;
				};
				
				/**
				 * @param {Object} json
				 * @returns {smolweb.framed.FrameError.UnexpectedPayloadSize}
				 */
				UnexpectedPayloadSize.fromJSON = (obj) => {
					const msg = UnexpectedPayloadSize.fromObject(obj);
					msg.verify();
					return msg;
				};
				
				/**
				 * @param {smolweb.framed.FrameError.UnexpectedPayloadSize | Object | ArrayBuffer | string} saved
				 * @returns {smolweb.framed.FrameError.UnexpectedPayloadSize}
				 */
				UnexpectedPayloadSize.from = (saved) => {
					if (saved instanceof UnexpectedPayloadSize) {
						return saved;
					} else if (saved instanceof ArrayBuffer) {
						return UnexpectedPayloadSize.fromBuffer(saved);
					} else if (typeof saved === 'string') {
						return UnexpectedPayloadSize.fromString(saved);
					} else if (typeof saved === 'object') {
						return UnexpectedPayloadSize.fromJSON(saved);
					} else {
						throw new Error("Unrecognized save format: " + saved?.constructor?.name);
					}
				};
				
				/**
				 * @returns {smolweb.framed.FrameError.UnexpectedPayloadSize}
				 */
				UnexpectedPayloadSize.prototype.copy = function () {
					// TODO: find more efficient way to do this?
					//   we only need to make defensive copies to prevent mutation
					//   but there's no need for a full buffer encoding/decoding roundtrip
					return this.constructor.fromBuffer(this.toBuffer());
				};
				
				/**
				 * WebStorm needs a little help understanding that this class implements ProtobufOpen
				 * @returns {ProtobufOpen.<smolweb.framed.FrameError.UnexpectedPayloadSize>}
				 */
				UnexpectedPayloadSize.opener = function () {
					return this;
				};
				
				return UnexpectedPayloadSize;
				
			})();

			
			/**
			 * @returns {?string}
			 */		
			FrameError.prototype.verify = function () {
				const err = FrameError.verify(this);
				if (err != null) {
					throw new Error("Verification failed for FrameError: " + err);
				}
			};
			
			/**
			 * @returns {ArrayBuffer}
			 */
			FrameError.prototype.toBuffer = function () {
				return FrameError.encode(this).finish();
			};
			
			/**
			 * @param {string | number} [spacer]
			 * @returns {string}
			 */
			FrameError.prototype.toString = function (spacer) {
				return JSON.stringify(this.toJSON(), null, spacer);
			};
			
			/**
			 * @param {SaveMode} mode
			 * @returns {ArrayBuffer | string | Object}
			 */
			FrameError.prototype.to = function (mode) {
				switch (mode) {
					case SaveModes.BUF: return this.toBuffer();
					case SaveModes.STR: return this.toString();
					case SaveModes.JSON: return this.toJSON();
					default: throw new Error("unrecognized save mode: " + mode);
				}
			};
			
			/**
			 * @param {smolweb_framed_TFrameError} obj
			 * @returns {smolweb.framed.FrameError}
			 */
			FrameError.new = (obj) => {
				const msg = FrameError.create(obj);
				msg.verify();
				return msg;
			};
			
			/**
			 * @param {ArrayBuffer} buf
			 * @returns {smolweb.framed.FrameError}
			 */
			FrameError.fromBuffer = (buf) => {
				const msg = FrameError.decode(buf);
				msg.verify();
				return msg;
			};
			
			/**
			 * @param {string} str
			 * @returns {smolweb.framed.FrameError}
			 */
			FrameError.fromString = (str) => {
				const msg = FrameError.fromJSON(JSON.parse(str));
				msg.verify();
				return msg;
			};
			
			/**
			 * @param {Object} json
			 * @returns {smolweb.framed.FrameError}
			 */
			FrameError.fromJSON = (obj) => {
				const msg = FrameError.fromObject(obj);
				msg.verify();
				return msg;
			};
			
			/**
			 * @param {smolweb.framed.FrameError | Object | ArrayBuffer | string} saved
			 * @returns {smolweb.framed.FrameError}
			 */
			FrameError.from = (saved) => {
				if (saved instanceof FrameError) {
					return saved;
				} else if (saved instanceof ArrayBuffer) {
					return FrameError.fromBuffer(saved);
				} else if (typeof saved === 'string') {
					return FrameError.fromString(saved);
				} else if (typeof saved === 'object') {
					return FrameError.fromJSON(saved);
				} else {
					throw new Error("Unrecognized save format: " + saved?.constructor?.name);
				}
			};
			
			/**
			 * @returns {smolweb.framed.FrameError}
			 */
			FrameError.prototype.copy = function () {
				// TODO: find more efficient way to do this?
				//   we only need to make defensive copies to prevent mutation
				//   but there's no need for a full buffer encoding/decoding roundtrip
				return this.constructor.fromBuffer(this.toBuffer());
			};
			
			/**
			 * WebStorm needs a little help understanding that this class implements ProtobufOpen
			 * @returns {ProtobufOpen.<smolweb.framed.FrameError>}
			 */
			FrameError.opener = function () {
				return this;
			};
			
			return FrameError;
			
		})();

		return framed;
	})();

	smolweb.fetch = (function() {

		/**
		 * Namespace fetch.
		 * @memberof smolweb
		 * @namespace
		 */
		const fetch = {};

		fetch.Header = (function() {

			/**
			 * Properties of a Header.
			 * @memberof smolweb.fetch
			 * @typedef {Object} smolweb_fetch_THeader
			 * @property {string|null} [name] Header name
			 * @property {string|null} [value] Header value
			 */

			/**
			 * Constructs a new Header.
			 * @memberof smolweb.fetch
			 * @classdesc Represents a Header.
			 * @implements ProtobufSave
			 * @constructor
			 * @param {smolweb.fetch.Header=} [properties] Properties to set
			 */
			function Header(properties) {
				if (properties)
					for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
						if (properties[keys[i]] != null)
							this[keys[i]] = properties[keys[i]];
			}

			/**
			 * Header name.
			 * @member {string} name
			 * @memberof smolweb.fetch.Header
			 * @instance
			 */
			Header.prototype.name = "";

			/**
			 * Header value.
			 * @member {string} value
			 * @memberof smolweb.fetch.Header
			 * @instance
			 */
			Header.prototype.value = "";

			/**
			 * Creates a new Header instance using the specified properties.
			 * @function create
			 * @memberof smolweb.fetch.Header
			 * @static
			 * @param {smolweb.fetch.Header=} [properties] Properties to set
			 * @returns {smolweb.fetch.Header} Header instance
			 */
			Header.create = function create(properties) {
				return new Header(properties);
			};

			/**
			 * Encodes the specified Header message. Does not implicitly {@link smolweb.fetch.Header.verify|verify} messages.
			 * @function encode
			 * @memberof smolweb.fetch.Header
			 * @static
			 * @param {smolweb.fetch.Header} message Header message or plain object to encode
			 * @param {ArrayBufferWriter} [writer] Writer to encode to
			 * @returns {ArrayBufferWriter} Writer
			 */
			Header.encode = function encode(message, writer) {
				if (!writer)
					writer = $Writer.create();
				if (message.name != null && Object.hasOwnProperty.call(message, "name"))
					writer.uint32(/* id 1, wireType 2 =*/10).string(message.name);
				if (message.value != null && Object.hasOwnProperty.call(message, "value"))
					writer.uint32(/* id 2, wireType 2 =*/18).string(message.value);
				return writer;
			};

			/**
			 * Decodes a Header message from the specified reader or buffer.
			 * @function decode
			 * @memberof smolweb.fetch.Header
			 * @static
			 * @param {ArrayBufferReader|ArrayBuffer} reader Reader or buffer to decode from
			 * @param {number} [length] Message length if known beforehand
			 * @returns {smolweb.fetch.Header} Header
			 * @throws {Error} If the payload is not a reader or valid buffer
			 * @throws {$protobuf.util.ProtocolError} If required fields are missing
			 */
			Header.decode = function decode(reader, length) {
				if (!(reader instanceof $Reader))
					reader = $Reader.create(reader);
				let end = length === undefined ? reader.len : reader.pos + length, message = new $root.smolweb.fetch.Header();
				while (reader.pos < end) {
					let tag = reader.uint32();
					switch (tag >>> 3) {
					case 1: {
							message.name = reader.string();
							break;
						}
					case 2: {
							message.value = reader.string();
							break;
						}
					default:
						reader.skipType(tag & 7);
						break;
					}
				}
				return message;
			};

			/**
			 * Verifies a Header message.
			 * @function verify
			 * @memberof smolweb.fetch.Header
			 * @static
			 * @param {Object.<string,*>} message Plain object to verify
			 * @returns {string|null} `null` if valid, otherwise the reason why it is not
			 */
			Header.verify = function verify(message) {
				if (typeof message !== "object" || message === null)
					return "object expected";
				if (message.name != null && message.hasOwnProperty("name"))
					if (!$util.isString(message.name))
						return "name: string expected";
				if (message.value != null && message.hasOwnProperty("value"))
					if (!$util.isString(message.value))
						return "value: string expected";
				return null;
			};

			/**
			 * Creates a Header message from a plain object. Also converts values to their respective internal types.
			 * @function fromObject
			 * @memberof smolweb.fetch.Header
			 * @static
			 * @param {Object.<string,*>} object Plain object
			 * @returns {smolweb.fetch.Header} Header
			 */
			Header.fromObject = function fromObject(object) {
				if (object instanceof $root.smolweb.fetch.Header)
					return object;
				let message = new $root.smolweb.fetch.Header();
				if (object.name != null)
					message.name = String(object.name);
				if (object.value != null)
					message.value = String(object.value);
				return message;
			};

			/**
			 * Creates a plain object from a Header message. Also converts values to other types if specified.
			 * @function toObject
			 * @memberof smolweb.fetch.Header
			 * @static
			 * @param {smolweb.fetch.Header} message Header
			 * @param {$protobuf.IConversionOptions} [options] Conversion options
			 * @returns {Object.<string,*>} Plain object
			 */
			Header.toObject = function toObject(message, options) {
				if (!options)
					options = {};
				let object = {};
				if (options.defaults) {
					object.name = "";
					object.value = "";
				}
				if (message.name != null && message.hasOwnProperty("name"))
					object.name = message.name;
				if (message.value != null && message.hasOwnProperty("value"))
					object.value = message.value;
				return object;
			};

			/**
			 * Converts this Header to JSON.
			 * @function toJSON
			 * @memberof smolweb.fetch.Header
			 * @instance
			 * @returns {Object.<string,*>} JSON object
			 */
			Header.prototype.toJSON = function toJSON() {
				return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
			};

			/**
			 * Gets the default type url for Header
			 * @function getTypeUrl
			 * @memberof smolweb.fetch.Header
			 * @static
			 * @param {string} [typeUrlPrefix] your custom typeUrlPrefix(default "type.cuchazinteractive.org")
			 * @returns {string} The default type url
			 */
			Header.getTypeUrl = function getTypeUrl(typeUrlPrefix) {
				if (typeUrlPrefix === undefined) {
					typeUrlPrefix = "type.cuchazinteractive.org";
				}
				return typeUrlPrefix + "/smolweb.fetch.Header";
			};

			
			/**
			 * @returns {?string}
			 */		
			Header.prototype.verify = function () {
				const err = Header.verify(this);
				if (err != null) {
					throw new Error("Verification failed for Header: " + err);
				}
			};
			
			/**
			 * @returns {ArrayBuffer}
			 */
			Header.prototype.toBuffer = function () {
				return Header.encode(this).finish();
			};
			
			/**
			 * @param {string | number} [spacer]
			 * @returns {string}
			 */
			Header.prototype.toString = function (spacer) {
				return JSON.stringify(this.toJSON(), null, spacer);
			};
			
			/**
			 * @param {SaveMode} mode
			 * @returns {ArrayBuffer | string | Object}
			 */
			Header.prototype.to = function (mode) {
				switch (mode) {
					case SaveModes.BUF: return this.toBuffer();
					case SaveModes.STR: return this.toString();
					case SaveModes.JSON: return this.toJSON();
					default: throw new Error("unrecognized save mode: " + mode);
				}
			};
			
			/**
			 * @param {smolweb_fetch_THeader} obj
			 * @returns {smolweb.fetch.Header}
			 */
			Header.new = (obj) => {
				const msg = Header.create(obj);
				msg.verify();
				return msg;
			};
			
			/**
			 * @param {ArrayBuffer} buf
			 * @returns {smolweb.fetch.Header}
			 */
			Header.fromBuffer = (buf) => {
				const msg = Header.decode(buf);
				msg.verify();
				return msg;
			};
			
			/**
			 * @param {string} str
			 * @returns {smolweb.fetch.Header}
			 */
			Header.fromString = (str) => {
				const msg = Header.fromJSON(JSON.parse(str));
				msg.verify();
				return msg;
			};
			
			/**
			 * @param {Object} json
			 * @returns {smolweb.fetch.Header}
			 */
			Header.fromJSON = (obj) => {
				const msg = Header.fromObject(obj);
				msg.verify();
				return msg;
			};
			
			/**
			 * @param {smolweb.fetch.Header | Object | ArrayBuffer | string} saved
			 * @returns {smolweb.fetch.Header}
			 */
			Header.from = (saved) => {
				if (saved instanceof Header) {
					return saved;
				} else if (saved instanceof ArrayBuffer) {
					return Header.fromBuffer(saved);
				} else if (typeof saved === 'string') {
					return Header.fromString(saved);
				} else if (typeof saved === 'object') {
					return Header.fromJSON(saved);
				} else {
					throw new Error("Unrecognized save format: " + saved?.constructor?.name);
				}
			};
			
			/**
			 * @returns {smolweb.fetch.Header}
			 */
			Header.prototype.copy = function () {
				// TODO: find more efficient way to do this?
				//   we only need to make defensive copies to prevent mutation
				//   but there's no need for a full buffer encoding/decoding roundtrip
				return this.constructor.fromBuffer(this.toBuffer());
			};
			
			/**
			 * WebStorm needs a little help understanding that this class implements ProtobufOpen
			 * @returns {ProtobufOpen.<smolweb.fetch.Header>}
			 */
			Header.opener = function () {
				return this;
			};
			
			return Header;
			
		})();

		fetch.Request = (function() {

			/**
			 * Properties of a Request.
			 * @memberof smolweb.fetch
			 * @typedef {Object} smolweb_fetch_TRequest
			 * @property {string|null} [method] Request method
			 * @property {string|null} [path] Request path
			 * @property {Array.<smolweb.fetch.Header>|null} [headers] Request headers
			 * @property {boolean|null} [hasBody] Request hasBody
			 */

			/**
			 * Constructs a new Request.
			 * @memberof smolweb.fetch
			 * @classdesc Represents a Request.
			 * @implements ProtobufSave
			 * @constructor
			 * @param {smolweb.fetch.Request=} [properties] Properties to set
			 */
			function Request(properties) {
				this.headers = [];
				if (properties)
					for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
						if (properties[keys[i]] != null)
							this[keys[i]] = properties[keys[i]];
			}

			/**
			 * Request method.
			 * @member {string} method
			 * @memberof smolweb.fetch.Request
			 * @instance
			 */
			Request.prototype.method = "";

			/**
			 * Request path.
			 * @member {string} path
			 * @memberof smolweb.fetch.Request
			 * @instance
			 */
			Request.prototype.path = "";

			/**
			 * Request headers.
			 * @member {Array.<smolweb.fetch.Header>} headers
			 * @memberof smolweb.fetch.Request
			 * @instance
			 */
			Request.prototype.headers = $util.emptyArray;

			/**
			 * Request hasBody.
			 * @member {boolean} hasBody
			 * @memberof smolweb.fetch.Request
			 * @instance
			 */
			Request.prototype.hasBody = false;

			/**
			 * Creates a new Request instance using the specified properties.
			 * @function create
			 * @memberof smolweb.fetch.Request
			 * @static
			 * @param {smolweb.fetch.Request=} [properties] Properties to set
			 * @returns {smolweb.fetch.Request} Request instance
			 */
			Request.create = function create(properties) {
				return new Request(properties);
			};

			/**
			 * Encodes the specified Request message. Does not implicitly {@link smolweb.fetch.Request.verify|verify} messages.
			 * @function encode
			 * @memberof smolweb.fetch.Request
			 * @static
			 * @param {smolweb.fetch.Request} message Request message or plain object to encode
			 * @param {ArrayBufferWriter} [writer] Writer to encode to
			 * @returns {ArrayBufferWriter} Writer
			 */
			Request.encode = function encode(message, writer) {
				if (!writer)
					writer = $Writer.create();
				if (message.method != null && Object.hasOwnProperty.call(message, "method"))
					writer.uint32(/* id 1, wireType 2 =*/10).string(message.method);
				if (message.path != null && Object.hasOwnProperty.call(message, "path"))
					writer.uint32(/* id 2, wireType 2 =*/18).string(message.path);
				if (message.headers != null && message.headers.length)
					for (let i = 0; i < message.headers.length; ++i)
						$root.smolweb.fetch.Header.encode(message.headers[i], writer.uint32(/* id 3, wireType 2 =*/26).fork()).ldelim();
				if (message.hasBody != null && Object.hasOwnProperty.call(message, "hasBody"))
					writer.uint32(/* id 4, wireType 0 =*/32).bool(message.hasBody);
				return writer;
			};

			/**
			 * Decodes a Request message from the specified reader or buffer.
			 * @function decode
			 * @memberof smolweb.fetch.Request
			 * @static
			 * @param {ArrayBufferReader|ArrayBuffer} reader Reader or buffer to decode from
			 * @param {number} [length] Message length if known beforehand
			 * @returns {smolweb.fetch.Request} Request
			 * @throws {Error} If the payload is not a reader or valid buffer
			 * @throws {$protobuf.util.ProtocolError} If required fields are missing
			 */
			Request.decode = function decode(reader, length) {
				if (!(reader instanceof $Reader))
					reader = $Reader.create(reader);
				let end = length === undefined ? reader.len : reader.pos + length, message = new $root.smolweb.fetch.Request();
				while (reader.pos < end) {
					let tag = reader.uint32();
					switch (tag >>> 3) {
					case 1: {
							message.method = reader.string();
							break;
						}
					case 2: {
							message.path = reader.string();
							break;
						}
					case 3: {
							if (!(message.headers && message.headers.length))
								message.headers = [];
							message.headers.push($root.smolweb.fetch.Header.decode(reader, reader.uint32()));
							break;
						}
					case 4: {
							message.hasBody = reader.bool();
							break;
						}
					default:
						reader.skipType(tag & 7);
						break;
					}
				}
				return message;
			};

			/**
			 * Verifies a Request message.
			 * @function verify
			 * @memberof smolweb.fetch.Request
			 * @static
			 * @param {Object.<string,*>} message Plain object to verify
			 * @returns {string|null} `null` if valid, otherwise the reason why it is not
			 */
			Request.verify = function verify(message) {
				if (typeof message !== "object" || message === null)
					return "object expected";
				if (message.method != null && message.hasOwnProperty("method"))
					if (!$util.isString(message.method))
						return "method: string expected";
				if (message.path != null && message.hasOwnProperty("path"))
					if (!$util.isString(message.path))
						return "path: string expected";
				if (message.headers != null && message.hasOwnProperty("headers")) {
					if (!Array.isArray(message.headers))
						return "headers: array expected";
					for (let i = 0; i < message.headers.length; ++i) {
						let error = $root.smolweb.fetch.Header.verify(message.headers[i]);
						if (error)
							return "headers." + error;
					}
				}
				if (message.hasBody != null && message.hasOwnProperty("hasBody"))
					if (typeof message.hasBody !== "boolean")
						return "hasBody: boolean expected";
				return null;
			};

			/**
			 * Creates a Request message from a plain object. Also converts values to their respective internal types.
			 * @function fromObject
			 * @memberof smolweb.fetch.Request
			 * @static
			 * @param {Object.<string,*>} object Plain object
			 * @returns {smolweb.fetch.Request} Request
			 */
			Request.fromObject = function fromObject(object) {
				if (object instanceof $root.smolweb.fetch.Request)
					return object;
				let message = new $root.smolweb.fetch.Request();
				if (object.method != null)
					message.method = String(object.method);
				if (object.path != null)
					message.path = String(object.path);
				if (object.headers) {
					if (!Array.isArray(object.headers))
						throw TypeError(".smolweb.fetch.Request.headers: array expected");
					message.headers = [];
					for (let i = 0; i < object.headers.length; ++i) {
						if (typeof object.headers[i] !== "object")
							throw TypeError(".smolweb.fetch.Request.headers: object expected");
						message.headers[i] = $root.smolweb.fetch.Header.fromObject(object.headers[i]);
					}
				}
				if (object.hasBody != null)
					message.hasBody = Boolean(object.hasBody);
				return message;
			};

			/**
			 * Creates a plain object from a Request message. Also converts values to other types if specified.
			 * @function toObject
			 * @memberof smolweb.fetch.Request
			 * @static
			 * @param {smolweb.fetch.Request} message Request
			 * @param {$protobuf.IConversionOptions} [options] Conversion options
			 * @returns {Object.<string,*>} Plain object
			 */
			Request.toObject = function toObject(message, options) {
				if (!options)
					options = {};
				let object = {};
				if (options.arrays || options.defaults)
					object.headers = [];
				if (options.defaults) {
					object.method = "";
					object.path = "";
					object.hasBody = false;
				}
				if (message.method != null && message.hasOwnProperty("method"))
					object.method = message.method;
				if (message.path != null && message.hasOwnProperty("path"))
					object.path = message.path;
				if (message.headers && message.headers.length) {
					object.headers = [];
					for (let j = 0; j < message.headers.length; ++j)
						object.headers[j] = $root.smolweb.fetch.Header.toObject(message.headers[j], options);
				}
				if (message.hasBody != null && message.hasOwnProperty("hasBody"))
					object.hasBody = message.hasBody;
				return object;
			};

			/**
			 * Converts this Request to JSON.
			 * @function toJSON
			 * @memberof smolweb.fetch.Request
			 * @instance
			 * @returns {Object.<string,*>} JSON object
			 */
			Request.prototype.toJSON = function toJSON() {
				return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
			};

			/**
			 * Gets the default type url for Request
			 * @function getTypeUrl
			 * @memberof smolweb.fetch.Request
			 * @static
			 * @param {string} [typeUrlPrefix] your custom typeUrlPrefix(default "type.cuchazinteractive.org")
			 * @returns {string} The default type url
			 */
			Request.getTypeUrl = function getTypeUrl(typeUrlPrefix) {
				if (typeUrlPrefix === undefined) {
					typeUrlPrefix = "type.cuchazinteractive.org";
				}
				return typeUrlPrefix + "/smolweb.fetch.Request";
			};

			
			/**
			 * @returns {?string}
			 */		
			Request.prototype.verify = function () {
				const err = Request.verify(this);
				if (err != null) {
					throw new Error("Verification failed for Request: " + err);
				}
			};
			
			/**
			 * @returns {ArrayBuffer}
			 */
			Request.prototype.toBuffer = function () {
				return Request.encode(this).finish();
			};
			
			/**
			 * @param {string | number} [spacer]
			 * @returns {string}
			 */
			Request.prototype.toString = function (spacer) {
				return JSON.stringify(this.toJSON(), null, spacer);
			};
			
			/**
			 * @param {SaveMode} mode
			 * @returns {ArrayBuffer | string | Object}
			 */
			Request.prototype.to = function (mode) {
				switch (mode) {
					case SaveModes.BUF: return this.toBuffer();
					case SaveModes.STR: return this.toString();
					case SaveModes.JSON: return this.toJSON();
					default: throw new Error("unrecognized save mode: " + mode);
				}
			};
			
			/**
			 * @param {smolweb_fetch_TRequest} obj
			 * @returns {smolweb.fetch.Request}
			 */
			Request.new = (obj) => {
				const msg = Request.create(obj);
				msg.verify();
				return msg;
			};
			
			/**
			 * @param {ArrayBuffer} buf
			 * @returns {smolweb.fetch.Request}
			 */
			Request.fromBuffer = (buf) => {
				const msg = Request.decode(buf);
				msg.verify();
				return msg;
			};
			
			/**
			 * @param {string} str
			 * @returns {smolweb.fetch.Request}
			 */
			Request.fromString = (str) => {
				const msg = Request.fromJSON(JSON.parse(str));
				msg.verify();
				return msg;
			};
			
			/**
			 * @param {Object} json
			 * @returns {smolweb.fetch.Request}
			 */
			Request.fromJSON = (obj) => {
				const msg = Request.fromObject(obj);
				msg.verify();
				return msg;
			};
			
			/**
			 * @param {smolweb.fetch.Request | Object | ArrayBuffer | string} saved
			 * @returns {smolweb.fetch.Request}
			 */
			Request.from = (saved) => {
				if (saved instanceof Request) {
					return saved;
				} else if (saved instanceof ArrayBuffer) {
					return Request.fromBuffer(saved);
				} else if (typeof saved === 'string') {
					return Request.fromString(saved);
				} else if (typeof saved === 'object') {
					return Request.fromJSON(saved);
				} else {
					throw new Error("Unrecognized save format: " + saved?.constructor?.name);
				}
			};
			
			/**
			 * @returns {smolweb.fetch.Request}
			 */
			Request.prototype.copy = function () {
				// TODO: find more efficient way to do this?
				//   we only need to make defensive copies to prevent mutation
				//   but there's no need for a full buffer encoding/decoding roundtrip
				return this.constructor.fromBuffer(this.toBuffer());
			};
			
			/**
			 * WebStorm needs a little help understanding that this class implements ProtobufOpen
			 * @returns {ProtobufOpen.<smolweb.fetch.Request>}
			 */
			Request.opener = function () {
				return this;
			};
			
			return Request;
			
		})();

		fetch.Response = (function() {

			/**
			 * Properties of a Response.
			 * @memberof smolweb.fetch
			 * @typedef {Object} smolweb_fetch_TResponse
			 * @property {smolweb.fetch.Response.ServerResponse|null} [serverResponse] Response serverResponse
			 * @property {smolweb.fetch.Response.GatewayResponse|null} [gatewayResponse] Response gatewayResponse
			 */

			/**
			 * Constructs a new Response.
			 * @memberof smolweb.fetch
			 * @classdesc Represents a Response.
			 * @implements ProtobufSave
			 * @constructor
			 * @param {smolweb.fetch.Response=} [properties] Properties to set
			 */
			function Response(properties) {
				if (properties)
					for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
						if (properties[keys[i]] != null)
							this[keys[i]] = properties[keys[i]];
			}

			/**
			 * Response serverResponse.
			 * @member {smolweb.fetch.Response.ServerResponse|null|undefined} serverResponse
			 * @memberof smolweb.fetch.Response
			 * @instance
			 */
			Response.prototype.serverResponse = null;

			/**
			 * Response gatewayResponse.
			 * @member {smolweb.fetch.Response.GatewayResponse|null|undefined} gatewayResponse
			 * @memberof smolweb.fetch.Response
			 * @instance
			 */
			Response.prototype.gatewayResponse = null;

			// OneOf field names bound to virtual getters and setters
			let $oneOfFields;

			/**
			 * Response response.
			 * @member {"serverResponse"|"gatewayResponse"|undefined} response
			 * @memberof smolweb.fetch.Response
			 * @instance
			 */
			Object.defineProperty(Response.prototype, "response", {
				get: $util.oneOfGetter($oneOfFields = ["serverResponse", "gatewayResponse"]),
				set: $util.oneOfSetter($oneOfFields)
			});

			/**
			 * Creates a new Response instance using the specified properties.
			 * @function create
			 * @memberof smolweb.fetch.Response
			 * @static
			 * @param {smolweb.fetch.Response=} [properties] Properties to set
			 * @returns {smolweb.fetch.Response} Response instance
			 */
			Response.create = function create(properties) {
				return new Response(properties);
			};

			/**
			 * Encodes the specified Response message. Does not implicitly {@link smolweb.fetch.Response.verify|verify} messages.
			 * @function encode
			 * @memberof smolweb.fetch.Response
			 * @static
			 * @param {smolweb.fetch.Response} message Response message or plain object to encode
			 * @param {ArrayBufferWriter} [writer] Writer to encode to
			 * @returns {ArrayBufferWriter} Writer
			 */
			Response.encode = function encode(message, writer) {
				if (!writer)
					writer = $Writer.create();
				if (message.serverResponse != null && Object.hasOwnProperty.call(message, "serverResponse"))
					$root.smolweb.fetch.Response.ServerResponse.encode(message.serverResponse, writer.uint32(/* id 1, wireType 2 =*/10).fork()).ldelim();
				if (message.gatewayResponse != null && Object.hasOwnProperty.call(message, "gatewayResponse"))
					$root.smolweb.fetch.Response.GatewayResponse.encode(message.gatewayResponse, writer.uint32(/* id 2, wireType 2 =*/18).fork()).ldelim();
				return writer;
			};

			/**
			 * Decodes a Response message from the specified reader or buffer.
			 * @function decode
			 * @memberof smolweb.fetch.Response
			 * @static
			 * @param {ArrayBufferReader|ArrayBuffer} reader Reader or buffer to decode from
			 * @param {number} [length] Message length if known beforehand
			 * @returns {smolweb.fetch.Response} Response
			 * @throws {Error} If the payload is not a reader or valid buffer
			 * @throws {$protobuf.util.ProtocolError} If required fields are missing
			 */
			Response.decode = function decode(reader, length) {
				if (!(reader instanceof $Reader))
					reader = $Reader.create(reader);
				let end = length === undefined ? reader.len : reader.pos + length, message = new $root.smolweb.fetch.Response();
				while (reader.pos < end) {
					let tag = reader.uint32();
					switch (tag >>> 3) {
					case 1: {
							message.serverResponse = $root.smolweb.fetch.Response.ServerResponse.decode(reader, reader.uint32());
							break;
						}
					case 2: {
							message.gatewayResponse = $root.smolweb.fetch.Response.GatewayResponse.decode(reader, reader.uint32());
							break;
						}
					default:
						reader.skipType(tag & 7);
						break;
					}
				}
				return message;
			};

			/**
			 * Verifies a Response message.
			 * @function verify
			 * @memberof smolweb.fetch.Response
			 * @static
			 * @param {Object.<string,*>} message Plain object to verify
			 * @returns {string|null} `null` if valid, otherwise the reason why it is not
			 */
			Response.verify = function verify(message) {
				if (typeof message !== "object" || message === null)
					return "object expected";
				let properties = {};
				if (message.serverResponse != null && message.hasOwnProperty("serverResponse")) {
					properties.response = 1;
					{
						let error = $root.smolweb.fetch.Response.ServerResponse.verify(message.serverResponse);
						if (error)
							return "serverResponse." + error;
					}
				}
				if (message.gatewayResponse != null && message.hasOwnProperty("gatewayResponse")) {
					if (properties.response === 1)
						return "response: multiple values";
					properties.response = 1;
					{
						let error = $root.smolweb.fetch.Response.GatewayResponse.verify(message.gatewayResponse);
						if (error)
							return "gatewayResponse." + error;
					}
				}
				return null;
			};

			/**
			 * Creates a Response message from a plain object. Also converts values to their respective internal types.
			 * @function fromObject
			 * @memberof smolweb.fetch.Response
			 * @static
			 * @param {Object.<string,*>} object Plain object
			 * @returns {smolweb.fetch.Response} Response
			 */
			Response.fromObject = function fromObject(object) {
				if (object instanceof $root.smolweb.fetch.Response)
					return object;
				let message = new $root.smolweb.fetch.Response();
				if (object.serverResponse != null) {
					if (typeof object.serverResponse !== "object")
						throw TypeError(".smolweb.fetch.Response.serverResponse: object expected");
					message.serverResponse = $root.smolweb.fetch.Response.ServerResponse.fromObject(object.serverResponse);
				}
				if (object.gatewayResponse != null) {
					if (typeof object.gatewayResponse !== "object")
						throw TypeError(".smolweb.fetch.Response.gatewayResponse: object expected");
					message.gatewayResponse = $root.smolweb.fetch.Response.GatewayResponse.fromObject(object.gatewayResponse);
				}
				return message;
			};

			/**
			 * Creates a plain object from a Response message. Also converts values to other types if specified.
			 * @function toObject
			 * @memberof smolweb.fetch.Response
			 * @static
			 * @param {smolweb.fetch.Response} message Response
			 * @param {$protobuf.IConversionOptions} [options] Conversion options
			 * @returns {Object.<string,*>} Plain object
			 */
			Response.toObject = function toObject(message, options) {
				if (!options)
					options = {};
				let object = {};
				if (message.serverResponse != null && message.hasOwnProperty("serverResponse")) {
					object.serverResponse = $root.smolweb.fetch.Response.ServerResponse.toObject(message.serverResponse, options);
					if (options.oneofs)
						object.response = "serverResponse";
				}
				if (message.gatewayResponse != null && message.hasOwnProperty("gatewayResponse")) {
					object.gatewayResponse = $root.smolweb.fetch.Response.GatewayResponse.toObject(message.gatewayResponse, options);
					if (options.oneofs)
						object.response = "gatewayResponse";
				}
				return object;
			};

			/**
			 * Converts this Response to JSON.
			 * @function toJSON
			 * @memberof smolweb.fetch.Response
			 * @instance
			 * @returns {Object.<string,*>} JSON object
			 */
			Response.prototype.toJSON = function toJSON() {
				return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
			};

			/**
			 * Gets the default type url for Response
			 * @function getTypeUrl
			 * @memberof smolweb.fetch.Response
			 * @static
			 * @param {string} [typeUrlPrefix] your custom typeUrlPrefix(default "type.cuchazinteractive.org")
			 * @returns {string} The default type url
			 */
			Response.getTypeUrl = function getTypeUrl(typeUrlPrefix) {
				if (typeUrlPrefix === undefined) {
					typeUrlPrefix = "type.cuchazinteractive.org";
				}
				return typeUrlPrefix + "/smolweb.fetch.Response";
			};

			Response.ServerResponse = (function() {

				/**
				 * Properties of a ServerResponse.
				 * @memberof smolweb.fetch.Response
				 * @typedef {Object} smolweb_fetch_Response_TServerResponse
				 * @property {number|null} [status] ServerResponse status
				 * @property {Array.<smolweb.fetch.Header>|null} [headers] ServerResponse headers
				 * @property {smolweb.fetch.Response.ServerResponse.BodyType|null} [bodyType] ServerResponse bodyType
				 */

				/**
				 * Constructs a new ServerResponse.
				 * @memberof smolweb.fetch.Response
				 * @classdesc Represents a ServerResponse.
				 * @implements ProtobufSave
				 * @constructor
				 * @param {smolweb.fetch.Response.ServerResponse=} [properties] Properties to set
				 */
				function ServerResponse(properties) {
					this.headers = [];
					if (properties)
						for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
							if (properties[keys[i]] != null)
								this[keys[i]] = properties[keys[i]];
				}

				/**
				 * ServerResponse status.
				 * @member {number} status
				 * @memberof smolweb.fetch.Response.ServerResponse
				 * @instance
				 */
				ServerResponse.prototype.status = 0;

				/**
				 * ServerResponse headers.
				 * @member {Array.<smolweb.fetch.Header>} headers
				 * @memberof smolweb.fetch.Response.ServerResponse
				 * @instance
				 */
				ServerResponse.prototype.headers = $util.emptyArray;

				/**
				 * ServerResponse bodyType.
				 * @member {smolweb.fetch.Response.ServerResponse.BodyType} bodyType
				 * @memberof smolweb.fetch.Response.ServerResponse
				 * @instance
				 */
				ServerResponse.prototype.bodyType = 0;

				/**
				 * Creates a new ServerResponse instance using the specified properties.
				 * @function create
				 * @memberof smolweb.fetch.Response.ServerResponse
				 * @static
				 * @param {smolweb.fetch.Response.ServerResponse=} [properties] Properties to set
				 * @returns {smolweb.fetch.Response.ServerResponse} ServerResponse instance
				 */
				ServerResponse.create = function create(properties) {
					return new ServerResponse(properties);
				};

				/**
				 * Encodes the specified ServerResponse message. Does not implicitly {@link smolweb.fetch.Response.ServerResponse.verify|verify} messages.
				 * @function encode
				 * @memberof smolweb.fetch.Response.ServerResponse
				 * @static
				 * @param {smolweb.fetch.Response.ServerResponse} message ServerResponse message or plain object to encode
				 * @param {ArrayBufferWriter} [writer] Writer to encode to
				 * @returns {ArrayBufferWriter} Writer
				 */
				ServerResponse.encode = function encode(message, writer) {
					if (!writer)
						writer = $Writer.create();
					if (message.status != null && Object.hasOwnProperty.call(message, "status"))
						writer.uint32(/* id 1, wireType 0 =*/8).int32(message.status);
					if (message.headers != null && message.headers.length)
						for (let i = 0; i < message.headers.length; ++i)
							$root.smolweb.fetch.Header.encode(message.headers[i], writer.uint32(/* id 2, wireType 2 =*/18).fork()).ldelim();
					if (message.bodyType != null && Object.hasOwnProperty.call(message, "bodyType"))
						writer.uint32(/* id 3, wireType 0 =*/24).int32(message.bodyType);
					return writer;
				};

				/**
				 * Decodes a ServerResponse message from the specified reader or buffer.
				 * @function decode
				 * @memberof smolweb.fetch.Response.ServerResponse
				 * @static
				 * @param {ArrayBufferReader|ArrayBuffer} reader Reader or buffer to decode from
				 * @param {number} [length] Message length if known beforehand
				 * @returns {smolweb.fetch.Response.ServerResponse} ServerResponse
				 * @throws {Error} If the payload is not a reader or valid buffer
				 * @throws {$protobuf.util.ProtocolError} If required fields are missing
				 */
				ServerResponse.decode = function decode(reader, length) {
					if (!(reader instanceof $Reader))
						reader = $Reader.create(reader);
					let end = length === undefined ? reader.len : reader.pos + length, message = new $root.smolweb.fetch.Response.ServerResponse();
					while (reader.pos < end) {
						let tag = reader.uint32();
						switch (tag >>> 3) {
						case 1: {
								message.status = reader.int32();
								break;
							}
						case 2: {
								if (!(message.headers && message.headers.length))
									message.headers = [];
								message.headers.push($root.smolweb.fetch.Header.decode(reader, reader.uint32()));
								break;
							}
						case 3: {
								message.bodyType = reader.int32();
								break;
							}
						default:
							reader.skipType(tag & 7);
							break;
						}
					}
					return message;
				};

				/**
				 * Verifies a ServerResponse message.
				 * @function verify
				 * @memberof smolweb.fetch.Response.ServerResponse
				 * @static
				 * @param {Object.<string,*>} message Plain object to verify
				 * @returns {string|null} `null` if valid, otherwise the reason why it is not
				 */
				ServerResponse.verify = function verify(message) {
					if (typeof message !== "object" || message === null)
						return "object expected";
					if (message.status != null && message.hasOwnProperty("status"))
						if (!$util.isInteger(message.status))
							return "status: integer|bigint expected";
					if (message.headers != null && message.hasOwnProperty("headers")) {
						if (!Array.isArray(message.headers))
							return "headers: array expected";
						for (let i = 0; i < message.headers.length; ++i) {
							let error = $root.smolweb.fetch.Header.verify(message.headers[i]);
							if (error)
								return "headers." + error;
						}
					}
					if (message.bodyType != null && message.hasOwnProperty("bodyType"))
						switch (message.bodyType) {
						default:
							return "bodyType: enum value expected";
						case 0:
						case 1:
						case 2:
							break;
						}
					return null;
				};

				/**
				 * Creates a ServerResponse message from a plain object. Also converts values to their respective internal types.
				 * @function fromObject
				 * @memberof smolweb.fetch.Response.ServerResponse
				 * @static
				 * @param {Object.<string,*>} object Plain object
				 * @returns {smolweb.fetch.Response.ServerResponse} ServerResponse
				 */
				ServerResponse.fromObject = function fromObject(object) {
					if (object instanceof $root.smolweb.fetch.Response.ServerResponse)
						return object;
					let message = new $root.smolweb.fetch.Response.ServerResponse();
					if (object.status != null)
						message.status = object.status | 0;
					if (object.headers) {
						if (!Array.isArray(object.headers))
							throw TypeError(".smolweb.fetch.Response.ServerResponse.headers: array expected");
						message.headers = [];
						for (let i = 0; i < object.headers.length; ++i) {
							if (typeof object.headers[i] !== "object")
								throw TypeError(".smolweb.fetch.Response.ServerResponse.headers: object expected");
							message.headers[i] = $root.smolweb.fetch.Header.fromObject(object.headers[i]);
						}
					}
					switch (object.bodyType) {
					default:
						if (typeof object.bodyType === "number") {
							message.bodyType = object.bodyType;
							break;
						}
						break;
					case "None":
					case 0:
						message.bodyType = 0;
						break;
					case "SingleStream":
					case 1:
						message.bodyType = 1;
						break;
					case "MultiStream":
					case 2:
						message.bodyType = 2;
						break;
					}
					return message;
				};

				/**
				 * Creates a plain object from a ServerResponse message. Also converts values to other types if specified.
				 * @function toObject
				 * @memberof smolweb.fetch.Response.ServerResponse
				 * @static
				 * @param {smolweb.fetch.Response.ServerResponse} message ServerResponse
				 * @param {$protobuf.IConversionOptions} [options] Conversion options
				 * @returns {Object.<string,*>} Plain object
				 */
				ServerResponse.toObject = function toObject(message, options) {
					if (!options)
						options = {};
					let object = {};
					if (options.arrays || options.defaults)
						object.headers = [];
					if (options.defaults) {
						object.status = 0;
						object.bodyType = options.enums === String ? "None" : 0;
					}
					if (message.status != null && message.hasOwnProperty("status"))
						object.status = message.status;
					if (message.headers && message.headers.length) {
						object.headers = [];
						for (let j = 0; j < message.headers.length; ++j)
							object.headers[j] = $root.smolweb.fetch.Header.toObject(message.headers[j], options);
					}
					if (message.bodyType != null && message.hasOwnProperty("bodyType"))
						object.bodyType = options.enums === String ? $root.smolweb.fetch.Response.ServerResponse.BodyType[message.bodyType] === undefined ? message.bodyType : $root.smolweb.fetch.Response.ServerResponse.BodyType[message.bodyType] : message.bodyType;
					return object;
				};

				/**
				 * Converts this ServerResponse to JSON.
				 * @function toJSON
				 * @memberof smolweb.fetch.Response.ServerResponse
				 * @instance
				 * @returns {Object.<string,*>} JSON object
				 */
				ServerResponse.prototype.toJSON = function toJSON() {
					return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
				};

				/**
				 * Gets the default type url for ServerResponse
				 * @function getTypeUrl
				 * @memberof smolweb.fetch.Response.ServerResponse
				 * @static
				 * @param {string} [typeUrlPrefix] your custom typeUrlPrefix(default "type.cuchazinteractive.org")
				 * @returns {string} The default type url
				 */
				ServerResponse.getTypeUrl = function getTypeUrl(typeUrlPrefix) {
					if (typeUrlPrefix === undefined) {
						typeUrlPrefix = "type.cuchazinteractive.org";
					}
					return typeUrlPrefix + "/smolweb.fetch.Response.ServerResponse";
				};

				/**
				 * BodyType enum.
				 * @name smolweb.fetch.Response.ServerResponse.BodyType
				 * @enum {number}
				 * @property {number} None=0 None value
				 * @property {number} SingleStream=1 SingleStream value
				 * @property {number} MultiStream=2 MultiStream value
				 */
				ServerResponse.BodyType = (function() {
					const valuesById = {}, values = Object.create(valuesById);
					values[valuesById[0] = "None"] = 0;
					values[valuesById[1] = "SingleStream"] = 1;
					values[valuesById[2] = "MultiStream"] = 2;
					return values;
					/**
					 * @typedef {number} smolweb.fetch.Response.ServerResponse.BodyType
					 */
					
				})();

				
				/**
				 * @returns {?string}
				 */		
				ServerResponse.prototype.verify = function () {
					const err = ServerResponse.verify(this);
					if (err != null) {
						throw new Error("Verification failed for ServerResponse: " + err);
					}
				};
				
				/**
				 * @returns {ArrayBuffer}
				 */
				ServerResponse.prototype.toBuffer = function () {
					return ServerResponse.encode(this).finish();
				};
				
				/**
				 * @param {string | number} [spacer]
				 * @returns {string}
				 */
				ServerResponse.prototype.toString = function (spacer) {
					return JSON.stringify(this.toJSON(), null, spacer);
				};
				
				/**
				 * @param {SaveMode} mode
				 * @returns {ArrayBuffer | string | Object}
				 */
				ServerResponse.prototype.to = function (mode) {
					switch (mode) {
						case SaveModes.BUF: return this.toBuffer();
						case SaveModes.STR: return this.toString();
						case SaveModes.JSON: return this.toJSON();
						default: throw new Error("unrecognized save mode: " + mode);
					}
				};
				
				/**
				 * @param {smolweb_fetch_Response_TServerResponse} obj
				 * @returns {smolweb.fetch.Response.ServerResponse}
				 */
				ServerResponse.new = (obj) => {
					const msg = ServerResponse.create(obj);
					msg.verify();
					return msg;
				};
				
				/**
				 * @param {ArrayBuffer} buf
				 * @returns {smolweb.fetch.Response.ServerResponse}
				 */
				ServerResponse.fromBuffer = (buf) => {
					const msg = ServerResponse.decode(buf);
					msg.verify();
					return msg;
				};
				
				/**
				 * @param {string} str
				 * @returns {smolweb.fetch.Response.ServerResponse}
				 */
				ServerResponse.fromString = (str) => {
					const msg = ServerResponse.fromJSON(JSON.parse(str));
					msg.verify();
					return msg;
				};
				
				/**
				 * @param {Object} json
				 * @returns {smolweb.fetch.Response.ServerResponse}
				 */
				ServerResponse.fromJSON = (obj) => {
					const msg = ServerResponse.fromObject(obj);
					msg.verify();
					return msg;
				};
				
				/**
				 * @param {smolweb.fetch.Response.ServerResponse | Object | ArrayBuffer | string} saved
				 * @returns {smolweb.fetch.Response.ServerResponse}
				 */
				ServerResponse.from = (saved) => {
					if (saved instanceof ServerResponse) {
						return saved;
					} else if (saved instanceof ArrayBuffer) {
						return ServerResponse.fromBuffer(saved);
					} else if (typeof saved === 'string') {
						return ServerResponse.fromString(saved);
					} else if (typeof saved === 'object') {
						return ServerResponse.fromJSON(saved);
					} else {
						throw new Error("Unrecognized save format: " + saved?.constructor?.name);
					}
				};
				
				/**
				 * @returns {smolweb.fetch.Response.ServerResponse}
				 */
				ServerResponse.prototype.copy = function () {
					// TODO: find more efficient way to do this?
					//   we only need to make defensive copies to prevent mutation
					//   but there's no need for a full buffer encoding/decoding roundtrip
					return this.constructor.fromBuffer(this.toBuffer());
				};
				
				/**
				 * WebStorm needs a little help understanding that this class implements ProtobufOpen
				 * @returns {ProtobufOpen.<smolweb.fetch.Response.ServerResponse>}
				 */
				ServerResponse.opener = function () {
					return this;
				};
				
				return ServerResponse;
				
			})();

			Response.GatewayResponse = (function() {

				/**
				 * Properties of a GatewayResponse.
				 * @memberof smolweb.fetch.Response
				 * @typedef {Object} smolweb_fetch_Response_TGatewayResponse
				 * @property {string|null} [invalidRequest] GatewayResponse invalidRequest
				 * @property {boolean|null} [serverUnreachable] GatewayResponse serverUnreachable
				 * @property {boolean|null} [serverResponseIncomplete] GatewayResponse serverResponseIncomplete
				 * @property {boolean|null} [serverResponseInvalid] GatewayResponse serverResponseInvalid
				 */

				/**
				 * Constructs a new GatewayResponse.
				 * @memberof smolweb.fetch.Response
				 * @classdesc Represents a GatewayResponse.
				 * @implements ProtobufSave
				 * @constructor
				 * @param {smolweb.fetch.Response.GatewayResponse=} [properties] Properties to set
				 */
				function GatewayResponse(properties) {
					if (properties)
						for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
							if (properties[keys[i]] != null)
								this[keys[i]] = properties[keys[i]];
				}

				/**
				 * GatewayResponse invalidRequest.
				 * @member {string|null|undefined} invalidRequest
				 * @memberof smolweb.fetch.Response.GatewayResponse
				 * @instance
				 */
				GatewayResponse.prototype.invalidRequest = null;

				/**
				 * GatewayResponse serverUnreachable.
				 * @member {boolean|null|undefined} serverUnreachable
				 * @memberof smolweb.fetch.Response.GatewayResponse
				 * @instance
				 */
				GatewayResponse.prototype.serverUnreachable = null;

				/**
				 * GatewayResponse serverResponseIncomplete.
				 * @member {boolean|null|undefined} serverResponseIncomplete
				 * @memberof smolweb.fetch.Response.GatewayResponse
				 * @instance
				 */
				GatewayResponse.prototype.serverResponseIncomplete = null;

				/**
				 * GatewayResponse serverResponseInvalid.
				 * @member {boolean|null|undefined} serverResponseInvalid
				 * @memberof smolweb.fetch.Response.GatewayResponse
				 * @instance
				 */
				GatewayResponse.prototype.serverResponseInvalid = null;

				// OneOf field names bound to virtual getters and setters
				let $oneOfFields;

				/**
				 * GatewayResponse response.
				 * @member {"invalidRequest"|"serverUnreachable"|"serverResponseIncomplete"|"serverResponseInvalid"|undefined} response
				 * @memberof smolweb.fetch.Response.GatewayResponse
				 * @instance
				 */
				Object.defineProperty(GatewayResponse.prototype, "response", {
					get: $util.oneOfGetter($oneOfFields = ["invalidRequest", "serverUnreachable", "serverResponseIncomplete", "serverResponseInvalid"]),
					set: $util.oneOfSetter($oneOfFields)
				});

				/**
				 * Creates a new GatewayResponse instance using the specified properties.
				 * @function create
				 * @memberof smolweb.fetch.Response.GatewayResponse
				 * @static
				 * @param {smolweb.fetch.Response.GatewayResponse=} [properties] Properties to set
				 * @returns {smolweb.fetch.Response.GatewayResponse} GatewayResponse instance
				 */
				GatewayResponse.create = function create(properties) {
					return new GatewayResponse(properties);
				};

				/**
				 * Encodes the specified GatewayResponse message. Does not implicitly {@link smolweb.fetch.Response.GatewayResponse.verify|verify} messages.
				 * @function encode
				 * @memberof smolweb.fetch.Response.GatewayResponse
				 * @static
				 * @param {smolweb.fetch.Response.GatewayResponse} message GatewayResponse message or plain object to encode
				 * @param {ArrayBufferWriter} [writer] Writer to encode to
				 * @returns {ArrayBufferWriter} Writer
				 */
				GatewayResponse.encode = function encode(message, writer) {
					if (!writer)
						writer = $Writer.create();
					if (message.invalidRequest != null && Object.hasOwnProperty.call(message, "invalidRequest"))
						writer.uint32(/* id 1, wireType 2 =*/10).string(message.invalidRequest);
					if (message.serverUnreachable != null && Object.hasOwnProperty.call(message, "serverUnreachable"))
						writer.uint32(/* id 2, wireType 0 =*/16).bool(message.serverUnreachable);
					if (message.serverResponseIncomplete != null && Object.hasOwnProperty.call(message, "serverResponseIncomplete"))
						writer.uint32(/* id 3, wireType 0 =*/24).bool(message.serverResponseIncomplete);
					if (message.serverResponseInvalid != null && Object.hasOwnProperty.call(message, "serverResponseInvalid"))
						writer.uint32(/* id 4, wireType 0 =*/32).bool(message.serverResponseInvalid);
					return writer;
				};

				/**
				 * Decodes a GatewayResponse message from the specified reader or buffer.
				 * @function decode
				 * @memberof smolweb.fetch.Response.GatewayResponse
				 * @static
				 * @param {ArrayBufferReader|ArrayBuffer} reader Reader or buffer to decode from
				 * @param {number} [length] Message length if known beforehand
				 * @returns {smolweb.fetch.Response.GatewayResponse} GatewayResponse
				 * @throws {Error} If the payload is not a reader or valid buffer
				 * @throws {$protobuf.util.ProtocolError} If required fields are missing
				 */
				GatewayResponse.decode = function decode(reader, length) {
					if (!(reader instanceof $Reader))
						reader = $Reader.create(reader);
					let end = length === undefined ? reader.len : reader.pos + length, message = new $root.smolweb.fetch.Response.GatewayResponse();
					while (reader.pos < end) {
						let tag = reader.uint32();
						switch (tag >>> 3) {
						case 1: {
								message.invalidRequest = reader.string();
								break;
							}
						case 2: {
								message.serverUnreachable = reader.bool();
								break;
							}
						case 3: {
								message.serverResponseIncomplete = reader.bool();
								break;
							}
						case 4: {
								message.serverResponseInvalid = reader.bool();
								break;
							}
						default:
							reader.skipType(tag & 7);
							break;
						}
					}
					return message;
				};

				/**
				 * Verifies a GatewayResponse message.
				 * @function verify
				 * @memberof smolweb.fetch.Response.GatewayResponse
				 * @static
				 * @param {Object.<string,*>} message Plain object to verify
				 * @returns {string|null} `null` if valid, otherwise the reason why it is not
				 */
				GatewayResponse.verify = function verify(message) {
					if (typeof message !== "object" || message === null)
						return "object expected";
					let properties = {};
					if (message.invalidRequest != null && message.hasOwnProperty("invalidRequest")) {
						properties.response = 1;
						if (!$util.isString(message.invalidRequest))
							return "invalidRequest: string expected";
					}
					if (message.serverUnreachable != null && message.hasOwnProperty("serverUnreachable")) {
						if (properties.response === 1)
							return "response: multiple values";
						properties.response = 1;
						if (typeof message.serverUnreachable !== "boolean")
							return "serverUnreachable: boolean expected";
					}
					if (message.serverResponseIncomplete != null && message.hasOwnProperty("serverResponseIncomplete")) {
						if (properties.response === 1)
							return "response: multiple values";
						properties.response = 1;
						if (typeof message.serverResponseIncomplete !== "boolean")
							return "serverResponseIncomplete: boolean expected";
					}
					if (message.serverResponseInvalid != null && message.hasOwnProperty("serverResponseInvalid")) {
						if (properties.response === 1)
							return "response: multiple values";
						properties.response = 1;
						if (typeof message.serverResponseInvalid !== "boolean")
							return "serverResponseInvalid: boolean expected";
					}
					return null;
				};

				/**
				 * Creates a GatewayResponse message from a plain object. Also converts values to their respective internal types.
				 * @function fromObject
				 * @memberof smolweb.fetch.Response.GatewayResponse
				 * @static
				 * @param {Object.<string,*>} object Plain object
				 * @returns {smolweb.fetch.Response.GatewayResponse} GatewayResponse
				 */
				GatewayResponse.fromObject = function fromObject(object) {
					if (object instanceof $root.smolweb.fetch.Response.GatewayResponse)
						return object;
					let message = new $root.smolweb.fetch.Response.GatewayResponse();
					if (object.invalidRequest != null)
						message.invalidRequest = String(object.invalidRequest);
					if (object.serverUnreachable != null)
						message.serverUnreachable = Boolean(object.serverUnreachable);
					if (object.serverResponseIncomplete != null)
						message.serverResponseIncomplete = Boolean(object.serverResponseIncomplete);
					if (object.serverResponseInvalid != null)
						message.serverResponseInvalid = Boolean(object.serverResponseInvalid);
					return message;
				};

				/**
				 * Creates a plain object from a GatewayResponse message. Also converts values to other types if specified.
				 * @function toObject
				 * @memberof smolweb.fetch.Response.GatewayResponse
				 * @static
				 * @param {smolweb.fetch.Response.GatewayResponse} message GatewayResponse
				 * @param {$protobuf.IConversionOptions} [options] Conversion options
				 * @returns {Object.<string,*>} Plain object
				 */
				GatewayResponse.toObject = function toObject(message, options) {
					if (!options)
						options = {};
					let object = {};
					if (message.invalidRequest != null && message.hasOwnProperty("invalidRequest")) {
						object.invalidRequest = message.invalidRequest;
						if (options.oneofs)
							object.response = "invalidRequest";
					}
					if (message.serverUnreachable != null && message.hasOwnProperty("serverUnreachable")) {
						object.serverUnreachable = message.serverUnreachable;
						if (options.oneofs)
							object.response = "serverUnreachable";
					}
					if (message.serverResponseIncomplete != null && message.hasOwnProperty("serverResponseIncomplete")) {
						object.serverResponseIncomplete = message.serverResponseIncomplete;
						if (options.oneofs)
							object.response = "serverResponseIncomplete";
					}
					if (message.serverResponseInvalid != null && message.hasOwnProperty("serverResponseInvalid")) {
						object.serverResponseInvalid = message.serverResponseInvalid;
						if (options.oneofs)
							object.response = "serverResponseInvalid";
					}
					return object;
				};

				/**
				 * Converts this GatewayResponse to JSON.
				 * @function toJSON
				 * @memberof smolweb.fetch.Response.GatewayResponse
				 * @instance
				 * @returns {Object.<string,*>} JSON object
				 */
				GatewayResponse.prototype.toJSON = function toJSON() {
					return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
				};

				/**
				 * Gets the default type url for GatewayResponse
				 * @function getTypeUrl
				 * @memberof smolweb.fetch.Response.GatewayResponse
				 * @static
				 * @param {string} [typeUrlPrefix] your custom typeUrlPrefix(default "type.cuchazinteractive.org")
				 * @returns {string} The default type url
				 */
				GatewayResponse.getTypeUrl = function getTypeUrl(typeUrlPrefix) {
					if (typeUrlPrefix === undefined) {
						typeUrlPrefix = "type.cuchazinteractive.org";
					}
					return typeUrlPrefix + "/smolweb.fetch.Response.GatewayResponse";
				};

				
				/**
				 * @returns {?string}
				 */		
				GatewayResponse.prototype.verify = function () {
					const err = GatewayResponse.verify(this);
					if (err != null) {
						throw new Error("Verification failed for GatewayResponse: " + err);
					}
				};
				
				/**
				 * @returns {ArrayBuffer}
				 */
				GatewayResponse.prototype.toBuffer = function () {
					return GatewayResponse.encode(this).finish();
				};
				
				/**
				 * @param {string | number} [spacer]
				 * @returns {string}
				 */
				GatewayResponse.prototype.toString = function (spacer) {
					return JSON.stringify(this.toJSON(), null, spacer);
				};
				
				/**
				 * @param {SaveMode} mode
				 * @returns {ArrayBuffer | string | Object}
				 */
				GatewayResponse.prototype.to = function (mode) {
					switch (mode) {
						case SaveModes.BUF: return this.toBuffer();
						case SaveModes.STR: return this.toString();
						case SaveModes.JSON: return this.toJSON();
						default: throw new Error("unrecognized save mode: " + mode);
					}
				};
				
				/**
				 * @param {smolweb_fetch_Response_TGatewayResponse} obj
				 * @returns {smolweb.fetch.Response.GatewayResponse}
				 */
				GatewayResponse.new = (obj) => {
					const msg = GatewayResponse.create(obj);
					msg.verify();
					return msg;
				};
				
				/**
				 * @param {ArrayBuffer} buf
				 * @returns {smolweb.fetch.Response.GatewayResponse}
				 */
				GatewayResponse.fromBuffer = (buf) => {
					const msg = GatewayResponse.decode(buf);
					msg.verify();
					return msg;
				};
				
				/**
				 * @param {string} str
				 * @returns {smolweb.fetch.Response.GatewayResponse}
				 */
				GatewayResponse.fromString = (str) => {
					const msg = GatewayResponse.fromJSON(JSON.parse(str));
					msg.verify();
					return msg;
				};
				
				/**
				 * @param {Object} json
				 * @returns {smolweb.fetch.Response.GatewayResponse}
				 */
				GatewayResponse.fromJSON = (obj) => {
					const msg = GatewayResponse.fromObject(obj);
					msg.verify();
					return msg;
				};
				
				/**
				 * @param {smolweb.fetch.Response.GatewayResponse | Object | ArrayBuffer | string} saved
				 * @returns {smolweb.fetch.Response.GatewayResponse}
				 */
				GatewayResponse.from = (saved) => {
					if (saved instanceof GatewayResponse) {
						return saved;
					} else if (saved instanceof ArrayBuffer) {
						return GatewayResponse.fromBuffer(saved);
					} else if (typeof saved === 'string') {
						return GatewayResponse.fromString(saved);
					} else if (typeof saved === 'object') {
						return GatewayResponse.fromJSON(saved);
					} else {
						throw new Error("Unrecognized save format: " + saved?.constructor?.name);
					}
				};
				
				/**
				 * @returns {smolweb.fetch.Response.GatewayResponse}
				 */
				GatewayResponse.prototype.copy = function () {
					// TODO: find more efficient way to do this?
					//   we only need to make defensive copies to prevent mutation
					//   but there's no need for a full buffer encoding/decoding roundtrip
					return this.constructor.fromBuffer(this.toBuffer());
				};
				
				/**
				 * WebStorm needs a little help understanding that this class implements ProtobufOpen
				 * @returns {ProtobufOpen.<smolweb.fetch.Response.GatewayResponse>}
				 */
				GatewayResponse.opener = function () {
					return this;
				};
				
				return GatewayResponse;
				
			})();

			
			/**
			 * @returns {?string}
			 */		
			Response.prototype.verify = function () {
				const err = Response.verify(this);
				if (err != null) {
					throw new Error("Verification failed for Response: " + err);
				}
			};
			
			/**
			 * @returns {ArrayBuffer}
			 */
			Response.prototype.toBuffer = function () {
				return Response.encode(this).finish();
			};
			
			/**
			 * @param {string | number} [spacer]
			 * @returns {string}
			 */
			Response.prototype.toString = function (spacer) {
				return JSON.stringify(this.toJSON(), null, spacer);
			};
			
			/**
			 * @param {SaveMode} mode
			 * @returns {ArrayBuffer | string | Object}
			 */
			Response.prototype.to = function (mode) {
				switch (mode) {
					case SaveModes.BUF: return this.toBuffer();
					case SaveModes.STR: return this.toString();
					case SaveModes.JSON: return this.toJSON();
					default: throw new Error("unrecognized save mode: " + mode);
				}
			};
			
			/**
			 * @param {smolweb_fetch_TResponse} obj
			 * @returns {smolweb.fetch.Response}
			 */
			Response.new = (obj) => {
				const msg = Response.create(obj);
				msg.verify();
				return msg;
			};
			
			/**
			 * @param {ArrayBuffer} buf
			 * @returns {smolweb.fetch.Response}
			 */
			Response.fromBuffer = (buf) => {
				const msg = Response.decode(buf);
				msg.verify();
				return msg;
			};
			
			/**
			 * @param {string} str
			 * @returns {smolweb.fetch.Response}
			 */
			Response.fromString = (str) => {
				const msg = Response.fromJSON(JSON.parse(str));
				msg.verify();
				return msg;
			};
			
			/**
			 * @param {Object} json
			 * @returns {smolweb.fetch.Response}
			 */
			Response.fromJSON = (obj) => {
				const msg = Response.fromObject(obj);
				msg.verify();
				return msg;
			};
			
			/**
			 * @param {smolweb.fetch.Response | Object | ArrayBuffer | string} saved
			 * @returns {smolweb.fetch.Response}
			 */
			Response.from = (saved) => {
				if (saved instanceof Response) {
					return saved;
				} else if (saved instanceof ArrayBuffer) {
					return Response.fromBuffer(saved);
				} else if (typeof saved === 'string') {
					return Response.fromString(saved);
				} else if (typeof saved === 'object') {
					return Response.fromJSON(saved);
				} else {
					throw new Error("Unrecognized save format: " + saved?.constructor?.name);
				}
			};
			
			/**
			 * @returns {smolweb.fetch.Response}
			 */
			Response.prototype.copy = function () {
				// TODO: find more efficient way to do this?
				//   we only need to make defensive copies to prevent mutation
				//   but there's no need for a full buffer encoding/decoding roundtrip
				return this.constructor.fromBuffer(this.toBuffer());
			};
			
			/**
			 * WebStorm needs a little help understanding that this class implements ProtobufOpen
			 * @returns {ProtobufOpen.<smolweb.fetch.Response>}
			 */
			Response.opener = function () {
				return this;
			};
			
			return Response;
			
		})();

		return fetch;
	})();

	return smolweb;
})();

export { $root as default };
