
import { orTimeout, TimeoutSignal } from 'food-runner/src/promises.js';

import { ControlledConnection, ControllingConnection } from 'smolweb/src/webrtc/connection.js';


export class GuestConnectionRace {

	/** @type {?ControllingConnection} */
	#connControlling;

	/** @type {?ControlledConnection} */
	#connControlled;

	/** @type {?string} */
	#winningName = null;

	/** @type {?OpenDataChannel} */
	#dataChannel = null;


	/**
	 * @param optionsControlling
	 * @param optionsControlled
	 */
	constructor(optionsControlling, optionsControlled) {
		this.#connControlling = new ControllingConnection({
			name: "controlling",
			... optionsControlling
		});
		this.#connControlled = new ControlledConnection({
			name: "controlled",
			... optionsControlled
		});
	}


	/**
	 * @returns {Promise.<string>} offer1
	 */
	async offer() {
		return await this.#connControlling.offer();
	}

	/**
	 * @param {string} answer1
	 * @param {string} offer2
	 * @returns {Promise.<string>} answer2
	 */
	async connect(answer1, offer2) {
		await this.#connControlling.connect(answer1);
		return await this.#connControlled.connect(offer2);
	}

	/**
	 * @returns {Promise.<OpenDataChannel>}
	 */
	async open() {

		if (this.#dataChannel != null) {
			return this.#dataChannel;
		}

		// open both connections in parallel, wait until one of them connects
		while (true) {

			// prefer the controlling connection (opposite of the host side)
			const dataChannelControlling = this.#connControlling.dataChannel;
			const dataChannelControlled = this.#connControlled.dataChannel;
			if (dataChannelControlling != null) {

				// accept the controlling connection
				this.#winningName = this.#connControlling.name;
				this.#dataChannel = dataChannelControlling;

				// abort the other one
				this.#connControlled.close();
				this.#connControlled = null;

				return this.#dataChannel;

			} else if (dataChannelControlled != null) {

				// Give the controlling connection a little bit of extra time to connect too.
				// Otherwise, we can run into a weird race condition where both sides have a connected
				// controlled connection (because the controlled side seems to open about one transit
				// time before the controlling side) and then they both drop the other unconnected
				// controlling connection, so neither connection ends up fully connecting.
				// So we should wait about one transit time to see if the controlling
				// connection is going to open too, because we might actually prefer to
				// use the controlling connection if we're a guest.
				// But if the controlling connection actually shows up sooner, stop waiting.
				// TODO: actually measure average (median?) transit time instead of just kludging 500ms in here?
				if (await orTimeout(this.#connControlling.open(), 500) === TimeoutSignal) {

					// controlling connection timed out, accept the controlled connection
					this.#winningName = this.#connControlled.name;
					this.#dataChannel = dataChannelControlled;

					// abort the other one
					this.#connControlling.close();
					this.#connControlling = null;

					return this.#dataChannel;

				} else {
					// controlling connection opened! go around again to pick it up
					continue;
				}
			}

			// otherwise, wait for something to open
			await Promise.race([
				this.#connControlling.open(),
				this.#connControlled.open()
			]);
		}
	}

	/** @returns {string} */
	get winningName() {
		return this.#winningName;
	}

	/** @returns {boolean} */
	get isChannelOpen() {
		return this.#dataChannel != null;
	}

	/** @type {?OpenDataChannel} */
	get dataChannel() {
		return this.#dataChannel;
	}

	close() {
		this.#dataChannel = null;
		this.#connControlling?.close?.();
		this.#connControlled?.close?.();
	}
}
