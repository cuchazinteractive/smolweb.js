
import { Promiser, orTimeout, TimeoutSignal } from 'food-runner/src/promises.js';

import { FrameReceiver } from 'smolweb/src/framed/receiver.js';
import { ChannelReceiver } from 'smolweb/src/framed/reader.js';


// `ConnectionOptions` is too vague, and WebStorm confuses it with another type, so give it a more unique name
/**
 * @typedef {Object} SmolwebConnectionOptions
 * @property {string} [name]
 * @property {HostAddress} [stunAddr]
 * @property {number} [timeoutIceGatheringMs]
 * @property {number} [timeoutChannelOpenMs]
 */


class Connection {

	/** @type {SmolwebConnectionOptions} */
	#options;

	/** @type {RTCPeerConnection} */
	connection;

	/** @type {RTCDataChannel} */
	#dataChannel

	/** @type {ChannelReceiver} */
	#dataChannelReceiver;

	/** @type {Promiser.<void>} */
	#pChannelOpen;


	/**
	 * @param {SmolwebConnectionOptions} [options]
	 */
	constructor(options=undefined) {

		// apply option defaults
		options = {
			timeoutIceGatheringMs: 15_000,
			timeoutChannelOpenMs: 15_000,
			name: "conn",
			... options
		};

		this.#options = options;

		const config = {
			iceServers: []
		};

		if (this.#options.stunAddr != null) {
			config.iceServers.push({
				urls: `stun:${this.#options.stunAddr.toString()}`
			});
		}

		this.connection = new RTCPeerConnection(config);

		// listen to connection events
		this.connection.onconnectionstatechange = (_event) => {
			console.log(`WebRTC connection "${this.#options.name}" state:`, this.connection.connectionState);
			// TODO: bubble events up to caller?
		};

		// negotiate the data channel with id=5, to match the gateway
		this.#dataChannel = this.connection.createDataChannel('data', {
			negotiated: true,
			id: 5
		});
		this.#dataChannelReceiver = new ChannelReceiver(this.#dataChannel);

		// listen to data channel events
		this.#pChannelOpen = new Promiser();
		this.#dataChannel.onopen = () => {
			this.#pChannelOpen.resolve();
		};
	}


	/** @returns {string} */
	get name() {
		return this.#options.name;
	}

	/**
	 * @param {RTCSessionDescriptionInit} sessionDescription
	 * @returns {Promise.<void>}
	 */
	async gatherIceCandidates(sessionDescription) {

		// make a promise that finishes when ICE candidate gathering does
		const p = new Promiser();
		const stateListener = (_event) => {
			if (this.connection.iceGatheringState === 'complete') {
				p.resolve();
			}
		};

		// start the ICE candidate gathering process and wait for it to finish
		this.connection.addEventListener('icegatheringstatechange', stateListener);
		try {
			await this.connection.setLocalDescription(sessionDescription);
			if (await orTimeout(p.promise(), this.#options.timeoutIceGatheringMs) === TimeoutSignal) {
				throw new Error(`Timed out waiting ${this.#options.timeoutIceGatheringMs} ms for ICE candidate gathering`);
			}
		} finally {
			this.connection.removeEventListener('icegatheringstatechange', stateListener);
		}
	}

	/**
	 * wait for the channel to be open
	 * @returns {Promise.<?OpenDataChannel>} null if timeout
	 */
	async open() {
		if (await orTimeout(this.#pChannelOpen.promise(), this.#options.timeoutChannelOpenMs) === TimeoutSignal) {
			return null;
		}
		return this.dataChannel;
	}

	/** @returns {?OpenDataChannel} */
	get dataChannel() {
		if (this.#dataChannel.readyState === "open") {
			return OpenDataChannel.fromDataChannel(this.#dataChannel, this.#dataChannelReceiver);
		} else {
			return null;
		}
	}

	close() {
		// try to close the connection gracefully
		this.#dataChannelReceiver.close();
		// TODO: do the browsers even implement this correctly?
		this.#dataChannel.close();
		this.connection.close();
	}
}


export class ControllingConnection {

	/** @type {Connection} */
	#inner;


	/**
	 * @param {SmolwebConnectionOptions} options
	 */
	constructor(options) {
		this.#inner = new Connection(options);
	}


	/** @returns {string} */
	get name() {
		return this.#inner.name;
	}

	/**
	 * @returns {Promise.<string>} offer1
	 */
	async offer() {
		const offer = await this.#inner.connection.createOffer();
		await this.#inner.gatherIceCandidates(offer);
		return this.#inner.connection.localDescription
			.toJSON()
			.sdp;
	}

	/**
	 * @param {string} answer
	 * @returns {Promise.<void>}
	 */
	async connect(answer) {
		await this.#inner.connection.setRemoteDescription({
			type: 'answer',
			sdp: answer
		});
	}

	/**
	 * @returns {Promise.<OpenDataChannel>}
	 */
	async open() {
		await this.#inner.open();
	}

	/** @returns {?OpenDataChannel} */
	get dataChannel() {
		return this.#inner.dataChannel;
	}

	close() {
		this.#inner.close();
	}
}


export class ControlledConnection {

	/** @type {Connection} */
	#inner;


	/**
	 * @param {SmolwebConnectionOptions} options
	 */
	constructor(options) {
		this.#inner = new Connection(options);
	}


	/** @returns {string} */
	get name() {
		return this.#inner.name;
	}

	/**
	 * @param {string} offer
	 * @returns {Promise.<string>} answer
	 */
	async connect(offer) {
		await this.#inner.connection.setRemoteDescription({
			type: 'offer',
			sdp: offer
		});
		const answer = await this.#inner.connection.createAnswer();
		await this.#inner.gatherIceCandidates(answer);
		return this.#inner.connection.localDescription
			.toJSON()
			.sdp;
	}

	/**
	 * @returns {Promise.<OpenDataChannel>}
	 */
	async open() {
		await this.#inner.open();
	}

	/** @returns {?OpenDataChannel} */
	get dataChannel() {
		return this.#inner.dataChannel;
	}

	close() {
		this.#inner.close();
	}
}


export class OpenDataChannel {

	/** @type {FrameReceiver} */
	#receiver;


	/**
	 * @param {RTCDataChannel} dataChannel
	 * @param {ChannelReceiver} receiver
	 * @returns {OpenDataChannel}
	 */
	static fromDataChannel(dataChannel, receiver) {

		// because JavaScript is dumb ...
		if (dataChannel == null) {
			throw new Error("dataChannel is required");
		}
		if (receiver == null) {
			throw new Error("receiver is required");
		}

		const out = new this();
		out.#receiver = FrameReceiver.fromDataChannel(dataChannel, receiver);
		return out;
	}


	/** @returns {FrameReceiver} */
	get frameReceiver() {
		return this.#receiver;
	}
}
