
import * as errors from 'food-runner/src/errors.js';
import { IdnTag } from 'burgerid/src/identity.js';

import { DEFAULT_BEACON_PORT, HostAddress, makeBeaconAddr } from 'smolweb/src/addresses.js';


export const PROTO = 'ext+smolweb';


/**
 * @typedef {SmolWebRawAuthority | SmolWebDomainAuthority} SmolWebAuthority
 */


export class SmolWebRawAuthority {

	/**
	 * @param {string} str
	 */
	static fromString(str) {

		const parts = str.split('@');
		if (parts.length !== 2) {
			throw new Error(`unrecognized raw authority: ${str}`);
		}

		// decode the host identification tag
		const hostIdnTag = IdnTag.fromString(parts[0]);
		if (hostIdnTag == null) {
			throw new Error(`Error decoding host identification tag: ${parts[0]}`);
		}

		let /** @type {HostAddress} */ beaconAddr;
		try {
			beaconAddr = HostAddress.fromString(parts[1], DEFAULT_BEACON_PORT);
		} catch (err) {
			throw errors.wrap(new Error(`Error decoding beacon address: ${parts[1]}`), err);
		}

		return new this(hostIdnTag, beaconAddr);
	}


	/** @type {IdnTag} */
	hostIdnTag;

	/** @type {HostAddress} */
	beaconAddr;


	/**
	 * @param {IdnTag} hostIdnTag
	 * @param {HostAddress} beaconAddr
	 */
	constructor(hostIdnTag, beaconAddr) {

		// just in case ... because it's actually happened
		if (!(beaconAddr instanceof HostAddress)) {
			throw new Error(`beaconAddr has wrong type: ${beaconAddr?.constructor?.name}`);
		}

		this.hostIdnTag = hostIdnTag;
		this.beaconAddr = beaconAddr;
	}


	/**
	 * @param {SmolWebAuthority} other
	 * @returns {boolean}
	 */
	equals(other) {
		return other instanceof this.constructor
			&& this.hostIdnTag.equals(other.hostIdnTag)
			&& makeBeaconAddr(this.beaconAddr).equals(makeBeaconAddr(other.beaconAddr));
	}

	/**
	 * @returns {SmolWebRawAuthority}
	 */
	copy() {
		return new this.constructor(
			this.hostIdnTag.copy(),
			this.beaconAddr.copy()
		);
	}

	/**
	 * @returns {string}
	 */
	toString() {
		const beaconAddr = makeBeaconAddr(this.beaconAddr);
		return `${this.hostIdnTag.toString()}@${beaconAddr.toString(DEFAULT_BEACON_PORT)}`;
	}

	/**
	 * @returns {string}
	 */
	id() {
		const beaconAddr = makeBeaconAddr(this.beaconAddr);
		return JSON.stringify([
			this.hostIdnTag.toString(),
			beaconAddr.hostname,
			beaconAddr.port
		]);
	}
}


export class SmolWebDomainAuthority {

	/**
	 * @param {string} str
	 * @returns {SmolWebDomainAuthority}
	 */
	static fromString(str) {
		return new this(str);
	}

	/**
	 * @param {string | SmolWebDomainAuthority} input
	 * @returns {SmolWebDomainAuthority}
	 */
	static wrap(input) {
		if (typeof input === 'string') {
			return new this(input);
		} else if (input instanceof this) {
			return input;
		} else {
			throw new Error(`don't know how to wrap: ${input?.constructor?.name}`);
		}
	}


	/** @type {string} */
	domain;


	/**
	 * @param {string} domain
	 */
	constructor(domain) {

		// domains shouldn't have ports
		if (domain.includes(':')) {
			throw new Error(`Domains shouldn't have ports: ${domain}`);
		}

		this.domain = domain;
	}


	/**
	 * @param {SmolWebAuthority} other
	 * @returns {boolean}
	 */
	equals(other) {

		if (!(other instanceof this.constructor)) {
			return false;
		}

		return this.domain === other.domain;
	}

	/**
	 * @returns {SmolWebDomainAuthority}
	 */
	copy() {
		return new this.constructor(this.domain);
	}

	/**
	 * @returns {string}
	 */
	toString() {
		return this.domain;
	}
}


export class SmolWebAddress {


	/**
	 * @param {string} url
	 * @returns {SmolWebAddress}
	 */
	static fromUrl(url) {

		// NOTE: we can't use regular URL parsing here
		// because, for whatever reason, URL parsing is scheme-specific
		// and so browsers don't do any meaningful parsing of custom schemes at all

		// check the protocol
		const prefix = PROTO + '://';
		if (!url.startsWith(prefix)) {
			throw new Error(`invalid protocol: ${url}`);
		}

		const parts = url
			.substring(prefix.length)
			.split('/');

		// parse the authority
		const authorityStr = parts[0];
		if (authorityStr === '') {
			throw new Error(`No authority in URL: ${url}`);
		}
		let /** @type {SmolWebAuthority} */ authority;
		switch (authorityStr.split('@').length) {

			case 1: {
				authority = SmolWebDomainAuthority.fromString(authorityStr);
			} break;

			case 2: {
				authority = SmolWebRawAuthority.fromString(authorityStr);
			} break;

			default: throw new Error(`Unrecognized authority: ${authorityStr}`);
		}

		// re-assemble the path
		let path = '/';
		if (parts.length > 1) {
			parts[0] = '';
			path = parts.join('/');
		}

		return new this(authority, path);
	}


	/** @type {SmolWebAuthority} */
	authority;

	/** @type {string} */
	#path;


	/**
	 * @param {SmolWebAuthority} authority
	 * @param {string} [path]
	 */
	constructor(authority, path='/') {

		this.authority = authority;
		this.path = path;
	}


	/** @returns {string} */
	get path() {
		return this.#path;
	}

	/** @param {?string | undefined} val */
	set path(val) {

		// all paths here are absolute
		if (val == null) {
			val = '/';
		} else if (!val.startsWith('/')) {
			val = `/${val}`;
		}

		this.#path = val;
	}


	/**
	 * @param {SmolWebAddress} other
	 */
	equals(other) {

		if (!(other instanceof SmolWebAddress)) {
			return false;
		}

		return this.authority.equals(other.authority)
			&& this.path === other.path;
	}

	/**
	 * @returns {SmolWebAddress}
	 */
	copy() {
		return new this.constructor(
			this.authority.copy(),
			this.path
		);
	}

	/**
	 * @returns {string}
	 */
	toUrl() {
		return `${PROTO}://${this.authority}${this.path}`;
	}

	/**
	 * @returns {string}
	 */
	toString() {
		return this.toUrl();
	}
}
