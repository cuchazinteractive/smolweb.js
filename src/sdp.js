
/**
 * @typedef {Object} ParsedSessionDescription
 * @property {ParsedICECandidate[]} iceCandidates
 * @property {boolean} endOfCandidates
 */

/**
 * @typedef {Object} ParsedICECandidate
 * @property {string} raw
 * @property {string} foundation
 * @property {number} componentId
 * @property {string} transport
 * @property {number} priority
 * @property {string} connectionAddress
 * @property {number} port
 * @property {string} candidateType
 */


/**
 * @param {string} sdp
 * @returns {ParsedSessionDescription}
 */
export function parseSessionDescription(sdp) {

	// SDP: https://www.rfc-editor.org/rfc/rfc4566#page-7
	// ICE: https://www.rfc-editor.org/rfc/rfc5245#page-73
	// TCP: https://www.rfc-editor.org/rfc/rfc6544.html#page-11
	// Trickle: https://www.rfc-editor.org/rfc/rfc8840.html#name-sdp-end-of-candidates-attrib

	/* Example:
		SDP offer v=0
		o=mozilla...THIS_IS_SDPARTA-99.0 8062464941081393431 0 IN IP4 0.0.0.0
		s=-
		t=0 0
		a=sendrecv
		a=fingerprint:sha-256 NOPENOPENOPE
		a=group:BUNDLE 0
		a=ice-options:trickle
		a=msid-semantic:WMS *
		m=application 38076 UDP/DTLS/SCTP webrtc-datachannel
		c=IN IP4 <ipv4>
		a=candidate:0 1 UDP 2122187007 d749dfb0-f1c7-4691-8508-4fbd697b8424.local 38076 typ host
		a=candidate:2 1 UDP 2122252543 3b4c99e9-1966-49e9-87ed-39397a5095e9.local 55025 typ host
		a=candidate:4 1 TCP 2105458943 d749dfb0-f1c7-4691-8508-4fbd697b8424.local 9 typ host tcptype active
		a=candidate:5 1 TCP 2105524479 3b4c99e9-1966-49e9-87ed-39397a5095e9.local 9 typ host tcptype active
		a=candidate:1 1 UDP 1685987327 <ipv4> 38076 typ srflx raddr 0.0.0.0 rport 0
		a=candidate:3 1 UDP 1686052607 <ipv6> 55025 typ srflx raddr 0.0.0.0 rport 0
		a=sendrecv
		a=end-of-candidates
		a=ice-pwd:NOPENOPENOPE
		a=ice-ufrag:NOPE
		a=mid:0
		a=setup:actpass
		a=sctp-port:5000
		a=max-message-size:1073741823
	*/

	// NOTE: The random.local domains are mDNS stuff.
	//       ie, a privacy-aware stand-in for a local IP address without actaully sending your local IP to _The Internet_
	//       see: https://bloggeek.me/psa-mdns-and-local-ice-candidates-are-coming/

	const /** @type {ParsedSessionDescription} */ out = {
		iceCandidates: [],
		endOfCandidates: false
	};

	sdp.split('\n')
		.map(line => line.trim()) // get rid of extra line endings
		.forEach(line => {

			// grab the ICE candidates
			if (line.startsWith("a=candidate:")) {
				out.iceCandidates.push(parseIceCandidate(line));

			// look for end-of-candidates flag
			} else if (line === "a=end-of-candidates") {
				out.endOfCandidates = true;
			}
		});

	return out;
}


/**
 * @param {string} line
 * @returns {?ParsedICECandidate}
 */
function parseIceCandidate(line) {

	/* Examples:
		a=candidate:0 1 UDP 2122187007 d749dfb0-f1c7-4691-8508-4fbd697b8424.local 38076 typ host
		a=candidate:2 1 UDP 2122252543 3b4c99e9-1966-49e9-87ed-39397a5095e9.local 55025 typ host
		a=candidate:4 1 TCP 2105458943 d749dfb0-f1c7-4691-8508-4fbd697b8424.local 9 typ host tcptype active
		a=candidate:5 1 TCP 2105524479 3b4c99e9-1966-49e9-87ed-39397a5095e9.local 9 typ host tcptype active
		a=candidate:1 1 UDP 1685987327 <ipv4> 38076 typ srflx raddr 0.0.0.0 rport 0
		a=candidate:3 1 UDP 1686052607 <ipv6> 55025 typ srflx raddr 0.0.0.0 rport 0
	*/

	const candidate = line.substring("a=candidate:".length);
	const parts = candidate.split(" ");

	const /** @type {ParsedICECandidate} */ out = {
		raw: candidate,
		foundation: undefined,
		componentId: undefined,
		transport: undefined,
		priority: undefined,
		connectionAddress: undefined,
		port: undefined,
		candidateType: undefined
	};

	try {
		out.foundation = parts[0];
	} catch {}

	try {
		out.componentId = parseInt(parts[1]);
	} catch {}

	try {
		out.transport = parts[2];
	} catch {}

	try {
		out.priority = parseInt(parts[3]);
	} catch {}

	try {
		out.connectionAddress = parts[4];
	} catch {}

	try {
		out.port = parseInt(parts[5]);
	} catch {}

	// next part should be "typ"
	if (parts[6] !== 'typ') {
		return out;
	}

	try {
		out.candidateType = parts[7];
	} catch {}

	return out;
}
