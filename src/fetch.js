
import * as promises from 'food-runner/src/promises.js';
import * as buffers from 'food-runner/src/buffers.js';
import * as errors from 'food-runner/src/errors.js';
import { iterSync } from 'food-runner/src/iter.js';

import { smolweb } from 'smolweb/src/protobuf/gen.js';
import { FrameEvent } from 'smolweb/src/framed/reader.js';
import { MAX_PAYLOAD_SIZE } from 'smolweb/src/framed/sizes.js';
import { ConnectionPool } from 'smolweb/src/connectionPool.js';
import { SmolWebAddress, SmolWebDomainAuthority, SmolWebRawAuthority } from 'smolweb/src/proto.js';
import * as dns from 'smolweb/src/dns.js';


export const DEFAULT_CONNECTION_POOL = new ConnectionPool();


/**
 * @typedef {Object} FetchOverSmolWebOptions
 * @property {number} [timeoutMs]
 * @property {DnsResolver} [dnsResolver]
 * @property {DnsCache} [dnsCache]
 * @property {ConnectionPool} [connectionPool]
 * @property {PortalRequest} [log]
 */

/**
 * @param {RequestInfo} request
 * @param {RequestInit} [init] request initializer
 * @param {FetchOverSmolWebOptions} [options] fetch options
 * @returns {Promise.<Response>}
 * @throws {FetchGatewayError}
 */
export async function fetchOverSmolWeb(request, init={}, options={}) {

    // handle options
    options = {
        timeoutMs: 30_000,
        dnsResolver: dns.DEFAULT_RESOLVER,
        dnsCacle: dns.DEFAULT_CACHE,
        connectionPool: DEFAULT_CONNECTION_POOL,
        ... options
    };

    // normalize the request
    if (!(request instanceof Request)) {
        request = new Request(request, init);
    }

    // get the address from the request URL
    const addr = SmolWebAddress.fromUrl(request.url);

    // get the raw authority
    let /** @type {SmolWebRawAuthority} */ rawAuthority;
    if (addr.authority instanceof SmolWebRawAuthority) {
        rawAuthority = addr.authority;
    } else if (addr.authority instanceof SmolWebDomainAuthority) {

        // lookup using DNS
        const records = await dns.lookup(addr.authority.domain, {
            resolver: options.dnsResolver,
            cache: options.dnsCache
        });
        options.log?.setDnsRecords?.(records);
        if (records.length <= 0) {
            throw new Error(`DNS resolution for domain ${addr.authority} returned no results`);
        }

        // just take the first record for now and ignore the rest
        const record = records[0];

        rawAuthority = record.rawAuthority();

    } else {
        throw new Error(`Unrecognized SmolWeb authority type: ${addr.authority?.constructor?.name}`);
    }

    // init cleanup
    const /** @type {function()[]} */ cleanupTasks = [];
    const cleanup = () => {
        for (const task of cleanupTasks) {
            task();
        }
    }

    // get the client from the connection pool
    let /** @type {SmolWebClient} */ client;
    try {
        client = await options.connectionPool.get(rawAuthority, {
            events: options.log?.events
        });
    } catch (err) {
        err = errors.wrap(new Error("Connecting to SmolWeb host failed"), err);
        options.log?.setError?.(err);
        throw err;
    }

    try {

        const channel = await client.reserveChannel();

        cleanupTasks.push(() => {
            client.returnChannel(channel);
        });

        // send the request
        const fetchRequest = smolweb.fetch.Request.new({
            method: init.method ?? 'GET',
            path: addr.path,
            headers: ingestHeaders(init.headers),
            hasBody: init.body != null
        });
        channel.frameReceiver.writeAll(fetchRequest.toBuffer());

        // send the body, if needed
        if (init.body != null) {
            if (init.body instanceof ReadableStream) {
                await streamRequestBody(fetchRequest, init.body, channel);
            } else if (init.body instanceof Blob) {
                channel.frameReceiver.writeAll(await init.body.arrayBuffer());
            } else if (init.body instanceof ArrayBuffer) {
                channel.frameReceiver.writeAll(init.body);
            } else if (typeof init.body === 'string' || init.body instanceof String) {
                channel.frameReceiver.writeAll(buffers.fromString(init.body));
            } else {
                // TODO: TypedArray, DataView, FormData, URLSearchParams?
                // noinspection ExceptionCaughtLocallyJS ('tis a silly lint)
                throw new Error(`unrecognized body type: ${init.body.constructor.name}}`);
            }
        }

        // wait for the response
        const fetchResponseBuf = await channel.frameReceiver.recvAll({
            timeoutNowMs: options.timeoutMs
        });

        // parse the response
        const fetchResponse = smolweb.fetch.Response.fromBuffer(fetchResponseBuf);
        switch (fetchResponse.response) {

            case 'gatewayResponse':
                // noinspection ExceptionCaughtLocallyJS ('tis a silly lint)
                throw new FetchGatewayError(fetchResponse.gatewayResponse);

            case 'serverResponse': {

                options.log?.setHttpStatus?.(fetchResponse.serverResponse.status);

                // receive the body, if needed
                let /** @type {ReadableStream | ArrayBuffer} */ body = null;
                switch (fetchResponse.serverResponse.bodyType) {

                    case smolweb.fetch.Response.ServerResponse.BodyType.None:
                        // no body, can cleanup now
                        cleanup();
                        break;

                    case smolweb.fetch.Response.ServerResponse.BodyType.SingleStream:
                        body = responseBodyStreamer(channel, options.timeoutMs, cleanup);
                        // NOTE: delegate cleanup to the streamer
                        break;

                    case smolweb.fetch.Response.ServerResponse.BodyType.MultiStream:
                        // TODO: implement multi streams
                        // TEMP: ignore the body for now
                        cleanup();
                        break;
                }

                // make the response object
                const response = new Response(body, {
                    status: fetchResponse.serverResponse.status,
                    headers: new Headers(fetchResponse.serverResponse.headers.map(h => [h.name, h.value]))
                });

                // HACKHACK: there's no way to put the URL back into a Response via the constructor,
                // so we'll have to define a new property instead
                Object.defineProperty(response, "url", { value: request.url });

                return response;
            }

            default:
                // noinspection ExceptionCaughtLocallyJS ('tis a silly lint)
                throw new Error(`unrecognized response type: ${fetchResponse.response}`);
        }

    } catch (err) {
        try {
            cleanup();
        } catch (err) {
            console.error(err);
        }
        options.log?.setError?.(err);
        throw err;
    }
}


export class FetchGatewayError extends Error {

    /** @type {smolweb.fetch.Response.GatewayResponse} */
    response;

    /**
     * @param {smolweb.fetch.Response.GatewayResponse} response
     */
    constructor(response) {
        super(`Fetch Gateway Error: ${response.errMsg()}`);
        this.response = response;
    }
}


smolweb.fetch.Response.GatewayResponse.prototype.errMsg = function () {
    switch (this.response) {
        case 'invalidRequest': return "Invalid request";
        case 'serverUnreachable': return "Server unreachable";
        case 'serverResponseIncomplete': return "Server response incomplete";
        case 'serverResponseInvalid': return "Server response invalid";
        default: return `(unrecognized error)`;
    }
};


/**
 * @param {[string,string][] | Record.<string,string> | Headers | undefined} headers
 * @returns {smolweb.fetch.Header[]}
 */
function ingestHeaders(headers) {

    /**
     * @param {string} name
     * @param {string} value
     * @returns {smolweb.fetch.Header}
     */
    function h(name, value) {
        return smolweb.fetch.Header.new({ name, value })
    }

    if (headers == null) {
        return [];
    } else if (headers instanceof Array) {
        return headers
            .map(([name, value]) => h(name, value));
    } else if (headers instanceof Headers) {
        return iterSync(headers.entries())
            .map(([name, value]) => h(name, value))
            .toArray();
    } else if (typeof headers === 'object') {
        return Object.getOwnPropertyNames(headers)
            .map(name => h(name, headers[name]));
    } else {
        throw new Error(`unrecognized headers format: ${headers.constructor.name}`);
    }
}


/**
 * @param {smolweb.fetch.Request} fetchRequest
 * @param {ReadableStream} body
 * @param {OpenDataChannel} channel
 * @returns {Promise.<void>}
 */
async function streamRequestBody(fetchRequest, body, channel) {

    // need a content-length header to get the stream size
    const contentLength = fetchRequest.headers
        .find(header => header.name.toLowerCase() === 'content-length');
    if (contentLength == null) {
        throw new Error("streaming request body requires content-length header");
    }
    const bodySize = BigInt(contentLength.value);

    let bodyReceivedBytes = 0;

    /** @type {?Uint8Array} */
    let leftovers = null;

    // relay the chunks
    const reader = body.getReader();
    const writer = channel.frameReceiver.writeStream(bodySize)
    while (bodyReceivedBytes < bodySize) {

        // do we need to wait for a new chunk?
        const { done, value } = await reader.read();
        if (done) {
            // stream aborted early
            break;
        }

        // get the incoming chunk, as a view over the underlying buffer
        let /** @type {Uint8Array} */ chunk;
        if (value instanceof Uint8Array) {
            chunk = value;
        } else if (typeof value === 'string') {
            chunk = new Uint8Array(buffers.fromString(value));
        }
        bodyReceivedBytes += chunk.byteLength;

        // buffer the chunk into payloads
        let bytesMoved = 0;
        while (bytesMoved < chunk.byteLength) {

            let /** @type {ArrayBuffer} */ payload;
            let /** @type {number} */ payloadOffset;
            if (leftovers != null) {
                // use the leftovers as the next payload
                payload = leftovers.buffer;
                payloadOffset = leftovers.byteLength;
                leftovers = null;
            } else {
                // no leftovers, make a new payload
                payload = new ArrayBuffer(MAX_PAYLOAD_SIZE);
                payloadOffset = 0;
            }

            // fill the rest of the payload with chunk
            const payloadSpace = MAX_PAYLOAD_SIZE - payloadOffset;
            const chunkLeft = chunk.byteLength - bytesMoved;
            const copyLen = Math.min(payloadSpace, chunkLeft);
            new Uint8Array(payload).set(chunk.subarray(bytesMoved, bytesMoved + copyLen), payloadOffset);
            payloadOffset += copyLen;
            bytesMoved += copyLen;

            if (payloadOffset === MAX_PAYLOAD_SIZE) {

                // full payload, send it!
                writer.write(payload);

            } else if (bodyReceivedBytes >= bodySize) {

                // not expecting any more chunks, send the leftovers too
                writer.write(payload.slice(0, payloadOffset));

            } else {

                // not full payload and expecting another chunk, so make it into leftovers
                leftovers = new Uint8Array(payload, 0, payloadOffset);
            }
        }
    }
}


/**
 * @param {OpenDataChannel} channel
 * @param {number} timeoutMs
 * @param {function()} cleanup
 * @returns {ReadableStream}
 */
function responseBodyStreamer(channel, timeoutMs, cleanup) {

    return new ReadableStream({

        // NOTE: We can turn on 'byte' readable streams here,
        //       and theoretically that should give us a performance boost by enabling zero-copy buffer transfers,
        //       (see https://developer.mozilla.org/en-US/docs/Web/API/Streams_API/Using_readable_byte_streams)
        //       but Firefox doesn't seem to even turn on BYOB requests for reading fetch response bodies,
        //       so it seems to have no effect here even if we implement the BYOB responses on the send side.
        //       Plus, it's not a supported feature in Safari, so ... maybe we just don't for now?
        //type: 'bytes',

        async pull(controller) {

            // get the next frame, end with error, or timeout
            let /** @type {FrameEvent | promises.TimeoutSignal} */ event;
            try {
                event = await channel.frameReceiver.recv({ timeoutMs });
            } catch (err) {
                channel.frameReceiver.abandonStream();
                controller.error(err);
                cleanup();
                return;
            }
            if (event === promises.TimeoutSignal) {
                channel.frameReceiver.abandonStream();
                controller.error("timeout");
                cleanup();
                return;
            }

            // handle the frame
            switch (event.type) {

                case FrameEvent.START: {
                    // TODO: can we tell the streamer the size somehow?
                    controller.enqueue(new Uint8Array(event.start.payload));
                } break;

                case FrameEvent.CONTINUE: {
                    controller.enqueue(new Uint8Array(event.continue.payload));
                } break;

                case FrameEvent.FINISH: {
                    controller.enqueue(new Uint8Array(event.finish.payload));
                    controller.close();
                    cleanup();
                } break;
            }
        }
    });
}
