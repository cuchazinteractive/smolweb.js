
import { Promiser } from 'food-runner/src/promises.js';

import { SmolWebClient } from 'smolweb/src/client.js';
import { HostAddress } from 'smolweb/src/addresses.js';
import { ChannelReceiver } from 'smolweb/src/framed/reader.js';
import { OpenDataChannel } from 'smolweb/src/webrtc/connection.js';


// noinspection JSUnusedGlobalSymbols
export class SmolWebClientMock {

	/** @type {SmolWebRawAuthority} */
	#authority;

	/** @type {?PairedDataChannel} */
	#dataChannel

	/** @type {?ChannelReceiver} */
	#dataChannelReceiver;

	/** @type {boolean} */
	#channelAvailable = true;

	/** @type {?Promiser} */
	#channelWaiter = null;


	/**
	 * @param {SmolWebRawAuthority} authority
	 * @param {PairedDataChannel} channel
	 */
	constructor(authority, channel) {
		this.#authority = authority;
		this.#dataChannel = channel;
	}


	/** @returns {SmolWebRawAuthority} */
	get authority() {
		return this.#authority;
	}

	/** @returns {HostAddress} */
	get stunAddr() {
		return this.#authority.beaconAddr;
	}

	/**
	 * @returns {Promise.<void>}
	 */
	async connect() {

		if (this.#dataChannel == null) {
			throw new Error("already closed");
		}
		if (this.#dataChannelReceiver != null) {
			throw new Error("already connected");
		}

		this.#dataChannelReceiver = new ChannelReceiver(this.#dataChannel.mock);
	}

	/** @returns {boolean} */
	get isConnected() {
		return this.#dataChannelReceiver != null;
	}

	/**
	 * @returns {Promise.<OpenDataChannel>}
	 */
	async reserveChannel() {

		if (this.#dataChannel == null) {
			throw new Error("closed");
		}
		if (this.#dataChannelReceiver == null) {
			throw new Error("not connected");
		}

		while (true) {

			if (this.#channelAvailable) {
				this.#channelAvailable = false;
				return OpenDataChannel.fromDataChannel(this.#dataChannel.mock, this.#dataChannelReceiver);
			}

			const p = new Promiser();
			this.#channelWaiter = p;
			await p.promise();
		}
	}

	/**
	 * @param {OpenDataChannel} channel
	 */
	returnChannel(channel) {
		this.#channelAvailable = true;
		const p = this.#channelWaiter;
		this.#channelWaiter = null;
		p?.resolve();
	}

	/**
	 * @returns {boolean}
	 **/
	anyChannelsReserved() {
		return !this.#channelAvailable;
	}

	close() {
		this.#dataChannelReceiver.close();
		this.#dataChannelReceiver = null;
		this.#dataChannel = null;
	}

	/** @returns {SmolWebClient} */
	get mock() {
		return /** @type {SmolWebClient} */ this;
	}
}
