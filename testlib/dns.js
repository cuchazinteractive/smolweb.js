
import dnsPacket from 'dns-packet';
import { Buffer } from 'buffer';

import { DnsResolver, SmolWebDnsRecord } from 'smolweb/src/dns.js';


export class TestDnsResolver extends DnsResolver {

	/** @type {Object.<string,SmolWebDnsRecord[]>} */
	zone = {};


	constructor() {
		super('');
	}


	async resolve(domain) {

		const response = {
			answers: []
		};

		const records = this.zone[domain];
		if (records != null) {
			for (const record of records) {
				response.answers.push({
					name: domain.domain,
					type: 'TXT',
					data: record.encode()
						.map(buf => new Buffer(buf))
				});
			}
		}

		const packet = dnsPacket.encode(response);

		// TODO: do we need to trim this?
		return /** @type {ArrayBuffer} */ packet.buffer;
	}
}
