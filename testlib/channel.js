
import * as arrays from 'food-runner/src/arrays.js';


// noinspection JSUnusedGlobalSymbols
/**
 * A one-sided data channel mock that exposes a remote interface to directly send and receive messages.
 */
export class RTCDataChannelMock {

    /** @type {Listeners} */
    #listeners = new Listeners();

    /** @type {RTCMessage[]} */
    #buffer = [];


    /**
     * @param {string} name
     * @param {RTCMessageListener} listener
     */
    addEventListener(name, listener) {
        this.#listeners.add(name, listener);
    }

    /**
     * @param {string} name
     * @param {RTCMessageListener} listener
     */
    removeEventListener(name, listener) {
        this.#listeners.remove(name, listener);
    }

    /**
     * @param {RTCMessage} data
     */
    send(data) {

        // save the message for later
        this.#buffer.push(data);
    }

    /**
     * @param {RTCMessage} msg
     * @returns {Promise.<void>}
     */
    async remoteSend(msg) {
        await this.#listeners.dispatch(msg);
    }

    /**
     * @returns {?RTCMessage} msg
     */
    remoteRecv() {
        return this.#buffer.shift() ?? null;
    }

    /**
     * @returns {boolean}
     */
    remoteIsEmpty() {
        return this.#buffer.length <= 0;
    }

    close() {
        // nothing to do
    }

    /** @returns {RTCDataChannel} */
    get mock() {
        return /** @type {RTCDataChannel} */ this;
    }
}


// noinspection JSUnusedGlobalSymbols
/**
 * A double-sided data channel that can send and recive messages symmetrically on either side
 */
export class PairedDataChannel {

    /**
     * @returns {[PairedDataChannel,PairedDataChannel]}
     */
    static new() {
        const a = new this();
        const b = new this();
        a.#other = b;
        b.#other = a;
        return [a, b];
    }


    /** @type {Listeners} */
    #listeners = new Listeners();

    /** @type {PairedDataChannel} */
    #other;


    /**
     * @param {string} name
     * @param {RTCMessageListener} listener
     */
    addEventListener(name, listener) {
        this.#listeners.add(name, listener);
    }

    /**
     * @param {string} name
     * @param {RTCMessageListener} listener
     */
    removeEventListener(name, listener) {
        this.#listeners.remove(name, listener);
    }

    /**
     * @param {RTCMessage} data
     */
    send(data) {
        (async () => {
            await this.#other.#listeners.dispatch(data);
        })();
    }

    /** @returns {RTCDataChannel} */
    get mock() {
        return /** @type {RTCDataChannel} */ this;
    }
}


class Listeners {

    /** @type {RTCMessageListener[]} */
    #listeners = [];


    /**
     * @param {string} name
     * @param {RTCMessageListener} listener
     */
    add(name, listener) {

        if (name !== 'message') {
            throw new Error("only message events are supported");
        }

        this.#listeners.push(listener);
    }

    /**
     * @param {string} name
     * @param {RTCMessageListener} listener
     */
    remove(name, listener) {
        arrays.removeFirst(this.#listeners, it => it === listener);
    }

    /**
     * @param {RTCMessage} msg
     */
    async dispatch(msg) {

        if (this.#listeners.length <= 0) {
            throw new Error("can't dispatch, no listeners");
        }

        const event = /** @type {RTCMessageEvent} */ {
            data: new Blob([msg])
        };
        for (const listener of this.#listeners) {
            await listener(event);
        }
    }
}
