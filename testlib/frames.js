
import { smolweb } from 'smolweb/src/protobuf/gen.js';


/**
 * @param {number | BigInt} size
 * @param {ArrayBuffer} payload
 * @returns {ArrayBuffer}
 */
export function startFrame(size, payload) {
	return smolweb.framed.Frame.new({
		start: smolweb.framed.FrameStart.new({
			streamSize: BigInt(size),
			payload
		})
	}).toBuffer();
}

/**
 * @param {number | BigInt} index
 * @param {ArrayBuffer} payload
 * @returns {ArrayBuffer}
 */
export function continueFrame(index, payload) {
	return smolweb.framed.Frame.new({
		continue: smolweb.framed.FrameContinue.new({
			frameIndex: BigInt(index),
			payload
		})
	}).toBuffer();
}

/**
 * @param {smolweb_framed_TFrameError} err
 * @returns {ArrayBuffer}
 */
export function errorFrame(err) {
	return smolweb.framed.Frame.new({
		error: smolweb.framed.FrameError.new(err)
	}).toBuffer();
}
