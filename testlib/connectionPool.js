
import { ConnectionPool } from 'smolweb/src/connectionPool.js';

import { SmolWebClientMock } from 'smolweb/testlib/client.js';
import { FetchServer } from 'smolweb/testlib/fetch.js';


export class TestConnectionPool extends ConnectionPool {

	/** @type {Map.<string,FetchServer>} */
	servers = new Map();

	/** @type {number} */
	connections = 0;


	constructor() {
		super({
			cleanIntervalMs: 100,
			keepAliveMs: 200,
			clientConnector: async (authority) => await this.#clientConnector(authority)
		});
	}

	/**
	 * @param {SmolWebRawAuthority} authority
	 * @returns {Promise.<SmolWebClient>}
	 */
	async #clientConnector(authority) {

		// get the fetch server, if any
		let fetchServer = this.servers.get(authority.id());
		if (fetchServer == null) {

			// cache miss, make a new one
			fetchServer = new FetchServer(authority);
			this.servers.set(authority.id(), fetchServer);
		}

		const client = new SmolWebClientMock(authority, fetchServer.clientChannel).mock;
		await client.connect();
		this.connections += 1;
		return client;
	}
}
