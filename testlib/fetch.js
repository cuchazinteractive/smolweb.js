
import * as buffers from 'food-runner/src/buffers.js';

import { FrameReceiver } from 'smolweb/src/framed/receiver.js';
import { smolweb } from 'smolweb/src/protobuf/gen.js';
import { SmolWebAddress, SmolWebRawAuthority } from 'smolweb/src/proto.js';
import { ChannelReceiver } from 'smolweb/src/framed/reader.js';

import { PairedDataChannel } from 'smolweb/testlib/channel.js';
import { SmolWebClientMock } from 'smolweb/testlib/client.js';


export class FetchServer {

	/** @type {SmolWebRawAuthority} */
	authority

	/** @type {FrameReceiver} */
	receiver;

	/** @type {PairedDataChannel} */
	clientChannel;


	/**
	 * @param {SmolWebRawAuthority} authority
	 */
	constructor(authority) {
		this.authority = authority;
		const [serverChannel, clientChannel] = PairedDataChannel.new();
		this.receiver = FrameReceiver.fromDataChannel(serverChannel.mock, new ChannelReceiver(serverChannel.mock));
		this.clientChannel = clientChannel;
	}


	/**
	 * @returns {SmolWebClientMock}
	 */
	client() {
		return new SmolWebClientMock(this.authority, this.clientChannel);
	}

	/**
	 * @param {?string} [path]
	 * @returns {string}
	 */
	url(path) {
		return new SmolWebAddress(this.authority, path).toUrl();
	}

	/**
	 * @returns {smolweb.fetch.Request}
	 */
	async readRequest() {
		const buf = await this.receiver.recvAll();
		return smolweb.fetch.Request.fromBuffer(buf);
	}

	/**
	 * @returns {Promise.<ArrayBuffer>}
	 */
	async readBody() {
		return await this.receiver.recvAll();
	}

	/**
	 * @param {smolweb_fetch_TResponse} response
	 */
	writeResponse(response) {
		const buf = smolweb.fetch.Response.new(response).toBuffer();
		this.receiver.writeAll(buf);
	}

	/**
	 * @param {smolweb_fetch_Response_TGatewayResponse} response
	 */
	writeGatewayResponse(response) {
		this.writeResponse({
			gatewayResponse: smolweb.fetch.Response.GatewayResponse.new(response)
		})
	}

	/**
	 * @param {smolweb_fetch_Response_TServerResponse} response
	 */
	writeServerResponse(response) {
		this.writeResponse({
			serverResponse: smolweb.fetch.Response.ServerResponse.new(response)
		})
	}

	/**
	 * @param {ArrayBuffer} body
	 */
	writeBody(body) {
		this.receiver.writeAll(body);
	}

	/**
	 * @param {[string,string][]} headers
	 * @param {string | ArrayBuffer} body
	 */
	writeDoc(headers, body) {
		this.writeServerResponse({
			status: 200,
			headers: headers.map(([name, value]) => smolweb.fetch.Header.new({ name, value })),
			bodyType: smolweb.fetch.Response.ServerResponse.BodyType.SingleStream
		});
		if (typeof body === 'string') {
			body = buffers.fromString(body);
		}
		this.writeBody(body);
	}

	/**
	 * @param {string} html
	 */
	writeHtml(html) {
		this.writeDoc(
			[
				['content-type', 'text/html']
			],
			html
		);
	}
}
