
import chai from 'chai';

import { FrameReaderTimeoutError } from 'smolweb/src/framed/reader.js';


// Chai API docs:
// https://www.chaijs.com/api/bdd/
// https://www.chaijs.com/guide/helpers/


chai.use((chai, utils) => {

	const Assertion = chai.Assertion;

	Assertion.addMethod('methodEquals', function (_super) {
		return function (exp) {

			const obs = this._obj;

			this.assert(
				exp.equals(obs),
				`Expected ${obs} to .equals() ${exp}`
			);
		};
	});

	Assertion.addChainableMethod(
		'frameReader',
		function () {
			// not using assertions here
			throw new Error("frameReader isn't a function, don't call it");
		},
		function () {
			utils.flag(this, 'frameReader', true);
		}
	);

	Assertion.prototype.rejectedTimeout = async function() {
		if (utils.flag(this, 'frameReader') === true) {

			const e = new Assertion(this._obj).eventually.rejected;
			e.instanceof(FrameReaderTimeoutError);

			this._obj = e._obj;
		}
	};

	Assertion.prototype.rejectedNotTimeout = async function () {
		if (utils.flag(this, 'frameReader') === true) {

			const e = new Assertion(this._obj).eventually.rejected;
			e.not.instanceof(FrameReaderTimeoutError);

			this._obj = e._obj;
		}
	};
});
